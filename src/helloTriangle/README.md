Simple sample that features:

- GL4
- pure newt
- glWindow options
- animator
- indexed drawing
- dynamic attribute binding
- dynamic frag data binding
- interleaved data
- vertex array object (VAO)
- vertex buffer object (VBO) with normalization on one attribute
- index buffer object (IBO) with shorts
- uniform
- glsl program (with specific suffix) jogl util 
- matrix jogl util 
- gl error check
- key listener 
- right way to dispose
