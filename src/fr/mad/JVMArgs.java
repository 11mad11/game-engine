package fr.mad;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.HashMap;
import java.util.List;

import fr.mad.engine.log.LOG;

public class JVMArgs {
	private static HashMap<Character,HashMap<String,String>> x;
	private static LOG log;
	static{
		log = LOG.get("Static");
		x = new HashMap<Character,HashMap<String,String>>();
		RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
		List<String> jvmArgs = runtimeMXBean.getInputArguments();
		
		for (String arg : jvmArgs) {
		    System.err.println(arg);
		    if(x.get(arg.charAt(1)) == null)
		    	x.put(arg.charAt(1), new HashMap<String, String>());
		    x.get(arg.charAt(1)).put(arg.substring(2).split("=")[0], arg.substring(2).split("=")[1]);
		}
	}
	public static String get(char c,String key){
		return x.get(c)!=null?x.get(c).get(key):null;
	}
	public static boolean getBool(char c, String string) {
		return get(c,string)=="true"?true:false;
	}
}
