package fr.mad.test;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.io.IOException;

import com.jogamp.newt.event.MouseEvent;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.math.FloatUtil;
import com.jogamp.opengl.util.gl2.GLUT;

import fr.mad.Workplace;
import fr.mad.engine.GameEngine;
import fr.mad.engine.interfaces.Action;
import fr.mad.engine.interfaces.Drawable;
import fr.mad.engine.interfaces.Initiable;
import fr.mad.engine.interfaces.Tickable;
import fr.mad.engine.level.Level;
import fr.mad.engine.log.LOG;
import fr.mad.engine.model.Model;
import fr.mad.engine.setting.Setting;

public class TestLevel extends Level implements Drawable, Tickable, MouseMotionListener, MouseListener, Initiable, MouseWheelListener {
	
	private GLU glu;
	private double x = 0;
	private double y = 0;
	private double z = 0;
	private double d = 10;
	private Model m;
	private TestShader s;
	private double cx = 0;
	private double cy = 0;
	private double upx = 0;
	private double upy = 1;
	private double upz = 0;
	private boolean side = true;
	private GLUT glut;
	private float angle;
	private LOG log;
	private float[] scale = new float[16];
	private float[] rotation = new float[16];
	private float[] modelToClip = new float[16];
	private int modelToClipMatrixUL;
	
	public TestLevel(Setting s) {
		super(s);
		glu = new GLU();
		this.getGameEngine().addMouseMotionListener(this);
		this.getGameEngine().addMouseListener(this);
		this.getGameEngine().addMouseWheelListener(this);
		log = new LOG(s.getLOG(), "TestLevel");
		this.getInputHandler().addKey(KeyEvent.VK_UP, new Action() {
			
			@Override
			public void fire() {
				pitch += 0.1;
			}
			
		});
	}
	
	@Override
	public void tick(GameEngine ge) {
		if (yaw > 360)
			yaw -= 360;
		if (yaw < 0)
			yaw += 360;
		if (pitch > 360) {
			pitch -= 360;
		}
		if (pitch <= 0) {
			pitch += 360;
		}
		if (side ? 180 >= pitch : 180 <= pitch)
			side = !side;
		upy = side ? 1 : -1;
		
		int v = 99;
		
		double cosYaw = Math.cos(Math.toRadians(yaw));
		//cosYaw += (1/cosYaw)/v;
		double sinYaw = Math.sin(Math.toRadians(yaw));
		//sinYaw += (1/sinYaw)/v;
		double cosPitch = Math.cos(Math.toRadians(pitch));
		//cosPitch += (1/cosPitch)/v;
		double sinPitch = Math.sin(Math.toRadians(pitch));
		//sinPitch += (1/sinPitch)/v;
		
		double tx = d * cosYaw * sinPitch;
		double tz = d * sinYaw * sinPitch;
		
		//System.out.println(cosYaw + " : " + sinYaw + " : " + cosPitch + " : " + sinPitch);
		
		//		if(tx<=0.00001&&tx>=-0.00001&&tz<=0.00001&&tz>=-0.00001){
		//			tx=0.1;
		//			tz=0.1;
		//		}
		
		x = tx;
		z = tz;
		y = d * cosPitch;
		angle += 1;
	}
	
	@Override
	public void draw(GameEngine ge) {
		GL2 gl = ge.getGL().getGL2();
		ge.GLU3D(gl);
		glu.gluLookAt(x, y, z, 0, 0, 0, upx, upy, upz);
		cross(gl);
		
		s.start(gl);
		
//		scale = FloatUtil.makeScale(scale, true, 0.2f, 0.2f, 0.2f);
//		rotation = FloatUtil.makeRotationEuler(rotation, 0, 0, 0, 0);
//		modelToClip = FloatUtil.multMatrix(scale, rotation);
//		gl.glUniformMatrix4fv(modelToClipMatrixUL, 1, false, modelToClip, 0);
		
		cube(gl);
		
		m.draw(ge);
		
		s.stop(gl);
	}
	
	private void cube(GL2 gl) {
		int i = 1;
		gl.glBegin(GL2.GL_QUADS);
		{
			gl.glVertex3d(0, 0, 0);
			gl.glVertex3d(i, 0, 0);
			gl.glVertex3d(i, i, 0);
			gl.glVertex3d(0, i, 0);
			gl.glVertex3d(0, 0, 0);
			gl.glVertex3d(0, 0, i);
			gl.glVertex3d(0, i, i);
			gl.glVertex3d(0, i, 0);
			gl.glVertex3d(0, 0, 0);
		}
		gl.glEnd();
	}
	
	public void cross(GL2 gl) {
		double d = 100;
		gl.glBegin(GL2.GL_LINE_STRIP);
		{
			gl.glColor3f(1f, 1f, 0f);
			gl.glVertex3d(0, 0, 0);
			gl.glVertex3d(d, 0, 0);
		}
		gl.glEnd();
		gl.glBegin(GL2.GL_LINE_STRIP);
		{
			gl.glColor3f(0f, 1f, 0f);
			gl.glVertex3d(0, 0, 0);
			gl.glVertex3d(0, d, 0);
		}
		gl.glEnd();
		gl.glBegin(GL2.GL_LINE_STRIP);
		{
			gl.glColor3f(0f, 1f, 1f);
			gl.glVertex3d(0, 0, 0);
			gl.glVertex3d(0, 0, d);
		}
		gl.glEnd();
		
		//		gl.glBegin(GL2.GL_LINE_STRIP);
		//		{
		//			gl.glColor3f(1f, 1f, 0f);
		//			gl.glVertex3d(0, 0, 0);
		//			gl.glVertex3d(-d, 0, 0);
		//		}
		//		gl.glEnd();
		//		gl.glBegin(GL2.GL_LINE_STRIP);
		//		{
		//			gl.glColor3f(0f, 1f, 0f);
		//			gl.glVertex3d(0, 0, 0);
		//			gl.glVertex3d(0, -d, 0);
		//		}
		//		gl.glEnd();
		//		gl.glBegin(GL2.GL_LINE_STRIP);
		//		{
		//			gl.glColor3f(0f, 1f, 1f);
		//			gl.glVertex3d(0, 0, 0);
		//			gl.glVertex3d(0, 0, -d);
		//		}
		//gl.glEnd();
	}
	
	@Override
	public void init(GL gl) {
		glut = new GLUT();
		m = new Model(gl, new File("test.obj"));
		s = new TestShader(gl.getGL2(), log);
		modelToClipMatrixUL = gl.getGL2().glGetUniformLocation(s.id(), "modelToClipMatrix");
		this.getGameEngine().add(s);
	}
	
	Point last;
	private double yaw;
	private double pitch;
	private boolean side2;
	
	@Override
	public void mouseDragged(java.awt.event.MouseEvent e) {
		Point now = e.getPoint();
		if (last == null)
			last = now;
			
		double rx = now.x - last.x;
		double rz = now.y - last.y;
		
		rx = rx * 0.1;
		rz = rz * 0.1;
		
		yaw += side2 ? rx : -rx;
		pitch += rz;
		
		last = now;
	}
	
	@Override
	public void mouseMoved(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseClicked(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {
		side2 = side;
	}
	
	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {
		last = null;
	}
	
	@Override
	public void mouseEntered(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseExited(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		System.err.println(d);
		d += e.getWheelRotation();
	}
}
