package fr.mad.test;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import com.jogamp.common.nio.PointerBuffer;
import com.jogamp.opengl.*;

public class GLFunc {
	GL2 gl;

	public void glEnableClientState(int arrayName) {
		gl.glEnableClientState(arrayName);
	}

	public void glDisableClientState(int arrayName) {
		gl.glDisableClientState(arrayName);
	}

	public void glVertexPointer(GLArrayData array) {
		gl.glVertexPointer(array);
	}

	public void glVertexPointer(int size, int type, int stride, Buffer pointer) {
		gl.glVertexPointer(size, type, stride, pointer);
	}

	public void glVertexPointer(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glVertexPointer(size, type, stride, pointer_buffer_offset);
	}

	public void glColorPointer(GLArrayData array) {
		gl.glColorPointer(array);
	}

	public void glColorPointer(int size, int type, int stride, Buffer pointer) {
		gl.glColorPointer(size, type, stride, pointer);
	}

	public void glColorPointer(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glColorPointer(size, type, stride, pointer_buffer_offset);
	}

	public void glColor4f(float red, float green, float blue, float alpha) {
		gl.glColor4f(red, green, blue, alpha);
	}

	public void glNormalPointer(GLArrayData array) {
		gl.glNormalPointer(array);
	}

	public void glNormalPointer(int type, int stride, Buffer pointer) {
		gl.glNormalPointer(type, stride, pointer);
	}

	public void glNormalPointer(int type, int stride, long pointer_buffer_offset) {
		gl.glNormalPointer(type, stride, pointer_buffer_offset);
	}

	public void glTexCoordPointer(GLArrayData array) {
		gl.glTexCoordPointer(array);
	}

	public void glTexCoordPointer(int size, int type, int stride, Buffer pointer) {
		gl.glTexCoordPointer(size, type, stride, pointer);
	}

	public void glTexCoordPointer(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glTexCoordPointer(size, type, stride, pointer_buffer_offset);
	}

	public void glLightfv(int light, int pname, FloatBuffer params) {
		gl.glLightfv(light, pname, params);
	}

	public void glLightfv(int light, int pname, float[] params, int params_offset) {
		gl.glLightfv(light, pname, params, params_offset);
	}

	public void glMaterialf(int face, int pname, float param) {
		gl.glMaterialf(face, pname, param);
	}

	public void glMaterialfv(int face, int pname, FloatBuffer params) {
		gl.glMaterialfv(face, pname, params);
	}

	public void glMaterialfv(int face, int pname, float[] params, int params_offset) {
		gl.glMaterialfv(face, pname, params, params_offset);
	}

	public void glShadeModel(int mode) {
		gl.glShadeModel(mode);
	}

	public void glMatrixMode(int mode) {
		gl.glMatrixMode(mode);
	}

	public boolean isGL() {
		return gl.isGL();
	}

	public boolean isGL4bc() {
		return gl.isGL4bc();
	}

	public void glPushMatrix() {
		gl.glPushMatrix();
	}

	public boolean isGL4() {
		return gl.isGL4();
	}

	public void glPopMatrix() {
		gl.glPopMatrix();
	}

	public void glLoadIdentity() {
		gl.glLoadIdentity();
	}

	public boolean isGL3bc() {
		return gl.isGL3bc();
	}

	public void glLoadMatrixf(FloatBuffer m) {
		gl.glLoadMatrixf(m);
	}

	public boolean isGL3() {
		return gl.isGL3();
	}

	public void glLoadMatrixf(float[] m, int m_offset) {
		gl.glLoadMatrixf(m, m_offset);
	}

	public boolean isGL2() {
		return gl.isGL2();
	}

	public void glMultMatrixf(FloatBuffer m) {
		gl.glMultMatrixf(m);
	}

	public boolean isGLES1() {
		return gl.isGLES1();
	}

	public void glMultMatrixf(float[] m, int m_offset) {
		gl.glMultMatrixf(m, m_offset);
	}

	public boolean isGLES2() {
		return gl.isGLES2();
	}

	public void glTranslatef(float x, float y, float z) {
		gl.glTranslatef(x, y, z);
	}

	public void glRotatef(float angle, float x, float y, float z) {
		gl.glRotatef(angle, x, y, z);
	}

	public void glScalef(float x, float y, float z) {
		gl.glScalef(x, y, z);
	}

	public boolean isGLES3() {
		return gl.isGLES3();
	}

	public void glOrthof(float left, float right, float bottom, float top, float zNear, float zFar) {
		gl.glOrthof(left, right, bottom, top, zNear, zFar);
	}

	public void glFrustumf(float left, float right, float bottom, float top, float zNear, float zFar) {
		gl.glFrustumf(left, right, bottom, top, zNear, zFar);
	}

	public boolean isGLES() {
		return gl.isGLES();
	}

	public boolean isGL2ES1() {
		return gl.isGL2ES1();
	}

	public boolean isGL2ES2() {
		return gl.isGL2ES2();
	}

	public boolean isGL2ES3() {
		return gl.isGL2ES3();
	}

	public boolean isGL3ES3() {
		return gl.isGL3ES3();
	}

	public boolean isGL4ES3() {
		return gl.isGL4ES3();
	}

	public boolean isGL2GL3() {
		return gl.isGL2GL3();
	}

	public boolean isGL4core() {
		return gl.isGL4core();
	}

	public boolean isGL3core() {
		return gl.isGL3core();
	}

	public boolean isGLcore() {
		return gl.isGLcore();
	}

	public boolean isGLES2Compatible() {
		return gl.isGLES2Compatible();
	}

	public boolean isGLES3Compatible() {
		return gl.isGLES3Compatible();
	}

	public boolean isGLES31Compatible() {
		return gl.isGLES31Compatible();
	}

	public boolean hasGLSL() {
		return gl.hasGLSL();
	}

	public GL getDownstreamGL() throws GLException {
		return gl.getDownstreamGL();
	}

	public GL getRootGL() throws GLException {
		return gl.getRootGL();
	}

	public GL getGL() throws GLException {
		return gl.getGL();
	}

	public GL4bc getGL4bc() throws GLException {
		return gl.getGL4bc();
	}

	public GL4 getGL4() throws GLException {
		return gl.getGL4();
	}

	public GL3bc getGL3bc() throws GLException {
		return gl.getGL3bc();
	}

	public GL3 getGL3() throws GLException {
		return gl.getGL3();
	}

	public GL2 getGL2() throws GLException {
		return gl.getGL2();
	}

	public GLES1 getGLES1() throws GLException {
		return gl.getGLES1();
	}

	public GLES2 getGLES2() throws GLException {
		return gl.getGLES2();
	}

	public GLES3 getGLES3() throws GLException {
		return gl.getGLES3();
	}

	public GL2ES1 getGL2ES1() throws GLException {
		return gl.getGL2ES1();
	}

	public GL2ES2 getGL2ES2() throws GLException {
		return gl.getGL2ES2();
	}

	public GL2ES3 getGL2ES3() throws GLException {
		return gl.getGL2ES3();
	}

	public GL3ES3 getGL3ES3() throws GLException {
		return gl.getGL3ES3();
	}

	public GL4ES3 getGL4ES3() throws GLException {
		return gl.getGL4ES3();
	}

	public GL2GL3 getGL2GL3() throws GLException {
		return gl.getGL2GL3();
	}

	public GLProfile getGLProfile() {
		return gl.getGLProfile();
	}

	public GLContext getContext() {
		return gl.getContext();
	}

	public boolean isFunctionAvailable(String glFunctionName) {
		return gl.isFunctionAvailable(glFunctionName);
	}

	public boolean isExtensionAvailable(String glExtensionName) {
		return gl.isExtensionAvailable(glExtensionName);
	}

	public boolean hasBasicFBOSupport() {
		return gl.hasBasicFBOSupport();
	}

	public boolean hasFullFBOSupport() {
		return gl.hasFullFBOSupport();
	}

	public int getMaxRenderbufferSamples() {
		return gl.getMaxRenderbufferSamples();
	}

	public boolean isNPOTTextureAvailable() {
		return gl.isNPOTTextureAvailable();
	}

	public boolean isTextureFormatBGRA8888Available() {
		return gl.isTextureFormatBGRA8888Available();
	}

	public void setSwapInterval(int interval) {
		gl.setSwapInterval(interval);
	}

	public int getSwapInterval() {
		return gl.getSwapInterval();
	}

	public Object getPlatformGLExtensions() {
		return gl.getPlatformGLExtensions();
	}

	public Object getExtension(String extensionName) {
		return gl.getExtension(extensionName);
	}

	public void glClearDepth(double depth) {
		gl.glClearDepth(depth);
	}

	public void glDepthRange(double zNear, double zFar) {
		gl.glDepthRange(zNear, zFar);
	}

	public int getBoundBuffer(int target) {
		return gl.getBoundBuffer(target);
	}

	public GLBufferStorage getBufferStorage(int bufferName) {
		return gl.getBufferStorage(bufferName);
	}

	public GLBufferStorage mapBuffer(int target, int access) throws GLException {
		return gl.mapBuffer(target, access);
	}

	public void glAlphaFunc(int func, float ref) {
		gl.glAlphaFunc(func, ref);
	}

	public GLBufferStorage mapBufferRange(int target, long offset, long length, int access) throws GLException {
		return gl.mapBufferRange(target, offset, length, access);
	}

	public void glClientActiveTexture(int texture) {
		gl.glClientActiveTexture(texture);
	}

	public void glColor4ub(byte red, byte green, byte blue, byte alpha) {
		gl.glColor4ub(red, green, blue, alpha);
	}

	public void glFogf(int pname, float param) {
		gl.glFogf(pname, param);
	}

	public void glFogfv(int pname, FloatBuffer params) {
		gl.glFogfv(pname, params);
	}

	public void glFogfv(int pname, float[] params, int params_offset) {
		gl.glFogfv(pname, params, params_offset);
	}

	public boolean isVBOArrayBound() {
		return gl.isVBOArrayBound();
	}

	public void glGetLightfv(int light, int pname, FloatBuffer params) {
		gl.glGetLightfv(light, pname, params);
	}

	public boolean isVBOElementArrayBound() {
		return gl.isVBOElementArrayBound();
	}

	public int getBoundFramebuffer(int target) {
		return gl.getBoundFramebuffer(target);
	}

	public void glGetLightfv(int light, int pname, float[] params, int params_offset) {
		gl.glGetLightfv(light, pname, params, params_offset);
	}

	public int getDefaultDrawFramebuffer() {
		return gl.getDefaultDrawFramebuffer();
	}

	public void glGetMaterialfv(int face, int pname, FloatBuffer params) {
		gl.glGetMaterialfv(face, pname, params);
	}

	public int getDefaultReadFramebuffer() {
		return gl.getDefaultReadFramebuffer();
	}

	public void glGetMaterialfv(int face, int pname, float[] params, int params_offset) {
		gl.glGetMaterialfv(face, pname, params, params_offset);
	}

	public int getDefaultReadBuffer() {
		return gl.getDefaultReadBuffer();
	}

	public void glGetTexEnvfv(int tenv, int pname, FloatBuffer params) {
		gl.glGetTexEnvfv(tenv, pname, params);
	}

	public void glGetTexEnvfv(int tenv, int pname, float[] params, int params_offset) {
		gl.glGetTexEnvfv(tenv, pname, params, params_offset);
	}

	public void glGetTexEnviv(int tenv, int pname, IntBuffer params) {
		gl.glGetTexEnviv(tenv, pname, params);
	}

	public void glGetTexEnviv(int tenv, int pname, int[] params, int params_offset) {
		gl.glGetTexEnviv(tenv, pname, params, params_offset);
	}

	public void glLightModelf(int pname, float param) {
		gl.glLightModelf(pname, param);
	}

	public void glLightModelfv(int pname, FloatBuffer params) {
		gl.glLightModelfv(pname, params);
	}

	public void glLightModelfv(int pname, float[] params, int params_offset) {
		gl.glLightModelfv(pname, params, params_offset);
	}

	public void glLightf(int light, int pname, float param) {
		gl.glLightf(light, pname, param);
	}

	public void glLogicOp(int opcode) {
		gl.glLogicOp(opcode);
	}

	public void glMultiTexCoord4f(int target, float s, float t, float r, float q) {
		gl.glMultiTexCoord4f(target, s, t, r, q);
	}

	public void glNormal3f(float nx, float ny, float nz) {
		gl.glNormal3f(nx, ny, nz);
	}

	public void glPointParameterf(int pname, float param) {
		gl.glPointParameterf(pname, param);
	}

	public void glPointParameterfv(int pname, FloatBuffer params) {
		gl.glPointParameterfv(pname, params);
	}

	public void glPointParameterfv(int pname, float[] params, int params_offset) {
		gl.glPointParameterfv(pname, params, params_offset);
	}

	public void glPointSize(float size) {
		gl.glPointSize(size);
	}

	public void glTexEnvf(int target, int pname, float param) {
		gl.glTexEnvf(target, pname, param);
	}

	public void glTexEnvfv(int target, int pname, FloatBuffer params) {
		gl.glTexEnvfv(target, pname, params);
	}

	public void glTexEnvfv(int target, int pname, float[] params, int params_offset) {
		gl.glTexEnvfv(target, pname, params, params_offset);
	}

	public void glTexEnvi(int target, int pname, int param) {
		gl.glTexEnvi(target, pname, param);
	}

	public void glTexEnviv(int target, int pname, IntBuffer params) {
		gl.glTexEnviv(target, pname, params);
	}

	public void glTexEnviv(int target, int pname, int[] params, int params_offset) {
		gl.glTexEnviv(target, pname, params, params_offset);
	}

	public void glOrtho(double left, double right, double bottom, double top, double near_val, double far_val) {
		gl.glOrtho(left, right, bottom, top, near_val, far_val);
	}

	public void glFrustum(double left, double right, double bottom, double top, double zNear, double zFar) {
		gl.glFrustum(left, right, bottom, top, zNear, zFar);
	}

	public void glDrawElements(int mode, int count, int type, Buffer indices) {
		gl.glDrawElements(mode, count, type, indices);
	}

	public void glActiveShaderProgram(int pipeline, int program) {
		gl.glActiveShaderProgram(pipeline, program);
	}

	public void glAttachShader(int program, int shader) {
		gl.glAttachShader(program, shader);
	}

	public void glBeginQuery(int target, int id) {
		gl.glBeginQuery(target, id);
	}

	public void glBindAttribLocation(int program, int index, String name) {
		gl.glBindAttribLocation(program, index, name);
	}

	public void glBindProgramPipeline(int pipeline) {
		gl.glBindProgramPipeline(pipeline);
	}

	public void glBlendColor(float red, float green, float blue, float alpha) {
		gl.glBlendColor(red, green, blue, alpha);
	}

	public void glCompileShader(int shader) {
		gl.glCompileShader(shader);
	}

	public void glCompressedTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int imageSize, Buffer data) {
		gl.glCompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data);
	}

	public void glCompressedTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int imageSize, long data_buffer_offset) {
		gl.glCompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data_buffer_offset);
	}

	public void glCompressedTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, Buffer data) {
		gl.glCompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
	}

	public void glCompressedTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, long data_buffer_offset) {
		gl.glCompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data_buffer_offset);
	}

	public void glCopyImageSubData(int srcName, int srcTarget, int srcLevel, int srcX, int srcY, int srcZ, int dstName, int dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth) {
		gl.glCopyImageSubData(srcName, srcTarget, srcLevel, srcX, srcY, srcZ, dstName, dstTarget, dstLevel, dstX, dstY, dstZ, srcWidth, srcHeight, srcDepth);
	}

	public void glCopyTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) {
		gl.glCopyTexSubImage3D(target, level, xoffset, yoffset, zoffset, x, y, width, height);
	}

	public int glCreateProgram() {
		return gl.glCreateProgram();
	}

	public int glCreateShader(int type) {
		return gl.glCreateShader(type);
	}

	public int glCreateShaderProgramv(int type, int count, String[] strings) {
		return gl.glCreateShaderProgramv(type, count, strings);
	}

	public void glDebugMessageControl(int source, int type, int severity, int count, IntBuffer ids, boolean enabled) {
		gl.glDebugMessageControl(source, type, severity, count, ids, enabled);
	}

	public void glDebugMessageControl(int source, int type, int severity, int count, int[] ids, int ids_offset, boolean enabled) {
		gl.glDebugMessageControl(source, type, severity, count, ids, ids_offset, enabled);
	}

	public void glDebugMessageInsert(int source, int type, int id, int severity, int length, String buf) {
		gl.glDebugMessageInsert(source, type, id, severity, length, buf);
	}

	public void glDeleteProgram(int program) {
		gl.glDeleteProgram(program);
	}

	public void glDeleteProgramPipelines(int n, IntBuffer pipelines) {
		gl.glDeleteProgramPipelines(n, pipelines);
	}

	public void glDeleteProgramPipelines(int n, int[] pipelines, int pipelines_offset) {
		gl.glDeleteProgramPipelines(n, pipelines, pipelines_offset);
	}

	public void glDeleteQueries(int n, IntBuffer ids) {
		gl.glDeleteQueries(n, ids);
	}

	public void glDeleteQueries(int n, int[] ids, int ids_offset) {
		gl.glDeleteQueries(n, ids, ids_offset);
	}

	public void glDeleteShader(int shader) {
		gl.glDeleteShader(shader);
	}

	public void glDetachShader(int program, int shader) {
		gl.glDetachShader(program, shader);
	}

	public void glDisableVertexAttribArray(int index) {
		gl.glDisableVertexAttribArray(index);
	}

	public void glDrawArraysInstancedBaseInstance(int mode, int first, int count, int instancecount, int baseinstance) {
		gl.glDrawArraysInstancedBaseInstance(mode, first, count, instancecount, baseinstance);
	}

	public void glDrawBuffers(int n, IntBuffer bufs) {
		gl.glDrawBuffers(n, bufs);
	}

	public void glDrawBuffers(int n, int[] bufs, int bufs_offset) {
		gl.glDrawBuffers(n, bufs, bufs_offset);
	}

	public void glDrawElementsInstancedBaseInstance(int mode, int count, int type, long indices_buffer_offset, int instancecount, int baseinstance) {
		gl.glDrawElementsInstancedBaseInstance(mode, count, type, indices_buffer_offset, instancecount, baseinstance);
	}

	public void glBeginQueryIndexed(int target, int index, int id) {
		gl.glBeginQueryIndexed(target, index, id);
	}

	public void glDrawElementsInstancedBaseVertexBaseInstance(int mode, int count, int type, long indices_buffer_offset, int instancecount, int basevertex, int baseinstance) {
		gl.glDrawElementsInstancedBaseVertexBaseInstance(mode, count, type, indices_buffer_offset, instancecount, basevertex, baseinstance);
	}

	public void glBindFragDataLocation(int program, int color, String name) {
		gl.glBindFragDataLocation(program, color, name);
	}

	public void glBlendEquationSeparatei(int buf, int modeRGB, int modeAlpha) {
		gl.glBlendEquationSeparatei(buf, modeRGB, modeAlpha);
	}

	public void glEnableVertexAttribArray(int index) {
		gl.glEnableVertexAttribArray(index);
	}

	public void glBlendEquationi(int buf, int mode) {
		gl.glBlendEquationi(buf, mode);
	}

	public void glEndQuery(int target) {
		gl.glEndQuery(target);
	}

	public void glFramebufferTexture3D(int target, int attachment, int textarget, int texture, int level, int zoffset) {
		gl.glFramebufferTexture3D(target, attachment, textarget, texture, level, zoffset);
	}

	public void glBlendFuncSeparatei(int buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {
		gl.glBlendFuncSeparatei(buf, srcRGB, dstRGB, srcAlpha, dstAlpha);
	}

	public void glGenProgramPipelines(int n, IntBuffer pipelines) {
		gl.glGenProgramPipelines(n, pipelines);
	}

	public void glBlendFunci(int buf, int src, int dst) {
		gl.glBlendFunci(buf, src, dst);
	}

	public void glGenProgramPipelines(int n, int[] pipelines, int pipelines_offset) {
		gl.glGenProgramPipelines(n, pipelines, pipelines_offset);
	}

	public void glBufferAddressRangeNV(int pname, int index, long address, long length) {
		gl.glBufferAddressRangeNV(pname, index, address, length);
	}

	public void glBufferPageCommitmentARB(int target, long offset, long size, boolean commit) {
		gl.glBufferPageCommitmentARB(target, offset, size, commit);
	}

	public void glGenQueries(int n, IntBuffer ids) {
		gl.glGenQueries(n, ids);
	}

	public void glClampColor(int target, int clamp) {
		gl.glClampColor(target, clamp);
	}

	public void glGenQueries(int n, int[] ids, int ids_offset) {
		gl.glGenQueries(n, ids, ids_offset);
	}

	public void glClearBufferData(int target, int internalformat, int format, int type, Buffer data) {
		gl.glClearBufferData(target, internalformat, format, type, data);
	}

	public void glBeginConditionalRender(int id, int mode) {
		gl.glBeginConditionalRender(id, mode);
	}

	public void glBeginTransformFeedback(int primitiveMode) {
		gl.glBeginTransformFeedback(primitiveMode);
	}

	public void glGetActiveAttrib(int program, int index, int bufSize, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
		gl.glGetActiveAttrib(program, index, bufSize, length, size, type, name);
	}

	public void glClearBufferSubData(int target, int internalformat, long offset, long size, int format, int type, Buffer data) {
		gl.glClearBufferSubData(target, internalformat, offset, size, format, type, data);
	}

	public void glBindBufferBase(int target, int index, int buffer) {
		gl.glBindBufferBase(target, index, buffer);
	}

	public void glColorFormatNV(int size, int type, int stride) {
		gl.glColorFormatNV(size, type, stride);
	}

	public void glGetActiveAttrib(int program, int index, int bufSize, int[] length, int length_offset, int[] size, int size_offset, int[] type, int type_offset, byte[] name, int name_offset) {
		gl.glGetActiveAttrib(program, index, bufSize, length, length_offset, size, size_offset, type, type_offset, name, name_offset);
	}

	public void glColorMaski(int index, boolean r, boolean g, boolean b, boolean a) {
		gl.glColorMaski(index, r, g, b, a);
	}

	public void glBindBufferRange(int target, int index, int buffer, long offset, long size) {
		gl.glBindBufferRange(target, index, buffer, offset, size);
	}

	public void glCompressedTexImage1D(int target, int level, int internalformat, int width, int border, int imageSize, Buffer data) {
		gl.glCompressedTexImage1D(target, level, internalformat, width, border, imageSize, data);
	}

	public void glGetActiveUniform(int program, int index, int bufSize, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
		gl.glGetActiveUniform(program, index, bufSize, length, size, type, name);
	}

	public void glBindImageTexture(int unit, int texture, int level, boolean layered, int layer, int access, int format) {
		gl.glBindImageTexture(unit, texture, level, layered, layer, access, format);
	}

	public void glCompressedTexImage1D(int target, int level, int internalformat, int width, int border, int imageSize, long data_buffer_offset) {
		gl.glCompressedTexImage1D(target, level, internalformat, width, border, imageSize, data_buffer_offset);
	}

	public void glBindTransformFeedback(int target, int id) {
		gl.glBindTransformFeedback(target, id);
	}

	public void glGetActiveUniform(int program, int index, int bufSize, int[] length, int length_offset, int[] size, int size_offset, int[] type, int type_offset, byte[] name, int name_offset) {
		gl.glGetActiveUniform(program, index, bufSize, length, length_offset, size, size_offset, type, type_offset, name, name_offset);
	}

	public void glBindVertexArray(int array) {
		gl.glBindVertexArray(array);
	}

	public void glCompressedTexSubImage1D(int target, int level, int xoffset, int width, int format, int imageSize, Buffer data) {
		gl.glCompressedTexSubImage1D(target, level, xoffset, width, format, imageSize, data);
	}

	public void glGetAttachedShaders(int program, int maxCount, IntBuffer count, IntBuffer shaders) {
		gl.glGetAttachedShaders(program, maxCount, count, shaders);
	}

	public void glBlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter) {
		gl.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
	}

	public void glCompressedTexSubImage1D(int target, int level, int xoffset, int width, int format, int imageSize, long data_buffer_offset) {
		gl.glCompressedTexSubImage1D(target, level, xoffset, width, format, imageSize, data_buffer_offset);
	}

	public void glGetAttachedShaders(int program, int maxCount, int[] count, int count_offset, int[] shaders, int shaders_offset) {
		gl.glGetAttachedShaders(program, maxCount, count, count_offset, shaders, shaders_offset);
	}

	public void glCopyTexImage1D(int target, int level, int internalformat, int x, int y, int width, int border) {
		gl.glCopyTexImage1D(target, level, internalformat, x, y, width, border);
	}

	public void glClearBufferfi(int buffer, int drawbuffer, float depth, int stencil) {
		gl.glClearBufferfi(buffer, drawbuffer, depth, stencil);
	}

	public int glGetAttribLocation(int program, String name) {
		return gl.glGetAttribLocation(program, name);
	}

	public void glClearBufferfv(int buffer, int drawbuffer, FloatBuffer value) {
		gl.glClearBufferfv(buffer, drawbuffer, value);
	}

	public void glCopyTexSubImage1D(int target, int level, int xoffset, int x, int y, int width) {
		gl.glCopyTexSubImage1D(target, level, xoffset, x, y, width);
	}

	public int glGetDebugMessageLog(int count, int bufSize, IntBuffer sources, IntBuffer types, IntBuffer ids, IntBuffer severities, IntBuffer lengths, ByteBuffer messageLog) {
		return gl.glGetDebugMessageLog(count, bufSize, sources, types, ids, severities, lengths, messageLog);
	}

	public void glClearBufferfv(int buffer, int drawbuffer, float[] value, int value_offset) {
		gl.glClearBufferfv(buffer, drawbuffer, value, value_offset);
	}

	public void glDebugMessageEnableAMD(int category, int severity, int count, IntBuffer ids, boolean enabled) {
		gl.glDebugMessageEnableAMD(category, severity, count, ids, enabled);
	}

	public void glClearBufferiv(int buffer, int drawbuffer, IntBuffer value) {
		gl.glClearBufferiv(buffer, drawbuffer, value);
	}

	public void glDebugMessageEnableAMD(int category, int severity, int count, int[] ids, int ids_offset, boolean enabled) {
		gl.glDebugMessageEnableAMD(category, severity, count, ids, ids_offset, enabled);
	}

	public void glActiveTexture(int texture) {
		gl.glActiveTexture(texture);
	}

	public void glClearBufferiv(int buffer, int drawbuffer, int[] value, int value_offset) {
		gl.glClearBufferiv(buffer, drawbuffer, value, value_offset);
	}

	public void glDebugMessageInsertAMD(int category, int severity, int id, int length, String buf) {
		gl.glDebugMessageInsertAMD(category, severity, id, length, buf);
	}

	public void glBindBuffer(int target, int buffer) {
		gl.glBindBuffer(target, buffer);
	}

	public int glGetDebugMessageLog(int count, int bufSize, int[] sources, int sources_offset, int[] types, int types_offset, int[] ids, int ids_offset, int[] severities, int severities_offset, int[] lengths, int lengths_offset, byte[] messageLog, int messageLog_offset) {
		return gl.glGetDebugMessageLog(count, bufSize, sources, sources_offset, types, types_offset, ids, ids_offset, severities, severities_offset, lengths, lengths_offset, messageLog, messageLog_offset);
	}

	public void glClearBufferuiv(int buffer, int drawbuffer, IntBuffer value) {
		gl.glClearBufferuiv(buffer, drawbuffer, value);
	}

	public void glBindFramebuffer(int target, int framebuffer) {
		gl.glBindFramebuffer(target, framebuffer);
	}

	public void glClearBufferuiv(int buffer, int drawbuffer, int[] value, int value_offset) {
		gl.glClearBufferuiv(buffer, drawbuffer, value, value_offset);
	}

	public void glDisablei(int target, int index) {
		gl.glDisablei(target, index);
	}

	public void glBindRenderbuffer(int target, int renderbuffer) {
		gl.glBindRenderbuffer(target, renderbuffer);
	}

	public void glCopyBufferSubData(int readTarget, int writeTarget, long readOffset, long writeOffset, long size) {
		gl.glCopyBufferSubData(readTarget, writeTarget, readOffset, writeOffset, size);
	}

	public void glGetMultisamplefv(int pname, int index, FloatBuffer val) {
		gl.glGetMultisamplefv(pname, index, val);
	}

	public void glDrawBuffer(int mode) {
		gl.glDrawBuffer(mode);
	}

	public void glDrawTransformFeedback(int mode, int id) {
		gl.glDrawTransformFeedback(mode, id);
	}

	public void glBindTexture(int target, int texture) {
		gl.glBindTexture(target, texture);
	}

	public void glDeleteTransformFeedbacks(int n, IntBuffer ids) {
		gl.glDeleteTransformFeedbacks(n, ids);
	}

	public void glGetMultisamplefv(int pname, int index, float[] val, int val_offset) {
		gl.glGetMultisamplefv(pname, index, val, val_offset);
	}

	public void glDrawTransformFeedbackStream(int mode, int id, int stream) {
		gl.glDrawTransformFeedbackStream(mode, id, stream);
	}

	public void glBlendEquation(int mode) {
		gl.glBlendEquation(mode);
	}

	public void glGetObjectLabel(int identifier, int name, int bufSize, IntBuffer length, ByteBuffer label) {
		gl.glGetObjectLabel(identifier, name, bufSize, length, label);
	}

	public void glDeleteTransformFeedbacks(int n, int[] ids, int ids_offset) {
		gl.glDeleteTransformFeedbacks(n, ids, ids_offset);
	}

	public void glEdgeFlagFormatNV(int stride) {
		gl.glEdgeFlagFormatNV(stride);
	}

	public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
		gl.glBlendEquationSeparate(modeRGB, modeAlpha);
	}

	public void glDeleteVertexArrays(int n, IntBuffer arrays) {
		gl.glDeleteVertexArrays(n, arrays);
	}

	public void glEnablei(int target, int index) {
		gl.glEnablei(target, index);
	}

	public void glGetObjectLabel(int identifier, int name, int bufSize, int[] length, int length_offset, byte[] label, int label_offset) {
		gl.glGetObjectLabel(identifier, name, bufSize, length, length_offset, label, label_offset);
	}

	public void glBlendFunc(int sfactor, int dfactor) {
		gl.glBlendFunc(sfactor, dfactor);
	}

	public void glDeleteVertexArrays(int n, int[] arrays, int arrays_offset) {
		gl.glDeleteVertexArrays(n, arrays, arrays_offset);
	}

	public void glEndQueryIndexed(int target, int index) {
		gl.glEndQueryIndexed(target, index);
	}

	public void glBlendFuncSeparate(int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha) {
		gl.glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
	}

	public void glGetObjectPtrLabel(Buffer ptr, int bufSize, IntBuffer length, ByteBuffer label) {
		gl.glGetObjectPtrLabel(ptr, bufSize, length, label);
	}

	public void glFogCoordFormatNV(int type, int stride) {
		gl.glFogCoordFormatNV(type, stride);
	}

	public void glDrawArraysInstanced(int mode, int first, int count, int instancecount) {
		gl.glDrawArraysInstanced(mode, first, count, instancecount);
	}

	public void glFramebufferTexture1D(int target, int attachment, int textarget, int texture, int level) {
		gl.glFramebufferTexture1D(target, attachment, textarget, texture, level);
	}

	public void glBufferData(int target, long size, Buffer data, int usage) {
		gl.glBufferData(target, size, data, usage);
	}

	public void glGetObjectPtrLabel(Buffer ptr, int bufSize, int[] length, int length_offset, byte[] label, int label_offset) {
		gl.glGetObjectPtrLabel(ptr, bufSize, length, length_offset, label, label_offset);
	}

	public void glGetActiveAtomicCounterBufferiv(int program, int bufferIndex, int pname, IntBuffer params) {
		gl.glGetActiveAtomicCounterBufferiv(program, bufferIndex, pname, params);
	}

	public void glDrawElementsInstanced(int mode, int count, int type, long indices_buffer_offset, int instancecount) {
		gl.glDrawElementsInstanced(mode, count, type, indices_buffer_offset, instancecount);
	}

	public void glGetProgramBinary(int program, int bufSize, IntBuffer length, IntBuffer binaryFormat, Buffer binary) {
		gl.glGetProgramBinary(program, bufSize, length, binaryFormat, binary);
	}

	public void glBufferSubData(int target, long offset, long size, Buffer data) {
		gl.glBufferSubData(target, offset, size, data);
	}

	public void glGetActiveAtomicCounterBufferiv(int program, int bufferIndex, int pname, int[] params, int params_offset) {
		gl.glGetActiveAtomicCounterBufferiv(program, bufferIndex, pname, params, params_offset);
	}

	public void glGetActiveUniformName(int program, int uniformIndex, int bufSize, IntBuffer length, ByteBuffer uniformName) {
		gl.glGetActiveUniformName(program, uniformIndex, bufSize, length, uniformName);
	}

	public void glDrawRangeElements(int mode, int start, int end, int count, int type, long indices_buffer_offset) {
		gl.glDrawRangeElements(mode, start, end, count, type, indices_buffer_offset);
	}

	public int glCheckFramebufferStatus(int target) {
		return gl.glCheckFramebufferStatus(target);
	}

	public void glGetProgramBinary(int program, int bufSize, int[] length, int length_offset, int[] binaryFormat, int binaryFormat_offset, Buffer binary) {
		gl.glGetProgramBinary(program, bufSize, length, length_offset, binaryFormat, binaryFormat_offset, binary);
	}

	public void glEndConditionalRender() {
		gl.glEndConditionalRender();
	}

	public void glClear(int mask) {
		gl.glClear(mask);
	}

	public void glGetActiveUniformName(int program, int uniformIndex, int bufSize, int[] length, int length_offset, byte[] uniformName, int uniformName_offset) {
		gl.glGetActiveUniformName(program, uniformIndex, bufSize, length, length_offset, uniformName, uniformName_offset);
	}

	public void glClearColor(float red, float green, float blue, float alpha) {
		gl.glClearColor(red, green, blue, alpha);
	}

	public void glEndTransformFeedback() {
		gl.glEndTransformFeedback();
	}

	public void glGetProgramInfoLog(int program, int bufSize, IntBuffer length, ByteBuffer infoLog) {
		gl.glGetProgramInfoLog(program, bufSize, length, infoLog);
	}

	public void glGetBufferParameterui64vNV(int target, int pname, LongBuffer params) {
		gl.glGetBufferParameterui64vNV(target, pname, params);
	}

	public void glClearDepthf(float d) {
		gl.glClearDepthf(d);
	}

	public void glFramebufferParameteri(int target, int pname, int param) {
		gl.glFramebufferParameteri(target, pname, param);
	}

	public void glGetProgramInfoLog(int program, int bufSize, int[] length, int length_offset, byte[] infoLog, int infoLog_offset) {
		gl.glGetProgramInfoLog(program, bufSize, length, length_offset, infoLog, infoLog_offset);
	}

	public void glGetBufferParameterui64vNV(int target, int pname, long[] params, int params_offset) {
		gl.glGetBufferParameterui64vNV(target, pname, params, params_offset);
	}

	public void glClearStencil(int s) {
		gl.glClearStencil(s);
	}

	public void glFramebufferTextureEXT(int target, int attachment, int texture, int level) {
		gl.glFramebufferTextureEXT(target, attachment, texture, level);
	}

	public void glGetProgramPipelineInfoLog(int pipeline, int bufSize, IntBuffer length, ByteBuffer infoLog) {
		gl.glGetProgramPipelineInfoLog(pipeline, bufSize, length, infoLog);
	}

	public void glGetBufferSubData(int target, long offset, long size, Buffer data) {
		gl.glGetBufferSubData(target, offset, size, data);
	}

	public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
		gl.glColorMask(red, green, blue, alpha);
	}

	public void glFramebufferTextureLayer(int target, int attachment, int texture, int level, int layer) {
		gl.glFramebufferTextureLayer(target, attachment, texture, level, layer);
	}

	public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, int imageSize, Buffer data) {
		gl.glCompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data);
	}

	public void glGetCompressedTexImage(int target, int level, Buffer img) {
		gl.glGetCompressedTexImage(target, level, img);
	}

	public void glGetProgramPipelineInfoLog(int pipeline, int bufSize, int[] length, int length_offset, byte[] infoLog, int infoLog_offset) {
		gl.glGetProgramPipelineInfoLog(pipeline, bufSize, length, length_offset, infoLog, infoLog_offset);
	}

	public void glGenTransformFeedbacks(int n, IntBuffer ids) {
		gl.glGenTransformFeedbacks(n, ids);
	}

	public void glGetCompressedTexImage(int target, int level, long img_buffer_offset) {
		gl.glGetCompressedTexImage(target, level, img_buffer_offset);
	}

	public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, int imageSize, long data_buffer_offset) {
		gl.glCompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data_buffer_offset);
	}

	public void glGetProgramPipelineiv(int pipeline, int pname, IntBuffer params) {
		gl.glGetProgramPipelineiv(pipeline, pname, params);
	}

	public int glGetDebugMessageLogAMD(int count, int bufsize, IntBuffer categories, IntBuffer severities, IntBuffer ids, IntBuffer lengths, ByteBuffer message) {
		return gl.glGetDebugMessageLogAMD(count, bufsize, categories, severities, ids, lengths, message);
	}

	public void glGenTransformFeedbacks(int n, int[] ids, int ids_offset) {
		gl.glGenTransformFeedbacks(n, ids, ids_offset);
	}

	public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer data) {
		gl.glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data);
	}

	public void glGenVertexArrays(int n, IntBuffer arrays) {
		gl.glGenVertexArrays(n, arrays);
	}

	public void glGetProgramPipelineiv(int pipeline, int pname, int[] params, int params_offset) {
		gl.glGetProgramPipelineiv(pipeline, pname, params, params_offset);
	}

	public int glGetDebugMessageLogAMD(int count, int bufsize, int[] categories, int categories_offset, int[] severities, int severities_offset, int[] ids, int ids_offset, int[] lengths, int lengths_offset, byte[] message, int message_offset) {
		return gl.glGetDebugMessageLogAMD(count, bufsize, categories, categories_offset, severities, severities_offset, ids, ids_offset, lengths, lengths_offset, message, message_offset);
	}

	public void glGetProgramiv(int program, int pname, IntBuffer params) {
		gl.glGetProgramiv(program, pname, params);
	}

	public void glGenVertexArrays(int n, int[] arrays, int arrays_offset) {
		gl.glGenVertexArrays(n, arrays, arrays_offset);
	}

	public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, long data_buffer_offset) {
		gl.glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data_buffer_offset);
	}

	public void glGetDoublev(int pname, DoubleBuffer params) {
		gl.glGetDoublev(pname, params);
	}

	public void glGetActiveUniformBlockName(int program, int uniformBlockIndex, int bufSize, IntBuffer length, ByteBuffer uniformBlockName) {
		gl.glGetActiveUniformBlockName(program, uniformBlockIndex, bufSize, length, uniformBlockName);
	}

	public void glGetProgramiv(int program, int pname, int[] params, int params_offset) {
		gl.glGetProgramiv(program, pname, params, params_offset);
	}

	public void glGetDoublev(int pname, double[] params, int params_offset) {
		gl.glGetDoublev(pname, params, params_offset);
	}

	public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
		gl.glCopyTexImage2D(target, level, internalformat, x, y, width, height, border);
	}

	public void glGetIntegerui64i_vNV(int value, int index, LongBuffer result) {
		gl.glGetIntegerui64i_vNV(value, index, result);
	}

	public void glGetQueryObjecti64v(int id, int pname, LongBuffer params) {
		gl.glGetQueryObjecti64v(id, pname, params);
	}

	public void glGetActiveUniformBlockName(int program, int uniformBlockIndex, int bufSize, int[] length, int length_offset, byte[] uniformBlockName, int uniformBlockName_offset) {
		gl.glGetActiveUniformBlockName(program, uniformBlockIndex, bufSize, length, length_offset, uniformBlockName, uniformBlockName_offset);
	}

	public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
		gl.glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
	}

	public void glGetIntegerui64i_vNV(int value, int index, long[] result, int result_offset) {
		gl.glGetIntegerui64i_vNV(value, index, result, result_offset);
	}

	public void glGetQueryObjecti64v(int id, int pname, long[] params, int params_offset) {
		gl.glGetQueryObjecti64v(id, pname, params, params_offset);
	}

	public void glGetIntegerui64vNV(int value, LongBuffer result) {
		gl.glGetIntegerui64vNV(value, result);
	}

	public void glGetActiveUniformBlockiv(int program, int uniformBlockIndex, int pname, IntBuffer params) {
		gl.glGetActiveUniformBlockiv(program, uniformBlockIndex, pname, params);
	}

	public void glCullFace(int mode) {
		gl.glCullFace(mode);
	}

	public void glGetIntegerui64vNV(int value, long[] result, int result_offset) {
		gl.glGetIntegerui64vNV(value, result, result_offset);
	}

	public void glGetQueryObjectiv(int id, int pname, IntBuffer params) {
		gl.glGetQueryObjectiv(id, pname, params);
	}

	public void glDeleteBuffers(int n, IntBuffer buffers) {
		gl.glDeleteBuffers(n, buffers);
	}

	public void glGetActiveUniformBlockiv(int program, int uniformBlockIndex, int pname, int[] params, int params_offset) {
		gl.glGetActiveUniformBlockiv(program, uniformBlockIndex, pname, params, params_offset);
	}

	public void glGetInternalformati64v(int target, int internalformat, int pname, int bufSize, LongBuffer params) {
		gl.glGetInternalformati64v(target, internalformat, pname, bufSize, params);
	}

	public void glGetQueryObjectiv(int id, int pname, int[] params, int params_offset) {
		gl.glGetQueryObjectiv(id, pname, params, params_offset);
	}

	public void glDeleteBuffers(int n, int[] buffers, int buffers_offset) {
		gl.glDeleteBuffers(n, buffers, buffers_offset);
	}

	public void glGetActiveUniformsiv(int program, int uniformCount, IntBuffer uniformIndices, int pname, IntBuffer params) {
		gl.glGetActiveUniformsiv(program, uniformCount, uniformIndices, pname, params);
	}

	public void glGetInternalformati64v(int target, int internalformat, int pname, int bufSize, long[] params, int params_offset) {
		gl.glGetInternalformati64v(target, internalformat, pname, bufSize, params, params_offset);
	}

	public void glGetQueryObjectui64v(int id, int pname, LongBuffer params) {
		gl.glGetQueryObjectui64v(id, pname, params);
	}

	public void glDeleteFramebuffers(int n, IntBuffer framebuffers) {
		gl.glDeleteFramebuffers(n, framebuffers);
	}

	public void glGetNamedBufferParameterui64vNV(int buffer, int pname, LongBuffer params) {
		gl.glGetNamedBufferParameterui64vNV(buffer, pname, params);
	}

	public void glGetActiveUniformsiv(int program, int uniformCount, int[] uniformIndices, int uniformIndices_offset, int pname, int[] params, int params_offset) {
		gl.glGetActiveUniformsiv(program, uniformCount, uniformIndices, uniformIndices_offset, pname, params, params_offset);
	}

	public void glGetQueryObjectui64v(int id, int pname, long[] params, int params_offset) {
		gl.glGetQueryObjectui64v(id, pname, params, params_offset);
	}

	public void glDeleteFramebuffers(int n, int[] framebuffers, int framebuffers_offset) {
		gl.glDeleteFramebuffers(n, framebuffers, framebuffers_offset);
	}

	public void glGetNamedBufferParameterui64vNV(int buffer, int pname, long[] params, int params_offset) {
		gl.glGetNamedBufferParameterui64vNV(buffer, pname, params, params_offset);
	}

	public void glGetBooleani_v(int target, int index, ByteBuffer data) {
		gl.glGetBooleani_v(target, index, data);
	}

	public void glGetQueryIndexediv(int target, int index, int pname, IntBuffer params) {
		gl.glGetQueryIndexediv(target, index, pname, params);
	}

	public void glGetQueryObjectuiv(int id, int pname, IntBuffer params) {
		gl.glGetQueryObjectuiv(id, pname, params);
	}

	public void glDeleteRenderbuffers(int n, IntBuffer renderbuffers) {
		gl.glDeleteRenderbuffers(n, renderbuffers);
	}

	public void glGetBooleani_v(int target, int index, byte[] data, int data_offset) {
		gl.glGetBooleani_v(target, index, data, data_offset);
	}

	public void glGetQueryIndexediv(int target, int index, int pname, int[] params, int params_offset) {
		gl.glGetQueryIndexediv(target, index, pname, params, params_offset);
	}

	public int glGetFragDataLocation(int program, String name) {
		return gl.glGetFragDataLocation(program, name);
	}

	public void glGetQueryObjectuiv(int id, int pname, int[] params, int params_offset) {
		gl.glGetQueryObjectuiv(id, pname, params, params_offset);
	}

	public void glDeleteRenderbuffers(int n, int[] renderbuffers, int renderbuffers_offset) {
		gl.glDeleteRenderbuffers(n, renderbuffers, renderbuffers_offset);
	}

	public void glGetTexImage(int target, int level, int format, int type, Buffer pixels) {
		gl.glGetTexImage(target, level, format, type, pixels);
	}

	public void glGetFramebufferParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetFramebufferParameteriv(target, pname, params);
	}

	public void glGetQueryiv(int target, int pname, IntBuffer params) {
		gl.glGetQueryiv(target, pname, params);
	}

	public void glGetTexImage(int target, int level, int format, int type, long pixels_buffer_offset) {
		gl.glGetTexImage(target, level, format, type, pixels_buffer_offset);
	}

	public void glDeleteTextures(int n, IntBuffer textures) {
		gl.glDeleteTextures(n, textures);
	}

	public void glGetFramebufferParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetFramebufferParameteriv(target, pname, params, params_offset);
	}

	public void glGetUniformui64vNV(int program, int location, LongBuffer params) {
		gl.glGetUniformui64vNV(program, location, params);
	}

	public void glGetQueryiv(int target, int pname, int[] params, int params_offset) {
		gl.glGetQueryiv(target, pname, params, params_offset);
	}

	public void glDeleteTextures(int n, int[] textures, int textures_offset) {
		gl.glDeleteTextures(n, textures, textures_offset);
	}

	public void glGetUniformui64vNV(int program, int location, long[] params, int params_offset) {
		gl.glGetUniformui64vNV(program, location, params, params_offset);
	}

	public void glGetIntegeri_v(int target, int index, IntBuffer data) {
		gl.glGetIntegeri_v(target, index, data);
	}

	public void glGetSamplerParameterIiv(int sampler, int pname, IntBuffer params) {
		gl.glGetSamplerParameterIiv(sampler, pname, params);
	}

	public void glDepthFunc(int func) {
		gl.glDepthFunc(func);
	}

	public void glGetVertexAttribLdv(int index, int pname, DoubleBuffer params) {
		gl.glGetVertexAttribLdv(index, pname, params);
	}

	public void glGetIntegeri_v(int target, int index, int[] data, int data_offset) {
		gl.glGetIntegeri_v(target, index, data, data_offset);
	}

	public void glDepthMask(boolean flag) {
		gl.glDepthMask(flag);
	}

	public void glGetSamplerParameterIiv(int sampler, int pname, int[] params, int params_offset) {
		gl.glGetSamplerParameterIiv(sampler, pname, params, params_offset);
	}

	public void glDepthRangef(float n, float f) {
		gl.glDepthRangef(n, f);
	}

	public void glGetVertexAttribLdv(int index, int pname, double[] params, int params_offset) {
		gl.glGetVertexAttribLdv(index, pname, params, params_offset);
	}

	public void glGetInternalformativ(int target, int internalformat, int pname, int bufSize, IntBuffer params) {
		gl.glGetInternalformativ(target, internalformat, pname, bufSize, params);
	}

	public void glDisable(int cap) {
		gl.glDisable(cap);
	}

	public void glGetSamplerParameterIuiv(int sampler, int pname, IntBuffer params) {
		gl.glGetSamplerParameterIuiv(sampler, pname, params);
	}

	public void glGetVertexAttribdv(int index, int pname, DoubleBuffer params) {
		gl.glGetVertexAttribdv(index, pname, params);
	}

	public void glGetInternalformativ(int target, int internalformat, int pname, int bufSize, int[] params, int params_offset) {
		gl.glGetInternalformativ(target, internalformat, pname, bufSize, params, params_offset);
	}

	public void glDrawArrays(int mode, int first, int count) {
		gl.glDrawArrays(mode, first, count);
	}

	public void glGetVertexAttribdv(int index, int pname, double[] params, int params_offset) {
		gl.glGetVertexAttribdv(index, pname, params, params_offset);
	}

	public void glGetSamplerParameterIuiv(int sampler, int pname, int[] params, int params_offset) {
		gl.glGetSamplerParameterIuiv(sampler, pname, params, params_offset);
	}

	public String glGetStringi(int name, int index) {
		return gl.glGetStringi(name, index);
	}

	public void glDrawElements(int mode, int count, int type, long indices_buffer_offset) {
		gl.glDrawElements(mode, count, type, indices_buffer_offset);
	}

	public void glGetTexLevelParameterfv(int target, int level, int pname, FloatBuffer params) {
		gl.glGetTexLevelParameterfv(target, level, pname, params);
	}

	public void glEnable(int cap) {
		gl.glEnable(cap);
	}

	public void glGetnCompressedTexImage(int target, int lod, int bufSize, Buffer pixels) {
		gl.glGetnCompressedTexImage(target, lod, bufSize, pixels);
	}

	public void glGetShaderInfoLog(int shader, int bufSize, IntBuffer length, ByteBuffer infoLog) {
		gl.glGetShaderInfoLog(shader, bufSize, length, infoLog);
	}

	public void glFinish() {
		gl.glFinish();
	}

	public void glGetTexLevelParameterfv(int target, int level, int pname, float[] params, int params_offset) {
		gl.glGetTexLevelParameterfv(target, level, pname, params, params_offset);
	}

	public void glGetnTexImage(int target, int level, int format, int type, int bufSize, Buffer pixels) {
		gl.glGetnTexImage(target, level, format, type, bufSize, pixels);
	}

	public void glFlush() {
		gl.glFlush();
	}

	public void glGetShaderInfoLog(int shader, int bufSize, int[] length, int length_offset, byte[] infoLog, int infoLog_offset) {
		gl.glGetShaderInfoLog(shader, bufSize, length, length_offset, infoLog, infoLog_offset);
	}

	public void glGetTexLevelParameteriv(int target, int level, int pname, IntBuffer params) {
		gl.glGetTexLevelParameteriv(target, level, pname, params);
	}

	public void glFlushMappedBufferRange(int target, long offset, long length) {
		gl.glFlushMappedBufferRange(target, offset, length);
	}

	public void glGetnUniformdv(int program, int location, int bufSize, DoubleBuffer params) {
		gl.glGetnUniformdv(program, location, bufSize, params);
	}

	public void glGetShaderSource(int shader, int bufSize, IntBuffer length, ByteBuffer source) {
		gl.glGetShaderSource(shader, bufSize, length, source);
	}

	public void glGetTexLevelParameteriv(int target, int level, int pname, int[] params, int params_offset) {
		gl.glGetTexLevelParameteriv(target, level, pname, params, params_offset);
	}

	public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
		gl.glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer);
	}

	public void glGetnUniformdv(int program, int location, int bufSize, double[] params, int params_offset) {
		gl.glGetnUniformdv(program, location, bufSize, params, params_offset);
	}

	public void glGetTransformFeedbackVarying(int program, int index, int bufSize, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
		gl.glGetTransformFeedbackVarying(program, index, bufSize, length, size, type, name);
	}

	public void glGetShaderSource(int shader, int bufSize, int[] length, int length_offset, byte[] source, int source_offset) {
		gl.glGetShaderSource(shader, bufSize, length, length_offset, source, source_offset);
	}

	public void glGetnUniformuiv(int program, int location, int bufSize, IntBuffer params) {
		gl.glGetnUniformuiv(program, location, bufSize, params);
	}

	public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
		gl.glFramebufferTexture2D(target, attachment, textarget, texture, level);
	}

	public void glGetShaderiv(int shader, int pname, IntBuffer params) {
		gl.glGetShaderiv(shader, pname, params);
	}

	public void glGetnUniformuiv(int program, int location, int bufSize, int[] params, int params_offset) {
		gl.glGetnUniformuiv(program, location, bufSize, params, params_offset);
	}

	public void glGetTransformFeedbackVarying(int program, int index, int bufSize, int[] length, int length_offset, int[] size, int size_offset, int[] type, int type_offset, byte[] name, int name_offset) {
		gl.glGetTransformFeedbackVarying(program, index, bufSize, length, length_offset, size, size_offset, type, type_offset, name, name_offset);
	}

	public void glGetShaderiv(int shader, int pname, int[] params, int params_offset) {
		gl.glGetShaderiv(shader, pname, params, params_offset);
	}

	public void glFrontFace(int mode) {
		gl.glFrontFace(mode);
	}

	public void glGenBuffers(int n, IntBuffer buffers) {
		gl.glGenBuffers(n, buffers);
	}

	public void glGetTexParameterIiv(int target, int pname, IntBuffer params) {
		gl.glGetTexParameterIiv(target, pname, params);
	}

	public long glImportSyncEXT(int external_sync_type, long external_sync, int flags) {
		return gl.glImportSyncEXT(external_sync_type, external_sync, flags);
	}

	public int glGetUniformBlockIndex(int program, String uniformBlockName) {
		return gl.glGetUniformBlockIndex(program, uniformBlockName);
	}

	public void glIndexFormatNV(int type, int stride) {
		gl.glIndexFormatNV(type, stride);
	}

	public void glGenBuffers(int n, int[] buffers, int buffers_offset) {
		gl.glGenBuffers(n, buffers, buffers_offset);
	}

	public void glGetTexParameterIiv(int target, int pname, int[] params, int params_offset) {
		gl.glGetTexParameterIiv(target, pname, params, params_offset);
	}

	public void glInvalidateBufferData(int buffer) {
		gl.glInvalidateBufferData(buffer);
	}

	public void glGetUniformIndices(int program, int uniformCount, String[] uniformNames, IntBuffer uniformIndices) {
		gl.glGetUniformIndices(program, uniformCount, uniformNames, uniformIndices);
	}

	public void glGenFramebuffers(int n, IntBuffer framebuffers) {
		gl.glGenFramebuffers(n, framebuffers);
	}

	public void glInvalidateBufferSubData(int buffer, long offset, long length) {
		gl.glInvalidateBufferSubData(buffer, offset, length);
	}

	public void glGetTexParameterIuiv(int target, int pname, IntBuffer params) {
		gl.glGetTexParameterIuiv(target, pname, params);
	}

	public void glGetUniformIndices(int program, int uniformCount, String[] uniformNames, int[] uniformIndices, int uniformIndices_offset) {
		gl.glGetUniformIndices(program, uniformCount, uniformNames, uniformIndices, uniformIndices_offset);
	}

	public void glInvalidateTexImage(int texture, int level) {
		gl.glInvalidateTexImage(texture, level);
	}

	public void glGenFramebuffers(int n, int[] framebuffers, int framebuffers_offset) {
		gl.glGenFramebuffers(n, framebuffers, framebuffers_offset);
	}

	public void glInvalidateTexSubImage(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth) {
		gl.glInvalidateTexSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth);
	}

	public void glGetTexParameterIuiv(int target, int pname, int[] params, int params_offset) {
		gl.glGetTexParameterIuiv(target, pname, params, params_offset);
	}

	public void glGetUniformuiv(int program, int location, IntBuffer params) {
		gl.glGetUniformuiv(program, location, params);
	}

	public void glGenRenderbuffers(int n, IntBuffer renderbuffers) {
		gl.glGenRenderbuffers(n, renderbuffers);
	}

	public boolean glIsBufferResidentNV(int target) {
		return gl.glIsBufferResidentNV(target);
	}

	public int glGetUniformLocation(int program, String name) {
		return gl.glGetUniformLocation(program, name);
	}

	public void glGetUniformuiv(int program, int location, int[] params, int params_offset) {
		gl.glGetUniformuiv(program, location, params, params_offset);
	}

	public boolean glIsEnabledi(int target, int index) {
		return gl.glIsEnabledi(target, index);
	}

	public void glGenRenderbuffers(int n, int[] renderbuffers, int renderbuffers_offset) {
		gl.glGenRenderbuffers(n, renderbuffers, renderbuffers_offset);
	}

	public void glGetUniformfv(int program, int location, FloatBuffer params) {
		gl.glGetUniformfv(program, location, params);
	}

	public void glGetVertexAttribIiv(int index, int pname, IntBuffer params) {
		gl.glGetVertexAttribIiv(index, pname, params);
	}

	public boolean glIsNamedBufferResidentNV(int buffer) {
		return gl.glIsNamedBufferResidentNV(buffer);
	}

	public void glGetUniformfv(int program, int location, float[] params, int params_offset) {
		gl.glGetUniformfv(program, location, params, params_offset);
	}

	public void glGenTextures(int n, IntBuffer textures) {
		gl.glGenTextures(n, textures);
	}

	public void glGetVertexAttribIiv(int index, int pname, int[] params, int params_offset) {
		gl.glGetVertexAttribIiv(index, pname, params, params_offset);
	}

	public void glMakeBufferNonResidentNV(int target) {
		gl.glMakeBufferNonResidentNV(target);
	}

	public void glMakeBufferResidentNV(int target, int access) {
		gl.glMakeBufferResidentNV(target, access);
	}

	public void glGetUniformiv(int program, int location, IntBuffer params) {
		gl.glGetUniformiv(program, location, params);
	}

	public void glGetVertexAttribIuiv(int index, int pname, IntBuffer params) {
		gl.glGetVertexAttribIuiv(index, pname, params);
	}

	public void glGenTextures(int n, int[] textures, int textures_offset) {
		gl.glGenTextures(n, textures, textures_offset);
	}

	public void glMakeNamedBufferNonResidentNV(int buffer) {
		gl.glMakeNamedBufferNonResidentNV(buffer);
	}

	public void glGetUniformiv(int program, int location, int[] params, int params_offset) {
		gl.glGetUniformiv(program, location, params, params_offset);
	}

	public void glGenerateMipmap(int target) {
		gl.glGenerateMipmap(target);
	}

	public void glMakeNamedBufferResidentNV(int buffer, int access) {
		gl.glMakeNamedBufferResidentNV(buffer, access);
	}

	public void glGetVertexAttribIuiv(int index, int pname, int[] params, int params_offset) {
		gl.glGetVertexAttribIuiv(index, pname, params, params_offset);
	}

	public void glMinSampleShading(float value) {
		gl.glMinSampleShading(value);
	}

	public void glGetVertexAttribfv(int index, int pname, FloatBuffer params) {
		gl.glGetVertexAttribfv(index, pname, params);
	}

	public void glGetBooleanv(int pname, ByteBuffer data) {
		gl.glGetBooleanv(pname, data);
	}

	public void glInvalidateFramebuffer(int target, int numAttachments, IntBuffer attachments) {
		gl.glInvalidateFramebuffer(target, numAttachments, attachments);
	}

	public void glMultiDrawArrays(int mode, IntBuffer first, IntBuffer count, int drawcount) {
		gl.glMultiDrawArrays(mode, first, count, drawcount);
	}

	public void glGetBooleanv(int pname, byte[] data, int data_offset) {
		gl.glGetBooleanv(pname, data, data_offset);
	}

	public void glGetVertexAttribfv(int index, int pname, float[] params, int params_offset) {
		gl.glGetVertexAttribfv(index, pname, params, params_offset);
	}

	public void glInvalidateFramebuffer(int target, int numAttachments, int[] attachments, int attachments_offset) {
		gl.glInvalidateFramebuffer(target, numAttachments, attachments, attachments_offset);
	}

	public void glGetBufferParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetBufferParameteriv(target, pname, params);
	}

	public void glMultiDrawArrays(int mode, int[] first, int first_offset, int[] count, int count_offset, int drawcount) {
		gl.glMultiDrawArrays(mode, first, first_offset, count, count_offset, drawcount);
	}

	public void glInvalidateSubFramebuffer(int target, int numAttachments, IntBuffer attachments, int x, int y, int width, int height) {
		gl.glInvalidateSubFramebuffer(target, numAttachments, attachments, x, y, width, height);
	}

	public void glGetVertexAttribiv(int index, int pname, IntBuffer params) {
		gl.glGetVertexAttribiv(index, pname, params);
	}

	public void glMultiDrawArraysIndirectAMD(int mode, Buffer indirect, int primcount, int stride) {
		gl.glMultiDrawArraysIndirectAMD(mode, indirect, primcount, stride);
	}

	public void glGetBufferParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetBufferParameteriv(target, pname, params, params_offset);
	}

	public void glInvalidateSubFramebuffer(int target, int numAttachments, int[] attachments, int attachments_offset, int x, int y, int width, int height) {
		gl.glInvalidateSubFramebuffer(target, numAttachments, attachments, attachments_offset, x, y, width, height);
	}

	public void glGetVertexAttribiv(int index, int pname, int[] params, int params_offset) {
		gl.glGetVertexAttribiv(index, pname, params, params_offset);
	}

	public void glMultiDrawElements(int mode, IntBuffer count, int type, PointerBuffer indices, int drawcount) {
		gl.glMultiDrawElements(mode, count, type, indices, drawcount);
	}

	public int glGetError() {
		return gl.glGetError();
	}

	public void glGetFloatv(int pname, FloatBuffer data) {
		gl.glGetFloatv(pname, data);
	}

	public boolean glIsProgram(int program) {
		return gl.glIsProgram(program);
	}

	public boolean glIsTransformFeedback(int id) {
		return gl.glIsTransformFeedback(id);
	}

	public void glMultiDrawElementsIndirectAMD(int mode, int type, Buffer indirect, int primcount, int stride) {
		gl.glMultiDrawElementsIndirectAMD(mode, type, indirect, primcount, stride);
	}

	public void glGetFloatv(int pname, float[] data, int data_offset) {
		gl.glGetFloatv(pname, data, data_offset);
	}

	public boolean glIsVertexArray(int array) {
		return gl.glIsVertexArray(array);
	}

	public boolean glIsProgramPipeline(int pipeline) {
		return gl.glIsProgramPipeline(pipeline);
	}

	public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, IntBuffer params) {
		gl.glGetFramebufferAttachmentParameteriv(target, attachment, pname, params);
	}

	public void glNamedBufferPageCommitmentARB(int buffer, long offset, long size, boolean commit) {
		gl.glNamedBufferPageCommitmentARB(buffer, offset, size, commit);
	}

	public boolean glIsQuery(int id) {
		return gl.glIsQuery(id);
	}

	public void glMemoryBarrier(int barriers) {
		gl.glMemoryBarrier(barriers);
	}

	public void glNamedBufferPageCommitmentEXT(int buffer, long offset, long size, boolean commit) {
		gl.glNamedBufferPageCommitmentEXT(buffer, offset, size, commit);
	}

	public boolean glIsShader(int shader) {
		return gl.glIsShader(shader);
	}

	public void glPauseTransformFeedback() {
		gl.glPauseTransformFeedback();
	}

	public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, int[] params, int params_offset) {
		gl.glGetFramebufferAttachmentParameteriv(target, attachment, pname, params, params_offset);
	}

	public void glNormalFormatNV(int type, int stride) {
		gl.glNormalFormatNV(type, stride);
	}

	public void glLinkProgram(int program) {
		gl.glLinkProgram(program);
	}

	public void glPixelStoref(int pname, float param) {
		gl.glPixelStoref(pname, param);
	}

	public void glReadBuffer(int mode) {
		gl.glReadBuffer(mode);
	}

	public void glObjectLabel(int identifier, int name, int length, ByteBuffer label) {
		gl.glObjectLabel(identifier, name, length, label);
	}

	public void glResumeTransformFeedback() {
		gl.glResumeTransformFeedback();
	}

	public int glGetGraphicsResetStatus() {
		return gl.glGetGraphicsResetStatus();
	}

	public void glObjectLabel(int identifier, int name, int length, byte[] label, int label_offset) {
		gl.glObjectLabel(identifier, name, length, label, label_offset);
	}

	public void glTexStorage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
		gl.glTexStorage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations);
	}

	public void glGetIntegerv(int pname, IntBuffer data) {
		gl.glGetIntegerv(pname, data);
	}

	public void glObjectPtrLabel(Buffer ptr, int length, ByteBuffer label) {
		gl.glObjectPtrLabel(ptr, length, label);
	}

	public void glGetIntegerv(int pname, int[] data, int data_offset) {
		gl.glGetIntegerv(pname, data, data_offset);
	}

	public void glTransformFeedbackVaryings(int program, int count, String[] varyings, int bufferMode) {
		gl.glTransformFeedbackVaryings(program, count, varyings, bufferMode);
	}

	public void glGetRenderbufferParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetRenderbufferParameteriv(target, pname, params);
	}

	public void glObjectPtrLabel(Buffer ptr, int length, byte[] label, int label_offset) {
		gl.glObjectPtrLabel(ptr, length, label, label_offset);
	}

	public void glUniform1ui(int location, int v0) {
		gl.glUniform1ui(location, v0);
	}

	public void glPointParameteri(int pname, int param) {
		gl.glPointParameteri(pname, param);
	}

	public void glPopDebugGroup() {
		gl.glPopDebugGroup();
	}

	public void glUniform1uiv(int location, int count, IntBuffer value) {
		gl.glUniform1uiv(location, count, value);
	}

	public void glGetRenderbufferParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetRenderbufferParameteriv(target, pname, params, params_offset);
	}

	public void glPointParameteriv(int pname, IntBuffer params) {
		gl.glPointParameteriv(pname, params);
	}

	public void glProgramBinary(int program, int binaryFormat, Buffer binary, int length) {
		gl.glProgramBinary(program, binaryFormat, binary, length);
	}

	public void glUniform1uiv(int location, int count, int[] value, int value_offset) {
		gl.glUniform1uiv(location, count, value, value_offset);
	}

	public void glPointParameteriv(int pname, int[] params, int params_offset) {
		gl.glPointParameteriv(pname, params, params_offset);
	}

	public String glGetString(int name) {
		return gl.glGetString(name);
	}

	public void glProgramParameteri(int program, int pname, int value) {
		gl.glProgramParameteri(program, pname, value);
	}

	public void glUniform2ui(int location, int v0, int v1) {
		gl.glUniform2ui(location, v0, v1);
	}

	public void glGetTexParameterfv(int target, int pname, FloatBuffer params) {
		gl.glGetTexParameterfv(target, pname, params);
	}

	public void glPolygonMode(int face, int mode) {
		gl.glPolygonMode(face, mode);
	}

	public void glUniform2uiv(int location, int count, IntBuffer value) {
		gl.glUniform2uiv(location, count, value);
	}

	public void glGetTexParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glGetTexParameterfv(target, pname, params, params_offset);
	}

	public void glPrimitiveRestartIndex(int index) {
		gl.glPrimitiveRestartIndex(index);
	}

	public void glProgramUniform1f(int program, int location, float v0) {
		gl.glProgramUniform1f(program, location, v0);
	}

	public void glProgramUniform1d(int program, int location, double v0) {
		gl.glProgramUniform1d(program, location, v0);
	}

	public void glUniform2uiv(int location, int count, int[] value, int value_offset) {
		gl.glUniform2uiv(location, count, value, value_offset);
	}

	public void glGetTexParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetTexParameteriv(target, pname, params);
	}

	public void glProgramUniform1fv(int program, int location, int count, FloatBuffer value) {
		gl.glProgramUniform1fv(program, location, count, value);
	}

	public void glProgramUniform1dv(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform1dv(program, location, count, value);
	}

	public void glUniform3ui(int location, int v0, int v1, int v2) {
		gl.glUniform3ui(location, v0, v1, v2);
	}

	public void glGetTexParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetTexParameteriv(target, pname, params, params_offset);
	}

	public void glProgramUniform1fv(int program, int location, int count, float[] value, int value_offset) {
		gl.glProgramUniform1fv(program, location, count, value, value_offset);
	}

	public void glUniform3uiv(int location, int count, IntBuffer value) {
		gl.glUniform3uiv(location, count, value);
	}

	public void glGetnUniformfv(int program, int location, int bufSize, FloatBuffer params) {
		gl.glGetnUniformfv(program, location, bufSize, params);
	}

	public void glProgramUniform1dv(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform1dv(program, location, count, value, value_offset);
	}

	public void glUniform3uiv(int location, int count, int[] value, int value_offset) {
		gl.glUniform3uiv(location, count, value, value_offset);
	}

	public void glProgramUniform1i(int program, int location, int v0) {
		gl.glProgramUniform1i(program, location, v0);
	}

	public void glGetnUniformfv(int program, int location, int bufSize, float[] params, int params_offset) {
		gl.glGetnUniformfv(program, location, bufSize, params, params_offset);
	}

	public void glProgramUniform2d(int program, int location, double v0, double v1) {
		gl.glProgramUniform2d(program, location, v0, v1);
	}

	public void glUniform4ui(int location, int v0, int v1, int v2, int v3) {
		gl.glUniform4ui(location, v0, v1, v2, v3);
	}

	public void glProgramUniform1iv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform1iv(program, location, count, value);
	}

	public void glProgramUniform2dv(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform2dv(program, location, count, value);
	}

	public void glGetnUniformiv(int program, int location, int bufSize, IntBuffer params) {
		gl.glGetnUniformiv(program, location, bufSize, params);
	}

	public void glUniform4uiv(int location, int count, IntBuffer value) {
		gl.glUniform4uiv(location, count, value);
	}

	public void glProgramUniform2dv(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform2dv(program, location, count, value, value_offset);
	}

	public void glProgramUniform1iv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform1iv(program, location, count, value, value_offset);
	}

	public void glUniform4uiv(int location, int count, int[] value, int value_offset) {
		gl.glUniform4uiv(location, count, value, value_offset);
	}

	public void glGetnUniformiv(int program, int location, int bufSize, int[] params, int params_offset) {
		gl.glGetnUniformiv(program, location, bufSize, params, params_offset);
	}

	public void glProgramUniform3d(int program, int location, double v0, double v1, double v2) {
		gl.glProgramUniform3d(program, location, v0, v1, v2);
	}

	public void glUniformBlockBinding(int program, int uniformBlockIndex, int uniformBlockBinding) {
		gl.glUniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding);
	}

	public void glProgramUniform1ui(int program, int location, int v0) {
		gl.glProgramUniform1ui(program, location, v0);
	}

	public void glHint(int target, int mode) {
		gl.glHint(target, mode);
	}

	public void glUniformMatrix2x3fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix2x3fv(location, count, transpose, value);
	}

	public void glProgramUniform3dv(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform3dv(program, location, count, value);
	}

	public boolean glIsBuffer(int buffer) {
		return gl.glIsBuffer(buffer);
	}

	public void glProgramUniform1uiv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform1uiv(program, location, count, value);
	}

	public boolean glIsEnabled(int cap) {
		return gl.glIsEnabled(cap);
	}

	public void glUniformMatrix2x3fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix2x3fv(location, count, transpose, value, value_offset);
	}

	public void glProgramUniform3dv(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform3dv(program, location, count, value, value_offset);
	}

	public boolean glIsFramebuffer(int framebuffer) {
		return gl.glIsFramebuffer(framebuffer);
	}

	public void glProgramUniform1uiv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform1uiv(program, location, count, value, value_offset);
	}

	public void glUniformMatrix2x4fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix2x4fv(location, count, transpose, value);
	}

	public void glProgramUniform4d(int program, int location, double v0, double v1, double v2, double v3) {
		gl.glProgramUniform4d(program, location, v0, v1, v2, v3);
	}

	public boolean glIsRenderbuffer(int renderbuffer) {
		return gl.glIsRenderbuffer(renderbuffer);
	}

	public void glProgramUniform2f(int program, int location, float v0, float v1) {
		gl.glProgramUniform2f(program, location, v0, v1);
	}

	public void glProgramUniform4dv(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform4dv(program, location, count, value);
	}

	public void glUniformMatrix2x4fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix2x4fv(location, count, transpose, value, value_offset);
	}

	public boolean glIsTexture(int texture) {
		return gl.glIsTexture(texture);
	}

	public void glProgramUniform2fv(int program, int location, int count, FloatBuffer value) {
		gl.glProgramUniform2fv(program, location, count, value);
	}

	public void glUniformMatrix3x2fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix3x2fv(location, count, transpose, value);
	}

	public void glLineWidth(float width) {
		gl.glLineWidth(width);
	}

	public void glProgramUniform4dv(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform4dv(program, location, count, value, value_offset);
	}

	public ByteBuffer glMapBuffer(int target, int access) {
		return gl.glMapBuffer(target, access);
	}

	public void glProgramUniform2fv(int program, int location, int count, float[] value, int value_offset) {
		gl.glProgramUniform2fv(program, location, count, value, value_offset);
	}

	public void glProgramUniformMatrix2dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix2dv(program, location, count, transpose, value);
	}

	public void glUniformMatrix3x2fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix3x2fv(location, count, transpose, value, value_offset);
	}

	public void glProgramUniform2i(int program, int location, int v0, int v1) {
		gl.glProgramUniform2i(program, location, v0, v1);
	}

	public void glUniformMatrix3x4fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix3x4fv(location, count, transpose, value);
	}

	public void glProgramUniformMatrix2dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix2dv(program, location, count, transpose, value, value_offset);
	}

	public ByteBuffer glMapBufferRange(int target, long offset, long length, int access) {
		return gl.glMapBufferRange(target, offset, length, access);
	}

	public void glProgramUniform2iv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform2iv(program, location, count, value);
	}

	public void glUniformMatrix3x4fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix3x4fv(location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix2x3dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix2x3dv(program, location, count, transpose, value);
	}

	public void glUniformMatrix4x2fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix4x2fv(location, count, transpose, value);
	}

	public void glProgramUniform2iv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform2iv(program, location, count, value, value_offset);
	}

	public void glPixelStorei(int pname, int param) {
		gl.glPixelStorei(pname, param);
	}

	public void glProgramUniformMatrix2x3dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix2x3dv(program, location, count, transpose, value, value_offset);
	}

	public void glPolygonOffset(float factor, float units) {
		gl.glPolygonOffset(factor, units);
	}

	public void glUniformMatrix4x2fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix4x2fv(location, count, transpose, value, value_offset);
	}

	public void glProgramUniform2ui(int program, int location, int v0, int v1) {
		gl.glProgramUniform2ui(program, location, v0, v1);
	}

	public void glProgramUniformMatrix2x4dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix2x4dv(program, location, count, transpose, value);
	}

	public void glReadPixels(int x, int y, int width, int height, int format, int type, Buffer pixels) {
		gl.glReadPixels(x, y, width, height, format, type, pixels);
	}

	public void glUniformMatrix4x3fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix4x3fv(location, count, transpose, value);
	}

	public void glProgramUniform2uiv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform2uiv(program, location, count, value);
	}

	public void glReadPixels(int x, int y, int width, int height, int format, int type, long pixels_buffer_offset) {
		gl.glReadPixels(x, y, width, height, format, type, pixels_buffer_offset);
	}

	public void glProgramUniformMatrix2x4dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix2x4dv(program, location, count, transpose, value, value_offset);
	}

	public void glUniformMatrix4x3fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix4x3fv(location, count, transpose, value, value_offset);
	}

	public void glProgramUniform2uiv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform2uiv(program, location, count, value, value_offset);
	}

	public void glReadnPixels(int x, int y, int width, int height, int format, int type, int bufSize, Buffer data) {
		gl.glReadnPixels(x, y, width, height, format, type, bufSize, data);
	}

	public void glProgramUniformMatrix3dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix3dv(program, location, count, transpose, value);
	}

	public void glVertexAttribDivisor(int index, int divisor) {
		gl.glVertexAttribDivisor(index, divisor);
	}

	public void glProgramUniform3f(int program, int location, float v0, float v1, float v2) {
		gl.glProgramUniform3f(program, location, v0, v1, v2);
	}

	public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
		gl.glRenderbufferStorage(target, internalformat, width, height);
	}

	public void glProgramUniformMatrix3dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix3dv(program, location, count, transpose, value, value_offset);
	}

	public void glVertexAttribI4i(int index, int x, int y, int z, int w) {
		gl.glVertexAttribI4i(index, x, y, z, w);
	}

	public void glProgramUniform3fv(int program, int location, int count, FloatBuffer value) {
		gl.glProgramUniform3fv(program, location, count, value);
	}

	public void glVertexAttribI4iv(int index, IntBuffer v) {
		gl.glVertexAttribI4iv(index, v);
	}

	public void glRenderbufferStorageMultisample(int target, int samples, int internalformat, int width, int height) {
		gl.glRenderbufferStorageMultisample(target, samples, internalformat, width, height);
	}

	public void glProgramUniformMatrix3x2dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix3x2dv(program, location, count, transpose, value);
	}

	public void glVertexAttribI4iv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI4iv(index, v, v_offset);
	}

	public void glProgramUniform3fv(int program, int location, int count, float[] value, int value_offset) {
		gl.glProgramUniform3fv(program, location, count, value, value_offset);
	}

	public void glProgramUniformMatrix3x2dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix3x2dv(program, location, count, transpose, value, value_offset);
	}

	public void glVertexAttribI4ui(int index, int x, int y, int z, int w) {
		gl.glVertexAttribI4ui(index, x, y, z, w);
	}

	public void glProgramUniform3i(int program, int location, int v0, int v1, int v2) {
		gl.glProgramUniform3i(program, location, v0, v1, v2);
	}

	public void glSampleCoverage(float value, boolean invert) {
		gl.glSampleCoverage(value, invert);
	}

	public void glProgramUniformMatrix3x4dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix3x4dv(program, location, count, transpose, value);
	}

	public void glVertexAttribI4uiv(int index, IntBuffer v) {
		gl.glVertexAttribI4uiv(index, v);
	}

	public void glScissor(int x, int y, int width, int height) {
		gl.glScissor(x, y, width, height);
	}

	public void glProgramUniform3iv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform3iv(program, location, count, value);
	}

	public void glVertexAttribI4uiv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI4uiv(index, v, v_offset);
	}

	public void glProgramUniformMatrix3x4dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix3x4dv(program, location, count, transpose, value, value_offset);
	}

	public void glStencilFunc(int func, int ref, int mask) {
		gl.glStencilFunc(func, ref, mask);
	}

	public void glVertexAttribIPointer(int index, int size, int type, int stride, long pointer_buffer_offset) {
		gl.glVertexAttribIPointer(index, size, type, stride, pointer_buffer_offset);
	}

	public void glProgramUniform3iv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform3iv(program, location, count, value, value_offset);
	}

	public void glStencilMask(int mask) {
		gl.glStencilMask(mask);
	}

	public void glProgramUniformMatrix4dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix4dv(program, location, count, transpose, value);
	}

	public void glStencilOp(int fail, int zfail, int zpass) {
		gl.glStencilOp(fail, zfail, zpass);
	}

	public void glProgramUniform3ui(int program, int location, int v0, int v1, int v2) {
		gl.glProgramUniform3ui(program, location, v0, v1, v2);
	}

	public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
		gl.glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
	}

	public boolean isPBOPackBound() {
		return gl.isPBOPackBound();
	}

	public boolean isPBOUnpackBound() {
		return gl.isPBOUnpackBound();
	}

	public void glProgramUniformMatrix4dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix4dv(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniform3uiv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform3uiv(program, location, count, value);
	}

	public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, long pixels_buffer_offset) {
		gl.glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels_buffer_offset);
	}

	public void glProgramUniformMatrix4x2dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix4x2dv(program, location, count, transpose, value);
	}

	public void glProgramUniform3uiv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform3uiv(program, location, count, value, value_offset);
	}

	public void glTexParameterf(int target, int pname, float param) {
		gl.glTexParameterf(target, pname, param);
	}

	public void glProgramUniformMatrix4x2dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix4x2dv(program, location, count, transpose, value, value_offset);
	}

	public void glTexParameterfv(int target, int pname, FloatBuffer params) {
		gl.glTexParameterfv(target, pname, params);
	}

	public void glProgramUniform4f(int program, int location, float v0, float v1, float v2, float v3) {
		gl.glProgramUniform4f(program, location, v0, v1, v2, v3);
	}

	public void glTexParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glTexParameterfv(target, pname, params, params_offset);
	}

	public void glProgramUniformMatrix4x3dv(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix4x3dv(program, location, count, transpose, value);
	}

	public void glProgramUniform4fv(int program, int location, int count, FloatBuffer value) {
		gl.glProgramUniform4fv(program, location, count, value);
	}

	public void glTexParameteri(int target, int pname, int param) {
		gl.glTexParameteri(target, pname, param);
	}

	public void glProgramUniformMatrix4x3dv(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix4x3dv(program, location, count, transpose, value, value_offset);
	}

	public void glTexParameteriv(int target, int pname, IntBuffer params) {
		gl.glTexParameteriv(target, pname, params);
	}

	public void glProgramUniform4fv(int program, int location, int count, float[] value, int value_offset) {
		gl.glProgramUniform4fv(program, location, count, value, value_offset);
	}

	public void glTexParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glTexParameteriv(target, pname, params, params_offset);
	}

	public void glProgramUniformui64NV(int program, int location, long value) {
		gl.glProgramUniformui64NV(program, location, value);
	}

	public void glTexStorage1D(int target, int levels, int internalformat, int width) {
		gl.glTexStorage1D(target, levels, internalformat, width);
	}

	public void glProgramUniformui64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniformui64vNV(program, location, count, value);
	}

	public void glProgramUniform4i(int program, int location, int v0, int v1, int v2, int v3) {
		gl.glProgramUniform4i(program, location, v0, v1, v2, v3);
	}

	public void glTexStorage2D(int target, int levels, int internalformat, int width, int height) {
		gl.glTexStorage2D(target, levels, internalformat, width, height);
	}

	public void glProgramUniformui64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniformui64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform4iv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform4iv(program, location, count, value);
	}

	public void glProvokingVertex(int mode) {
		gl.glProvokingVertex(mode);
	}

	public void glTexStorage3D(int target, int levels, int internalformat, int width, int height, int depth) {
		gl.glTexStorage3D(target, levels, internalformat, width, height, depth);
	}

	public void glSecondaryColorFormatNV(int size, int type, int stride) {
		gl.glSecondaryColorFormatNV(size, type, stride);
	}

	public void glProgramUniform4iv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform4iv(program, location, count, value, value_offset);
	}

	public void glSetMultisamplefvAMD(int pname, int index, FloatBuffer val) {
		gl.glSetMultisamplefvAMD(pname, index, val);
	}

	public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer pixels) {
		gl.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
	}

	public void glProgramUniform4ui(int program, int location, int v0, int v1, int v2, int v3) {
		gl.glProgramUniform4ui(program, location, v0, v1, v2, v3);
	}

	public void glSetMultisamplefvAMD(int pname, int index, float[] val, int val_offset) {
		gl.glSetMultisamplefvAMD(pname, index, val, val_offset);
	}

	public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, long pixels_buffer_offset) {
		gl.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels_buffer_offset);
	}

	public void glStencilOpValueAMD(int face, int value) {
		gl.glStencilOpValueAMD(face, value);
	}

	public void glProgramUniform4uiv(int program, int location, int count, IntBuffer value) {
		gl.glProgramUniform4uiv(program, location, count, value);
	}

	public void glTessellationFactorAMD(float factor) {
		gl.glTessellationFactorAMD(factor);
	}

	public void glTessellationModeAMD(int mode) {
		gl.glTessellationModeAMD(mode);
	}

	public void glTextureStorage1DEXT(int texture, int target, int levels, int internalformat, int width) {
		gl.glTextureStorage1DEXT(texture, target, levels, internalformat, width);
	}

	public void glProgramUniform4uiv(int program, int location, int count, int[] value, int value_offset) {
		gl.glProgramUniform4uiv(program, location, count, value, value_offset);
	}

	public void glTexBuffer(int target, int internalformat, int buffer) {
		gl.glTexBuffer(target, internalformat, buffer);
	}

	public void glTextureStorage2DEXT(int texture, int target, int levels, int internalformat, int width, int height) {
		gl.glTextureStorage2DEXT(texture, target, levels, internalformat, width, height);
	}

	public void glTexCoordFormatNV(int size, int type, int stride) {
		gl.glTexCoordFormatNV(size, type, stride);
	}

	public void glProgramUniformMatrix2fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix2fv(program, location, count, transpose, value);
	}

	public void glTextureStorage3DEXT(int texture, int target, int levels, int internalformat, int width, int height, int depth) {
		gl.glTextureStorage3DEXT(texture, target, levels, internalformat, width, height, depth);
	}

	public void glTexImage1D(int target, int level, int internalFormat, int width, int border, int format, int type, Buffer pixels) {
		gl.glTexImage1D(target, level, internalFormat, width, border, format, type, pixels);
	}

	public boolean glUnmapBuffer(int target) {
		return gl.glUnmapBuffer(target);
	}

	public void glProgramUniformMatrix2fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix2fv(program, location, count, transpose, value, value_offset);
	}

	public void glTexImage1D(int target, int level, int internalFormat, int width, int border, int format, int type, long pixels_buffer_offset) {
		gl.glTexImage1D(target, level, internalFormat, width, border, format, type, pixels_buffer_offset);
	}

	public void glViewport(int x, int y, int width, int height) {
		gl.glViewport(x, y, width, height);
	}

	public void glTexImage2DMultisampleCoverageNV(int target, int coverageSamples, int colorSamples, int internalFormat, int width, int height, boolean fixedSampleLocations) {
		gl.glTexImage2DMultisampleCoverageNV(target, coverageSamples, colorSamples, internalFormat, width, height, fixedSampleLocations);
	}

	public void glProgramUniformMatrix2x3fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix2x3fv(program, location, count, transpose, value);
	}

	public void glTexImage3DMultisampleCoverageNV(int target, int coverageSamples, int colorSamples, int internalFormat, int width, int height, int depth, boolean fixedSampleLocations) {
		gl.glTexImage3DMultisampleCoverageNV(target, coverageSamples, colorSamples, internalFormat, width, height, depth, fixedSampleLocations);
	}

	public void glProgramUniformMatrix2x3fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix2x3fv(program, location, count, transpose, value, value_offset);
	}

	public void glTexPageCommitmentARB(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, boolean resident) {
		gl.glTexPageCommitmentARB(target, level, xoffset, yoffset, zoffset, width, height, depth, resident);
	}

	public void glProgramUniformMatrix2x4fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix2x4fv(program, location, count, transpose, value);
	}

	public void glTexStorage3DMultisample(int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
		gl.glTexStorage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations);
	}

	public void glProgramUniformMatrix2x4fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix2x4fv(program, location, count, transpose, value, value_offset);
	}

	public void glTexSubImage1D(int target, int level, int xoffset, int width, int format, int type, Buffer pixels) {
		gl.glTexSubImage1D(target, level, xoffset, width, format, type, pixels);
	}

	public void glTexSubImage1D(int target, int level, int xoffset, int width, int format, int type, long pixels_buffer_offset) {
		gl.glTexSubImage1D(target, level, xoffset, width, format, type, pixels_buffer_offset);
	}

	public void glProgramUniformMatrix3fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix3fv(program, location, count, transpose, value);
	}

	public void glTextureImage2DMultisampleCoverageNV(int texture, int target, int coverageSamples, int colorSamples, int internalFormat, int width, int height, boolean fixedSampleLocations) {
		gl.glTextureImage2DMultisampleCoverageNV(texture, target, coverageSamples, colorSamples, internalFormat, width, height, fixedSampleLocations);
	}

	public void glProgramUniformMatrix3fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix3fv(program, location, count, transpose, value, value_offset);
	}

	public void glTextureImage2DMultisampleNV(int texture, int target, int samples, int internalFormat, int width, int height, boolean fixedSampleLocations) {
		gl.glTextureImage2DMultisampleNV(texture, target, samples, internalFormat, width, height, fixedSampleLocations);
	}

	public void glProgramUniformMatrix3x2fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix3x2fv(program, location, count, transpose, value);
	}

	public void glTextureImage3DMultisampleCoverageNV(int texture, int target, int coverageSamples, int colorSamples, int internalFormat, int width, int height, int depth, boolean fixedSampleLocations) {
		gl.glTextureImage3DMultisampleCoverageNV(texture, target, coverageSamples, colorSamples, internalFormat, width, height, depth, fixedSampleLocations);
	}

	public void glProgramUniformMatrix3x2fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix3x2fv(program, location, count, transpose, value, value_offset);
	}

	public void glTextureImage3DMultisampleNV(int texture, int target, int samples, int internalFormat, int width, int height, int depth, boolean fixedSampleLocations) {
		gl.glTextureImage3DMultisampleNV(texture, target, samples, internalFormat, width, height, depth, fixedSampleLocations);
	}

	public void glUniformui64NV(int location, long value) {
		gl.glUniformui64NV(location, value);
	}

	public void glProgramUniformMatrix3x4fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix3x4fv(program, location, count, transpose, value);
	}

	public void glUniformui64vNV(int location, int count, LongBuffer value) {
		gl.glUniformui64vNV(location, count, value);
	}

	public void glUniformui64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniformui64vNV(location, count, value, value_offset);
	}

	public void glProgramUniformMatrix3x4fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix3x4fv(program, location, count, transpose, value, value_offset);
	}

	public void glVertexAttrib1d(int index, double x) {
		gl.glVertexAttrib1d(index, x);
	}

	public void glVertexAttrib1dv(int index, DoubleBuffer v) {
		gl.glVertexAttrib1dv(index, v);
	}

	public void glProgramUniformMatrix4fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix4fv(program, location, count, transpose, value);
	}

	public void glVertexAttrib1dv(int index, double[] v, int v_offset) {
		gl.glVertexAttrib1dv(index, v, v_offset);
	}

	public void glProgramUniformMatrix4fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix4fv(program, location, count, transpose, value, value_offset);
	}

	public void glVertexAttrib1s(int index, short x) {
		gl.glVertexAttrib1s(index, x);
	}

	public void glVertexAttrib1sv(int index, ShortBuffer v) {
		gl.glVertexAttrib1sv(index, v);
	}

	public void glProgramUniformMatrix4x2fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix4x2fv(program, location, count, transpose, value);
	}

	public void glVertexAttrib1sv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib1sv(index, v, v_offset);
	}

	public void glProgramUniformMatrix4x2fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix4x2fv(program, location, count, transpose, value, value_offset);
	}

	public void glVertexAttrib2d(int index, double x, double y) {
		gl.glVertexAttrib2d(index, x, y);
	}

	public void glVertexAttrib2dv(int index, DoubleBuffer v) {
		gl.glVertexAttrib2dv(index, v);
	}

	public void glProgramUniformMatrix4x3fv(int program, int location, int count, boolean transpose, FloatBuffer value) {
		gl.glProgramUniformMatrix4x3fv(program, location, count, transpose, value);
	}

	public void glVertexAttrib2dv(int index, double[] v, int v_offset) {
		gl.glVertexAttrib2dv(index, v, v_offset);
	}

	public void glProgramUniformMatrix4x3fv(int program, int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glProgramUniformMatrix4x3fv(program, location, count, transpose, value, value_offset);
	}

	public void glVertexAttrib2s(int index, short x, short y) {
		gl.glVertexAttrib2s(index, x, y);
	}

	public void glVertexAttrib2sv(int index, ShortBuffer v) {
		gl.glVertexAttrib2sv(index, v);
	}

	public void glPushDebugGroup(int source, int id, int length, ByteBuffer message) {
		gl.glPushDebugGroup(source, id, length, message);
	}

	public void glVertexAttrib2sv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib2sv(index, v, v_offset);
	}

	public void glPushDebugGroup(int source, int id, int length, byte[] message, int message_offset) {
		gl.glPushDebugGroup(source, id, length, message, message_offset);
	}

	public void glVertexAttrib3d(int index, double x, double y, double z) {
		gl.glVertexAttrib3d(index, x, y, z);
	}

	public void glQueryCounter(int id, int target) {
		gl.glQueryCounter(id, target);
	}

	public void glVertexAttrib3dv(int index, DoubleBuffer v) {
		gl.glVertexAttrib3dv(index, v);
	}

	public void glSampleMaski(int index, int mask) {
		gl.glSampleMaski(index, mask);
	}

	public void glSamplerParameterIiv(int sampler, int pname, IntBuffer param) {
		gl.glSamplerParameterIiv(sampler, pname, param);
	}

	public void glVertexAttrib3dv(int index, double[] v, int v_offset) {
		gl.glVertexAttrib3dv(index, v, v_offset);
	}

	public void glVertexAttrib3s(int index, short x, short y, short z) {
		gl.glVertexAttrib3s(index, x, y, z);
	}

	public void glSamplerParameterIiv(int sampler, int pname, int[] param, int param_offset) {
		gl.glSamplerParameterIiv(sampler, pname, param, param_offset);
	}

	public void glVertexAttrib3sv(int index, ShortBuffer v) {
		gl.glVertexAttrib3sv(index, v);
	}

	public void glSamplerParameterIuiv(int sampler, int pname, IntBuffer param) {
		gl.glSamplerParameterIuiv(sampler, pname, param);
	}

	public void glVertexAttrib3sv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib3sv(index, v, v_offset);
	}

	public void glSamplerParameterIuiv(int sampler, int pname, int[] param, int param_offset) {
		gl.glSamplerParameterIuiv(sampler, pname, param, param_offset);
	}

	public void glVertexAttrib4Nbv(int index, ByteBuffer v) {
		gl.glVertexAttrib4Nbv(index, v);
	}

	public void glShaderSource(int shader, int count, String[] string, IntBuffer length) {
		gl.glShaderSource(shader, count, string, length);
	}

	public void glVertexAttrib4Nbv(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4Nbv(index, v, v_offset);
	}

	public void glVertexAttrib4Niv(int index, IntBuffer v) {
		gl.glVertexAttrib4Niv(index, v);
	}

	public void glShaderSource(int shader, int count, String[] string, int[] length, int length_offset) {
		gl.glShaderSource(shader, count, string, length, length_offset);
	}

	public void glVertexAttrib4Niv(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4Niv(index, v, v_offset);
	}

	public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
		gl.glStencilFuncSeparate(face, func, ref, mask);
	}

	public void glVertexAttrib4Nsv(int index, ShortBuffer v) {
		gl.glVertexAttrib4Nsv(index, v);
	}

	public void glStencilMaskSeparate(int face, int mask) {
		gl.glStencilMaskSeparate(face, mask);
	}

	public void glVertexAttrib4Nsv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4Nsv(index, v, v_offset);
	}

	public void glStencilOpSeparate(int face, int sfail, int dpfail, int dppass) {
		gl.glStencilOpSeparate(face, sfail, dpfail, dppass);
	}

	public void glVertexAttrib4Nub(int index, byte x, byte y, byte z, byte w) {
		gl.glVertexAttrib4Nub(index, x, y, z, w);
	}

	public void glTexImage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
		gl.glTexImage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations);
	}

	public void glVertexAttrib4Nubv(int index, ByteBuffer v) {
		gl.glVertexAttrib4Nubv(index, v);
	}

	public void glTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, Buffer pixels) {
		gl.glTexImage3D(target, level, internalformat, width, height, depth, border, format, type, pixels);
	}

	public void glVertexAttrib4Nubv(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4Nubv(index, v, v_offset);
	}

	public void glVertexAttrib4Nuiv(int index, IntBuffer v) {
		gl.glVertexAttrib4Nuiv(index, v);
	}

	public void glTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, long pixels_buffer_offset) {
		gl.glTexImage3D(target, level, internalformat, width, height, depth, border, format, type, pixels_buffer_offset);
	}

	public void glVertexAttrib4Nuiv(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4Nuiv(index, v, v_offset);
	}

	public void glVertexAttrib4Nusv(int index, ShortBuffer v) {
		gl.glVertexAttrib4Nusv(index, v);
	}

	public void glTexImage3DMultisample(int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
		gl.glTexImage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations);
	}

	public void glVertexAttrib4Nusv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4Nusv(index, v, v_offset);
	}

	public void glTexParameterIiv(int target, int pname, IntBuffer params) {
		gl.glTexParameterIiv(target, pname, params);
	}

	public void glVertexAttrib4bv(int index, ByteBuffer v) {
		gl.glVertexAttrib4bv(index, v);
	}

	public void glVertexAttrib4bv(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4bv(index, v, v_offset);
	}

	public void glTexParameterIiv(int target, int pname, int[] params, int params_offset) {
		gl.glTexParameterIiv(target, pname, params, params_offset);
	}

	public void glVertexAttrib4d(int index, double x, double y, double z, double w) {
		gl.glVertexAttrib4d(index, x, y, z, w);
	}

	public void glTexParameterIuiv(int target, int pname, IntBuffer params) {
		gl.glTexParameterIuiv(target, pname, params);
	}

	public void glVertexAttrib4dv(int index, DoubleBuffer v) {
		gl.glVertexAttrib4dv(index, v);
	}

	public void glTexParameterIuiv(int target, int pname, int[] params, int params_offset) {
		gl.glTexParameterIuiv(target, pname, params, params_offset);
	}

	public void glVertexAttrib4dv(int index, double[] v, int v_offset) {
		gl.glVertexAttrib4dv(index, v, v_offset);
	}

	public void glTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer pixels) {
		gl.glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
	}

	public void glVertexAttrib4iv(int index, IntBuffer v) {
		gl.glVertexAttrib4iv(index, v);
	}

	public void glVertexAttrib4iv(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4iv(index, v, v_offset);
	}

	public void glTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, long pixels_buffer_offset) {
		gl.glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels_buffer_offset);
	}

	public void glVertexAttrib4s(int index, short x, short y, short z, short w) {
		gl.glVertexAttrib4s(index, x, y, z, w);
	}

	public void glVertexAttrib4sv(int index, ShortBuffer v) {
		gl.glVertexAttrib4sv(index, v);
	}

	public void glUniform1f(int location, float v0) {
		gl.glUniform1f(location, v0);
	}

	public void glVertexAttrib4sv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4sv(index, v, v_offset);
	}

	public void glUniform1fv(int location, int count, FloatBuffer value) {
		gl.glUniform1fv(location, count, value);
	}

	public void glVertexAttrib4ubv(int index, ByteBuffer v) {
		gl.glVertexAttrib4ubv(index, v);
	}

	public void glUniform1fv(int location, int count, float[] value, int value_offset) {
		gl.glUniform1fv(location, count, value, value_offset);
	}

	public void glVertexAttrib4ubv(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4ubv(index, v, v_offset);
	}

	public void glUniform1i(int location, int v0) {
		gl.glUniform1i(location, v0);
	}

	public void glUniform1iv(int location, int count, IntBuffer value) {
		gl.glUniform1iv(location, count, value);
	}

	public void glVertexAttrib4uiv(int index, IntBuffer v) {
		gl.glVertexAttrib4uiv(index, v);
	}

	public void glVertexAttrib4uiv(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4uiv(index, v, v_offset);
	}

	public void glUniform1iv(int location, int count, int[] value, int value_offset) {
		gl.glUniform1iv(location, count, value, value_offset);
	}

	public void glVertexAttrib4usv(int index, ShortBuffer v) {
		gl.glVertexAttrib4usv(index, v);
	}

	public void glUniform2f(int location, float v0, float v1) {
		gl.glUniform2f(location, v0, v1);
	}

	public void glVertexAttrib4usv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4usv(index, v, v_offset);
	}

	public void glUniform2fv(int location, int count, FloatBuffer value) {
		gl.glUniform2fv(location, count, value);
	}

	public void glVertexAttribFormatNV(int index, int size, int type, boolean normalized, int stride) {
		gl.glVertexAttribFormatNV(index, size, type, normalized, stride);
	}

	public void glUniform2fv(int location, int count, float[] value, int value_offset) {
		gl.glUniform2fv(location, count, value, value_offset);
	}

	public void glVertexAttribI1i(int index, int x) {
		gl.glVertexAttribI1i(index, x);
	}

	public void glUniform2i(int location, int v0, int v1) {
		gl.glUniform2i(location, v0, v1);
	}

	public void glVertexAttribI1iv(int index, IntBuffer v) {
		gl.glVertexAttribI1iv(index, v);
	}

	public void glUniform2iv(int location, int count, IntBuffer value) {
		gl.glUniform2iv(location, count, value);
	}

	public void glVertexAttribI1iv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI1iv(index, v, v_offset);
	}

	public void glUniform2iv(int location, int count, int[] value, int value_offset) {
		gl.glUniform2iv(location, count, value, value_offset);
	}

	public void glVertexAttribI1ui(int index, int x) {
		gl.glVertexAttribI1ui(index, x);
	}

	public void glVertexAttribI1uiv(int index, IntBuffer v) {
		gl.glVertexAttribI1uiv(index, v);
	}

	public void glUniform3f(int location, float v0, float v1, float v2) {
		gl.glUniform3f(location, v0, v1, v2);
	}

	public void glVertexAttribI1uiv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI1uiv(index, v, v_offset);
	}

	public void glUniform3fv(int location, int count, FloatBuffer value) {
		gl.glUniform3fv(location, count, value);
	}

	public void glVertexAttribI2i(int index, int x, int y) {
		gl.glVertexAttribI2i(index, x, y);
	}

	public void glUniform3fv(int location, int count, float[] value, int value_offset) {
		gl.glUniform3fv(location, count, value, value_offset);
	}

	public void glVertexAttribI2iv(int index, IntBuffer v) {
		gl.glVertexAttribI2iv(index, v);
	}

	public void glUniform3i(int location, int v0, int v1, int v2) {
		gl.glUniform3i(location, v0, v1, v2);
	}

	public void glVertexAttribI2iv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI2iv(index, v, v_offset);
	}

	public void glUniform3iv(int location, int count, IntBuffer value) {
		gl.glUniform3iv(location, count, value);
	}

	public void glVertexAttribI2ui(int index, int x, int y) {
		gl.glVertexAttribI2ui(index, x, y);
	}

	public void glVertexAttribI2uiv(int index, IntBuffer v) {
		gl.glVertexAttribI2uiv(index, v);
	}

	public void glUniform3iv(int location, int count, int[] value, int value_offset) {
		gl.glUniform3iv(location, count, value, value_offset);
	}

	public void glVertexAttribI2uiv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI2uiv(index, v, v_offset);
	}

	public void glUniform4f(int location, float v0, float v1, float v2, float v3) {
		gl.glUniform4f(location, v0, v1, v2, v3);
	}

	public void glVertexAttribI3i(int index, int x, int y, int z) {
		gl.glVertexAttribI3i(index, x, y, z);
	}

	public void glUniform4fv(int location, int count, FloatBuffer value) {
		gl.glUniform4fv(location, count, value);
	}

	public void glVertexAttribI3iv(int index, IntBuffer v) {
		gl.glVertexAttribI3iv(index, v);
	}

	public void glUniform4fv(int location, int count, float[] value, int value_offset) {
		gl.glUniform4fv(location, count, value, value_offset);
	}

	public void glVertexAttribI3iv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI3iv(index, v, v_offset);
	}

	public void glUniform4i(int location, int v0, int v1, int v2, int v3) {
		gl.glUniform4i(location, v0, v1, v2, v3);
	}

	public void glVertexAttribI3ui(int index, int x, int y, int z) {
		gl.glVertexAttribI3ui(index, x, y, z);
	}

	public void glUniform4iv(int location, int count, IntBuffer value) {
		gl.glUniform4iv(location, count, value);
	}

	public void glVertexAttribI3uiv(int index, IntBuffer v) {
		gl.glVertexAttribI3uiv(index, v);
	}

	public void glUniform4iv(int location, int count, int[] value, int value_offset) {
		gl.glUniform4iv(location, count, value, value_offset);
	}

	public void glVertexAttribI3uiv(int index, int[] v, int v_offset) {
		gl.glVertexAttribI3uiv(index, v, v_offset);
	}

	public void glVertexAttribI4bv(int index, ByteBuffer v) {
		gl.glVertexAttribI4bv(index, v);
	}

	public void glUniformMatrix2fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix2fv(location, count, transpose, value);
	}

	public void glVertexAttribI4bv(int index, byte[] v, int v_offset) {
		gl.glVertexAttribI4bv(index, v, v_offset);
	}

	public void glUniformMatrix2fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix2fv(location, count, transpose, value, value_offset);
	}

	public void glVertexAttribI4sv(int index, ShortBuffer v) {
		gl.glVertexAttribI4sv(index, v);
	}

	public void glUniformMatrix3fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix3fv(location, count, transpose, value);
	}

	public void glVertexAttribI4sv(int index, short[] v, int v_offset) {
		gl.glVertexAttribI4sv(index, v, v_offset);
	}

	public void glVertexAttribI4ubv(int index, ByteBuffer v) {
		gl.glVertexAttribI4ubv(index, v);
	}

	public void glUniformMatrix3fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix3fv(location, count, transpose, value, value_offset);
	}

	public void glVertexAttribI4ubv(int index, byte[] v, int v_offset) {
		gl.glVertexAttribI4ubv(index, v, v_offset);
	}

	public void glUniformMatrix4fv(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix4fv(location, count, transpose, value);
	}

	public void glVertexAttribI4usv(int index, ShortBuffer v) {
		gl.glVertexAttribI4usv(index, v);
	}

	public void glUniformMatrix4fv(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix4fv(location, count, transpose, value, value_offset);
	}

	public void glVertexAttribI4usv(int index, short[] v, int v_offset) {
		gl.glVertexAttribI4usv(index, v, v_offset);
	}

	public void glUseProgram(int program) {
		gl.glUseProgram(program);
	}

	public void glVertexAttribIFormatNV(int index, int size, int type, int stride) {
		gl.glVertexAttribIFormatNV(index, size, type, stride);
	}

	public void glUseProgramStages(int pipeline, int stages, int program) {
		gl.glUseProgramStages(pipeline, stages, program);
	}

	public void glVertexAttribL1d(int index, double x) {
		gl.glVertexAttribL1d(index, x);
	}

	public void glValidateProgram(int program) {
		gl.glValidateProgram(program);
	}

	public void glVertexAttribL1dv(int index, DoubleBuffer v) {
		gl.glVertexAttribL1dv(index, v);
	}

	public void glValidateProgramPipeline(int pipeline) {
		gl.glValidateProgramPipeline(pipeline);
	}

	public void glVertexAttribL1dv(int index, double[] v, int v_offset) {
		gl.glVertexAttribL1dv(index, v, v_offset);
	}

	public void glVertexAttrib1f(int index, float x) {
		gl.glVertexAttrib1f(index, x);
	}

	public void glVertexAttribL2d(int index, double x, double y) {
		gl.glVertexAttribL2d(index, x, y);
	}

	public void glVertexAttrib1fv(int index, FloatBuffer v) {
		gl.glVertexAttrib1fv(index, v);
	}

	public void glVertexAttribL2dv(int index, DoubleBuffer v) {
		gl.glVertexAttribL2dv(index, v);
	}

	public void glVertexAttrib1fv(int index, float[] v, int v_offset) {
		gl.glVertexAttrib1fv(index, v, v_offset);
	}

	public void glVertexAttribL2dv(int index, double[] v, int v_offset) {
		gl.glVertexAttribL2dv(index, v, v_offset);
	}

	public void glVertexAttribL3d(int index, double x, double y, double z) {
		gl.glVertexAttribL3d(index, x, y, z);
	}

	public void glVertexAttrib2f(int index, float x, float y) {
		gl.glVertexAttrib2f(index, x, y);
	}

	public void glVertexAttribL3dv(int index, DoubleBuffer v) {
		gl.glVertexAttribL3dv(index, v);
	}

	public void glVertexAttrib2fv(int index, FloatBuffer v) {
		gl.glVertexAttrib2fv(index, v);
	}

	public void glVertexAttribL3dv(int index, double[] v, int v_offset) {
		gl.glVertexAttribL3dv(index, v, v_offset);
	}

	public void glVertexAttrib2fv(int index, float[] v, int v_offset) {
		gl.glVertexAttrib2fv(index, v, v_offset);
	}

	public void glVertexAttribL4d(int index, double x, double y, double z, double w) {
		gl.glVertexAttribL4d(index, x, y, z, w);
	}

	public void glVertexAttrib3f(int index, float x, float y, float z) {
		gl.glVertexAttrib3f(index, x, y, z);
	}

	public void glVertexAttribL4dv(int index, DoubleBuffer v) {
		gl.glVertexAttribL4dv(index, v);
	}

	public void glVertexAttrib3fv(int index, FloatBuffer v) {
		gl.glVertexAttrib3fv(index, v);
	}

	public void glVertexAttribL4dv(int index, double[] v, int v_offset) {
		gl.glVertexAttribL4dv(index, v, v_offset);
	}

	public void glVertexAttrib3fv(int index, float[] v, int v_offset) {
		gl.glVertexAttrib3fv(index, v, v_offset);
	}

	public void glVertexAttribLPointer(int index, int size, int type, int stride, long pointer_buffer_offset) {
		gl.glVertexAttribLPointer(index, size, type, stride, pointer_buffer_offset);
	}

	public void glVertexAttrib4f(int index, float x, float y, float z, float w) {
		gl.glVertexAttrib4f(index, x, y, z, w);
	}

	public void glVertexFormatNV(int size, int type, int stride) {
		gl.glVertexFormatNV(size, type, stride);
	}

	public void glVertexAttrib4fv(int index, FloatBuffer v) {
		gl.glVertexAttrib4fv(index, v);
	}

	public void glVertexAttrib4fv(int index, float[] v, int v_offset) {
		gl.glVertexAttrib4fv(index, v, v_offset);
	}

	public void glVertexAttribPointer(int index, int size, int type, boolean normalized, int stride, long pointer_buffer_offset) {
		gl.glVertexAttribPointer(index, size, type, normalized, stride, pointer_buffer_offset);
	}

	public void glReleaseShaderCompiler() {
		gl.glReleaseShaderCompiler();
	}

	public void glShaderBinary(int n, IntBuffer shaders, int binaryformat, Buffer binary, int length) {
		gl.glShaderBinary(n, shaders, binaryformat, binary, length);
	}

	public void glShaderBinary(int n, int[] shaders, int shaders_offset, int binaryformat, Buffer binary, int length) {
		gl.glShaderBinary(n, shaders, shaders_offset, binaryformat, binary, length);
	}

	public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, IntBuffer range, IntBuffer precision) {
		gl.glGetShaderPrecisionFormat(shadertype, precisiontype, range, precision);
	}

	public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, int[] range, int range_offset, int[] precision, int precision_offset) {
		gl.glGetShaderPrecisionFormat(shadertype, precisiontype, range, range_offset, precision, precision_offset);
	}

	public void glAccum(int op, float value) {
		gl.glAccum(op, value);
	}

	public void glActiveStencilFaceEXT(int face) {
		gl.glActiveStencilFaceEXT(face);
	}

	public void glApplyTextureEXT(int mode) {
		gl.glApplyTextureEXT(mode);
	}

	public boolean glAreTexturesResident(int n, IntBuffer textures, ByteBuffer residences) {
		return gl.glAreTexturesResident(n, textures, residences);
	}

	public void glVertexAttribPointer(GLArrayData array) {
		gl.glVertexAttribPointer(array);
	}

	public void glUniform(GLUniformData data) {
		gl.glUniform(data);
	}

	public boolean glAreTexturesResident(int n, int[] textures, int textures_offset, byte[] residences, int residences_offset) {
		return gl.glAreTexturesResident(n, textures, textures_offset, residences, residences_offset);
	}

	public void glArrayElement(int i) {
		gl.glArrayElement(i);
	}

	public void glAttachObjectARB(long containerObj, long obj) {
		gl.glAttachObjectARB(containerObj, obj);
	}

	public void glBegin(int mode) {
		gl.glBegin(mode);
	}

	public void glBeginConditionalRenderNVX(int id) {
		gl.glBeginConditionalRenderNVX(id);
	}

	public void glBeginOcclusionQueryNV(int id) {
		gl.glBeginOcclusionQueryNV(id);
	}

	public void glBeginPerfMonitorAMD(int monitor) {
		gl.glBeginPerfMonitorAMD(monitor);
	}

	public void glBeginPerfQueryINTEL(int queryHandle) {
		gl.glBeginPerfQueryINTEL(queryHandle);
	}

	public void glBeginVertexShaderEXT() {
		gl.glBeginVertexShaderEXT();
	}

	public void glBeginVideoCaptureNV(int video_capture_slot) {
		gl.glBeginVideoCaptureNV(video_capture_slot);
	}

	public int glBindLightParameterEXT(int light, int value) {
		return gl.glBindLightParameterEXT(light, value);
	}

	public int glBindMaterialParameterEXT(int face, int value) {
		return gl.glBindMaterialParameterEXT(face, value);
	}

	public void glBindMultiTextureEXT(int texunit, int target, int texture) {
		gl.glBindMultiTextureEXT(texunit, target, texture);
	}

	public int glBindParameterEXT(int value) {
		return gl.glBindParameterEXT(value);
	}

	public void glBindProgramARB(int target, int program) {
		gl.glBindProgramARB(target, program);
	}

	public int glBindTexGenParameterEXT(int unit, int coord, int value) {
		return gl.glBindTexGenParameterEXT(unit, coord, value);
	}

	public int glBindTextureUnitParameterEXT(int unit, int value) {
		return gl.glBindTextureUnitParameterEXT(unit, value);
	}

	public void glBindTransformFeedbackNV(int target, int id) {
		gl.glBindTransformFeedbackNV(target, id);
	}

	public void glBindVertexShaderEXT(int id) {
		gl.glBindVertexShaderEXT(id);
	}

	public void glBindVideoCaptureStreamBufferNV(int video_capture_slot, int stream, int frame_region, long offset) {
		gl.glBindVideoCaptureStreamBufferNV(video_capture_slot, stream, frame_region, offset);
	}

	public void glBindVideoCaptureStreamTextureNV(int video_capture_slot, int stream, int frame_region, int target, int texture) {
		gl.glBindVideoCaptureStreamTextureNV(video_capture_slot, stream, frame_region, target, texture);
	}

	public void glBitmap(int width, int height, float xorig, float yorig, float xmove, float ymove, ByteBuffer bitmap) {
		gl.glBitmap(width, height, xorig, yorig, xmove, ymove, bitmap);
	}

	public void glBitmap(int width, int height, float xorig, float yorig, float xmove, float ymove, byte[] bitmap, int bitmap_offset) {
		gl.glBitmap(width, height, xorig, yorig, xmove, ymove, bitmap, bitmap_offset);
	}

	public void glBitmap(int width, int height, float xorig, float yorig, float xmove, float ymove, long bitmap_buffer_offset) {
		gl.glBitmap(width, height, xorig, yorig, xmove, ymove, bitmap_buffer_offset);
	}

	public void glBlendEquationIndexedAMD(int buf, int mode) {
		gl.glBlendEquationIndexedAMD(buf, mode);
	}

	public void glBlendEquationSeparateIndexedAMD(int buf, int modeRGB, int modeAlpha) {
		gl.glBlendEquationSeparateIndexedAMD(buf, modeRGB, modeAlpha);
	}

	public void glBlendFuncIndexedAMD(int buf, int src, int dst) {
		gl.glBlendFuncIndexedAMD(buf, src, dst);
	}

	public void glBlendFuncSeparateINGR(int sfactorRGB, int dfactorRGB, int sfactorAlpha, int dfactorAlpha) {
		gl.glBlendFuncSeparateINGR(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
	}

	public void glBlendFuncSeparateIndexedAMD(int buf, int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {
		gl.glBlendFuncSeparateIndexedAMD(buf, srcRGB, dstRGB, srcAlpha, dstAlpha);
	}

	public void glBufferParameteri(int target, int pname, int param) {
		gl.glBufferParameteri(target, pname, param);
	}

	public void glCallList(int list) {
		gl.glCallList(list);
	}

	public void glCallLists(int n, int type, Buffer lists) {
		gl.glCallLists(n, type, lists);
	}

	public int glCheckNamedFramebufferStatusEXT(int framebuffer, int target) {
		return gl.glCheckNamedFramebufferStatusEXT(framebuffer, target);
	}

	public void glClearAccum(float red, float green, float blue, float alpha) {
		gl.glClearAccum(red, green, blue, alpha);
	}

	public void glClearColorIi(int red, int green, int blue, int alpha) {
		gl.glClearColorIi(red, green, blue, alpha);
	}

	public void glClearColorIui(int red, int green, int blue, int alpha) {
		gl.glClearColorIui(red, green, blue, alpha);
	}

	public void glClearIndex(float c) {
		gl.glClearIndex(c);
	}

	public void glClearNamedBufferData(int buffer, int internalformat, int format, int type, Buffer data) {
		gl.glClearNamedBufferData(buffer, internalformat, format, type, data);
	}

	public void glClearNamedBufferSubData(int buffer, int internalformat, long offset, long size, int format, int type, Buffer data) {
		gl.glClearNamedBufferSubData(buffer, internalformat, offset, size, format, type, data);
	}

	public void glClientAttribDefaultEXT(int mask) {
		gl.glClientAttribDefaultEXT(mask);
	}

	public void glClipPlane(int plane, DoubleBuffer equation) {
		gl.glClipPlane(plane, equation);
	}

	public void glClipPlane(int plane, double[] equation, int equation_offset) {
		gl.glClipPlane(plane, equation, equation_offset);
	}

	public void glClipPlanef(int plane, FloatBuffer equation) {
		gl.glClipPlanef(plane, equation);
	}

	public void glClipPlanef(int plane, float[] equation, int equation_offset) {
		gl.glClipPlanef(plane, equation, equation_offset);
	}

	public void glColor3b(byte red, byte green, byte blue) {
		gl.glColor3b(red, green, blue);
	}

	public void glColor3bv(ByteBuffer v) {
		gl.glColor3bv(v);
	}

	public void glColor3bv(byte[] v, int v_offset) {
		gl.glColor3bv(v, v_offset);
	}

	public void glColor3d(double red, double green, double blue) {
		gl.glColor3d(red, green, blue);
	}

	public void glColor3dv(DoubleBuffer v) {
		gl.glColor3dv(v);
	}

	public void glColor3dv(double[] v, int v_offset) {
		gl.glColor3dv(v, v_offset);
	}

	public void glColor3f(float red, float green, float blue) {
		gl.glColor3f(red, green, blue);
	}

	public void glColor3fv(FloatBuffer v) {
		gl.glColor3fv(v);
	}

	public void glColor3fv(float[] v, int v_offset) {
		gl.glColor3fv(v, v_offset);
	}

	public void glColor3h(short red, short green, short blue) {
		gl.glColor3h(red, green, blue);
	}

	public void glColor3hv(ShortBuffer v) {
		gl.glColor3hv(v);
	}

	public void glColor3hv(short[] v, int v_offset) {
		gl.glColor3hv(v, v_offset);
	}

	public void glColor3i(int red, int green, int blue) {
		gl.glColor3i(red, green, blue);
	}

	public void glColor3iv(IntBuffer v) {
		gl.glColor3iv(v);
	}

	public void glColor3iv(int[] v, int v_offset) {
		gl.glColor3iv(v, v_offset);
	}

	public void glColor3s(short red, short green, short blue) {
		gl.glColor3s(red, green, blue);
	}

	public void glColor3sv(ShortBuffer v) {
		gl.glColor3sv(v);
	}

	public void glColor3sv(short[] v, int v_offset) {
		gl.glColor3sv(v, v_offset);
	}

	public void glColor3ub(byte red, byte green, byte blue) {
		gl.glColor3ub(red, green, blue);
	}

	public void glColor3ubv(ByteBuffer v) {
		gl.glColor3ubv(v);
	}

	public void glColor3ubv(byte[] v, int v_offset) {
		gl.glColor3ubv(v, v_offset);
	}

	public void glColor3ui(int red, int green, int blue) {
		gl.glColor3ui(red, green, blue);
	}

	public void glColor3uiv(IntBuffer v) {
		gl.glColor3uiv(v);
	}

	public void glColor3uiv(int[] v, int v_offset) {
		gl.glColor3uiv(v, v_offset);
	}

	public void glColor3us(short red, short green, short blue) {
		gl.glColor3us(red, green, blue);
	}

	public void glColor3usv(ShortBuffer v) {
		gl.glColor3usv(v);
	}

	public void glColor3usv(short[] v, int v_offset) {
		gl.glColor3usv(v, v_offset);
	}

	public void glColor4b(byte red, byte green, byte blue, byte alpha) {
		gl.glColor4b(red, green, blue, alpha);
	}

	public void glColor4bv(ByteBuffer v) {
		gl.glColor4bv(v);
	}

	public void glColor4bv(byte[] v, int v_offset) {
		gl.glColor4bv(v, v_offset);
	}

	public void glColor4d(double red, double green, double blue, double alpha) {
		gl.glColor4d(red, green, blue, alpha);
	}

	public void glColor4dv(DoubleBuffer v) {
		gl.glColor4dv(v);
	}

	public void glColor4dv(double[] v, int v_offset) {
		gl.glColor4dv(v, v_offset);
	}

	public void glColor4fv(FloatBuffer v) {
		gl.glColor4fv(v);
	}

	public void glColor4fv(float[] v, int v_offset) {
		gl.glColor4fv(v, v_offset);
	}

	public void glColor4h(short red, short green, short blue, short alpha) {
		gl.glColor4h(red, green, blue, alpha);
	}

	public void glColor4hv(ShortBuffer v) {
		gl.glColor4hv(v);
	}

	public void glColor4hv(short[] v, int v_offset) {
		gl.glColor4hv(v, v_offset);
	}

	public void glColor4i(int red, int green, int blue, int alpha) {
		gl.glColor4i(red, green, blue, alpha);
	}

	public void glColor4iv(IntBuffer v) {
		gl.glColor4iv(v);
	}

	public void glColor4iv(int[] v, int v_offset) {
		gl.glColor4iv(v, v_offset);
	}

	public void glColor4s(short red, short green, short blue, short alpha) {
		gl.glColor4s(red, green, blue, alpha);
	}

	public void glColor4sv(ShortBuffer v) {
		gl.glColor4sv(v);
	}

	public void glColor4sv(short[] v, int v_offset) {
		gl.glColor4sv(v, v_offset);
	}

	public void glColor4ubv(ByteBuffer v) {
		gl.glColor4ubv(v);
	}

	public void glColor4ubv(byte[] v, int v_offset) {
		gl.glColor4ubv(v, v_offset);
	}

	public void glColor4ui(int red, int green, int blue, int alpha) {
		gl.glColor4ui(red, green, blue, alpha);
	}

	public void glColor4uiv(IntBuffer v) {
		gl.glColor4uiv(v);
	}

	public void glColor4uiv(int[] v, int v_offset) {
		gl.glColor4uiv(v, v_offset);
	}

	public void glColor4us(short red, short green, short blue, short alpha) {
		gl.glColor4us(red, green, blue, alpha);
	}

	public void glColor4usv(ShortBuffer v) {
		gl.glColor4usv(v);
	}

	public void glColor4usv(short[] v, int v_offset) {
		gl.glColor4usv(v, v_offset);
	}

	public void glColorMaskIndexed(int index, boolean r, boolean g, boolean b, boolean a) {
		gl.glColorMaskIndexed(index, r, g, b, a);
	}

	public void glColorMaterial(int face, int mode) {
		gl.glColorMaterial(face, mode);
	}

	public void glColorSubTable(int target, int start, int count, int format, int type, Buffer data) {
		gl.glColorSubTable(target, start, count, format, type, data);
	}

	public void glColorSubTable(int target, int start, int count, int format, int type, long data_buffer_offset) {
		gl.glColorSubTable(target, start, count, format, type, data_buffer_offset);
	}

	public void glColorTable(int target, int internalformat, int width, int format, int type, Buffer table) {
		gl.glColorTable(target, internalformat, width, format, type, table);
	}

	public void glColorTable(int target, int internalformat, int width, int format, int type, long table_buffer_offset) {
		gl.glColorTable(target, internalformat, width, format, type, table_buffer_offset);
	}

	public void glColorTableParameterfv(int target, int pname, FloatBuffer params) {
		gl.glColorTableParameterfv(target, pname, params);
	}

	public void glColorTableParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glColorTableParameterfv(target, pname, params, params_offset);
	}

	public void glColorTableParameteriv(int target, int pname, IntBuffer params) {
		gl.glColorTableParameteriv(target, pname, params);
	}

	public void glColorTableParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glColorTableParameteriv(target, pname, params, params_offset);
	}

	public void glCompileShaderARB(long shaderObj) {
		gl.glCompileShaderARB(shaderObj);
	}

	public void glCompressedMultiTexImage1DEXT(int texunit, int target, int level, int internalformat, int width, int border, int imageSize, Buffer bits) {
		gl.glCompressedMultiTexImage1DEXT(texunit, target, level, internalformat, width, border, imageSize, bits);
	}

	public void glCompressedMultiTexImage2DEXT(int texunit, int target, int level, int internalformat, int width, int height, int border, int imageSize, Buffer bits) {
		gl.glCompressedMultiTexImage2DEXT(texunit, target, level, internalformat, width, height, border, imageSize, bits);
	}

	public void glCompressedMultiTexImage3DEXT(int texunit, int target, int level, int internalformat, int width, int height, int depth, int border, int imageSize, Buffer bits) {
		gl.glCompressedMultiTexImage3DEXT(texunit, target, level, internalformat, width, height, depth, border, imageSize, bits);
	}

	public void glCompressedMultiTexSubImage1DEXT(int texunit, int target, int level, int xoffset, int width, int format, int imageSize, Buffer bits) {
		gl.glCompressedMultiTexSubImage1DEXT(texunit, target, level, xoffset, width, format, imageSize, bits);
	}

	public void glCompressedMultiTexSubImage2DEXT(int texunit, int target, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer bits) {
		gl.glCompressedMultiTexSubImage2DEXT(texunit, target, level, xoffset, yoffset, width, height, format, imageSize, bits);
	}

	public void glCompressedMultiTexSubImage3DEXT(int texunit, int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, Buffer bits) {
		gl.glCompressedMultiTexSubImage3DEXT(texunit, target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, bits);
	}

	public void glCompressedTextureImage1DEXT(int texture, int target, int level, int internalformat, int width, int border, int imageSize, Buffer bits) {
		gl.glCompressedTextureImage1DEXT(texture, target, level, internalformat, width, border, imageSize, bits);
	}

	public void glCompressedTextureImage2DEXT(int texture, int target, int level, int internalformat, int width, int height, int border, int imageSize, Buffer bits) {
		gl.glCompressedTextureImage2DEXT(texture, target, level, internalformat, width, height, border, imageSize, bits);
	}

	public void glCompressedTextureImage3DEXT(int texture, int target, int level, int internalformat, int width, int height, int depth, int border, int imageSize, Buffer bits) {
		gl.glCompressedTextureImage3DEXT(texture, target, level, internalformat, width, height, depth, border, imageSize, bits);
	}

	public void glCompressedTextureSubImage1DEXT(int texture, int target, int level, int xoffset, int width, int format, int imageSize, Buffer bits) {
		gl.glCompressedTextureSubImage1DEXT(texture, target, level, xoffset, width, format, imageSize, bits);
	}

	public void glCompressedTextureSubImage2DEXT(int texture, int target, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer bits) {
		gl.glCompressedTextureSubImage2DEXT(texture, target, level, xoffset, yoffset, width, height, format, imageSize, bits);
	}

	public void glCompressedTextureSubImage3DEXT(int texture, int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int imageSize, Buffer bits) {
		gl.glCompressedTextureSubImage3DEXT(texture, target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, bits);
	}

	public void glConvolutionFilter1D(int target, int internalformat, int width, int format, int type, Buffer image) {
		gl.glConvolutionFilter1D(target, internalformat, width, format, type, image);
	}

	public void glConvolutionFilter1D(int target, int internalformat, int width, int format, int type, long image_buffer_offset) {
		gl.glConvolutionFilter1D(target, internalformat, width, format, type, image_buffer_offset);
	}

	public void glConvolutionFilter2D(int target, int internalformat, int width, int height, int format, int type, Buffer image) {
		gl.glConvolutionFilter2D(target, internalformat, width, height, format, type, image);
	}

	public void glConvolutionFilter2D(int target, int internalformat, int width, int height, int format, int type, long image_buffer_offset) {
		gl.glConvolutionFilter2D(target, internalformat, width, height, format, type, image_buffer_offset);
	}

	public void glConvolutionParameterf(int target, int pname, float params) {
		gl.glConvolutionParameterf(target, pname, params);
	}

	public void glConvolutionParameterfv(int target, int pname, FloatBuffer params) {
		gl.glConvolutionParameterfv(target, pname, params);
	}

	public void glConvolutionParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glConvolutionParameterfv(target, pname, params, params_offset);
	}

	public void glConvolutionParameteri(int target, int pname, int params) {
		gl.glConvolutionParameteri(target, pname, params);
	}

	public void glConvolutionParameteriv(int target, int pname, IntBuffer params) {
		gl.glConvolutionParameteriv(target, pname, params);
	}

	public void glConvolutionParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glConvolutionParameteriv(target, pname, params, params_offset);
	}

	public void glCopyColorSubTable(int target, int start, int x, int y, int width) {
		gl.glCopyColorSubTable(target, start, x, y, width);
	}

	public void glCopyColorTable(int target, int internalformat, int x, int y, int width) {
		gl.glCopyColorTable(target, internalformat, x, y, width);
	}

	public void glCopyConvolutionFilter1D(int target, int internalformat, int x, int y, int width) {
		gl.glCopyConvolutionFilter1D(target, internalformat, x, y, width);
	}

	public void glCopyConvolutionFilter2D(int target, int internalformat, int x, int y, int width, int height) {
		gl.glCopyConvolutionFilter2D(target, internalformat, x, y, width, height);
	}

	public void glCopyImageSubDataNV(int srcName, int srcTarget, int srcLevel, int srcX, int srcY, int srcZ, int dstName, int dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int width, int height, int depth) {
		gl.glCopyImageSubDataNV(srcName, srcTarget, srcLevel, srcX, srcY, srcZ, dstName, dstTarget, dstLevel, dstX, dstY, dstZ, width, height, depth);
	}

	public void glCopyMultiTexImage1DEXT(int texunit, int target, int level, int internalformat, int x, int y, int width, int border) {
		gl.glCopyMultiTexImage1DEXT(texunit, target, level, internalformat, x, y, width, border);
	}

	public void glCopyMultiTexImage2DEXT(int texunit, int target, int level, int internalformat, int x, int y, int width, int height, int border) {
		gl.glCopyMultiTexImage2DEXT(texunit, target, level, internalformat, x, y, width, height, border);
	}

	public void glCopyMultiTexSubImage1DEXT(int texunit, int target, int level, int xoffset, int x, int y, int width) {
		gl.glCopyMultiTexSubImage1DEXT(texunit, target, level, xoffset, x, y, width);
	}

	public void glCopyMultiTexSubImage2DEXT(int texunit, int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
		gl.glCopyMultiTexSubImage2DEXT(texunit, target, level, xoffset, yoffset, x, y, width, height);
	}

	public void glCopyMultiTexSubImage3DEXT(int texunit, int target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) {
		gl.glCopyMultiTexSubImage3DEXT(texunit, target, level, xoffset, yoffset, zoffset, x, y, width, height);
	}

	public void glCopyPixels(int x, int y, int width, int height, int type) {
		gl.glCopyPixels(x, y, width, height, type);
	}

	public void glCopyTextureImage1DEXT(int texture, int target, int level, int internalformat, int x, int y, int width, int border) {
		gl.glCopyTextureImage1DEXT(texture, target, level, internalformat, x, y, width, border);
	}

	public void glCopyTextureImage2DEXT(int texture, int target, int level, int internalformat, int x, int y, int width, int height, int border) {
		gl.glCopyTextureImage2DEXT(texture, target, level, internalformat, x, y, width, height, border);
	}

	public void glCopyTextureSubImage1DEXT(int texture, int target, int level, int xoffset, int x, int y, int width) {
		gl.glCopyTextureSubImage1DEXT(texture, target, level, xoffset, x, y, width);
	}

	public void glCopyTextureSubImage2DEXT(int texture, int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
		gl.glCopyTextureSubImage2DEXT(texture, target, level, xoffset, yoffset, x, y, width, height);
	}

	public void glCopyTextureSubImage3DEXT(int texture, int target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) {
		gl.glCopyTextureSubImage3DEXT(texture, target, level, xoffset, yoffset, zoffset, x, y, width, height);
	}

	public void glCoverageModulationNV(int components) {
		gl.glCoverageModulationNV(components);
	}

	public void glCoverageModulationTableNV(int n, FloatBuffer v) {
		gl.glCoverageModulationTableNV(n, v);
	}

	public void glCoverageModulationTableNV(int n, float[] v, int v_offset) {
		gl.glCoverageModulationTableNV(n, v, v_offset);
	}

	public void glCreatePerfQueryINTEL(int queryId, IntBuffer queryHandle) {
		gl.glCreatePerfQueryINTEL(queryId, queryHandle);
	}

	public void glCreatePerfQueryINTEL(int queryId, int[] queryHandle, int queryHandle_offset) {
		gl.glCreatePerfQueryINTEL(queryId, queryHandle, queryHandle_offset);
	}

	public long glCreateProgramObjectARB() {
		return gl.glCreateProgramObjectARB();
	}

	public long glCreateShaderObjectARB(int shaderType) {
		return gl.glCreateShaderObjectARB(shaderType);
	}

	public void glCullParameterdvEXT(int pname, DoubleBuffer params) {
		gl.glCullParameterdvEXT(pname, params);
	}

	public void glCullParameterdvEXT(int pname, double[] params, int params_offset) {
		gl.glCullParameterdvEXT(pname, params, params_offset);
	}

	public void glCullParameterfvEXT(int pname, FloatBuffer params) {
		gl.glCullParameterfvEXT(pname, params);
	}

	public void glCullParameterfvEXT(int pname, float[] params, int params_offset) {
		gl.glCullParameterfvEXT(pname, params, params_offset);
	}

	public void glCurrentPaletteMatrixARB(int index) {
		gl.glCurrentPaletteMatrixARB(index);
	}

	public void glDeleteLists(int list, int range) {
		gl.glDeleteLists(list, range);
	}

	public void glDeleteNamesAMD(int identifier, int num, IntBuffer names) {
		gl.glDeleteNamesAMD(identifier, num, names);
	}

	public void glDeleteNamesAMD(int identifier, int num, int[] names, int names_offset) {
		gl.glDeleteNamesAMD(identifier, num, names, names_offset);
	}

	public void glDeleteObjectARB(long obj) {
		gl.glDeleteObjectARB(obj);
	}

	public void glDeleteOcclusionQueriesNV(int n, IntBuffer ids) {
		gl.glDeleteOcclusionQueriesNV(n, ids);
	}

	public void glDeleteOcclusionQueriesNV(int n, int[] ids, int ids_offset) {
		gl.glDeleteOcclusionQueriesNV(n, ids, ids_offset);
	}

	public void glDeletePerfMonitorsAMD(int n, IntBuffer monitors) {
		gl.glDeletePerfMonitorsAMD(n, monitors);
	}

	public void glDeletePerfMonitorsAMD(int n, int[] monitors, int monitors_offset) {
		gl.glDeletePerfMonitorsAMD(n, monitors, monitors_offset);
	}

	public void glDeletePerfQueryINTEL(int queryHandle) {
		gl.glDeletePerfQueryINTEL(queryHandle);
	}

	public void glDeleteProgramsARB(int n, IntBuffer programs) {
		gl.glDeleteProgramsARB(n, programs);
	}

	public void glDeleteProgramsARB(int n, int[] programs, int programs_offset) {
		gl.glDeleteProgramsARB(n, programs, programs_offset);
	}

	public void glDeleteTransformFeedbacksNV(int n, IntBuffer ids) {
		gl.glDeleteTransformFeedbacksNV(n, ids);
	}

	public void glDeleteTransformFeedbacksNV(int n, int[] ids, int ids_offset) {
		gl.glDeleteTransformFeedbacksNV(n, ids, ids_offset);
	}

	public void glDeleteVertexShaderEXT(int id) {
		gl.glDeleteVertexShaderEXT(id);
	}

	public void glDepthBoundsEXT(double zmin, double zmax) {
		gl.glDepthBoundsEXT(zmin, zmax);
	}

	public void glDetachObjectARB(long containerObj, long attachedObj) {
		gl.glDetachObjectARB(containerObj, attachedObj);
	}

	public void glDisableClientStateIndexedEXT(int array, int index) {
		gl.glDisableClientStateIndexedEXT(array, index);
	}

	public void glDisableClientStateiEXT(int array, int index) {
		gl.glDisableClientStateiEXT(array, index);
	}

	public void glDisableIndexed(int target, int index) {
		gl.glDisableIndexed(target, index);
	}

	public void glDisableVariantClientStateEXT(int id) {
		gl.glDisableVariantClientStateEXT(id);
	}

	public void glDisableVertexArrayAttribEXT(int vaobj, int index) {
		gl.glDisableVertexArrayAttribEXT(vaobj, index);
	}

	public void glDisableVertexArrayEXT(int vaobj, int array) {
		gl.glDisableVertexArrayEXT(vaobj, array);
	}

	public void glDisableVertexAttribAPPLE(int index, int pname) {
		gl.glDisableVertexAttribAPPLE(index, pname);
	}

	public void glDisableVertexAttribArrayARB(int index) {
		gl.glDisableVertexAttribArrayARB(index);
	}

	public void glDrawBuffersATI(int n, IntBuffer bufs) {
		gl.glDrawBuffersATI(n, bufs);
	}

	public void glDrawBuffersATI(int n, int[] bufs, int bufs_offset) {
		gl.glDrawBuffersATI(n, bufs, bufs_offset);
	}

	public void glDrawPixels(int width, int height, int format, int type, Buffer pixels) {
		gl.glDrawPixels(width, height, format, type, pixels);
	}

	public void glDrawPixels(int width, int height, int format, int type, long pixels_buffer_offset) {
		gl.glDrawPixels(width, height, format, type, pixels_buffer_offset);
	}

	public void glDrawTextureNV(int texture, int sampler, float x0, float y0, float x1, float y1, float z, float s0, float t0, float s1, float t1) {
		gl.glDrawTextureNV(texture, sampler, x0, y0, x1, y1, z, s0, t0, s1, t1);
	}

	public void glDrawTransformFeedbackNV(int mode, int id) {
		gl.glDrawTransformFeedbackNV(mode, id);
	}

	public void glEdgeFlag(boolean flag) {
		gl.glEdgeFlag(flag);
	}

	public void glEdgeFlagPointer(int stride, Buffer ptr) {
		gl.glEdgeFlagPointer(stride, ptr);
	}

	public void glEdgeFlagPointer(int stride, long ptr_buffer_offset) {
		gl.glEdgeFlagPointer(stride, ptr_buffer_offset);
	}

	public void glEdgeFlagv(ByteBuffer flag) {
		gl.glEdgeFlagv(flag);
	}

	public void glEdgeFlagv(byte[] flag, int flag_offset) {
		gl.glEdgeFlagv(flag, flag_offset);
	}

	public void glEnableClientStateIndexedEXT(int array, int index) {
		gl.glEnableClientStateIndexedEXT(array, index);
	}

	public void glEnableClientStateiEXT(int array, int index) {
		gl.glEnableClientStateiEXT(array, index);
	}

	public void glEnableIndexed(int target, int index) {
		gl.glEnableIndexed(target, index);
	}

	public void glEnableVariantClientStateEXT(int id) {
		gl.glEnableVariantClientStateEXT(id);
	}

	public void glEnableVertexArrayAttribEXT(int vaobj, int index) {
		gl.glEnableVertexArrayAttribEXT(vaobj, index);
	}

	public void glEnableVertexArrayEXT(int vaobj, int array) {
		gl.glEnableVertexArrayEXT(vaobj, array);
	}

	public void glEnableVertexAttribAPPLE(int index, int pname) {
		gl.glEnableVertexAttribAPPLE(index, pname);
	}

	public void glEnableVertexAttribArrayARB(int index) {
		gl.glEnableVertexAttribArrayARB(index);
	}

	public void glEnd() {
		gl.glEnd();
	}

	public void glEndConditionalRenderNVX() {
		gl.glEndConditionalRenderNVX();
	}

	public void glEndList() {
		gl.glEndList();
	}

	public void glEndOcclusionQueryNV() {
		gl.glEndOcclusionQueryNV();
	}

	public void glEndPerfMonitorAMD(int monitor) {
		gl.glEndPerfMonitorAMD(monitor);
	}

	public void glEndPerfQueryINTEL(int queryHandle) {
		gl.glEndPerfQueryINTEL(queryHandle);
	}

	public void glEndVertexShaderEXT() {
		gl.glEndVertexShaderEXT();
	}

	public void glEndVideoCaptureNV(int video_capture_slot) {
		gl.glEndVideoCaptureNV(video_capture_slot);
	}

	public void glEvalCoord1d(double u) {
		gl.glEvalCoord1d(u);
	}

	public void glEvalCoord1dv(DoubleBuffer u) {
		gl.glEvalCoord1dv(u);
	}

	public void glEvalCoord1dv(double[] u, int u_offset) {
		gl.glEvalCoord1dv(u, u_offset);
	}

	public void glEvalCoord1f(float u) {
		gl.glEvalCoord1f(u);
	}

	public void glEvalCoord1fv(FloatBuffer u) {
		gl.glEvalCoord1fv(u);
	}

	public void glEvalCoord1fv(float[] u, int u_offset) {
		gl.glEvalCoord1fv(u, u_offset);
	}

	public void glEvalCoord2d(double u, double v) {
		gl.glEvalCoord2d(u, v);
	}

	public void glEvalCoord2dv(DoubleBuffer u) {
		gl.glEvalCoord2dv(u);
	}

	public void glEvalCoord2dv(double[] u, int u_offset) {
		gl.glEvalCoord2dv(u, u_offset);
	}

	public void glEvalCoord2f(float u, float v) {
		gl.glEvalCoord2f(u, v);
	}

	public void glEvalCoord2fv(FloatBuffer u) {
		gl.glEvalCoord2fv(u);
	}

	public void glEvalCoord2fv(float[] u, int u_offset) {
		gl.glEvalCoord2fv(u, u_offset);
	}

	public void glEvalMapsNV(int target, int mode) {
		gl.glEvalMapsNV(target, mode);
	}

	public void glEvalMesh1(int mode, int i1, int i2) {
		gl.glEvalMesh1(mode, i1, i2);
	}

	public void glEvalMesh2(int mode, int i1, int i2, int j1, int j2) {
		gl.glEvalMesh2(mode, i1, i2, j1, j2);
	}

	public void glEvalPoint1(int i) {
		gl.glEvalPoint1(i);
	}

	public void glEvalPoint2(int i, int j) {
		gl.glEvalPoint2(i, j);
	}

	public void glExtractComponentEXT(int res, int src, int num) {
		gl.glExtractComponentEXT(res, src, num);
	}

	public void glFeedbackBuffer(int size, int type, FloatBuffer buffer) {
		gl.glFeedbackBuffer(size, type, buffer);
	}

	public void glFinishTextureSUNX() {
		gl.glFinishTextureSUNX();
	}

	public void glFlushMappedNamedBufferRangeEXT(int buffer, long offset, long length) {
		gl.glFlushMappedNamedBufferRangeEXT(buffer, offset, length);
	}

	public void glFlushPixelDataRangeNV(int target) {
		gl.glFlushPixelDataRangeNV(target);
	}

	public void glFlushVertexArrayRangeAPPLE(int length, Buffer pointer) {
		gl.glFlushVertexArrayRangeAPPLE(length, pointer);
	}

	public void glFogCoordPointer(int type, int stride, Buffer pointer) {
		gl.glFogCoordPointer(type, stride, pointer);
	}

	public void glFogCoordPointer(int type, int stride, long pointer_buffer_offset) {
		gl.glFogCoordPointer(type, stride, pointer_buffer_offset);
	}

	public void glFogCoordd(double coord) {
		gl.glFogCoordd(coord);
	}

	public void glFogCoorddv(DoubleBuffer coord) {
		gl.glFogCoorddv(coord);
	}

	public void glFogCoorddv(double[] coord, int coord_offset) {
		gl.glFogCoorddv(coord, coord_offset);
	}

	public void glFogCoordf(float coord) {
		gl.glFogCoordf(coord);
	}

	public void glFogCoordfv(FloatBuffer coord) {
		gl.glFogCoordfv(coord);
	}

	public void glFogCoordfv(float[] coord, int coord_offset) {
		gl.glFogCoordfv(coord, coord_offset);
	}

	public void glFogCoordh(short fog) {
		gl.glFogCoordh(fog);
	}

	public void glFogCoordhv(ShortBuffer fog) {
		gl.glFogCoordhv(fog);
	}

	public void glFogCoordhv(short[] fog, int fog_offset) {
		gl.glFogCoordhv(fog, fog_offset);
	}

	public void glFogi(int pname, int param) {
		gl.glFogi(pname, param);
	}

	public void glFogiv(int pname, IntBuffer params) {
		gl.glFogiv(pname, params);
	}

	public void glFogiv(int pname, int[] params, int params_offset) {
		gl.glFogiv(pname, params, params_offset);
	}

	public void glFragmentCoverageColorNV(int color) {
		gl.glFragmentCoverageColorNV(color);
	}

	public void glFrameTerminatorGREMEDY() {
		gl.glFrameTerminatorGREMEDY();
	}

	public void glFramebufferDrawBufferEXT(int framebuffer, int mode) {
		gl.glFramebufferDrawBufferEXT(framebuffer, mode);
	}

	public void glFramebufferDrawBuffersEXT(int framebuffer, int n, IntBuffer bufs) {
		gl.glFramebufferDrawBuffersEXT(framebuffer, n, bufs);
	}

	public void glFramebufferDrawBuffersEXT(int framebuffer, int n, int[] bufs, int bufs_offset) {
		gl.glFramebufferDrawBuffersEXT(framebuffer, n, bufs, bufs_offset);
	}

	public void glFramebufferReadBufferEXT(int framebuffer, int mode) {
		gl.glFramebufferReadBufferEXT(framebuffer, mode);
	}

	public void glFramebufferSampleLocationsfvNV(int target, int start, int count, FloatBuffer v) {
		gl.glFramebufferSampleLocationsfvNV(target, start, count, v);
	}

	public void glFramebufferSampleLocationsfvNV(int target, int start, int count, float[] v, int v_offset) {
		gl.glFramebufferSampleLocationsfvNV(target, start, count, v, v_offset);
	}

	public void glFramebufferTextureFaceEXT(int target, int attachment, int texture, int level, int face) {
		gl.glFramebufferTextureFaceEXT(target, attachment, texture, level, face);
	}

	public int glGenLists(int range) {
		return gl.glGenLists(range);
	}

	public void glGenNamesAMD(int identifier, int num, IntBuffer names) {
		gl.glGenNamesAMD(identifier, num, names);
	}

	public void glGenNamesAMD(int identifier, int num, int[] names, int names_offset) {
		gl.glGenNamesAMD(identifier, num, names, names_offset);
	}

	public void glGenOcclusionQueriesNV(int n, IntBuffer ids) {
		gl.glGenOcclusionQueriesNV(n, ids);
	}

	public void glGenOcclusionQueriesNV(int n, int[] ids, int ids_offset) {
		gl.glGenOcclusionQueriesNV(n, ids, ids_offset);
	}

	public void glGenPerfMonitorsAMD(int n, IntBuffer monitors) {
		gl.glGenPerfMonitorsAMD(n, monitors);
	}

	public void glGenPerfMonitorsAMD(int n, int[] monitors, int monitors_offset) {
		gl.glGenPerfMonitorsAMD(n, monitors, monitors_offset);
	}

	public void glGenProgramsARB(int n, IntBuffer programs) {
		gl.glGenProgramsARB(n, programs);
	}

	public void glGenProgramsARB(int n, int[] programs, int programs_offset) {
		gl.glGenProgramsARB(n, programs, programs_offset);
	}

	public int glGenSymbolsEXT(int datatype, int storagetype, int range, int components) {
		return gl.glGenSymbolsEXT(datatype, storagetype, range, components);
	}

	public void glGenTransformFeedbacksNV(int n, IntBuffer ids) {
		gl.glGenTransformFeedbacksNV(n, ids);
	}

	public void glGenTransformFeedbacksNV(int n, int[] ids, int ids_offset) {
		gl.glGenTransformFeedbacksNV(n, ids, ids_offset);
	}

	public int glGenVertexShadersEXT(int range) {
		return gl.glGenVertexShadersEXT(range);
	}

	public void glGenerateMultiTexMipmapEXT(int texunit, int target) {
		gl.glGenerateMultiTexMipmapEXT(texunit, target);
	}

	public void glGenerateTextureMipmapEXT(int texture, int target) {
		gl.glGenerateTextureMipmapEXT(texture, target);
	}

	public void glGetActiveUniformARB(long programObj, int index, int maxLength, IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
		gl.glGetActiveUniformARB(programObj, index, maxLength, length, size, type, name);
	}

	public void glGetActiveUniformARB(long programObj, int index, int maxLength, int[] length, int length_offset, int[] size, int size_offset, int[] type, int type_offset, byte[] name, int name_offset) {
		gl.glGetActiveUniformARB(programObj, index, maxLength, length, length_offset, size, size_offset, type, type_offset, name, name_offset);
	}

	public void glGetAttachedObjectsARB(long containerObj, int maxCount, IntBuffer count, LongBuffer obj) {
		gl.glGetAttachedObjectsARB(containerObj, maxCount, count, obj);
	}

	public void glGetAttachedObjectsARB(long containerObj, int maxCount, int[] count, int count_offset, long[] obj, int obj_offset) {
		gl.glGetAttachedObjectsARB(containerObj, maxCount, count, count_offset, obj, obj_offset);
	}

	public void glGetBooleanIndexedv(int target, int index, ByteBuffer data) {
		gl.glGetBooleanIndexedv(target, index, data);
	}

	public void glGetBooleanIndexedv(int target, int index, byte[] data, int data_offset) {
		gl.glGetBooleanIndexedv(target, index, data, data_offset);
	}

	public void glGetClipPlane(int plane, DoubleBuffer equation) {
		gl.glGetClipPlane(plane, equation);
	}

	public void glGetClipPlane(int plane, double[] equation, int equation_offset) {
		gl.glGetClipPlane(plane, equation, equation_offset);
	}

	public void glGetClipPlanef(int plane, FloatBuffer equation) {
		gl.glGetClipPlanef(plane, equation);
	}

	public void glGetClipPlanef(int plane, float[] equation, int equation_offset) {
		gl.glGetClipPlanef(plane, equation, equation_offset);
	}

	public void glGetColorTable(int target, int format, int type, Buffer table) {
		gl.glGetColorTable(target, format, type, table);
	}

	public void glGetColorTable(int target, int format, int type, long table_buffer_offset) {
		gl.glGetColorTable(target, format, type, table_buffer_offset);
	}

	public void glGetColorTableParameterfv(int target, int pname, FloatBuffer params) {
		gl.glGetColorTableParameterfv(target, pname, params);
	}

	public void glGetColorTableParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glGetColorTableParameterfv(target, pname, params, params_offset);
	}

	public void glGetColorTableParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetColorTableParameteriv(target, pname, params);
	}

	public void glGetColorTableParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetColorTableParameteriv(target, pname, params, params_offset);
	}

	public void glGetCompressedMultiTexImageEXT(int texunit, int target, int lod, Buffer img) {
		gl.glGetCompressedMultiTexImageEXT(texunit, target, lod, img);
	}

	public void glGetCompressedTextureImageEXT(int texture, int target, int lod, Buffer img) {
		gl.glGetCompressedTextureImageEXT(texture, target, lod, img);
	}

	public void glGetConvolutionFilter(int target, int format, int type, Buffer image) {
		gl.glGetConvolutionFilter(target, format, type, image);
	}

	public void glGetConvolutionFilter(int target, int format, int type, long image_buffer_offset) {
		gl.glGetConvolutionFilter(target, format, type, image_buffer_offset);
	}

	public void glGetConvolutionParameterfv(int target, int pname, FloatBuffer params) {
		gl.glGetConvolutionParameterfv(target, pname, params);
	}

	public void glGetConvolutionParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glGetConvolutionParameterfv(target, pname, params, params_offset);
	}

	public void glGetConvolutionParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetConvolutionParameteriv(target, pname, params);
	}

	public void glGetConvolutionParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetConvolutionParameteriv(target, pname, params, params_offset);
	}

	public void glGetCoverageModulationTableNV(int bufsize, FloatBuffer v) {
		gl.glGetCoverageModulationTableNV(bufsize, v);
	}

	public void glGetCoverageModulationTableNV(int bufsize, float[] v, int v_offset) {
		gl.glGetCoverageModulationTableNV(bufsize, v, v_offset);
	}

	public void glGetDoubleIndexedvEXT(int target, int index, DoubleBuffer data) {
		gl.glGetDoubleIndexedvEXT(target, index, data);
	}

	public void glGetDoubleIndexedvEXT(int target, int index, double[] data, int data_offset) {
		gl.glGetDoubleIndexedvEXT(target, index, data, data_offset);
	}

	public void glGetDoublei_vEXT(int pname, int index, DoubleBuffer params) {
		gl.glGetDoublei_vEXT(pname, index, params);
	}

	public void glGetDoublei_vEXT(int pname, int index, double[] params, int params_offset) {
		gl.glGetDoublei_vEXT(pname, index, params, params_offset);
	}

	public void glGetFirstPerfQueryIdINTEL(IntBuffer queryId) {
		gl.glGetFirstPerfQueryIdINTEL(queryId);
	}

	public void glGetFirstPerfQueryIdINTEL(int[] queryId, int queryId_offset) {
		gl.glGetFirstPerfQueryIdINTEL(queryId, queryId_offset);
	}

	public void glGetFloatIndexedvEXT(int target, int index, FloatBuffer data) {
		gl.glGetFloatIndexedvEXT(target, index, data);
	}

	public void glGetFloatIndexedvEXT(int target, int index, float[] data, int data_offset) {
		gl.glGetFloatIndexedvEXT(target, index, data, data_offset);
	}

	public void glGetFloati_vEXT(int pname, int index, FloatBuffer params) {
		gl.glGetFloati_vEXT(pname, index, params);
	}

	public void glGetFloati_vEXT(int pname, int index, float[] params, int params_offset) {
		gl.glGetFloati_vEXT(pname, index, params, params_offset);
	}

	public void glGetFramebufferParameterivEXT(int framebuffer, int pname, IntBuffer params) {
		gl.glGetFramebufferParameterivEXT(framebuffer, pname, params);
	}

	public void glGetFramebufferParameterivEXT(int framebuffer, int pname, int[] params, int params_offset) {
		gl.glGetFramebufferParameterivEXT(framebuffer, pname, params, params_offset);
	}

	public long glGetHandleARB(int pname) {
		return gl.glGetHandleARB(pname);
	}

	public void glGetHistogram(int target, boolean reset, int format, int type, Buffer values) {
		gl.glGetHistogram(target, reset, format, type, values);
	}

	public void glGetHistogram(int target, boolean reset, int format, int type, long values_buffer_offset) {
		gl.glGetHistogram(target, reset, format, type, values_buffer_offset);
	}

	public void glGetHistogramParameterfv(int target, int pname, FloatBuffer params) {
		gl.glGetHistogramParameterfv(target, pname, params);
	}

	public void glGetHistogramParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glGetHistogramParameterfv(target, pname, params, params_offset);
	}

	public void glGetHistogramParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetHistogramParameteriv(target, pname, params);
	}

	public void glGetHistogramParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetHistogramParameteriv(target, pname, params, params_offset);
	}

	public void glGetInfoLogARB(long obj, int maxLength, IntBuffer length, ByteBuffer infoLog) {
		gl.glGetInfoLogARB(obj, maxLength, length, infoLog);
	}

	public void glGetInfoLogARB(long obj, int maxLength, int[] length, int length_offset, byte[] infoLog, int infoLog_offset) {
		gl.glGetInfoLogARB(obj, maxLength, length, length_offset, infoLog, infoLog_offset);
	}

	public void glGetIntegerIndexedv(int target, int index, IntBuffer data) {
		gl.glGetIntegerIndexedv(target, index, data);
	}

	public void glGetIntegerIndexedv(int target, int index, int[] data, int data_offset) {
		gl.glGetIntegerIndexedv(target, index, data, data_offset);
	}

	public void glGetInvariantBooleanvEXT(int id, int value, ByteBuffer data) {
		gl.glGetInvariantBooleanvEXT(id, value, data);
	}

	public void glGetInvariantBooleanvEXT(int id, int value, byte[] data, int data_offset) {
		gl.glGetInvariantBooleanvEXT(id, value, data, data_offset);
	}

	public void glGetInvariantFloatvEXT(int id, int value, FloatBuffer data) {
		gl.glGetInvariantFloatvEXT(id, value, data);
	}

	public void glGetInvariantFloatvEXT(int id, int value, float[] data, int data_offset) {
		gl.glGetInvariantFloatvEXT(id, value, data, data_offset);
	}

	public void glGetInvariantIntegervEXT(int id, int value, IntBuffer data) {
		gl.glGetInvariantIntegervEXT(id, value, data);
	}

	public void glGetInvariantIntegervEXT(int id, int value, int[] data, int data_offset) {
		gl.glGetInvariantIntegervEXT(id, value, data, data_offset);
	}

	public void glGetLightiv(int light, int pname, IntBuffer params) {
		gl.glGetLightiv(light, pname, params);
	}

	public void glGetLightiv(int light, int pname, int[] params, int params_offset) {
		gl.glGetLightiv(light, pname, params, params_offset);
	}

	public void glGetLocalConstantBooleanvEXT(int id, int value, ByteBuffer data) {
		gl.glGetLocalConstantBooleanvEXT(id, value, data);
	}

	public void glGetLocalConstantBooleanvEXT(int id, int value, byte[] data, int data_offset) {
		gl.glGetLocalConstantBooleanvEXT(id, value, data, data_offset);
	}

	public void glGetLocalConstantFloatvEXT(int id, int value, FloatBuffer data) {
		gl.glGetLocalConstantFloatvEXT(id, value, data);
	}

	public void glGetLocalConstantFloatvEXT(int id, int value, float[] data, int data_offset) {
		gl.glGetLocalConstantFloatvEXT(id, value, data, data_offset);
	}

	public void glGetLocalConstantIntegervEXT(int id, int value, IntBuffer data) {
		gl.glGetLocalConstantIntegervEXT(id, value, data);
	}

	public void glGetLocalConstantIntegervEXT(int id, int value, int[] data, int data_offset) {
		gl.glGetLocalConstantIntegervEXT(id, value, data, data_offset);
	}

	public void glGetMapAttribParameterfvNV(int target, int index, int pname, FloatBuffer params) {
		gl.glGetMapAttribParameterfvNV(target, index, pname, params);
	}

	public void glGetMapAttribParameterfvNV(int target, int index, int pname, float[] params, int params_offset) {
		gl.glGetMapAttribParameterfvNV(target, index, pname, params, params_offset);
	}

	public void glGetMapAttribParameterivNV(int target, int index, int pname, IntBuffer params) {
		gl.glGetMapAttribParameterivNV(target, index, pname, params);
	}

	public void glGetMapAttribParameterivNV(int target, int index, int pname, int[] params, int params_offset) {
		gl.glGetMapAttribParameterivNV(target, index, pname, params, params_offset);
	}

	public void glGetMapControlPointsNV(int target, int index, int type, int ustride, int vstride, boolean packed, Buffer points) {
		gl.glGetMapControlPointsNV(target, index, type, ustride, vstride, packed, points);
	}

	public void glGetMapParameterfvNV(int target, int pname, FloatBuffer params) {
		gl.glGetMapParameterfvNV(target, pname, params);
	}

	public void glGetMapParameterfvNV(int target, int pname, float[] params, int params_offset) {
		gl.glGetMapParameterfvNV(target, pname, params, params_offset);
	}

	public void glGetMapParameterivNV(int target, int pname, IntBuffer params) {
		gl.glGetMapParameterivNV(target, pname, params);
	}

	public void glGetMapParameterivNV(int target, int pname, int[] params, int params_offset) {
		gl.glGetMapParameterivNV(target, pname, params, params_offset);
	}

	public void glGetMapdv(int target, int query, DoubleBuffer v) {
		gl.glGetMapdv(target, query, v);
	}

	public void glGetMapdv(int target, int query, double[] v, int v_offset) {
		gl.glGetMapdv(target, query, v, v_offset);
	}

	public void glGetMapfv(int target, int query, FloatBuffer v) {
		gl.glGetMapfv(target, query, v);
	}

	public void glGetMapfv(int target, int query, float[] v, int v_offset) {
		gl.glGetMapfv(target, query, v, v_offset);
	}

	public void glGetMapiv(int target, int query, IntBuffer v) {
		gl.glGetMapiv(target, query, v);
	}

	public void glGetMapiv(int target, int query, int[] v, int v_offset) {
		gl.glGetMapiv(target, query, v, v_offset);
	}

	public void glGetMaterialiv(int face, int pname, IntBuffer params) {
		gl.glGetMaterialiv(face, pname, params);
	}

	public void glGetMaterialiv(int face, int pname, int[] params, int params_offset) {
		gl.glGetMaterialiv(face, pname, params, params_offset);
	}

	public void glGetMinmax(int target, boolean reset, int format, int type, Buffer values) {
		gl.glGetMinmax(target, reset, format, type, values);
	}

	public void glGetMinmax(int target, boolean reset, int format, int type, long values_buffer_offset) {
		gl.glGetMinmax(target, reset, format, type, values_buffer_offset);
	}

	public void glGetMinmaxParameterfv(int target, int pname, FloatBuffer params) {
		gl.glGetMinmaxParameterfv(target, pname, params);
	}

	public void glGetMinmaxParameterfv(int target, int pname, float[] params, int params_offset) {
		gl.glGetMinmaxParameterfv(target, pname, params, params_offset);
	}

	public void glGetMinmaxParameteriv(int target, int pname, IntBuffer params) {
		gl.glGetMinmaxParameteriv(target, pname, params);
	}

	public void glGetMinmaxParameteriv(int target, int pname, int[] params, int params_offset) {
		gl.glGetMinmaxParameteriv(target, pname, params, params_offset);
	}

	public void glGetMultiTexEnvfvEXT(int texunit, int target, int pname, FloatBuffer params) {
		gl.glGetMultiTexEnvfvEXT(texunit, target, pname, params);
	}

	public void glGetMultiTexEnvfvEXT(int texunit, int target, int pname, float[] params, int params_offset) {
		gl.glGetMultiTexEnvfvEXT(texunit, target, pname, params, params_offset);
	}

	public void glGetMultiTexEnvivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glGetMultiTexEnvivEXT(texunit, target, pname, params);
	}

	public void glGetMultiTexEnvivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glGetMultiTexEnvivEXT(texunit, target, pname, params, params_offset);
	}

	public void glGetMultiTexGendvEXT(int texunit, int coord, int pname, DoubleBuffer params) {
		gl.glGetMultiTexGendvEXT(texunit, coord, pname, params);
	}

	public void glGetMultiTexGendvEXT(int texunit, int coord, int pname, double[] params, int params_offset) {
		gl.glGetMultiTexGendvEXT(texunit, coord, pname, params, params_offset);
	}

	public void glGetMultiTexGenfvEXT(int texunit, int coord, int pname, FloatBuffer params) {
		gl.glGetMultiTexGenfvEXT(texunit, coord, pname, params);
	}

	public void glGetMultiTexGenfvEXT(int texunit, int coord, int pname, float[] params, int params_offset) {
		gl.glGetMultiTexGenfvEXT(texunit, coord, pname, params, params_offset);
	}

	public void glGetMultiTexGenivEXT(int texunit, int coord, int pname, IntBuffer params) {
		gl.glGetMultiTexGenivEXT(texunit, coord, pname, params);
	}

	public void glGetMultiTexGenivEXT(int texunit, int coord, int pname, int[] params, int params_offset) {
		gl.glGetMultiTexGenivEXT(texunit, coord, pname, params, params_offset);
	}

	public void glGetMultiTexImageEXT(int texunit, int target, int level, int format, int type, Buffer pixels) {
		gl.glGetMultiTexImageEXT(texunit, target, level, format, type, pixels);
	}

	public void glGetMultiTexLevelParameterfvEXT(int texunit, int target, int level, int pname, FloatBuffer params) {
		gl.glGetMultiTexLevelParameterfvEXT(texunit, target, level, pname, params);
	}

	public void glGetMultiTexLevelParameterfvEXT(int texunit, int target, int level, int pname, float[] params, int params_offset) {
		gl.glGetMultiTexLevelParameterfvEXT(texunit, target, level, pname, params, params_offset);
	}

	public void glGetMultiTexLevelParameterivEXT(int texunit, int target, int level, int pname, IntBuffer params) {
		gl.glGetMultiTexLevelParameterivEXT(texunit, target, level, pname, params);
	}

	public void glGetMultiTexLevelParameterivEXT(int texunit, int target, int level, int pname, int[] params, int params_offset) {
		gl.glGetMultiTexLevelParameterivEXT(texunit, target, level, pname, params, params_offset);
	}

	public void glGetMultiTexParameterIivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glGetMultiTexParameterIivEXT(texunit, target, pname, params);
	}

	public void glGetMultiTexParameterIivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glGetMultiTexParameterIivEXT(texunit, target, pname, params, params_offset);
	}

	public void glGetMultiTexParameterIuivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glGetMultiTexParameterIuivEXT(texunit, target, pname, params);
	}

	public void glGetMultiTexParameterIuivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glGetMultiTexParameterIuivEXT(texunit, target, pname, params, params_offset);
	}

	public void glGetMultiTexParameterfvEXT(int texunit, int target, int pname, FloatBuffer params) {
		gl.glGetMultiTexParameterfvEXT(texunit, target, pname, params);
	}

	public void glGetMultiTexParameterfvEXT(int texunit, int target, int pname, float[] params, int params_offset) {
		gl.glGetMultiTexParameterfvEXT(texunit, target, pname, params, params_offset);
	}

	public void glGetMultiTexParameterivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glGetMultiTexParameterivEXT(texunit, target, pname, params);
	}

	public void glGetMultiTexParameterivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glGetMultiTexParameterivEXT(texunit, target, pname, params, params_offset);
	}

	public void glGetMultisamplefvNV(int pname, int index, FloatBuffer val) {
		gl.glGetMultisamplefvNV(pname, index, val);
	}

	public void glGetMultisamplefvNV(int pname, int index, float[] val, int val_offset) {
		gl.glGetMultisamplefvNV(pname, index, val, val_offset);
	}

	public void glGetNamedBufferParameterivEXT(int buffer, int pname, IntBuffer params) {
		gl.glGetNamedBufferParameterivEXT(buffer, pname, params);
	}

	public void glGetNamedBufferParameterivEXT(int buffer, int pname, int[] params, int params_offset) {
		gl.glGetNamedBufferParameterivEXT(buffer, pname, params, params_offset);
	}

	public void glGetNamedBufferSubDataEXT(int buffer, long offset, long size, Buffer data) {
		gl.glGetNamedBufferSubDataEXT(buffer, offset, size, data);
	}

	public void glGetNamedFramebufferAttachmentParameterivEXT(int framebuffer, int attachment, int pname, IntBuffer params) {
		gl.glGetNamedFramebufferAttachmentParameterivEXT(framebuffer, attachment, pname, params);
	}

	public void glGetNamedFramebufferAttachmentParameterivEXT(int framebuffer, int attachment, int pname, int[] params, int params_offset) {
		gl.glGetNamedFramebufferAttachmentParameterivEXT(framebuffer, attachment, pname, params, params_offset);
	}

	public void glGetNamedFramebufferParameteriv(int framebuffer, int pname, IntBuffer param) {
		gl.glGetNamedFramebufferParameteriv(framebuffer, pname, param);
	}

	public void glGetNamedFramebufferParameteriv(int framebuffer, int pname, int[] param, int param_offset) {
		gl.glGetNamedFramebufferParameteriv(framebuffer, pname, param, param_offset);
	}

	public void glGetNamedProgramLocalParameterIivEXT(int program, int target, int index, IntBuffer params) {
		gl.glGetNamedProgramLocalParameterIivEXT(program, target, index, params);
	}

	public void glGetNamedProgramLocalParameterIivEXT(int program, int target, int index, int[] params, int params_offset) {
		gl.glGetNamedProgramLocalParameterIivEXT(program, target, index, params, params_offset);
	}

	public void glGetNamedProgramLocalParameterIuivEXT(int program, int target, int index, IntBuffer params) {
		gl.glGetNamedProgramLocalParameterIuivEXT(program, target, index, params);
	}

	public void glGetNamedProgramLocalParameterIuivEXT(int program, int target, int index, int[] params, int params_offset) {
		gl.glGetNamedProgramLocalParameterIuivEXT(program, target, index, params, params_offset);
	}

	public void glGetNamedProgramLocalParameterdvEXT(int program, int target, int index, DoubleBuffer params) {
		gl.glGetNamedProgramLocalParameterdvEXT(program, target, index, params);
	}

	public void glGetNamedProgramLocalParameterdvEXT(int program, int target, int index, double[] params, int params_offset) {
		gl.glGetNamedProgramLocalParameterdvEXT(program, target, index, params, params_offset);
	}

	public void glGetNamedProgramLocalParameterfvEXT(int program, int target, int index, FloatBuffer params) {
		gl.glGetNamedProgramLocalParameterfvEXT(program, target, index, params);
	}

	public void glGetNamedProgramLocalParameterfvEXT(int program, int target, int index, float[] params, int params_offset) {
		gl.glGetNamedProgramLocalParameterfvEXT(program, target, index, params, params_offset);
	}

	public void glGetNamedProgramStringEXT(int program, int target, int pname, Buffer string) {
		gl.glGetNamedProgramStringEXT(program, target, pname, string);
	}

	public void glGetNamedProgramivEXT(int program, int target, int pname, IntBuffer params) {
		gl.glGetNamedProgramivEXT(program, target, pname, params);
	}

	public void glGetNamedProgramivEXT(int program, int target, int pname, int[] params, int params_offset) {
		gl.glGetNamedProgramivEXT(program, target, pname, params, params_offset);
	}

	public void glGetNamedRenderbufferParameterivEXT(int renderbuffer, int pname, IntBuffer params) {
		gl.glGetNamedRenderbufferParameterivEXT(renderbuffer, pname, params);
	}

	public void glGetNamedRenderbufferParameterivEXT(int renderbuffer, int pname, int[] params, int params_offset) {
		gl.glGetNamedRenderbufferParameterivEXT(renderbuffer, pname, params, params_offset);
	}

	public void glGetNextPerfQueryIdINTEL(int queryId, IntBuffer nextQueryId) {
		gl.glGetNextPerfQueryIdINTEL(queryId, nextQueryId);
	}

	public void glGetNextPerfQueryIdINTEL(int queryId, int[] nextQueryId, int nextQueryId_offset) {
		gl.glGetNextPerfQueryIdINTEL(queryId, nextQueryId, nextQueryId_offset);
	}

	public void glGetObjectParameterfvARB(long obj, int pname, FloatBuffer params) {
		gl.glGetObjectParameterfvARB(obj, pname, params);
	}

	public void glGetObjectParameterfvARB(long obj, int pname, float[] params, int params_offset) {
		gl.glGetObjectParameterfvARB(obj, pname, params, params_offset);
	}

	public void glGetObjectParameterivAPPLE(int objectType, int name, int pname, IntBuffer params) {
		gl.glGetObjectParameterivAPPLE(objectType, name, pname, params);
	}

	public void glGetObjectParameterivAPPLE(int objectType, int name, int pname, int[] params, int params_offset) {
		gl.glGetObjectParameterivAPPLE(objectType, name, pname, params, params_offset);
	}

	public void glGetObjectParameterivARB(long obj, int pname, IntBuffer params) {
		gl.glGetObjectParameterivARB(obj, pname, params);
	}

	public void glGetObjectParameterivARB(long obj, int pname, int[] params, int params_offset) {
		gl.glGetObjectParameterivARB(obj, pname, params, params_offset);
	}

	public void glGetOcclusionQueryivNV(int id, int pname, IntBuffer params) {
		gl.glGetOcclusionQueryivNV(id, pname, params);
	}

	public void glGetOcclusionQueryivNV(int id, int pname, int[] params, int params_offset) {
		gl.glGetOcclusionQueryivNV(id, pname, params, params_offset);
	}

	public void glGetOcclusionQueryuivNV(int id, int pname, IntBuffer params) {
		gl.glGetOcclusionQueryuivNV(id, pname, params);
	}

	public void glGetOcclusionQueryuivNV(int id, int pname, int[] params, int params_offset) {
		gl.glGetOcclusionQueryuivNV(id, pname, params, params_offset);
	}

	public void glGetPerfCounterInfoINTEL(int queryId, int counterId, int counterNameLength, ByteBuffer counterName, int counterDescLength, ByteBuffer counterDesc, IntBuffer counterOffset, IntBuffer counterDataSize, IntBuffer counterTypeEnum, IntBuffer counterDataTypeEnum, LongBuffer rawCounterMaxValue) {
		gl.glGetPerfCounterInfoINTEL(queryId, counterId, counterNameLength, counterName, counterDescLength, counterDesc, counterOffset, counterDataSize, counterTypeEnum, counterDataTypeEnum, rawCounterMaxValue);
	}

	public void glGetPerfCounterInfoINTEL(int queryId, int counterId, int counterNameLength, byte[] counterName, int counterName_offset, int counterDescLength, byte[] counterDesc, int counterDesc_offset, int[] counterOffset, int counterOffset_offset, int[] counterDataSize, int counterDataSize_offset, int[] counterTypeEnum, int counterTypeEnum_offset, int[] counterDataTypeEnum, int counterDataTypeEnum_offset, long[] rawCounterMaxValue, int rawCounterMaxValue_offset) {
		gl.glGetPerfCounterInfoINTEL(queryId, counterId, counterNameLength, counterName, counterName_offset, counterDescLength, counterDesc, counterDesc_offset, counterOffset, counterOffset_offset, counterDataSize, counterDataSize_offset, counterTypeEnum, counterTypeEnum_offset, counterDataTypeEnum, counterDataTypeEnum_offset, rawCounterMaxValue, rawCounterMaxValue_offset);
	}

	public void glGetPerfMonitorCounterDataAMD(int monitor, int pname, int dataSize, IntBuffer data, IntBuffer bytesWritten) {
		gl.glGetPerfMonitorCounterDataAMD(monitor, pname, dataSize, data, bytesWritten);
	}

	public void glGetPerfMonitorCounterDataAMD(int monitor, int pname, int dataSize, int[] data, int data_offset, int[] bytesWritten, int bytesWritten_offset) {
		gl.glGetPerfMonitorCounterDataAMD(monitor, pname, dataSize, data, data_offset, bytesWritten, bytesWritten_offset);
	}

	public void glGetPerfMonitorCounterInfoAMD(int group, int counter, int pname, Buffer data) {
		gl.glGetPerfMonitorCounterInfoAMD(group, counter, pname, data);
	}

	public void glGetPerfMonitorCounterStringAMD(int group, int counter, int bufSize, IntBuffer length, ByteBuffer counterString) {
		gl.glGetPerfMonitorCounterStringAMD(group, counter, bufSize, length, counterString);
	}

	public void glGetPerfMonitorCounterStringAMD(int group, int counter, int bufSize, int[] length, int length_offset, byte[] counterString, int counterString_offset) {
		gl.glGetPerfMonitorCounterStringAMD(group, counter, bufSize, length, length_offset, counterString, counterString_offset);
	}

	public void glGetPerfMonitorCountersAMD(int group, IntBuffer numCounters, IntBuffer maxActiveCounters, int counterSize, IntBuffer counters) {
		gl.glGetPerfMonitorCountersAMD(group, numCounters, maxActiveCounters, counterSize, counters);
	}

	public void glGetPerfMonitorCountersAMD(int group, int[] numCounters, int numCounters_offset, int[] maxActiveCounters, int maxActiveCounters_offset, int counterSize, int[] counters, int counters_offset) {
		gl.glGetPerfMonitorCountersAMD(group, numCounters, numCounters_offset, maxActiveCounters, maxActiveCounters_offset, counterSize, counters, counters_offset);
	}

	public void glGetPerfMonitorGroupStringAMD(int group, int bufSize, IntBuffer length, ByteBuffer groupString) {
		gl.glGetPerfMonitorGroupStringAMD(group, bufSize, length, groupString);
	}

	public void glGetPerfMonitorGroupStringAMD(int group, int bufSize, int[] length, int length_offset, byte[] groupString, int groupString_offset) {
		gl.glGetPerfMonitorGroupStringAMD(group, bufSize, length, length_offset, groupString, groupString_offset);
	}

	public void glGetPerfMonitorGroupsAMD(IntBuffer numGroups, int groupsSize, IntBuffer groups) {
		gl.glGetPerfMonitorGroupsAMD(numGroups, groupsSize, groups);
	}

	public void glGetPerfMonitorGroupsAMD(int[] numGroups, int numGroups_offset, int groupsSize, int[] groups, int groups_offset) {
		gl.glGetPerfMonitorGroupsAMD(numGroups, numGroups_offset, groupsSize, groups, groups_offset);
	}

	public void glGetPerfQueryDataINTEL(int queryHandle, int flags, int dataSize, Buffer data, IntBuffer bytesWritten) {
		gl.glGetPerfQueryDataINTEL(queryHandle, flags, dataSize, data, bytesWritten);
	}

	public void glGetPerfQueryDataINTEL(int queryHandle, int flags, int dataSize, Buffer data, int[] bytesWritten, int bytesWritten_offset) {
		gl.glGetPerfQueryDataINTEL(queryHandle, flags, dataSize, data, bytesWritten, bytesWritten_offset);
	}

	public void glGetPerfQueryIdByNameINTEL(ByteBuffer queryName, IntBuffer queryId) {
		gl.glGetPerfQueryIdByNameINTEL(queryName, queryId);
	}

	public void glGetPerfQueryIdByNameINTEL(byte[] queryName, int queryName_offset, int[] queryId, int queryId_offset) {
		gl.glGetPerfQueryIdByNameINTEL(queryName, queryName_offset, queryId, queryId_offset);
	}

	public void glGetPerfQueryInfoINTEL(int queryId, int queryNameLength, ByteBuffer queryName, IntBuffer dataSize, IntBuffer noCounters, IntBuffer noInstances, IntBuffer capsMask) {
		gl.glGetPerfQueryInfoINTEL(queryId, queryNameLength, queryName, dataSize, noCounters, noInstances, capsMask);
	}

	public void glGetPerfQueryInfoINTEL(int queryId, int queryNameLength, byte[] queryName, int queryName_offset, int[] dataSize, int dataSize_offset, int[] noCounters, int noCounters_offset, int[] noInstances, int noInstances_offset, int[] capsMask, int capsMask_offset) {
		gl.glGetPerfQueryInfoINTEL(queryId, queryNameLength, queryName, queryName_offset, dataSize, dataSize_offset, noCounters, noCounters_offset, noInstances, noInstances_offset, capsMask, capsMask_offset);
	}

	public void glGetPixelMapfv(int map, FloatBuffer values) {
		gl.glGetPixelMapfv(map, values);
	}

	public void glGetPixelMapfv(int map, float[] values, int values_offset) {
		gl.glGetPixelMapfv(map, values, values_offset);
	}

	public void glGetPixelMapfv(int map, long values_buffer_offset) {
		gl.glGetPixelMapfv(map, values_buffer_offset);
	}

	public void glGetPixelMapuiv(int map, IntBuffer values) {
		gl.glGetPixelMapuiv(map, values);
	}

	public void glGetPixelMapuiv(int map, int[] values, int values_offset) {
		gl.glGetPixelMapuiv(map, values, values_offset);
	}

	public void glGetPixelMapuiv(int map, long values_buffer_offset) {
		gl.glGetPixelMapuiv(map, values_buffer_offset);
	}

	public void glGetPixelMapusv(int map, ShortBuffer values) {
		gl.glGetPixelMapusv(map, values);
	}

	public void glGetPixelMapusv(int map, short[] values, int values_offset) {
		gl.glGetPixelMapusv(map, values, values_offset);
	}

	public void glGetPixelMapusv(int map, long values_buffer_offset) {
		gl.glGetPixelMapusv(map, values_buffer_offset);
	}

	public void glGetPixelTransformParameterfvEXT(int target, int pname, FloatBuffer params) {
		gl.glGetPixelTransformParameterfvEXT(target, pname, params);
	}

	public void glGetPixelTransformParameterfvEXT(int target, int pname, float[] params, int params_offset) {
		gl.glGetPixelTransformParameterfvEXT(target, pname, params, params_offset);
	}

	public void glGetPixelTransformParameterivEXT(int target, int pname, IntBuffer params) {
		gl.glGetPixelTransformParameterivEXT(target, pname, params);
	}

	public void glGetPixelTransformParameterivEXT(int target, int pname, int[] params, int params_offset) {
		gl.glGetPixelTransformParameterivEXT(target, pname, params, params_offset);
	}

	public void glGetPointeri_vEXT(int pname, int index, PointerBuffer params) {
		gl.glGetPointeri_vEXT(pname, index, params);
	}

	public void glGetPolygonStipple(ByteBuffer mask) {
		gl.glGetPolygonStipple(mask);
	}

	public void glGetPolygonStipple(byte[] mask, int mask_offset) {
		gl.glGetPolygonStipple(mask, mask_offset);
	}

	public void glGetPolygonStipple(long mask_buffer_offset) {
		gl.glGetPolygonStipple(mask_buffer_offset);
	}

	public void glGetProgramEnvParameterIivNV(int target, int index, IntBuffer params) {
		gl.glGetProgramEnvParameterIivNV(target, index, params);
	}

	public void glGetProgramEnvParameterIivNV(int target, int index, int[] params, int params_offset) {
		gl.glGetProgramEnvParameterIivNV(target, index, params, params_offset);
	}

	public void glGetProgramEnvParameterIuivNV(int target, int index, IntBuffer params) {
		gl.glGetProgramEnvParameterIuivNV(target, index, params);
	}

	public void glGetProgramEnvParameterIuivNV(int target, int index, int[] params, int params_offset) {
		gl.glGetProgramEnvParameterIuivNV(target, index, params, params_offset);
	}

	public void glGetProgramEnvParameterdvARB(int target, int index, DoubleBuffer params) {
		gl.glGetProgramEnvParameterdvARB(target, index, params);
	}

	public void glGetProgramEnvParameterdvARB(int target, int index, double[] params, int params_offset) {
		gl.glGetProgramEnvParameterdvARB(target, index, params, params_offset);
	}

	public void glGetProgramEnvParameterfvARB(int target, int index, FloatBuffer params) {
		gl.glGetProgramEnvParameterfvARB(target, index, params);
	}

	public void glGetProgramEnvParameterfvARB(int target, int index, float[] params, int params_offset) {
		gl.glGetProgramEnvParameterfvARB(target, index, params, params_offset);
	}

	public void glGetProgramLocalParameterIivNV(int target, int index, IntBuffer params) {
		gl.glGetProgramLocalParameterIivNV(target, index, params);
	}

	public void glGetProgramLocalParameterIivNV(int target, int index, int[] params, int params_offset) {
		gl.glGetProgramLocalParameterIivNV(target, index, params, params_offset);
	}

	public void glGetProgramLocalParameterIuivNV(int target, int index, IntBuffer params) {
		gl.glGetProgramLocalParameterIuivNV(target, index, params);
	}

	public void glGetProgramLocalParameterIuivNV(int target, int index, int[] params, int params_offset) {
		gl.glGetProgramLocalParameterIuivNV(target, index, params, params_offset);
	}

	public void glGetProgramLocalParameterdvARB(int target, int index, DoubleBuffer params) {
		gl.glGetProgramLocalParameterdvARB(target, index, params);
	}

	public void glGetProgramLocalParameterdvARB(int target, int index, double[] params, int params_offset) {
		gl.glGetProgramLocalParameterdvARB(target, index, params, params_offset);
	}

	public void glGetProgramLocalParameterfvARB(int target, int index, FloatBuffer params) {
		gl.glGetProgramLocalParameterfvARB(target, index, params);
	}

	public void glGetProgramLocalParameterfvARB(int target, int index, float[] params, int params_offset) {
		gl.glGetProgramLocalParameterfvARB(target, index, params, params_offset);
	}

	public void glGetProgramStringARB(int target, int pname, Buffer string) {
		gl.glGetProgramStringARB(target, pname, string);
	}

	public void glGetProgramSubroutineParameteruivNV(int target, int index, IntBuffer param) {
		gl.glGetProgramSubroutineParameteruivNV(target, index, param);
	}

	public void glGetProgramSubroutineParameteruivNV(int target, int index, int[] param, int param_offset) {
		gl.glGetProgramSubroutineParameteruivNV(target, index, param, param_offset);
	}

	public void glGetProgramivARB(int target, int pname, IntBuffer params) {
		gl.glGetProgramivARB(target, pname, params);
	}

	public void glGetProgramivARB(int target, int pname, int[] params, int params_offset) {
		gl.glGetProgramivARB(target, pname, params, params_offset);
	}

	public void glGetQueryObjecti64vEXT(int id, int pname, LongBuffer params) {
		gl.glGetQueryObjecti64vEXT(id, pname, params);
	}

	public void glGetQueryObjecti64vEXT(int id, int pname, long[] params, int params_offset) {
		gl.glGetQueryObjecti64vEXT(id, pname, params, params_offset);
	}

	public void glGetQueryObjectui64vEXT(int id, int pname, LongBuffer params) {
		gl.glGetQueryObjectui64vEXT(id, pname, params);
	}

	public void glGetQueryObjectui64vEXT(int id, int pname, long[] params, int params_offset) {
		gl.glGetQueryObjectui64vEXT(id, pname, params, params_offset);
	}

	public void glGetSeparableFilter(int target, int format, int type, Buffer row, Buffer column, Buffer span) {
		gl.glGetSeparableFilter(target, format, type, row, column, span);
	}

	public void glGetSeparableFilter(int target, int format, int type, long row_buffer_offset, long column_buffer_offset, long span_buffer_offset) {
		gl.glGetSeparableFilter(target, format, type, row_buffer_offset, column_buffer_offset, span_buffer_offset);
	}

	public void glGetShaderSourceARB(long obj, int maxLength, IntBuffer length, ByteBuffer source) {
		gl.glGetShaderSourceARB(obj, maxLength, length, source);
	}

	public void glGetShaderSourceARB(long obj, int maxLength, int[] length, int length_offset, byte[] source, int source_offset) {
		gl.glGetShaderSourceARB(obj, maxLength, length, length_offset, source, source_offset);
	}

	public void glGetTexGendv(int coord, int pname, DoubleBuffer params) {
		gl.glGetTexGendv(coord, pname, params);
	}

	public void glGetTexGendv(int coord, int pname, double[] params, int params_offset) {
		gl.glGetTexGendv(coord, pname, params, params_offset);
	}

	public void glGetTexGenfv(int coord, int pname, FloatBuffer params) {
		gl.glGetTexGenfv(coord, pname, params);
	}

	public void glGetTexGenfv(int coord, int pname, float[] params, int params_offset) {
		gl.glGetTexGenfv(coord, pname, params, params_offset);
	}

	public void glGetTexGeniv(int coord, int pname, IntBuffer params) {
		gl.glGetTexGeniv(coord, pname, params);
	}

	public void glGetTexGeniv(int coord, int pname, int[] params, int params_offset) {
		gl.glGetTexGeniv(coord, pname, params, params_offset);
	}

	public void glGetTextureImageEXT(int texture, int target, int level, int format, int type, Buffer pixels) {
		gl.glGetTextureImageEXT(texture, target, level, format, type, pixels);
	}

	public void glGetTextureLevelParameterfvEXT(int texture, int target, int level, int pname, FloatBuffer params) {
		gl.glGetTextureLevelParameterfvEXT(texture, target, level, pname, params);
	}

	public void glGetTextureLevelParameterfvEXT(int texture, int target, int level, int pname, float[] params, int params_offset) {
		gl.glGetTextureLevelParameterfvEXT(texture, target, level, pname, params, params_offset);
	}

	public void glGetTextureLevelParameterivEXT(int texture, int target, int level, int pname, IntBuffer params) {
		gl.glGetTextureLevelParameterivEXT(texture, target, level, pname, params);
	}

	public void glGetTextureLevelParameterivEXT(int texture, int target, int level, int pname, int[] params, int params_offset) {
		gl.glGetTextureLevelParameterivEXT(texture, target, level, pname, params, params_offset);
	}

	public void glGetTextureParameterIivEXT(int texture, int target, int pname, IntBuffer params) {
		gl.glGetTextureParameterIivEXT(texture, target, pname, params);
	}

	public void glGetTextureParameterIivEXT(int texture, int target, int pname, int[] params, int params_offset) {
		gl.glGetTextureParameterIivEXT(texture, target, pname, params, params_offset);
	}

	public void glGetTextureParameterIuivEXT(int texture, int target, int pname, IntBuffer params) {
		gl.glGetTextureParameterIuivEXT(texture, target, pname, params);
	}

	public void glGetTextureParameterIuivEXT(int texture, int target, int pname, int[] params, int params_offset) {
		gl.glGetTextureParameterIuivEXT(texture, target, pname, params, params_offset);
	}

	public void glGetTextureParameterfvEXT(int texture, int target, int pname, FloatBuffer params) {
		gl.glGetTextureParameterfvEXT(texture, target, pname, params);
	}

	public void glGetTextureParameterfvEXT(int texture, int target, int pname, float[] params, int params_offset) {
		gl.glGetTextureParameterfvEXT(texture, target, pname, params, params_offset);
	}

	public void glGetTextureParameterivEXT(int texture, int target, int pname, IntBuffer params) {
		gl.glGetTextureParameterivEXT(texture, target, pname, params);
	}

	public void glGetTextureParameterivEXT(int texture, int target, int pname, int[] params, int params_offset) {
		gl.glGetTextureParameterivEXT(texture, target, pname, params, params_offset);
	}

	public int glGetUniformBufferSizeEXT(int program, int location) {
		return gl.glGetUniformBufferSizeEXT(program, location);
	}

	public int glGetUniformLocationARB(long programObj, String name) {
		return gl.glGetUniformLocationARB(programObj, name);
	}

	public long glGetUniformOffsetEXT(int program, int location) {
		return gl.glGetUniformOffsetEXT(program, location);
	}

	public void glGetUniformfvARB(long programObj, int location, FloatBuffer params) {
		gl.glGetUniformfvARB(programObj, location, params);
	}

	public void glGetUniformfvARB(long programObj, int location, float[] params, int params_offset) {
		gl.glGetUniformfvARB(programObj, location, params, params_offset);
	}

	public void glGetUniformi64vNV(int program, int location, LongBuffer params) {
		gl.glGetUniformi64vNV(program, location, params);
	}

	public void glGetUniformi64vNV(int program, int location, long[] params, int params_offset) {
		gl.glGetUniformi64vNV(program, location, params, params_offset);
	}

	public void glGetUniformivARB(long programObj, int location, IntBuffer params) {
		gl.glGetUniformivARB(programObj, location, params);
	}

	public void glGetUniformivARB(long programObj, int location, int[] params, int params_offset) {
		gl.glGetUniformivARB(programObj, location, params, params_offset);
	}

	public void glGetVariantBooleanvEXT(int id, int value, ByteBuffer data) {
		gl.glGetVariantBooleanvEXT(id, value, data);
	}

	public void glGetVariantBooleanvEXT(int id, int value, byte[] data, int data_offset) {
		gl.glGetVariantBooleanvEXT(id, value, data, data_offset);
	}

	public void glGetVariantFloatvEXT(int id, int value, FloatBuffer data) {
		gl.glGetVariantFloatvEXT(id, value, data);
	}

	public void glGetVariantFloatvEXT(int id, int value, float[] data, int data_offset) {
		gl.glGetVariantFloatvEXT(id, value, data, data_offset);
	}

	public void glGetVariantIntegervEXT(int id, int value, IntBuffer data) {
		gl.glGetVariantIntegervEXT(id, value, data);
	}

	public void glGetVariantIntegervEXT(int id, int value, int[] data, int data_offset) {
		gl.glGetVariantIntegervEXT(id, value, data, data_offset);
	}

	public void glGetVertexArrayIntegeri_vEXT(int vaobj, int index, int pname, IntBuffer param) {
		gl.glGetVertexArrayIntegeri_vEXT(vaobj, index, pname, param);
	}

	public void glGetVertexArrayIntegeri_vEXT(int vaobj, int index, int pname, int[] param, int param_offset) {
		gl.glGetVertexArrayIntegeri_vEXT(vaobj, index, pname, param, param_offset);
	}

	public void glGetVertexArrayIntegervEXT(int vaobj, int pname, IntBuffer param) {
		gl.glGetVertexArrayIntegervEXT(vaobj, pname, param);
	}

	public void glGetVertexArrayIntegervEXT(int vaobj, int pname, int[] param, int param_offset) {
		gl.glGetVertexArrayIntegervEXT(vaobj, pname, param, param_offset);
	}

	public void glGetVertexArrayPointeri_vEXT(int vaobj, int index, int pname, PointerBuffer param) {
		gl.glGetVertexArrayPointeri_vEXT(vaobj, index, pname, param);
	}

	public void glGetVertexArrayPointervEXT(int vaobj, int pname, PointerBuffer param) {
		gl.glGetVertexArrayPointervEXT(vaobj, pname, param);
	}

	public void glGetVertexAttribIivEXT(int index, int pname, IntBuffer params) {
		gl.glGetVertexAttribIivEXT(index, pname, params);
	}

	public void glGetVertexAttribIivEXT(int index, int pname, int[] params, int params_offset) {
		gl.glGetVertexAttribIivEXT(index, pname, params, params_offset);
	}

	public void glGetVertexAttribIuivEXT(int index, int pname, IntBuffer params) {
		gl.glGetVertexAttribIuivEXT(index, pname, params);
	}

	public void glGetVertexAttribIuivEXT(int index, int pname, int[] params, int params_offset) {
		gl.glGetVertexAttribIuivEXT(index, pname, params, params_offset);
	}

	public void glGetVertexAttribLi64vNV(int index, int pname, LongBuffer params) {
		gl.glGetVertexAttribLi64vNV(index, pname, params);
	}

	public void glGetVertexAttribLi64vNV(int index, int pname, long[] params, int params_offset) {
		gl.glGetVertexAttribLi64vNV(index, pname, params, params_offset);
	}

	public void glGetVertexAttribLui64vNV(int index, int pname, LongBuffer params) {
		gl.glGetVertexAttribLui64vNV(index, pname, params);
	}

	public void glGetVertexAttribLui64vNV(int index, int pname, long[] params, int params_offset) {
		gl.glGetVertexAttribLui64vNV(index, pname, params, params_offset);
	}

	public void glGetVertexAttribdvARB(int index, int pname, DoubleBuffer params) {
		gl.glGetVertexAttribdvARB(index, pname, params);
	}

	public void glGetVertexAttribdvARB(int index, int pname, double[] params, int params_offset) {
		gl.glGetVertexAttribdvARB(index, pname, params, params_offset);
	}

	public void glGetVertexAttribfvARB(int index, int pname, FloatBuffer params) {
		gl.glGetVertexAttribfvARB(index, pname, params);
	}

	public void glGetVertexAttribfvARB(int index, int pname, float[] params, int params_offset) {
		gl.glGetVertexAttribfvARB(index, pname, params, params_offset);
	}

	public void glGetVertexAttribivARB(int index, int pname, IntBuffer params) {
		gl.glGetVertexAttribivARB(index, pname, params);
	}

	public void glGetVertexAttribivARB(int index, int pname, int[] params, int params_offset) {
		gl.glGetVertexAttribivARB(index, pname, params, params_offset);
	}

	public void glGetVideoCaptureStreamdvNV(int video_capture_slot, int stream, int pname, DoubleBuffer params) {
		gl.glGetVideoCaptureStreamdvNV(video_capture_slot, stream, pname, params);
	}

	public void glGetVideoCaptureStreamdvNV(int video_capture_slot, int stream, int pname, double[] params, int params_offset) {
		gl.glGetVideoCaptureStreamdvNV(video_capture_slot, stream, pname, params, params_offset);
	}

	public void glGetVideoCaptureStreamfvNV(int video_capture_slot, int stream, int pname, FloatBuffer params) {
		gl.glGetVideoCaptureStreamfvNV(video_capture_slot, stream, pname, params);
	}

	public void glGetVideoCaptureStreamfvNV(int video_capture_slot, int stream, int pname, float[] params, int params_offset) {
		gl.glGetVideoCaptureStreamfvNV(video_capture_slot, stream, pname, params, params_offset);
	}

	public void glGetVideoCaptureStreamivNV(int video_capture_slot, int stream, int pname, IntBuffer params) {
		gl.glGetVideoCaptureStreamivNV(video_capture_slot, stream, pname, params);
	}

	public void glGetVideoCaptureStreamivNV(int video_capture_slot, int stream, int pname, int[] params, int params_offset) {
		gl.glGetVideoCaptureStreamivNV(video_capture_slot, stream, pname, params, params_offset);
	}

	public void glGetVideoCaptureivNV(int video_capture_slot, int pname, IntBuffer params) {
		gl.glGetVideoCaptureivNV(video_capture_slot, pname, params);
	}

	public void glGetVideoCaptureivNV(int video_capture_slot, int pname, int[] params, int params_offset) {
		gl.glGetVideoCaptureivNV(video_capture_slot, pname, params, params_offset);
	}

	public void glGetnColorTable(int target, int format, int type, int bufSize, Buffer table) {
		gl.glGetnColorTable(target, format, type, bufSize, table);
	}

	public void glGetnConvolutionFilter(int target, int format, int type, int bufSize, Buffer image) {
		gl.glGetnConvolutionFilter(target, format, type, bufSize, image);
	}

	public void glGetnHistogram(int target, boolean reset, int format, int type, int bufSize, Buffer values) {
		gl.glGetnHistogram(target, reset, format, type, bufSize, values);
	}

	public void glGetnMapdv(int target, int query, int bufSize, DoubleBuffer v) {
		gl.glGetnMapdv(target, query, bufSize, v);
	}

	public void glGetnMapdv(int target, int query, int bufSize, double[] v, int v_offset) {
		gl.glGetnMapdv(target, query, bufSize, v, v_offset);
	}

	public void glGetnMapfv(int target, int query, int bufSize, FloatBuffer v) {
		gl.glGetnMapfv(target, query, bufSize, v);
	}

	public void glGetnMapfv(int target, int query, int bufSize, float[] v, int v_offset) {
		gl.glGetnMapfv(target, query, bufSize, v, v_offset);
	}

	public void glGetnMapiv(int target, int query, int bufSize, IntBuffer v) {
		gl.glGetnMapiv(target, query, bufSize, v);
	}

	public void glGetnMapiv(int target, int query, int bufSize, int[] v, int v_offset) {
		gl.glGetnMapiv(target, query, bufSize, v, v_offset);
	}

	public void glGetnMinmax(int target, boolean reset, int format, int type, int bufSize, Buffer values) {
		gl.glGetnMinmax(target, reset, format, type, bufSize, values);
	}

	public void glGetnPixelMapfv(int map, int bufSize, FloatBuffer values) {
		gl.glGetnPixelMapfv(map, bufSize, values);
	}

	public void glGetnPixelMapfv(int map, int bufSize, float[] values, int values_offset) {
		gl.glGetnPixelMapfv(map, bufSize, values, values_offset);
	}

	public void glGetnPixelMapuiv(int map, int bufSize, IntBuffer values) {
		gl.glGetnPixelMapuiv(map, bufSize, values);
	}

	public void glGetnPixelMapuiv(int map, int bufSize, int[] values, int values_offset) {
		gl.glGetnPixelMapuiv(map, bufSize, values, values_offset);
	}

	public void glGetnPixelMapusv(int map, int bufSize, ShortBuffer values) {
		gl.glGetnPixelMapusv(map, bufSize, values);
	}

	public void glGetnPixelMapusv(int map, int bufSize, short[] values, int values_offset) {
		gl.glGetnPixelMapusv(map, bufSize, values, values_offset);
	}

	public void glGetnPolygonStipple(int bufSize, ByteBuffer pattern) {
		gl.glGetnPolygonStipple(bufSize, pattern);
	}

	public void glGetnPolygonStipple(int bufSize, byte[] pattern, int pattern_offset) {
		gl.glGetnPolygonStipple(bufSize, pattern, pattern_offset);
	}

	public void glGetnSeparableFilter(int target, int format, int type, int rowBufSize, Buffer row, int columnBufSize, Buffer column, Buffer span) {
		gl.glGetnSeparableFilter(target, format, type, rowBufSize, row, columnBufSize, column, span);
	}

	public void glHintPGI(int target, int mode) {
		gl.glHintPGI(target, mode);
	}

	public void glHistogram(int target, int width, int internalformat, boolean sink) {
		gl.glHistogram(target, width, internalformat, sink);
	}

	public void glIndexFuncEXT(int func, float ref) {
		gl.glIndexFuncEXT(func, ref);
	}

	public void glIndexMask(int mask) {
		gl.glIndexMask(mask);
	}

	public void glIndexMaterialEXT(int face, int mode) {
		gl.glIndexMaterialEXT(face, mode);
	}

	public void glIndexPointer(int type, int stride, Buffer ptr) {
		gl.glIndexPointer(type, stride, ptr);
	}

	public void glIndexd(double c) {
		gl.glIndexd(c);
	}

	public void glIndexdv(DoubleBuffer c) {
		gl.glIndexdv(c);
	}

	public void glIndexdv(double[] c, int c_offset) {
		gl.glIndexdv(c, c_offset);
	}

	public void glIndexf(float c) {
		gl.glIndexf(c);
	}

	public void glIndexfv(FloatBuffer c) {
		gl.glIndexfv(c);
	}

	public void glIndexfv(float[] c, int c_offset) {
		gl.glIndexfv(c, c_offset);
	}

	public void glIndexi(int c) {
		gl.glIndexi(c);
	}

	public void glIndexiv(IntBuffer c) {
		gl.glIndexiv(c);
	}

	public void glIndexiv(int[] c, int c_offset) {
		gl.glIndexiv(c, c_offset);
	}

	public void glIndexs(short c) {
		gl.glIndexs(c);
	}

	public void glIndexsv(ShortBuffer c) {
		gl.glIndexsv(c);
	}

	public void glIndexsv(short[] c, int c_offset) {
		gl.glIndexsv(c, c_offset);
	}

	public void glIndexub(byte c) {
		gl.glIndexub(c);
	}

	public void glIndexubv(ByteBuffer c) {
		gl.glIndexubv(c);
	}

	public void glIndexubv(byte[] c, int c_offset) {
		gl.glIndexubv(c, c_offset);
	}

	public void glInitNames() {
		gl.glInitNames();
	}

	public void glInsertComponentEXT(int res, int src, int num) {
		gl.glInsertComponentEXT(res, src, num);
	}

	public void glInterleavedArrays(int format, int stride, Buffer pointer) {
		gl.glInterleavedArrays(format, stride, pointer);
	}

	public void glInterleavedArrays(int format, int stride, long pointer_buffer_offset) {
		gl.glInterleavedArrays(format, stride, pointer_buffer_offset);
	}

	public boolean glIsEnabledIndexed(int target, int index) {
		return gl.glIsEnabledIndexed(target, index);
	}

	public boolean glIsList(int list) {
		return gl.glIsList(list);
	}

	public boolean glIsNameAMD(int identifier, int name) {
		return gl.glIsNameAMD(identifier, name);
	}

	public boolean glIsOcclusionQueryNV(int id) {
		return gl.glIsOcclusionQueryNV(id);
	}

	public boolean glIsProgramARB(int program) {
		return gl.glIsProgramARB(program);
	}

	public boolean glIsTransformFeedbackNV(int id) {
		return gl.glIsTransformFeedbackNV(id);
	}

	public boolean glIsVariantEnabledEXT(int id, int cap) {
		return gl.glIsVariantEnabledEXT(id, cap);
	}

	public boolean glIsVertexAttribEnabledAPPLE(int index, int pname) {
		return gl.glIsVertexAttribEnabledAPPLE(index, pname);
	}

	public void glLightModeli(int pname, int param) {
		gl.glLightModeli(pname, param);
	}

	public void glLightModeliv(int pname, IntBuffer params) {
		gl.glLightModeliv(pname, params);
	}

	public void glLightModeliv(int pname, int[] params, int params_offset) {
		gl.glLightModeliv(pname, params, params_offset);
	}

	public void glLighti(int light, int pname, int param) {
		gl.glLighti(light, pname, param);
	}

	public void glLightiv(int light, int pname, IntBuffer params) {
		gl.glLightiv(light, pname, params);
	}

	public void glLightiv(int light, int pname, int[] params, int params_offset) {
		gl.glLightiv(light, pname, params, params_offset);
	}

	public void glLineStipple(int factor, short pattern) {
		gl.glLineStipple(factor, pattern);
	}

	public void glLinkProgramARB(long programObj) {
		gl.glLinkProgramARB(programObj);
	}

	public void glListBase(int base) {
		gl.glListBase(base);
	}

	public void glLoadMatrixd(DoubleBuffer m) {
		gl.glLoadMatrixd(m);
	}

	public void glLoadMatrixd(double[] m, int m_offset) {
		gl.glLoadMatrixd(m, m_offset);
	}

	public void glLoadName(int name) {
		gl.glLoadName(name);
	}

	public void glLoadTransposeMatrixd(DoubleBuffer m) {
		gl.glLoadTransposeMatrixd(m);
	}

	public void glLoadTransposeMatrixd(double[] m, int m_offset) {
		gl.glLoadTransposeMatrixd(m, m_offset);
	}

	public void glLoadTransposeMatrixf(FloatBuffer m) {
		gl.glLoadTransposeMatrixf(m);
	}

	public void glLoadTransposeMatrixf(float[] m, int m_offset) {
		gl.glLoadTransposeMatrixf(m, m_offset);
	}

	public void glLockArraysEXT(int first, int count) {
		gl.glLockArraysEXT(first, count);
	}

	public void glMap1d(int target, double u1, double u2, int stride, int order, DoubleBuffer points) {
		gl.glMap1d(target, u1, u2, stride, order, points);
	}

	public void glMap1d(int target, double u1, double u2, int stride, int order, double[] points, int points_offset) {
		gl.glMap1d(target, u1, u2, stride, order, points, points_offset);
	}

	public void glMap1f(int target, float u1, float u2, int stride, int order, FloatBuffer points) {
		gl.glMap1f(target, u1, u2, stride, order, points);
	}

	public void glMap1f(int target, float u1, float u2, int stride, int order, float[] points, int points_offset) {
		gl.glMap1f(target, u1, u2, stride, order, points, points_offset);
	}

	public void glMap2d(int target, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, DoubleBuffer points) {
		gl.glMap2d(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
	}

	public void glMap2d(int target, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, double[] points, int points_offset) {
		gl.glMap2d(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points, points_offset);
	}

	public void glMap2f(int target, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, FloatBuffer points) {
		gl.glMap2f(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
	}

	public void glMap2f(int target, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, float[] points, int points_offset) {
		gl.glMap2f(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points, points_offset);
	}

	public void glMapControlPointsNV(int target, int index, int type, int ustride, int vstride, int uorder, int vorder, boolean packed, Buffer points) {
		gl.glMapControlPointsNV(target, index, type, ustride, vstride, uorder, vorder, packed, points);
	}

	public void glMapGrid1d(int un, double u1, double u2) {
		gl.glMapGrid1d(un, u1, u2);
	}

	public void glMapGrid1f(int un, float u1, float u2) {
		gl.glMapGrid1f(un, u1, u2);
	}

	public void glMapGrid2d(int un, double u1, double u2, int vn, double v1, double v2) {
		gl.glMapGrid2d(un, u1, u2, vn, v1, v2);
	}

	public void glMapGrid2f(int un, float u1, float u2, int vn, float v1, float v2) {
		gl.glMapGrid2f(un, u1, u2, vn, v1, v2);
	}

	public ByteBuffer glMapNamedBufferEXT(int buffer, int access) {
		return gl.glMapNamedBufferEXT(buffer, access);
	}

	public ByteBuffer glMapNamedBufferRangeEXT(int buffer, long offset, long length, int access) {
		return gl.glMapNamedBufferRangeEXT(buffer, offset, length, access);
	}

	public void glMapParameterfvNV(int target, int pname, FloatBuffer params) {
		gl.glMapParameterfvNV(target, pname, params);
	}

	public void glMapParameterfvNV(int target, int pname, float[] params, int params_offset) {
		gl.glMapParameterfvNV(target, pname, params, params_offset);
	}

	public void glMapParameterivNV(int target, int pname, IntBuffer params) {
		gl.glMapParameterivNV(target, pname, params);
	}

	public void glMapParameterivNV(int target, int pname, int[] params, int params_offset) {
		gl.glMapParameterivNV(target, pname, params, params_offset);
	}

	public ByteBuffer glMapTexture2DINTEL(int texture, int level, int access, IntBuffer stride, IntBuffer layout) {
		return gl.glMapTexture2DINTEL(texture, level, access, stride, layout);
	}

	public ByteBuffer glMapTexture2DINTEL(int texture, int level, int access, int[] stride, int stride_offset, int[] layout, int layout_offset) {
		return gl.glMapTexture2DINTEL(texture, level, access, stride, stride_offset, layout, layout_offset);
	}

	public void glMapVertexAttrib1dAPPLE(int index, int size, double u1, double u2, int stride, int order, DoubleBuffer points) {
		gl.glMapVertexAttrib1dAPPLE(index, size, u1, u2, stride, order, points);
	}

	public void glMapVertexAttrib1dAPPLE(int index, int size, double u1, double u2, int stride, int order, double[] points, int points_offset) {
		gl.glMapVertexAttrib1dAPPLE(index, size, u1, u2, stride, order, points, points_offset);
	}

	public void glMapVertexAttrib1fAPPLE(int index, int size, float u1, float u2, int stride, int order, FloatBuffer points) {
		gl.glMapVertexAttrib1fAPPLE(index, size, u1, u2, stride, order, points);
	}

	public void glMapVertexAttrib1fAPPLE(int index, int size, float u1, float u2, int stride, int order, float[] points, int points_offset) {
		gl.glMapVertexAttrib1fAPPLE(index, size, u1, u2, stride, order, points, points_offset);
	}

	public void glMapVertexAttrib2dAPPLE(int index, int size, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, DoubleBuffer points) {
		gl.glMapVertexAttrib2dAPPLE(index, size, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
	}

	public void glMapVertexAttrib2dAPPLE(int index, int size, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, double[] points, int points_offset) {
		gl.glMapVertexAttrib2dAPPLE(index, size, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points, points_offset);
	}

	public void glMapVertexAttrib2fAPPLE(int index, int size, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, FloatBuffer points) {
		gl.glMapVertexAttrib2fAPPLE(index, size, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points);
	}

	public void glMapVertexAttrib2fAPPLE(int index, int size, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, float[] points, int points_offset) {
		gl.glMapVertexAttrib2fAPPLE(index, size, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points, points_offset);
	}

	public void glMateriali(int face, int pname, int param) {
		gl.glMateriali(face, pname, param);
	}

	public void glMaterialiv(int face, int pname, IntBuffer params) {
		gl.glMaterialiv(face, pname, params);
	}

	public void glMaterialiv(int face, int pname, int[] params, int params_offset) {
		gl.glMaterialiv(face, pname, params, params_offset);
	}

	public void glMatrixFrustumEXT(int mode, double left, double right, double bottom, double top, double zNear, double zFar) {
		gl.glMatrixFrustumEXT(mode, left, right, bottom, top, zNear, zFar);
	}

	public void glMatrixIndexPointerARB(int size, int type, int stride, Buffer pointer) {
		gl.glMatrixIndexPointerARB(size, type, stride, pointer);
	}

	public void glMatrixIndexPointerARB(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glMatrixIndexPointerARB(size, type, stride, pointer_buffer_offset);
	}

	public void glMatrixIndexubvARB(int size, ByteBuffer indices) {
		gl.glMatrixIndexubvARB(size, indices);
	}

	public void glMatrixIndexubvARB(int size, byte[] indices, int indices_offset) {
		gl.glMatrixIndexubvARB(size, indices, indices_offset);
	}

	public void glMatrixIndexuivARB(int size, IntBuffer indices) {
		gl.glMatrixIndexuivARB(size, indices);
	}

	public void glMatrixIndexuivARB(int size, int[] indices, int indices_offset) {
		gl.glMatrixIndexuivARB(size, indices, indices_offset);
	}

	public void glMatrixIndexusvARB(int size, ShortBuffer indices) {
		gl.glMatrixIndexusvARB(size, indices);
	}

	public void glMatrixIndexusvARB(int size, short[] indices, int indices_offset) {
		gl.glMatrixIndexusvARB(size, indices, indices_offset);
	}

	public void glMatrixLoadIdentityEXT(int mode) {
		gl.glMatrixLoadIdentityEXT(mode);
	}

	public void glMatrixLoadTransposedEXT(int mode, DoubleBuffer m) {
		gl.glMatrixLoadTransposedEXT(mode, m);
	}

	public void glMatrixLoadTransposedEXT(int mode, double[] m, int m_offset) {
		gl.glMatrixLoadTransposedEXT(mode, m, m_offset);
	}

	public void glMatrixLoadTransposefEXT(int mode, FloatBuffer m) {
		gl.glMatrixLoadTransposefEXT(mode, m);
	}

	public void glMatrixLoadTransposefEXT(int mode, float[] m, int m_offset) {
		gl.glMatrixLoadTransposefEXT(mode, m, m_offset);
	}

	public void glMatrixLoaddEXT(int mode, DoubleBuffer m) {
		gl.glMatrixLoaddEXT(mode, m);
	}

	public void glMatrixLoaddEXT(int mode, double[] m, int m_offset) {
		gl.glMatrixLoaddEXT(mode, m, m_offset);
	}

	public void glMatrixLoadfEXT(int mode, FloatBuffer m) {
		gl.glMatrixLoadfEXT(mode, m);
	}

	public void glMatrixLoadfEXT(int mode, float[] m, int m_offset) {
		gl.glMatrixLoadfEXT(mode, m, m_offset);
	}

	public void glMatrixMultTransposedEXT(int mode, DoubleBuffer m) {
		gl.glMatrixMultTransposedEXT(mode, m);
	}

	public void glMatrixMultTransposedEXT(int mode, double[] m, int m_offset) {
		gl.glMatrixMultTransposedEXT(mode, m, m_offset);
	}

	public void glMatrixMultTransposefEXT(int mode, FloatBuffer m) {
		gl.glMatrixMultTransposefEXT(mode, m);
	}

	public void glMatrixMultTransposefEXT(int mode, float[] m, int m_offset) {
		gl.glMatrixMultTransposefEXT(mode, m, m_offset);
	}

	public void glMatrixMultdEXT(int mode, DoubleBuffer m) {
		gl.glMatrixMultdEXT(mode, m);
	}

	public void glMatrixMultdEXT(int mode, double[] m, int m_offset) {
		gl.glMatrixMultdEXT(mode, m, m_offset);
	}

	public void glMatrixMultfEXT(int mode, FloatBuffer m) {
		gl.glMatrixMultfEXT(mode, m);
	}

	public void glMatrixMultfEXT(int mode, float[] m, int m_offset) {
		gl.glMatrixMultfEXT(mode, m, m_offset);
	}

	public void glMatrixOrthoEXT(int mode, double left, double right, double bottom, double top, double zNear, double zFar) {
		gl.glMatrixOrthoEXT(mode, left, right, bottom, top, zNear, zFar);
	}

	public void glMatrixPopEXT(int mode) {
		gl.glMatrixPopEXT(mode);
	}

	public void glMatrixPushEXT(int mode) {
		gl.glMatrixPushEXT(mode);
	}

	public void glMatrixRotatedEXT(int mode, double angle, double x, double y, double z) {
		gl.glMatrixRotatedEXT(mode, angle, x, y, z);
	}

	public void glMatrixRotatefEXT(int mode, float angle, float x, float y, float z) {
		gl.glMatrixRotatefEXT(mode, angle, x, y, z);
	}

	public void glMatrixScaledEXT(int mode, double x, double y, double z) {
		gl.glMatrixScaledEXT(mode, x, y, z);
	}

	public void glMatrixScalefEXT(int mode, float x, float y, float z) {
		gl.glMatrixScalefEXT(mode, x, y, z);
	}

	public void glMatrixTranslatedEXT(int mode, double x, double y, double z) {
		gl.glMatrixTranslatedEXT(mode, x, y, z);
	}

	public void glMatrixTranslatefEXT(int mode, float x, float y, float z) {
		gl.glMatrixTranslatefEXT(mode, x, y, z);
	}

	public void glMinmax(int target, int internalformat, boolean sink) {
		gl.glMinmax(target, internalformat, sink);
	}

	public void glMultMatrixd(DoubleBuffer m) {
		gl.glMultMatrixd(m);
	}

	public void glMultMatrixd(double[] m, int m_offset) {
		gl.glMultMatrixd(m, m_offset);
	}

	public void glMultTransposeMatrixd(DoubleBuffer m) {
		gl.glMultTransposeMatrixd(m);
	}

	public void glMultTransposeMatrixd(double[] m, int m_offset) {
		gl.glMultTransposeMatrixd(m, m_offset);
	}

	public void glMultTransposeMatrixf(FloatBuffer m) {
		gl.glMultTransposeMatrixf(m);
	}

	public void glMultTransposeMatrixf(float[] m, int m_offset) {
		gl.glMultTransposeMatrixf(m, m_offset);
	}

	public void glMultiDrawArraysIndirectBindlessCountNV(int mode, Buffer indirect, int drawCount, int maxDrawCount, int stride, int vertexBufferCount) {
		gl.glMultiDrawArraysIndirectBindlessCountNV(mode, indirect, drawCount, maxDrawCount, stride, vertexBufferCount);
	}

	public void glMultiDrawArraysIndirectBindlessNV(int mode, Buffer indirect, int drawCount, int stride, int vertexBufferCount) {
		gl.glMultiDrawArraysIndirectBindlessNV(mode, indirect, drawCount, stride, vertexBufferCount);
	}

	public void glMultiDrawElementsIndirectBindlessCountNV(int mode, int type, Buffer indirect, int drawCount, int maxDrawCount, int stride, int vertexBufferCount) {
		gl.glMultiDrawElementsIndirectBindlessCountNV(mode, type, indirect, drawCount, maxDrawCount, stride, vertexBufferCount);
	}

	public void glMultiDrawElementsIndirectBindlessNV(int mode, int type, Buffer indirect, int drawCount, int stride, int vertexBufferCount) {
		gl.glMultiDrawElementsIndirectBindlessNV(mode, type, indirect, drawCount, stride, vertexBufferCount);
	}

	public void glMultiTexBufferEXT(int texunit, int target, int internalformat, int buffer) {
		gl.glMultiTexBufferEXT(texunit, target, internalformat, buffer);
	}

	public void glMultiTexCoord1bOES(int texture, byte s) {
		gl.glMultiTexCoord1bOES(texture, s);
	}

	public void glMultiTexCoord1bvOES(int texture, ByteBuffer coords) {
		gl.glMultiTexCoord1bvOES(texture, coords);
	}

	public void glMultiTexCoord1bvOES(int texture, byte[] coords, int coords_offset) {
		gl.glMultiTexCoord1bvOES(texture, coords, coords_offset);
	}

	public void glMultiTexCoord1d(int target, double s) {
		gl.glMultiTexCoord1d(target, s);
	}

	public void glMultiTexCoord1dv(int target, DoubleBuffer v) {
		gl.glMultiTexCoord1dv(target, v);
	}

	public void glMultiTexCoord1dv(int target, double[] v, int v_offset) {
		gl.glMultiTexCoord1dv(target, v, v_offset);
	}

	public void glMultiTexCoord1f(int target, float s) {
		gl.glMultiTexCoord1f(target, s);
	}

	public void glMultiTexCoord1fv(int target, FloatBuffer v) {
		gl.glMultiTexCoord1fv(target, v);
	}

	public void glMultiTexCoord1fv(int target, float[] v, int v_offset) {
		gl.glMultiTexCoord1fv(target, v, v_offset);
	}

	public void glMultiTexCoord1h(int target, short s) {
		gl.glMultiTexCoord1h(target, s);
	}

	public void glMultiTexCoord1hv(int target, ShortBuffer v) {
		gl.glMultiTexCoord1hv(target, v);
	}

	public void glMultiTexCoord1hv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord1hv(target, v, v_offset);
	}

	public void glMultiTexCoord1i(int target, int s) {
		gl.glMultiTexCoord1i(target, s);
	}

	public void glMultiTexCoord1iv(int target, IntBuffer v) {
		gl.glMultiTexCoord1iv(target, v);
	}

	public void glMultiTexCoord1iv(int target, int[] v, int v_offset) {
		gl.glMultiTexCoord1iv(target, v, v_offset);
	}

	public void glMultiTexCoord1s(int target, short s) {
		gl.glMultiTexCoord1s(target, s);
	}

	public void glMultiTexCoord1sv(int target, ShortBuffer v) {
		gl.glMultiTexCoord1sv(target, v);
	}

	public void glMultiTexCoord1sv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord1sv(target, v, v_offset);
	}

	public void glMultiTexCoord2bOES(int texture, byte s, byte t) {
		gl.glMultiTexCoord2bOES(texture, s, t);
	}

	public void glMultiTexCoord2bvOES(int texture, ByteBuffer coords) {
		gl.glMultiTexCoord2bvOES(texture, coords);
	}

	public void glMultiTexCoord2bvOES(int texture, byte[] coords, int coords_offset) {
		gl.glMultiTexCoord2bvOES(texture, coords, coords_offset);
	}

	public void glMultiTexCoord2d(int target, double s, double t) {
		gl.glMultiTexCoord2d(target, s, t);
	}

	public void glMultiTexCoord2dv(int target, DoubleBuffer v) {
		gl.glMultiTexCoord2dv(target, v);
	}

	public void glMultiTexCoord2dv(int target, double[] v, int v_offset) {
		gl.glMultiTexCoord2dv(target, v, v_offset);
	}

	public void glMultiTexCoord2f(int target, float s, float t) {
		gl.glMultiTexCoord2f(target, s, t);
	}

	public void glMultiTexCoord2fv(int target, FloatBuffer v) {
		gl.glMultiTexCoord2fv(target, v);
	}

	public void glMultiTexCoord2fv(int target, float[] v, int v_offset) {
		gl.glMultiTexCoord2fv(target, v, v_offset);
	}

	public void glMultiTexCoord2h(int target, short s, short t) {
		gl.glMultiTexCoord2h(target, s, t);
	}

	public void glMultiTexCoord2hv(int target, ShortBuffer v) {
		gl.glMultiTexCoord2hv(target, v);
	}

	public void glMultiTexCoord2hv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord2hv(target, v, v_offset);
	}

	public void glMultiTexCoord2i(int target, int s, int t) {
		gl.glMultiTexCoord2i(target, s, t);
	}

	public void glMultiTexCoord2iv(int target, IntBuffer v) {
		gl.glMultiTexCoord2iv(target, v);
	}

	public void glMultiTexCoord2iv(int target, int[] v, int v_offset) {
		gl.glMultiTexCoord2iv(target, v, v_offset);
	}

	public void glMultiTexCoord2s(int target, short s, short t) {
		gl.glMultiTexCoord2s(target, s, t);
	}

	public void glMultiTexCoord2sv(int target, ShortBuffer v) {
		gl.glMultiTexCoord2sv(target, v);
	}

	public void glMultiTexCoord2sv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord2sv(target, v, v_offset);
	}

	public void glMultiTexCoord3bOES(int texture, byte s, byte t, byte r) {
		gl.glMultiTexCoord3bOES(texture, s, t, r);
	}

	public void glMultiTexCoord3bvOES(int texture, ByteBuffer coords) {
		gl.glMultiTexCoord3bvOES(texture, coords);
	}

	public void glMultiTexCoord3bvOES(int texture, byte[] coords, int coords_offset) {
		gl.glMultiTexCoord3bvOES(texture, coords, coords_offset);
	}

	public void glMultiTexCoord3d(int target, double s, double t, double r) {
		gl.glMultiTexCoord3d(target, s, t, r);
	}

	public void glMultiTexCoord3dv(int target, DoubleBuffer v) {
		gl.glMultiTexCoord3dv(target, v);
	}

	public void glMultiTexCoord3dv(int target, double[] v, int v_offset) {
		gl.glMultiTexCoord3dv(target, v, v_offset);
	}

	public void glMultiTexCoord3f(int target, float s, float t, float r) {
		gl.glMultiTexCoord3f(target, s, t, r);
	}

	public void glMultiTexCoord3fv(int target, FloatBuffer v) {
		gl.glMultiTexCoord3fv(target, v);
	}

	public void glMultiTexCoord3fv(int target, float[] v, int v_offset) {
		gl.glMultiTexCoord3fv(target, v, v_offset);
	}

	public void glMultiTexCoord3h(int target, short s, short t, short r) {
		gl.glMultiTexCoord3h(target, s, t, r);
	}

	public void glMultiTexCoord3hv(int target, ShortBuffer v) {
		gl.glMultiTexCoord3hv(target, v);
	}

	public void glMultiTexCoord3hv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord3hv(target, v, v_offset);
	}

	public void glMultiTexCoord3i(int target, int s, int t, int r) {
		gl.glMultiTexCoord3i(target, s, t, r);
	}

	public void glMultiTexCoord3iv(int target, IntBuffer v) {
		gl.glMultiTexCoord3iv(target, v);
	}

	public void glMultiTexCoord3iv(int target, int[] v, int v_offset) {
		gl.glMultiTexCoord3iv(target, v, v_offset);
	}

	public void glMultiTexCoord3s(int target, short s, short t, short r) {
		gl.glMultiTexCoord3s(target, s, t, r);
	}

	public void glMultiTexCoord3sv(int target, ShortBuffer v) {
		gl.glMultiTexCoord3sv(target, v);
	}

	public void glMultiTexCoord3sv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord3sv(target, v, v_offset);
	}

	public void glMultiTexCoord4bOES(int texture, byte s, byte t, byte r, byte q) {
		gl.glMultiTexCoord4bOES(texture, s, t, r, q);
	}

	public void glMultiTexCoord4bvOES(int texture, ByteBuffer coords) {
		gl.glMultiTexCoord4bvOES(texture, coords);
	}

	public void glMultiTexCoord4bvOES(int texture, byte[] coords, int coords_offset) {
		gl.glMultiTexCoord4bvOES(texture, coords, coords_offset);
	}

	public void glMultiTexCoord4d(int target, double s, double t, double r, double q) {
		gl.glMultiTexCoord4d(target, s, t, r, q);
	}

	public void glMultiTexCoord4dv(int target, DoubleBuffer v) {
		gl.glMultiTexCoord4dv(target, v);
	}

	public void glMultiTexCoord4dv(int target, double[] v, int v_offset) {
		gl.glMultiTexCoord4dv(target, v, v_offset);
	}

	public void glMultiTexCoord4fv(int target, FloatBuffer v) {
		gl.glMultiTexCoord4fv(target, v);
	}

	public void glMultiTexCoord4fv(int target, float[] v, int v_offset) {
		gl.glMultiTexCoord4fv(target, v, v_offset);
	}

	public void glMultiTexCoord4h(int target, short s, short t, short r, short q) {
		gl.glMultiTexCoord4h(target, s, t, r, q);
	}

	public void glMultiTexCoord4hv(int target, ShortBuffer v) {
		gl.glMultiTexCoord4hv(target, v);
	}

	public void glMultiTexCoord4hv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord4hv(target, v, v_offset);
	}

	public void glMultiTexCoord4i(int target, int s, int t, int r, int q) {
		gl.glMultiTexCoord4i(target, s, t, r, q);
	}

	public void glMultiTexCoord4iv(int target, IntBuffer v) {
		gl.glMultiTexCoord4iv(target, v);
	}

	public void glMultiTexCoord4iv(int target, int[] v, int v_offset) {
		gl.glMultiTexCoord4iv(target, v, v_offset);
	}

	public void glMultiTexCoord4s(int target, short s, short t, short r, short q) {
		gl.glMultiTexCoord4s(target, s, t, r, q);
	}

	public void glMultiTexCoord4sv(int target, ShortBuffer v) {
		gl.glMultiTexCoord4sv(target, v);
	}

	public void glMultiTexCoord4sv(int target, short[] v, int v_offset) {
		gl.glMultiTexCoord4sv(target, v, v_offset);
	}

	public void glMultiTexCoordPointerEXT(int texunit, int size, int type, int stride, Buffer pointer) {
		gl.glMultiTexCoordPointerEXT(texunit, size, type, stride, pointer);
	}

	public void glMultiTexEnvfEXT(int texunit, int target, int pname, float param) {
		gl.glMultiTexEnvfEXT(texunit, target, pname, param);
	}

	public void glMultiTexEnvfvEXT(int texunit, int target, int pname, FloatBuffer params) {
		gl.glMultiTexEnvfvEXT(texunit, target, pname, params);
	}

	public void glMultiTexEnvfvEXT(int texunit, int target, int pname, float[] params, int params_offset) {
		gl.glMultiTexEnvfvEXT(texunit, target, pname, params, params_offset);
	}

	public void glMultiTexEnviEXT(int texunit, int target, int pname, int param) {
		gl.glMultiTexEnviEXT(texunit, target, pname, param);
	}

	public void glMultiTexEnvivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glMultiTexEnvivEXT(texunit, target, pname, params);
	}

	public void glMultiTexEnvivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glMultiTexEnvivEXT(texunit, target, pname, params, params_offset);
	}

	public void glMultiTexGendEXT(int texunit, int coord, int pname, double param) {
		gl.glMultiTexGendEXT(texunit, coord, pname, param);
	}

	public void glMultiTexGendvEXT(int texunit, int coord, int pname, DoubleBuffer params) {
		gl.glMultiTexGendvEXT(texunit, coord, pname, params);
	}

	public void glMultiTexGendvEXT(int texunit, int coord, int pname, double[] params, int params_offset) {
		gl.glMultiTexGendvEXT(texunit, coord, pname, params, params_offset);
	}

	public void glMultiTexGenfEXT(int texunit, int coord, int pname, float param) {
		gl.glMultiTexGenfEXT(texunit, coord, pname, param);
	}

	public void glMultiTexGenfvEXT(int texunit, int coord, int pname, FloatBuffer params) {
		gl.glMultiTexGenfvEXT(texunit, coord, pname, params);
	}

	public void glMultiTexGenfvEXT(int texunit, int coord, int pname, float[] params, int params_offset) {
		gl.glMultiTexGenfvEXT(texunit, coord, pname, params, params_offset);
	}

	public void glMultiTexGeniEXT(int texunit, int coord, int pname, int param) {
		gl.glMultiTexGeniEXT(texunit, coord, pname, param);
	}

	public void glMultiTexGenivEXT(int texunit, int coord, int pname, IntBuffer params) {
		gl.glMultiTexGenivEXT(texunit, coord, pname, params);
	}

	public void glMultiTexGenivEXT(int texunit, int coord, int pname, int[] params, int params_offset) {
		gl.glMultiTexGenivEXT(texunit, coord, pname, params, params_offset);
	}

	public void glMultiTexImage1DEXT(int texunit, int target, int level, int internalformat, int width, int border, int format, int type, Buffer pixels) {
		gl.glMultiTexImage1DEXT(texunit, target, level, internalformat, width, border, format, type, pixels);
	}

	public void glMultiTexImage2DEXT(int texunit, int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
		gl.glMultiTexImage2DEXT(texunit, target, level, internalformat, width, height, border, format, type, pixels);
	}

	public void glMultiTexImage3DEXT(int texunit, int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, Buffer pixels) {
		gl.glMultiTexImage3DEXT(texunit, target, level, internalformat, width, height, depth, border, format, type, pixels);
	}

	public void glMultiTexParameterIivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glMultiTexParameterIivEXT(texunit, target, pname, params);
	}

	public void glMultiTexParameterIivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glMultiTexParameterIivEXT(texunit, target, pname, params, params_offset);
	}

	public void glMultiTexParameterIuivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glMultiTexParameterIuivEXT(texunit, target, pname, params);
	}

	public void glMultiTexParameterIuivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glMultiTexParameterIuivEXT(texunit, target, pname, params, params_offset);
	}

	public void glMultiTexParameterfEXT(int texunit, int target, int pname, float param) {
		gl.glMultiTexParameterfEXT(texunit, target, pname, param);
	}

	public void glMultiTexParameterfvEXT(int texunit, int target, int pname, FloatBuffer params) {
		gl.glMultiTexParameterfvEXT(texunit, target, pname, params);
	}

	public void glMultiTexParameterfvEXT(int texunit, int target, int pname, float[] params, int params_offset) {
		gl.glMultiTexParameterfvEXT(texunit, target, pname, params, params_offset);
	}

	public void glMultiTexParameteriEXT(int texunit, int target, int pname, int param) {
		gl.glMultiTexParameteriEXT(texunit, target, pname, param);
	}

	public void glMultiTexParameterivEXT(int texunit, int target, int pname, IntBuffer params) {
		gl.glMultiTexParameterivEXT(texunit, target, pname, params);
	}

	public void glMultiTexParameterivEXT(int texunit, int target, int pname, int[] params, int params_offset) {
		gl.glMultiTexParameterivEXT(texunit, target, pname, params, params_offset);
	}

	public void glMultiTexRenderbufferEXT(int texunit, int target, int renderbuffer) {
		gl.glMultiTexRenderbufferEXT(texunit, target, renderbuffer);
	}

	public void glMultiTexSubImage1DEXT(int texunit, int target, int level, int xoffset, int width, int format, int type, Buffer pixels) {
		gl.glMultiTexSubImage1DEXT(texunit, target, level, xoffset, width, format, type, pixels);
	}

	public void glMultiTexSubImage2DEXT(int texunit, int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer pixels) {
		gl.glMultiTexSubImage2DEXT(texunit, target, level, xoffset, yoffset, width, height, format, type, pixels);
	}

	public void glMultiTexSubImage3DEXT(int texunit, int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer pixels) {
		gl.glMultiTexSubImage3DEXT(texunit, target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
	}

	public void glNamedBufferDataEXT(int buffer, long size, Buffer data, int usage) {
		gl.glNamedBufferDataEXT(buffer, size, data, usage);
	}

	public void glNamedBufferStorageEXT(int buffer, long size, Buffer data, int flags) {
		gl.glNamedBufferStorageEXT(buffer, size, data, flags);
	}

	public void glNamedBufferSubDataEXT(int buffer, long offset, long size, Buffer data) {
		gl.glNamedBufferSubDataEXT(buffer, offset, size, data);
	}

	public void glNamedCopyBufferSubDataEXT(int readBuffer, int writeBuffer, long readOffset, long writeOffset, long size) {
		gl.glNamedCopyBufferSubDataEXT(readBuffer, writeBuffer, readOffset, writeOffset, size);
	}

	public void glNamedFramebufferParameteri(int framebuffer, int pname, int param) {
		gl.glNamedFramebufferParameteri(framebuffer, pname, param);
	}

	public void glNamedFramebufferRenderbufferEXT(int framebuffer, int attachment, int renderbuffertarget, int renderbuffer) {
		gl.glNamedFramebufferRenderbufferEXT(framebuffer, attachment, renderbuffertarget, renderbuffer);
	}

	public void glNamedFramebufferSampleLocationsfvNV(int framebuffer, int start, int count, FloatBuffer v) {
		gl.glNamedFramebufferSampleLocationsfvNV(framebuffer, start, count, v);
	}

	public void glNamedFramebufferSampleLocationsfvNV(int framebuffer, int start, int count, float[] v, int v_offset) {
		gl.glNamedFramebufferSampleLocationsfvNV(framebuffer, start, count, v, v_offset);
	}

	public void glNamedFramebufferTexture1DEXT(int framebuffer, int attachment, int textarget, int texture, int level) {
		gl.glNamedFramebufferTexture1DEXT(framebuffer, attachment, textarget, texture, level);
	}

	public void glNamedFramebufferTexture2DEXT(int framebuffer, int attachment, int textarget, int texture, int level) {
		gl.glNamedFramebufferTexture2DEXT(framebuffer, attachment, textarget, texture, level);
	}

	public void glNamedFramebufferTexture3DEXT(int framebuffer, int attachment, int textarget, int texture, int level, int zoffset) {
		gl.glNamedFramebufferTexture3DEXT(framebuffer, attachment, textarget, texture, level, zoffset);
	}

	public void glNamedFramebufferTextureEXT(int framebuffer, int attachment, int texture, int level) {
		gl.glNamedFramebufferTextureEXT(framebuffer, attachment, texture, level);
	}

	public void glNamedFramebufferTextureFaceEXT(int framebuffer, int attachment, int texture, int level, int face) {
		gl.glNamedFramebufferTextureFaceEXT(framebuffer, attachment, texture, level, face);
	}

	public void glNamedFramebufferTextureLayerEXT(int framebuffer, int attachment, int texture, int level, int layer) {
		gl.glNamedFramebufferTextureLayerEXT(framebuffer, attachment, texture, level, layer);
	}

	public void glNamedProgramLocalParameter4dEXT(int program, int target, int index, double x, double y, double z, double w) {
		gl.glNamedProgramLocalParameter4dEXT(program, target, index, x, y, z, w);
	}

	public void glNamedProgramLocalParameter4dvEXT(int program, int target, int index, DoubleBuffer params) {
		gl.glNamedProgramLocalParameter4dvEXT(program, target, index, params);
	}

	public void glNamedProgramLocalParameter4dvEXT(int program, int target, int index, double[] params, int params_offset) {
		gl.glNamedProgramLocalParameter4dvEXT(program, target, index, params, params_offset);
	}

	public void glNamedProgramLocalParameter4fEXT(int program, int target, int index, float x, float y, float z, float w) {
		gl.glNamedProgramLocalParameter4fEXT(program, target, index, x, y, z, w);
	}

	public void glNamedProgramLocalParameter4fvEXT(int program, int target, int index, FloatBuffer params) {
		gl.glNamedProgramLocalParameter4fvEXT(program, target, index, params);
	}

	public void glNamedProgramLocalParameter4fvEXT(int program, int target, int index, float[] params, int params_offset) {
		gl.glNamedProgramLocalParameter4fvEXT(program, target, index, params, params_offset);
	}

	public void glNamedProgramLocalParameterI4iEXT(int program, int target, int index, int x, int y, int z, int w) {
		gl.glNamedProgramLocalParameterI4iEXT(program, target, index, x, y, z, w);
	}

	public void glNamedProgramLocalParameterI4ivEXT(int program, int target, int index, IntBuffer params) {
		gl.glNamedProgramLocalParameterI4ivEXT(program, target, index, params);
	}

	public void glNamedProgramLocalParameterI4ivEXT(int program, int target, int index, int[] params, int params_offset) {
		gl.glNamedProgramLocalParameterI4ivEXT(program, target, index, params, params_offset);
	}

	public void glNamedProgramLocalParameterI4uiEXT(int program, int target, int index, int x, int y, int z, int w) {
		gl.glNamedProgramLocalParameterI4uiEXT(program, target, index, x, y, z, w);
	}

	public void glNamedProgramLocalParameterI4uivEXT(int program, int target, int index, IntBuffer params) {
		gl.glNamedProgramLocalParameterI4uivEXT(program, target, index, params);
	}

	public void glNamedProgramLocalParameterI4uivEXT(int program, int target, int index, int[] params, int params_offset) {
		gl.glNamedProgramLocalParameterI4uivEXT(program, target, index, params, params_offset);
	}

	public void glNamedProgramLocalParameters4fvEXT(int program, int target, int index, int count, FloatBuffer params) {
		gl.glNamedProgramLocalParameters4fvEXT(program, target, index, count, params);
	}

	public void glNamedProgramLocalParameters4fvEXT(int program, int target, int index, int count, float[] params, int params_offset) {
		gl.glNamedProgramLocalParameters4fvEXT(program, target, index, count, params, params_offset);
	}

	public void glNamedProgramLocalParametersI4ivEXT(int program, int target, int index, int count, IntBuffer params) {
		gl.glNamedProgramLocalParametersI4ivEXT(program, target, index, count, params);
	}

	public void glNamedProgramLocalParametersI4ivEXT(int program, int target, int index, int count, int[] params, int params_offset) {
		gl.glNamedProgramLocalParametersI4ivEXT(program, target, index, count, params, params_offset);
	}

	public void glNamedProgramLocalParametersI4uivEXT(int program, int target, int index, int count, IntBuffer params) {
		gl.glNamedProgramLocalParametersI4uivEXT(program, target, index, count, params);
	}

	public void glNamedProgramLocalParametersI4uivEXT(int program, int target, int index, int count, int[] params, int params_offset) {
		gl.glNamedProgramLocalParametersI4uivEXT(program, target, index, count, params, params_offset);
	}

	public void glNamedProgramStringEXT(int program, int target, int format, int len, Buffer string) {
		gl.glNamedProgramStringEXT(program, target, format, len, string);
	}

	public void glNamedRenderbufferStorageEXT(int renderbuffer, int internalformat, int width, int height) {
		gl.glNamedRenderbufferStorageEXT(renderbuffer, internalformat, width, height);
	}

	public void glNamedRenderbufferStorageMultisampleCoverageEXT(int renderbuffer, int coverageSamples, int colorSamples, int internalformat, int width, int height) {
		gl.glNamedRenderbufferStorageMultisampleCoverageEXT(renderbuffer, coverageSamples, colorSamples, internalformat, width, height);
	}

	public void glNamedRenderbufferStorageMultisampleEXT(int renderbuffer, int samples, int internalformat, int width, int height) {
		gl.glNamedRenderbufferStorageMultisampleEXT(renderbuffer, samples, internalformat, width, height);
	}

	public void glNewList(int list, int mode) {
		gl.glNewList(list, mode);
	}

	public void glNormal3b(byte nx, byte ny, byte nz) {
		gl.glNormal3b(nx, ny, nz);
	}

	public void glNormal3bv(ByteBuffer v) {
		gl.glNormal3bv(v);
	}

	public void glNormal3bv(byte[] v, int v_offset) {
		gl.glNormal3bv(v, v_offset);
	}

	public void glNormal3d(double nx, double ny, double nz) {
		gl.glNormal3d(nx, ny, nz);
	}

	public void glNormal3dv(DoubleBuffer v) {
		gl.glNormal3dv(v);
	}

	public void glNormal3dv(double[] v, int v_offset) {
		gl.glNormal3dv(v, v_offset);
	}

	public void glNormal3fv(FloatBuffer v) {
		gl.glNormal3fv(v);
	}

	public void glNormal3fv(float[] v, int v_offset) {
		gl.glNormal3fv(v, v_offset);
	}

	public void glNormal3h(short nx, short ny, short nz) {
		gl.glNormal3h(nx, ny, nz);
	}

	public void glNormal3hv(ShortBuffer v) {
		gl.glNormal3hv(v);
	}

	public void glNormal3hv(short[] v, int v_offset) {
		gl.glNormal3hv(v, v_offset);
	}

	public void glNormal3i(int nx, int ny, int nz) {
		gl.glNormal3i(nx, ny, nz);
	}

	public void glNormal3iv(IntBuffer v) {
		gl.glNormal3iv(v);
	}

	public void glNormal3iv(int[] v, int v_offset) {
		gl.glNormal3iv(v, v_offset);
	}

	public void glNormal3s(short nx, short ny, short nz) {
		gl.glNormal3s(nx, ny, nz);
	}

	public void glNormal3sv(ShortBuffer v) {
		gl.glNormal3sv(v);
	}

	public void glNormal3sv(short[] v, int v_offset) {
		gl.glNormal3sv(v, v_offset);
	}

	public int glObjectPurgeableAPPLE(int objectType, int name, int option) {
		return gl.glObjectPurgeableAPPLE(objectType, name, option);
	}

	public int glObjectUnpurgeableAPPLE(int objectType, int name, int option) {
		return gl.glObjectUnpurgeableAPPLE(objectType, name, option);
	}

	public void glPNTrianglesfATI(int pname, float param) {
		gl.glPNTrianglesfATI(pname, param);
	}

	public void glPNTrianglesiATI(int pname, int param) {
		gl.glPNTrianglesiATI(pname, param);
	}

	public void glPassThrough(float token) {
		gl.glPassThrough(token);
	}

	public void glPauseTransformFeedbackNV() {
		gl.glPauseTransformFeedbackNV();
	}

	public void glPixelDataRangeNV(int target, int length, Buffer pointer) {
		gl.glPixelDataRangeNV(target, length, pointer);
	}

	public void glPixelMapfv(int map, int mapsize, FloatBuffer values) {
		gl.glPixelMapfv(map, mapsize, values);
	}

	public void glPixelMapfv(int map, int mapsize, float[] values, int values_offset) {
		gl.glPixelMapfv(map, mapsize, values, values_offset);
	}

	public void glPixelMapfv(int map, int mapsize, long values_buffer_offset) {
		gl.glPixelMapfv(map, mapsize, values_buffer_offset);
	}

	public void glPixelMapuiv(int map, int mapsize, IntBuffer values) {
		gl.glPixelMapuiv(map, mapsize, values);
	}

	public void glPixelMapuiv(int map, int mapsize, int[] values, int values_offset) {
		gl.glPixelMapuiv(map, mapsize, values, values_offset);
	}

	public void glPixelMapuiv(int map, int mapsize, long values_buffer_offset) {
		gl.glPixelMapuiv(map, mapsize, values_buffer_offset);
	}

	public void glPixelMapusv(int map, int mapsize, ShortBuffer values) {
		gl.glPixelMapusv(map, mapsize, values);
	}

	public void glPixelMapusv(int map, int mapsize, short[] values, int values_offset) {
		gl.glPixelMapusv(map, mapsize, values, values_offset);
	}

	public void glPixelMapusv(int map, int mapsize, long values_buffer_offset) {
		gl.glPixelMapusv(map, mapsize, values_buffer_offset);
	}

	public void glPixelTransferf(int pname, float param) {
		gl.glPixelTransferf(pname, param);
	}

	public void glPixelTransferi(int pname, int param) {
		gl.glPixelTransferi(pname, param);
	}

	public void glPixelTransformParameterfEXT(int target, int pname, float param) {
		gl.glPixelTransformParameterfEXT(target, pname, param);
	}

	public void glPixelTransformParameterfvEXT(int target, int pname, FloatBuffer params) {
		gl.glPixelTransformParameterfvEXT(target, pname, params);
	}

	public void glPixelTransformParameterfvEXT(int target, int pname, float[] params, int params_offset) {
		gl.glPixelTransformParameterfvEXT(target, pname, params, params_offset);
	}

	public void glPixelTransformParameteriEXT(int target, int pname, int param) {
		gl.glPixelTransformParameteriEXT(target, pname, param);
	}

	public void glPixelTransformParameterivEXT(int target, int pname, IntBuffer params) {
		gl.glPixelTransformParameterivEXT(target, pname, params);
	}

	public void glPixelTransformParameterivEXT(int target, int pname, int[] params, int params_offset) {
		gl.glPixelTransformParameterivEXT(target, pname, params, params_offset);
	}

	public void glPixelZoom(float xfactor, float yfactor) {
		gl.glPixelZoom(xfactor, yfactor);
	}

	public void glPolygonOffsetClampEXT(float factor, float units, float clamp) {
		gl.glPolygonOffsetClampEXT(factor, units, clamp);
	}

	public void glPolygonStipple(ByteBuffer mask) {
		gl.glPolygonStipple(mask);
	}

	public void glPolygonStipple(byte[] mask, int mask_offset) {
		gl.glPolygonStipple(mask, mask_offset);
	}

	public void glPolygonStipple(long mask_buffer_offset) {
		gl.glPolygonStipple(mask_buffer_offset);
	}

	public void glPopAttrib() {
		gl.glPopAttrib();
	}

	public void glPopClientAttrib() {
		gl.glPopClientAttrib();
	}

	public void glPopName() {
		gl.glPopName();
	}

	public void glPrimitiveRestartIndexNV(int index) {
		gl.glPrimitiveRestartIndexNV(index);
	}

	public void glPrimitiveRestartNV() {
		gl.glPrimitiveRestartNV();
	}

	public void glPrioritizeTextures(int n, IntBuffer textures, FloatBuffer priorities) {
		gl.glPrioritizeTextures(n, textures, priorities);
	}

	public void glPrioritizeTextures(int n, int[] textures, int textures_offset, float[] priorities, int priorities_offset) {
		gl.glPrioritizeTextures(n, textures, textures_offset, priorities, priorities_offset);
	}

	public void glProgramBufferParametersIivNV(int target, int bindingIndex, int wordIndex, int count, IntBuffer params) {
		gl.glProgramBufferParametersIivNV(target, bindingIndex, wordIndex, count, params);
	}

	public void glProgramBufferParametersIivNV(int target, int bindingIndex, int wordIndex, int count, int[] params, int params_offset) {
		gl.glProgramBufferParametersIivNV(target, bindingIndex, wordIndex, count, params, params_offset);
	}

	public void glProgramBufferParametersIuivNV(int target, int bindingIndex, int wordIndex, int count, IntBuffer params) {
		gl.glProgramBufferParametersIuivNV(target, bindingIndex, wordIndex, count, params);
	}

	public void glProgramBufferParametersIuivNV(int target, int bindingIndex, int wordIndex, int count, int[] params, int params_offset) {
		gl.glProgramBufferParametersIuivNV(target, bindingIndex, wordIndex, count, params, params_offset);
	}

	public void glProgramBufferParametersfvNV(int target, int bindingIndex, int wordIndex, int count, FloatBuffer params) {
		gl.glProgramBufferParametersfvNV(target, bindingIndex, wordIndex, count, params);
	}

	public void glProgramBufferParametersfvNV(int target, int bindingIndex, int wordIndex, int count, float[] params, int params_offset) {
		gl.glProgramBufferParametersfvNV(target, bindingIndex, wordIndex, count, params, params_offset);
	}

	public void glProgramEnvParameter4dARB(int target, int index, double x, double y, double z, double w) {
		gl.glProgramEnvParameter4dARB(target, index, x, y, z, w);
	}

	public void glProgramEnvParameter4dvARB(int target, int index, DoubleBuffer params) {
		gl.glProgramEnvParameter4dvARB(target, index, params);
	}

	public void glProgramEnvParameter4dvARB(int target, int index, double[] params, int params_offset) {
		gl.glProgramEnvParameter4dvARB(target, index, params, params_offset);
	}

	public void glProgramEnvParameter4fARB(int target, int index, float x, float y, float z, float w) {
		gl.glProgramEnvParameter4fARB(target, index, x, y, z, w);
	}

	public void glProgramEnvParameter4fvARB(int target, int index, FloatBuffer params) {
		gl.glProgramEnvParameter4fvARB(target, index, params);
	}

	public void glProgramEnvParameter4fvARB(int target, int index, float[] params, int params_offset) {
		gl.glProgramEnvParameter4fvARB(target, index, params, params_offset);
	}

	public void glProgramEnvParameterI4iNV(int target, int index, int x, int y, int z, int w) {
		gl.glProgramEnvParameterI4iNV(target, index, x, y, z, w);
	}

	public void glProgramEnvParameterI4ivNV(int target, int index, IntBuffer params) {
		gl.glProgramEnvParameterI4ivNV(target, index, params);
	}

	public void glProgramEnvParameterI4ivNV(int target, int index, int[] params, int params_offset) {
		gl.glProgramEnvParameterI4ivNV(target, index, params, params_offset);
	}

	public void glProgramEnvParameterI4uiNV(int target, int index, int x, int y, int z, int w) {
		gl.glProgramEnvParameterI4uiNV(target, index, x, y, z, w);
	}

	public void glProgramEnvParameterI4uivNV(int target, int index, IntBuffer params) {
		gl.glProgramEnvParameterI4uivNV(target, index, params);
	}

	public void glProgramEnvParameterI4uivNV(int target, int index, int[] params, int params_offset) {
		gl.glProgramEnvParameterI4uivNV(target, index, params, params_offset);
	}

	public void glProgramEnvParameters4fvEXT(int target, int index, int count, FloatBuffer params) {
		gl.glProgramEnvParameters4fvEXT(target, index, count, params);
	}

	public void glProgramEnvParameters4fvEXT(int target, int index, int count, float[] params, int params_offset) {
		gl.glProgramEnvParameters4fvEXT(target, index, count, params, params_offset);
	}

	public void glProgramEnvParametersI4ivNV(int target, int index, int count, IntBuffer params) {
		gl.glProgramEnvParametersI4ivNV(target, index, count, params);
	}

	public void glProgramEnvParametersI4ivNV(int target, int index, int count, int[] params, int params_offset) {
		gl.glProgramEnvParametersI4ivNV(target, index, count, params, params_offset);
	}

	public void glProgramEnvParametersI4uivNV(int target, int index, int count, IntBuffer params) {
		gl.glProgramEnvParametersI4uivNV(target, index, count, params);
	}

	public void glProgramEnvParametersI4uivNV(int target, int index, int count, int[] params, int params_offset) {
		gl.glProgramEnvParametersI4uivNV(target, index, count, params, params_offset);
	}

	public void glProgramLocalParameter4dARB(int target, int index, double x, double y, double z, double w) {
		gl.glProgramLocalParameter4dARB(target, index, x, y, z, w);
	}

	public void glProgramLocalParameter4dvARB(int target, int index, DoubleBuffer params) {
		gl.glProgramLocalParameter4dvARB(target, index, params);
	}

	public void glProgramLocalParameter4dvARB(int target, int index, double[] params, int params_offset) {
		gl.glProgramLocalParameter4dvARB(target, index, params, params_offset);
	}

	public void glProgramLocalParameter4fARB(int target, int index, float x, float y, float z, float w) {
		gl.glProgramLocalParameter4fARB(target, index, x, y, z, w);
	}

	public void glProgramLocalParameter4fvARB(int target, int index, FloatBuffer params) {
		gl.glProgramLocalParameter4fvARB(target, index, params);
	}

	public void glProgramLocalParameter4fvARB(int target, int index, float[] params, int params_offset) {
		gl.glProgramLocalParameter4fvARB(target, index, params, params_offset);
	}

	public void glProgramLocalParameterI4iNV(int target, int index, int x, int y, int z, int w) {
		gl.glProgramLocalParameterI4iNV(target, index, x, y, z, w);
	}

	public void glProgramLocalParameterI4ivNV(int target, int index, IntBuffer params) {
		gl.glProgramLocalParameterI4ivNV(target, index, params);
	}

	public void glProgramLocalParameterI4ivNV(int target, int index, int[] params, int params_offset) {
		gl.glProgramLocalParameterI4ivNV(target, index, params, params_offset);
	}

	public void glProgramLocalParameterI4uiNV(int target, int index, int x, int y, int z, int w) {
		gl.glProgramLocalParameterI4uiNV(target, index, x, y, z, w);
	}

	public void glProgramLocalParameterI4uivNV(int target, int index, IntBuffer params) {
		gl.glProgramLocalParameterI4uivNV(target, index, params);
	}

	public void glProgramLocalParameterI4uivNV(int target, int index, int[] params, int params_offset) {
		gl.glProgramLocalParameterI4uivNV(target, index, params, params_offset);
	}

	public void glProgramLocalParameters4fvEXT(int target, int index, int count, FloatBuffer params) {
		gl.glProgramLocalParameters4fvEXT(target, index, count, params);
	}

	public void glProgramLocalParameters4fvEXT(int target, int index, int count, float[] params, int params_offset) {
		gl.glProgramLocalParameters4fvEXT(target, index, count, params, params_offset);
	}

	public void glProgramLocalParametersI4ivNV(int target, int index, int count, IntBuffer params) {
		gl.glProgramLocalParametersI4ivNV(target, index, count, params);
	}

	public void glProgramLocalParametersI4ivNV(int target, int index, int count, int[] params, int params_offset) {
		gl.glProgramLocalParametersI4ivNV(target, index, count, params, params_offset);
	}

	public void glProgramLocalParametersI4uivNV(int target, int index, int count, IntBuffer params) {
		gl.glProgramLocalParametersI4uivNV(target, index, count, params);
	}

	public void glProgramLocalParametersI4uivNV(int target, int index, int count, int[] params, int params_offset) {
		gl.glProgramLocalParametersI4uivNV(target, index, count, params, params_offset);
	}

	public void glProgramStringARB(int target, int format, int len, String string) {
		gl.glProgramStringARB(target, format, len, string);
	}

	public void glProgramSubroutineParametersuivNV(int target, int count, IntBuffer params) {
		gl.glProgramSubroutineParametersuivNV(target, count, params);
	}

	public void glProgramSubroutineParametersuivNV(int target, int count, int[] params, int params_offset) {
		gl.glProgramSubroutineParametersuivNV(target, count, params, params_offset);
	}

	public void glProgramUniform1dEXT(int program, int location, double x) {
		gl.glProgramUniform1dEXT(program, location, x);
	}

	public void glProgramUniform1dvEXT(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform1dvEXT(program, location, count, value);
	}

	public void glProgramUniform1dvEXT(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform1dvEXT(program, location, count, value, value_offset);
	}

	public void glProgramUniform1i64NV(int program, int location, long x) {
		gl.glProgramUniform1i64NV(program, location, x);
	}

	public void glProgramUniform1i64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform1i64vNV(program, location, count, value);
	}

	public void glProgramUniform1i64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform1i64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform1ui64NV(int program, int location, long x) {
		gl.glProgramUniform1ui64NV(program, location, x);
	}

	public void glProgramUniform1ui64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform1ui64vNV(program, location, count, value);
	}

	public void glProgramUniform1ui64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform1ui64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform2dEXT(int program, int location, double x, double y) {
		gl.glProgramUniform2dEXT(program, location, x, y);
	}

	public void glProgramUniform2dvEXT(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform2dvEXT(program, location, count, value);
	}

	public void glProgramUniform2dvEXT(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform2dvEXT(program, location, count, value, value_offset);
	}

	public void glProgramUniform2i64NV(int program, int location, long x, long y) {
		gl.glProgramUniform2i64NV(program, location, x, y);
	}

	public void glProgramUniform2i64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform2i64vNV(program, location, count, value);
	}

	public void glProgramUniform2i64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform2i64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform2ui64NV(int program, int location, long x, long y) {
		gl.glProgramUniform2ui64NV(program, location, x, y);
	}

	public void glProgramUniform2ui64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform2ui64vNV(program, location, count, value);
	}

	public void glProgramUniform2ui64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform2ui64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform3dEXT(int program, int location, double x, double y, double z) {
		gl.glProgramUniform3dEXT(program, location, x, y, z);
	}

	public void glProgramUniform3dvEXT(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform3dvEXT(program, location, count, value);
	}

	public void glProgramUniform3dvEXT(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform3dvEXT(program, location, count, value, value_offset);
	}

	public void glProgramUniform3i64NV(int program, int location, long x, long y, long z) {
		gl.glProgramUniform3i64NV(program, location, x, y, z);
	}

	public void glProgramUniform3i64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform3i64vNV(program, location, count, value);
	}

	public void glProgramUniform3i64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform3i64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform3ui64NV(int program, int location, long x, long y, long z) {
		gl.glProgramUniform3ui64NV(program, location, x, y, z);
	}

	public void glProgramUniform3ui64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform3ui64vNV(program, location, count, value);
	}

	public void glProgramUniform3ui64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform3ui64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform4dEXT(int program, int location, double x, double y, double z, double w) {
		gl.glProgramUniform4dEXT(program, location, x, y, z, w);
	}

	public void glProgramUniform4dvEXT(int program, int location, int count, DoubleBuffer value) {
		gl.glProgramUniform4dvEXT(program, location, count, value);
	}

	public void glProgramUniform4dvEXT(int program, int location, int count, double[] value, int value_offset) {
		gl.glProgramUniform4dvEXT(program, location, count, value, value_offset);
	}

	public void glProgramUniform4i64NV(int program, int location, long x, long y, long z, long w) {
		gl.glProgramUniform4i64NV(program, location, x, y, z, w);
	}

	public void glProgramUniform4i64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform4i64vNV(program, location, count, value);
	}

	public void glProgramUniform4i64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform4i64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniform4ui64NV(int program, int location, long x, long y, long z, long w) {
		gl.glProgramUniform4ui64NV(program, location, x, y, z, w);
	}

	public void glProgramUniform4ui64vNV(int program, int location, int count, LongBuffer value) {
		gl.glProgramUniform4ui64vNV(program, location, count, value);
	}

	public void glProgramUniform4ui64vNV(int program, int location, int count, long[] value, int value_offset) {
		gl.glProgramUniform4ui64vNV(program, location, count, value, value_offset);
	}

	public void glProgramUniformMatrix2dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix2dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix2dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix2dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix2x3dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix2x3dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix2x3dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix2x3dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix2x4dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix2x4dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix2x4dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix2x4dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix3dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix3dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix3dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix3dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix3x2dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix3x2dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix3x2dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix3x2dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix3x4dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix3x4dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix3x4dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix3x4dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix4dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix4dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix4dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix4dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix4x2dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix4x2dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix4x2dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix4x2dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramUniformMatrix4x3dvEXT(int program, int location, int count, boolean transpose, DoubleBuffer value) {
		gl.glProgramUniformMatrix4x3dvEXT(program, location, count, transpose, value);
	}

	public void glProgramUniformMatrix4x3dvEXT(int program, int location, int count, boolean transpose, double[] value, int value_offset) {
		gl.glProgramUniformMatrix4x3dvEXT(program, location, count, transpose, value, value_offset);
	}

	public void glProgramVertexLimitNV(int target, int limit) {
		gl.glProgramVertexLimitNV(target, limit);
	}

	public void glProvokingVertexEXT(int mode) {
		gl.glProvokingVertexEXT(mode);
	}

	public void glPushAttrib(int mask) {
		gl.glPushAttrib(mask);
	}

	public void glPushClientAttrib(int mask) {
		gl.glPushClientAttrib(mask);
	}

	public void glPushClientAttribDefaultEXT(int mask) {
		gl.glPushClientAttribDefaultEXT(mask);
	}

	public void glPushName(int name) {
		gl.glPushName(name);
	}

	public int glQueryMatrixxOES(IntBuffer mantissa, IntBuffer exponent) {
		return gl.glQueryMatrixxOES(mantissa, exponent);
	}

	public int glQueryMatrixxOES(int[] mantissa, int mantissa_offset, int[] exponent, int exponent_offset) {
		return gl.glQueryMatrixxOES(mantissa, mantissa_offset, exponent, exponent_offset);
	}

	public void glQueryObjectParameteruiAMD(int target, int id, int pname, int param) {
		gl.glQueryObjectParameteruiAMD(target, id, pname, param);
	}

	public void glRasterPos2d(double x, double y) {
		gl.glRasterPos2d(x, y);
	}

	public void glRasterPos2dv(DoubleBuffer v) {
		gl.glRasterPos2dv(v);
	}

	public void glRasterPos2dv(double[] v, int v_offset) {
		gl.glRasterPos2dv(v, v_offset);
	}

	public void glRasterPos2f(float x, float y) {
		gl.glRasterPos2f(x, y);
	}

	public void glRasterPos2fv(FloatBuffer v) {
		gl.glRasterPos2fv(v);
	}

	public void glRasterPos2fv(float[] v, int v_offset) {
		gl.glRasterPos2fv(v, v_offset);
	}

	public void glRasterPos2i(int x, int y) {
		gl.glRasterPos2i(x, y);
	}

	public void glRasterPos2iv(IntBuffer v) {
		gl.glRasterPos2iv(v);
	}

	public void glRasterPos2iv(int[] v, int v_offset) {
		gl.glRasterPos2iv(v, v_offset);
	}

	public void glRasterPos2s(short x, short y) {
		gl.glRasterPos2s(x, y);
	}

	public void glRasterPos2sv(ShortBuffer v) {
		gl.glRasterPos2sv(v);
	}

	public void glRasterPos2sv(short[] v, int v_offset) {
		gl.glRasterPos2sv(v, v_offset);
	}

	public void glRasterPos3d(double x, double y, double z) {
		gl.glRasterPos3d(x, y, z);
	}

	public void glRasterPos3dv(DoubleBuffer v) {
		gl.glRasterPos3dv(v);
	}

	public void glRasterPos3dv(double[] v, int v_offset) {
		gl.glRasterPos3dv(v, v_offset);
	}

	public void glRasterPos3f(float x, float y, float z) {
		gl.glRasterPos3f(x, y, z);
	}

	public void glRasterPos3fv(FloatBuffer v) {
		gl.glRasterPos3fv(v);
	}

	public void glRasterPos3fv(float[] v, int v_offset) {
		gl.glRasterPos3fv(v, v_offset);
	}

	public void glRasterPos3i(int x, int y, int z) {
		gl.glRasterPos3i(x, y, z);
	}

	public void glRasterPos3iv(IntBuffer v) {
		gl.glRasterPos3iv(v);
	}

	public void glRasterPos3iv(int[] v, int v_offset) {
		gl.glRasterPos3iv(v, v_offset);
	}

	public void glRasterPos3s(short x, short y, short z) {
		gl.glRasterPos3s(x, y, z);
	}

	public void glRasterPos3sv(ShortBuffer v) {
		gl.glRasterPos3sv(v);
	}

	public void glRasterPos3sv(short[] v, int v_offset) {
		gl.glRasterPos3sv(v, v_offset);
	}

	public void glRasterPos4d(double x, double y, double z, double w) {
		gl.glRasterPos4d(x, y, z, w);
	}

	public void glRasterPos4dv(DoubleBuffer v) {
		gl.glRasterPos4dv(v);
	}

	public void glRasterPos4dv(double[] v, int v_offset) {
		gl.glRasterPos4dv(v, v_offset);
	}

	public void glRasterPos4f(float x, float y, float z, float w) {
		gl.glRasterPos4f(x, y, z, w);
	}

	public void glRasterPos4fv(FloatBuffer v) {
		gl.glRasterPos4fv(v);
	}

	public void glRasterPos4fv(float[] v, int v_offset) {
		gl.glRasterPos4fv(v, v_offset);
	}

	public void glRasterPos4i(int x, int y, int z, int w) {
		gl.glRasterPos4i(x, y, z, w);
	}

	public void glRasterPos4iv(IntBuffer v) {
		gl.glRasterPos4iv(v);
	}

	public void glRasterPos4iv(int[] v, int v_offset) {
		gl.glRasterPos4iv(v, v_offset);
	}

	public void glRasterPos4s(short x, short y, short z, short w) {
		gl.glRasterPos4s(x, y, z, w);
	}

	public void glRasterPos4sv(ShortBuffer v) {
		gl.glRasterPos4sv(v);
	}

	public void glRasterPos4sv(short[] v, int v_offset) {
		gl.glRasterPos4sv(v, v_offset);
	}

	public void glRasterSamplesEXT(int samples, boolean fixedsamplelocations) {
		gl.glRasterSamplesEXT(samples, fixedsamplelocations);
	}

	public void glRectd(double x1, double y1, double x2, double y2) {
		gl.glRectd(x1, y1, x2, y2);
	}

	public void glRectdv(DoubleBuffer v1, DoubleBuffer v2) {
		gl.glRectdv(v1, v2);
	}

	public void glRectdv(double[] v1, int v1_offset, double[] v2, int v2_offset) {
		gl.glRectdv(v1, v1_offset, v2, v2_offset);
	}

	public void glRectf(float x1, float y1, float x2, float y2) {
		gl.glRectf(x1, y1, x2, y2);
	}

	public void glRectfv(FloatBuffer v1, FloatBuffer v2) {
		gl.glRectfv(v1, v2);
	}

	public void glRectfv(float[] v1, int v1_offset, float[] v2, int v2_offset) {
		gl.glRectfv(v1, v1_offset, v2, v2_offset);
	}

	public void glRecti(int x1, int y1, int x2, int y2) {
		gl.glRecti(x1, y1, x2, y2);
	}

	public void glRectiv(IntBuffer v1, IntBuffer v2) {
		gl.glRectiv(v1, v2);
	}

	public void glRectiv(int[] v1, int v1_offset, int[] v2, int v2_offset) {
		gl.glRectiv(v1, v1_offset, v2, v2_offset);
	}

	public void glRects(short x1, short y1, short x2, short y2) {
		gl.glRects(x1, y1, x2, y2);
	}

	public void glRectsv(ShortBuffer v1, ShortBuffer v2) {
		gl.glRectsv(v1, v2);
	}

	public void glRectsv(short[] v1, int v1_offset, short[] v2, int v2_offset) {
		gl.glRectsv(v1, v1_offset, v2, v2_offset);
	}

	public int glRenderMode(int mode) {
		return gl.glRenderMode(mode);
	}

	public void glRenderbufferStorageMultisampleCoverageNV(int target, int coverageSamples, int colorSamples, int internalformat, int width, int height) {
		gl.glRenderbufferStorageMultisampleCoverageNV(target, coverageSamples, colorSamples, internalformat, width, height);
	}

	public void glResetHistogram(int target) {
		gl.glResetHistogram(target);
	}

	public void glResetMinmax(int target) {
		gl.glResetMinmax(target);
	}

	public void glResolveDepthValuesNV() {
		gl.glResolveDepthValuesNV();
	}

	public void glResumeTransformFeedbackNV() {
		gl.glResumeTransformFeedbackNV();
	}

	public void glRotated(double angle, double x, double y, double z) {
		gl.glRotated(angle, x, y, z);
	}

	public void glSampleMaskIndexedNV(int index, int mask) {
		gl.glSampleMaskIndexedNV(index, mask);
	}

	public void glScaled(double x, double y, double z) {
		gl.glScaled(x, y, z);
	}

	public void glSecondaryColor3b(byte red, byte green, byte blue) {
		gl.glSecondaryColor3b(red, green, blue);
	}

	public void glSecondaryColor3bv(ByteBuffer v) {
		gl.glSecondaryColor3bv(v);
	}

	public void glSecondaryColor3bv(byte[] v, int v_offset) {
		gl.glSecondaryColor3bv(v, v_offset);
	}

	public void glSecondaryColor3d(double red, double green, double blue) {
		gl.glSecondaryColor3d(red, green, blue);
	}

	public void glSecondaryColor3dv(DoubleBuffer v) {
		gl.glSecondaryColor3dv(v);
	}

	public void glSecondaryColor3dv(double[] v, int v_offset) {
		gl.glSecondaryColor3dv(v, v_offset);
	}

	public void glSecondaryColor3f(float red, float green, float blue) {
		gl.glSecondaryColor3f(red, green, blue);
	}

	public void glSecondaryColor3fv(FloatBuffer v) {
		gl.glSecondaryColor3fv(v);
	}

	public void glSecondaryColor3fv(float[] v, int v_offset) {
		gl.glSecondaryColor3fv(v, v_offset);
	}

	public void glSecondaryColor3h(short red, short green, short blue) {
		gl.glSecondaryColor3h(red, green, blue);
	}

	public void glSecondaryColor3hv(ShortBuffer v) {
		gl.glSecondaryColor3hv(v);
	}

	public void glSecondaryColor3hv(short[] v, int v_offset) {
		gl.glSecondaryColor3hv(v, v_offset);
	}

	public void glSecondaryColor3i(int red, int green, int blue) {
		gl.glSecondaryColor3i(red, green, blue);
	}

	public void glSecondaryColor3iv(IntBuffer v) {
		gl.glSecondaryColor3iv(v);
	}

	public void glSecondaryColor3iv(int[] v, int v_offset) {
		gl.glSecondaryColor3iv(v, v_offset);
	}

	public void glSecondaryColor3s(short red, short green, short blue) {
		gl.glSecondaryColor3s(red, green, blue);
	}

	public void glSecondaryColor3sv(ShortBuffer v) {
		gl.glSecondaryColor3sv(v);
	}

	public void glSecondaryColor3sv(short[] v, int v_offset) {
		gl.glSecondaryColor3sv(v, v_offset);
	}

	public void glSecondaryColor3ub(byte red, byte green, byte blue) {
		gl.glSecondaryColor3ub(red, green, blue);
	}

	public void glSecondaryColor3ubv(ByteBuffer v) {
		gl.glSecondaryColor3ubv(v);
	}

	public void glSecondaryColor3ubv(byte[] v, int v_offset) {
		gl.glSecondaryColor3ubv(v, v_offset);
	}

	public void glSecondaryColor3ui(int red, int green, int blue) {
		gl.glSecondaryColor3ui(red, green, blue);
	}

	public void glSecondaryColor3uiv(IntBuffer v) {
		gl.glSecondaryColor3uiv(v);
	}

	public void glSecondaryColor3uiv(int[] v, int v_offset) {
		gl.glSecondaryColor3uiv(v, v_offset);
	}

	public void glSecondaryColor3us(short red, short green, short blue) {
		gl.glSecondaryColor3us(red, green, blue);
	}

	public void glSecondaryColor3usv(ShortBuffer v) {
		gl.glSecondaryColor3usv(v);
	}

	public void glSecondaryColor3usv(short[] v, int v_offset) {
		gl.glSecondaryColor3usv(v, v_offset);
	}

	public void glSecondaryColorPointer(int size, int type, int stride, Buffer pointer) {
		gl.glSecondaryColorPointer(size, type, stride, pointer);
	}

	public void glSecondaryColorPointer(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glSecondaryColorPointer(size, type, stride, pointer_buffer_offset);
	}

	public void glSelectBuffer(int size, IntBuffer buffer) {
		gl.glSelectBuffer(size, buffer);
	}

	public void glSelectPerfMonitorCountersAMD(int monitor, boolean enable, int group, int numCounters, IntBuffer counterList) {
		gl.glSelectPerfMonitorCountersAMD(monitor, enable, group, numCounters, counterList);
	}

	public void glSelectPerfMonitorCountersAMD(int monitor, boolean enable, int group, int numCounters, int[] counterList, int counterList_offset) {
		gl.glSelectPerfMonitorCountersAMD(monitor, enable, group, numCounters, counterList, counterList_offset);
	}

	public void glSeparableFilter2D(int target, int internalformat, int width, int height, int format, int type, Buffer row, Buffer column) {
		gl.glSeparableFilter2D(target, internalformat, width, height, format, type, row, column);
	}

	public void glSeparableFilter2D(int target, int internalformat, int width, int height, int format, int type, long row_buffer_offset, long column_buffer_offset) {
		gl.glSeparableFilter2D(target, internalformat, width, height, format, type, row_buffer_offset, column_buffer_offset);
	}

	public void glSetInvariantEXT(int id, int type, Buffer addr) {
		gl.glSetInvariantEXT(id, type, addr);
	}

	public void glSetLocalConstantEXT(int id, int type, Buffer addr) {
		gl.glSetLocalConstantEXT(id, type, addr);
	}

	public void glShaderOp1EXT(int op, int res, int arg1) {
		gl.glShaderOp1EXT(op, res, arg1);
	}

	public void glShaderOp2EXT(int op, int res, int arg1, int arg2) {
		gl.glShaderOp2EXT(op, res, arg1, arg2);
	}

	public void glShaderOp3EXT(int op, int res, int arg1, int arg2, int arg3) {
		gl.glShaderOp3EXT(op, res, arg1, arg2, arg3);
	}

	public void glShaderSourceARB(long shaderObj, int count, String[] string, IntBuffer length) {
		gl.glShaderSourceARB(shaderObj, count, string, length);
	}

	public void glShaderSourceARB(long shaderObj, int count, String[] string, int[] length, int length_offset) {
		gl.glShaderSourceARB(shaderObj, count, string, length, length_offset);
	}

	public void glStencilClearTagEXT(int stencilTagBits, int stencilClearTag) {
		gl.glStencilClearTagEXT(stencilTagBits, stencilClearTag);
	}

	public void glStringMarkerGREMEDY(int len, Buffer string) {
		gl.glStringMarkerGREMEDY(len, string);
	}

	public void glSubpixelPrecisionBiasNV(int xbits, int ybits) {
		gl.glSubpixelPrecisionBiasNV(xbits, ybits);
	}

	public void glSwizzleEXT(int res, int in, int outX, int outY, int outZ, int outW) {
		gl.glSwizzleEXT(res, in, outX, outY, outZ, outW);
	}

	public void glSyncTextureINTEL(int texture) {
		gl.glSyncTextureINTEL(texture);
	}

	public void glTexCoord1bOES(byte s) {
		gl.glTexCoord1bOES(s);
	}

	public void glTexCoord1bvOES(ByteBuffer coords) {
		gl.glTexCoord1bvOES(coords);
	}

	public void glTexCoord1bvOES(byte[] coords, int coords_offset) {
		gl.glTexCoord1bvOES(coords, coords_offset);
	}

	public void glTexCoord1d(double s) {
		gl.glTexCoord1d(s);
	}

	public void glTexCoord1dv(DoubleBuffer v) {
		gl.glTexCoord1dv(v);
	}

	public void glTexCoord1dv(double[] v, int v_offset) {
		gl.glTexCoord1dv(v, v_offset);
	}

	public void glTexCoord1f(float s) {
		gl.glTexCoord1f(s);
	}

	public void glTexCoord1fv(FloatBuffer v) {
		gl.glTexCoord1fv(v);
	}

	public void glTexCoord1fv(float[] v, int v_offset) {
		gl.glTexCoord1fv(v, v_offset);
	}

	public void glTexCoord1h(short s) {
		gl.glTexCoord1h(s);
	}

	public void glTexCoord1hv(ShortBuffer v) {
		gl.glTexCoord1hv(v);
	}

	public void glTexCoord1hv(short[] v, int v_offset) {
		gl.glTexCoord1hv(v, v_offset);
	}

	public void glTexCoord1i(int s) {
		gl.glTexCoord1i(s);
	}

	public void glTexCoord1iv(IntBuffer v) {
		gl.glTexCoord1iv(v);
	}

	public void glTexCoord1iv(int[] v, int v_offset) {
		gl.glTexCoord1iv(v, v_offset);
	}

	public void glTexCoord1s(short s) {
		gl.glTexCoord1s(s);
	}

	public void glTexCoord1sv(ShortBuffer v) {
		gl.glTexCoord1sv(v);
	}

	public void glTexCoord1sv(short[] v, int v_offset) {
		gl.glTexCoord1sv(v, v_offset);
	}

	public void glTexCoord2bOES(byte s, byte t) {
		gl.glTexCoord2bOES(s, t);
	}

	public void glTexCoord2bvOES(ByteBuffer coords) {
		gl.glTexCoord2bvOES(coords);
	}

	public void glTexCoord2bvOES(byte[] coords, int coords_offset) {
		gl.glTexCoord2bvOES(coords, coords_offset);
	}

	public void glTexCoord2d(double s, double t) {
		gl.glTexCoord2d(s, t);
	}

	public void glTexCoord2dv(DoubleBuffer v) {
		gl.glTexCoord2dv(v);
	}

	public void glTexCoord2dv(double[] v, int v_offset) {
		gl.glTexCoord2dv(v, v_offset);
	}

	public void glTexCoord2f(float s, float t) {
		gl.glTexCoord2f(s, t);
	}

	public void glTexCoord2fv(FloatBuffer v) {
		gl.glTexCoord2fv(v);
	}

	public void glTexCoord2fv(float[] v, int v_offset) {
		gl.glTexCoord2fv(v, v_offset);
	}

	public void glTexCoord2h(short s, short t) {
		gl.glTexCoord2h(s, t);
	}

	public void glTexCoord2hv(ShortBuffer v) {
		gl.glTexCoord2hv(v);
	}

	public void glTexCoord2hv(short[] v, int v_offset) {
		gl.glTexCoord2hv(v, v_offset);
	}

	public void glTexCoord2i(int s, int t) {
		gl.glTexCoord2i(s, t);
	}

	public void glTexCoord2iv(IntBuffer v) {
		gl.glTexCoord2iv(v);
	}

	public void glTexCoord2iv(int[] v, int v_offset) {
		gl.glTexCoord2iv(v, v_offset);
	}

	public void glTexCoord2s(short s, short t) {
		gl.glTexCoord2s(s, t);
	}

	public void glTexCoord2sv(ShortBuffer v) {
		gl.glTexCoord2sv(v);
	}

	public void glTexCoord2sv(short[] v, int v_offset) {
		gl.glTexCoord2sv(v, v_offset);
	}

	public void glTexCoord3bOES(byte s, byte t, byte r) {
		gl.glTexCoord3bOES(s, t, r);
	}

	public void glTexCoord3bvOES(ByteBuffer coords) {
		gl.glTexCoord3bvOES(coords);
	}

	public void glTexCoord3bvOES(byte[] coords, int coords_offset) {
		gl.glTexCoord3bvOES(coords, coords_offset);
	}

	public void glTexCoord3d(double s, double t, double r) {
		gl.glTexCoord3d(s, t, r);
	}

	public void glTexCoord3dv(DoubleBuffer v) {
		gl.glTexCoord3dv(v);
	}

	public void glTexCoord3dv(double[] v, int v_offset) {
		gl.glTexCoord3dv(v, v_offset);
	}

	public void glTexCoord3f(float s, float t, float r) {
		gl.glTexCoord3f(s, t, r);
	}

	public void glTexCoord3fv(FloatBuffer v) {
		gl.glTexCoord3fv(v);
	}

	public void glTexCoord3fv(float[] v, int v_offset) {
		gl.glTexCoord3fv(v, v_offset);
	}

	public void glTexCoord3h(short s, short t, short r) {
		gl.glTexCoord3h(s, t, r);
	}

	public void glTexCoord3hv(ShortBuffer v) {
		gl.glTexCoord3hv(v);
	}

	public void glTexCoord3hv(short[] v, int v_offset) {
		gl.glTexCoord3hv(v, v_offset);
	}

	public void glTexCoord3i(int s, int t, int r) {
		gl.glTexCoord3i(s, t, r);
	}

	public void glTexCoord3iv(IntBuffer v) {
		gl.glTexCoord3iv(v);
	}

	public void glTexCoord3iv(int[] v, int v_offset) {
		gl.glTexCoord3iv(v, v_offset);
	}

	public void glTexCoord3s(short s, short t, short r) {
		gl.glTexCoord3s(s, t, r);
	}

	public void glTexCoord3sv(ShortBuffer v) {
		gl.glTexCoord3sv(v);
	}

	public void glTexCoord3sv(short[] v, int v_offset) {
		gl.glTexCoord3sv(v, v_offset);
	}

	public void glTexCoord4bOES(byte s, byte t, byte r, byte q) {
		gl.glTexCoord4bOES(s, t, r, q);
	}

	public void glTexCoord4bvOES(ByteBuffer coords) {
		gl.glTexCoord4bvOES(coords);
	}

	public void glTexCoord4bvOES(byte[] coords, int coords_offset) {
		gl.glTexCoord4bvOES(coords, coords_offset);
	}

	public void glTexCoord4d(double s, double t, double r, double q) {
		gl.glTexCoord4d(s, t, r, q);
	}

	public void glTexCoord4dv(DoubleBuffer v) {
		gl.glTexCoord4dv(v);
	}

	public void glTexCoord4dv(double[] v, int v_offset) {
		gl.glTexCoord4dv(v, v_offset);
	}

	public void glTexCoord4f(float s, float t, float r, float q) {
		gl.glTexCoord4f(s, t, r, q);
	}

	public void glTexCoord4fv(FloatBuffer v) {
		gl.glTexCoord4fv(v);
	}

	public void glTexCoord4fv(float[] v, int v_offset) {
		gl.glTexCoord4fv(v, v_offset);
	}

	public void glTexCoord4h(short s, short t, short r, short q) {
		gl.glTexCoord4h(s, t, r, q);
	}

	public void glTexCoord4hv(ShortBuffer v) {
		gl.glTexCoord4hv(v);
	}

	public void glTexCoord4hv(short[] v, int v_offset) {
		gl.glTexCoord4hv(v, v_offset);
	}

	public void glTexCoord4i(int s, int t, int r, int q) {
		gl.glTexCoord4i(s, t, r, q);
	}

	public void glTexCoord4iv(IntBuffer v) {
		gl.glTexCoord4iv(v);
	}

	public void glTexCoord4iv(int[] v, int v_offset) {
		gl.glTexCoord4iv(v, v_offset);
	}

	public void glTexCoord4s(short s, short t, short r, short q) {
		gl.glTexCoord4s(s, t, r, q);
	}

	public void glTexCoord4sv(ShortBuffer v) {
		gl.glTexCoord4sv(v);
	}

	public void glTexCoord4sv(short[] v, int v_offset) {
		gl.glTexCoord4sv(v, v_offset);
	}

	public void glTexGend(int coord, int pname, double param) {
		gl.glTexGend(coord, pname, param);
	}

	public void glTexGendv(int coord, int pname, DoubleBuffer params) {
		gl.glTexGendv(coord, pname, params);
	}

	public void glTexGendv(int coord, int pname, double[] params, int params_offset) {
		gl.glTexGendv(coord, pname, params, params_offset);
	}

	public void glTexGenf(int coord, int pname, float param) {
		gl.glTexGenf(coord, pname, param);
	}

	public void glTexGenfv(int coord, int pname, FloatBuffer params) {
		gl.glTexGenfv(coord, pname, params);
	}

	public void glTexGenfv(int coord, int pname, float[] params, int params_offset) {
		gl.glTexGenfv(coord, pname, params, params_offset);
	}

	public void glTexGeni(int coord, int pname, int param) {
		gl.glTexGeni(coord, pname, param);
	}

	public void glTexGeniv(int coord, int pname, IntBuffer params) {
		gl.glTexGeniv(coord, pname, params);
	}

	public void glTexGeniv(int coord, int pname, int[] params, int params_offset) {
		gl.glTexGeniv(coord, pname, params, params_offset);
	}

	public void glTexRenderbufferNV(int target, int renderbuffer) {
		gl.glTexRenderbufferNV(target, renderbuffer);
	}

	public void glTexStorageSparseAMD(int target, int internalFormat, int width, int height, int depth, int layers, int flags) {
		gl.glTexStorageSparseAMD(target, internalFormat, width, height, depth, layers, flags);
	}

	public void glTextureBarrierNV() {
		gl.glTextureBarrierNV();
	}

	public void glTextureBufferEXT(int texture, int target, int internalformat, int buffer) {
		gl.glTextureBufferEXT(texture, target, internalformat, buffer);
	}

	public void glTextureBufferRangeEXT(int texture, int target, int internalformat, int buffer, long offset, long size) {
		gl.glTextureBufferRangeEXT(texture, target, internalformat, buffer, offset, size);
	}

	public void glTextureImage1DEXT(int texture, int target, int level, int internalformat, int width, int border, int format, int type, Buffer pixels) {
		gl.glTextureImage1DEXT(texture, target, level, internalformat, width, border, format, type, pixels);
	}

	public void glTextureImage2DEXT(int texture, int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
		gl.glTextureImage2DEXT(texture, target, level, internalformat, width, height, border, format, type, pixels);
	}

	public void glTextureImage3DEXT(int texture, int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, Buffer pixels) {
		gl.glTextureImage3DEXT(texture, target, level, internalformat, width, height, depth, border, format, type, pixels);
	}

	public void glTextureLightEXT(int pname) {
		gl.glTextureLightEXT(pname);
	}

	public void glTextureMaterialEXT(int face, int mode) {
		gl.glTextureMaterialEXT(face, mode);
	}

	public void glTextureNormalEXT(int mode) {
		gl.glTextureNormalEXT(mode);
	}

	public void glTexturePageCommitmentEXT(int texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, boolean resident) {
		gl.glTexturePageCommitmentEXT(texture, level, xoffset, yoffset, zoffset, width, height, depth, resident);
	}

	public void glTextureParameterIivEXT(int texture, int target, int pname, IntBuffer params) {
		gl.glTextureParameterIivEXT(texture, target, pname, params);
	}

	public void glTextureParameterIivEXT(int texture, int target, int pname, int[] params, int params_offset) {
		gl.glTextureParameterIivEXT(texture, target, pname, params, params_offset);
	}

	public void glTextureParameterIuivEXT(int texture, int target, int pname, IntBuffer params) {
		gl.glTextureParameterIuivEXT(texture, target, pname, params);
	}

	public void glTextureParameterIuivEXT(int texture, int target, int pname, int[] params, int params_offset) {
		gl.glTextureParameterIuivEXT(texture, target, pname, params, params_offset);
	}

	public void glTextureParameterfEXT(int texture, int target, int pname, float param) {
		gl.glTextureParameterfEXT(texture, target, pname, param);
	}

	public void glTextureParameterfvEXT(int texture, int target, int pname, FloatBuffer params) {
		gl.glTextureParameterfvEXT(texture, target, pname, params);
	}

	public void glTextureParameterfvEXT(int texture, int target, int pname, float[] params, int params_offset) {
		gl.glTextureParameterfvEXT(texture, target, pname, params, params_offset);
	}

	public void glTextureParameteriEXT(int texture, int target, int pname, int param) {
		gl.glTextureParameteriEXT(texture, target, pname, param);
	}

	public void glTextureParameterivEXT(int texture, int target, int pname, IntBuffer params) {
		gl.glTextureParameterivEXT(texture, target, pname, params);
	}

	public void glTextureParameterivEXT(int texture, int target, int pname, int[] params, int params_offset) {
		gl.glTextureParameterivEXT(texture, target, pname, params, params_offset);
	}

	public void glTextureRangeAPPLE(int target, int length, Buffer pointer) {
		gl.glTextureRangeAPPLE(target, length, pointer);
	}

	public void glTextureRenderbufferEXT(int texture, int target, int renderbuffer) {
		gl.glTextureRenderbufferEXT(texture, target, renderbuffer);
	}

	public void glTextureStorage2DMultisampleEXT(int texture, int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
		gl.glTextureStorage2DMultisampleEXT(texture, target, samples, internalformat, width, height, fixedsamplelocations);
	}

	public void glTextureStorage3DMultisampleEXT(int texture, int target, int samples, int internalformat, int width, int height, int depth, boolean fixedsamplelocations) {
		gl.glTextureStorage3DMultisampleEXT(texture, target, samples, internalformat, width, height, depth, fixedsamplelocations);
	}

	public void glTextureStorageSparseAMD(int texture, int target, int internalFormat, int width, int height, int depth, int layers, int flags) {
		gl.glTextureStorageSparseAMD(texture, target, internalFormat, width, height, depth, layers, flags);
	}

	public void glTextureSubImage1DEXT(int texture, int target, int level, int xoffset, int width, int format, int type, Buffer pixels) {
		gl.glTextureSubImage1DEXT(texture, target, level, xoffset, width, format, type, pixels);
	}

	public void glTextureSubImage2DEXT(int texture, int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer pixels) {
		gl.glTextureSubImage2DEXT(texture, target, level, xoffset, yoffset, width, height, format, type, pixels);
	}

	public void glTextureSubImage3DEXT(int texture, int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer pixels) {
		gl.glTextureSubImage3DEXT(texture, target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
	}

	public void glTranslated(double x, double y, double z) {
		gl.glTranslated(x, y, z);
	}

	public void glUniform1fARB(int location, float v0) {
		gl.glUniform1fARB(location, v0);
	}

	public void glUniform1fvARB(int location, int count, FloatBuffer value) {
		gl.glUniform1fvARB(location, count, value);
	}

	public void glUniform1fvARB(int location, int count, float[] value, int value_offset) {
		gl.glUniform1fvARB(location, count, value, value_offset);
	}

	public void glUniform1i64NV(int location, long x) {
		gl.glUniform1i64NV(location, x);
	}

	public void glUniform1i64vNV(int location, int count, LongBuffer value) {
		gl.glUniform1i64vNV(location, count, value);
	}

	public void glUniform1i64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform1i64vNV(location, count, value, value_offset);
	}

	public void glUniform1iARB(int location, int v0) {
		gl.glUniform1iARB(location, v0);
	}

	public void glUniform1ivARB(int location, int count, IntBuffer value) {
		gl.glUniform1ivARB(location, count, value);
	}

	public void glUniform1ivARB(int location, int count, int[] value, int value_offset) {
		gl.glUniform1ivARB(location, count, value, value_offset);
	}

	public void glUniform1ui64NV(int location, long x) {
		gl.glUniform1ui64NV(location, x);
	}

	public void glUniform1ui64vNV(int location, int count, LongBuffer value) {
		gl.glUniform1ui64vNV(location, count, value);
	}

	public void glUniform1ui64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform1ui64vNV(location, count, value, value_offset);
	}

	public void glUniform2fARB(int location, float v0, float v1) {
		gl.glUniform2fARB(location, v0, v1);
	}

	public void glUniform2fvARB(int location, int count, FloatBuffer value) {
		gl.glUniform2fvARB(location, count, value);
	}

	public void glUniform2fvARB(int location, int count, float[] value, int value_offset) {
		gl.glUniform2fvARB(location, count, value, value_offset);
	}

	public void glUniform2i64NV(int location, long x, long y) {
		gl.glUniform2i64NV(location, x, y);
	}

	public void glUniform2i64vNV(int location, int count, LongBuffer value) {
		gl.glUniform2i64vNV(location, count, value);
	}

	public void glUniform2i64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform2i64vNV(location, count, value, value_offset);
	}

	public void glUniform2iARB(int location, int v0, int v1) {
		gl.glUniform2iARB(location, v0, v1);
	}

	public void glUniform2ivARB(int location, int count, IntBuffer value) {
		gl.glUniform2ivARB(location, count, value);
	}

	public void glUniform2ivARB(int location, int count, int[] value, int value_offset) {
		gl.glUniform2ivARB(location, count, value, value_offset);
	}

	public void glUniform2ui64NV(int location, long x, long y) {
		gl.glUniform2ui64NV(location, x, y);
	}

	public void glUniform2ui64vNV(int location, int count, LongBuffer value) {
		gl.glUniform2ui64vNV(location, count, value);
	}

	public void glUniform2ui64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform2ui64vNV(location, count, value, value_offset);
	}

	public void glUniform3fARB(int location, float v0, float v1, float v2) {
		gl.glUniform3fARB(location, v0, v1, v2);
	}

	public void glUniform3fvARB(int location, int count, FloatBuffer value) {
		gl.glUniform3fvARB(location, count, value);
	}

	public void glUniform3fvARB(int location, int count, float[] value, int value_offset) {
		gl.glUniform3fvARB(location, count, value, value_offset);
	}

	public void glUniform3i64NV(int location, long x, long y, long z) {
		gl.glUniform3i64NV(location, x, y, z);
	}

	public void glUniform3i64vNV(int location, int count, LongBuffer value) {
		gl.glUniform3i64vNV(location, count, value);
	}

	public void glUniform3i64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform3i64vNV(location, count, value, value_offset);
	}

	public void glUniform3iARB(int location, int v0, int v1, int v2) {
		gl.glUniform3iARB(location, v0, v1, v2);
	}

	public void glUniform3ivARB(int location, int count, IntBuffer value) {
		gl.glUniform3ivARB(location, count, value);
	}

	public void glUniform3ivARB(int location, int count, int[] value, int value_offset) {
		gl.glUniform3ivARB(location, count, value, value_offset);
	}

	public void glUniform3ui64NV(int location, long x, long y, long z) {
		gl.glUniform3ui64NV(location, x, y, z);
	}

	public void glUniform3ui64vNV(int location, int count, LongBuffer value) {
		gl.glUniform3ui64vNV(location, count, value);
	}

	public void glUniform3ui64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform3ui64vNV(location, count, value, value_offset);
	}

	public void glUniform4fARB(int location, float v0, float v1, float v2, float v3) {
		gl.glUniform4fARB(location, v0, v1, v2, v3);
	}

	public void glUniform4fvARB(int location, int count, FloatBuffer value) {
		gl.glUniform4fvARB(location, count, value);
	}

	public void glUniform4fvARB(int location, int count, float[] value, int value_offset) {
		gl.glUniform4fvARB(location, count, value, value_offset);
	}

	public void glUniform4i64NV(int location, long x, long y, long z, long w) {
		gl.glUniform4i64NV(location, x, y, z, w);
	}

	public void glUniform4i64vNV(int location, int count, LongBuffer value) {
		gl.glUniform4i64vNV(location, count, value);
	}

	public void glUniform4i64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform4i64vNV(location, count, value, value_offset);
	}

	public void glUniform4iARB(int location, int v0, int v1, int v2, int v3) {
		gl.glUniform4iARB(location, v0, v1, v2, v3);
	}

	public void glUniform4ivARB(int location, int count, IntBuffer value) {
		gl.glUniform4ivARB(location, count, value);
	}

	public void glUniform4ivARB(int location, int count, int[] value, int value_offset) {
		gl.glUniform4ivARB(location, count, value, value_offset);
	}

	public void glUniform4ui64NV(int location, long x, long y, long z, long w) {
		gl.glUniform4ui64NV(location, x, y, z, w);
	}

	public void glUniform4ui64vNV(int location, int count, LongBuffer value) {
		gl.glUniform4ui64vNV(location, count, value);
	}

	public void glUniform4ui64vNV(int location, int count, long[] value, int value_offset) {
		gl.glUniform4ui64vNV(location, count, value, value_offset);
	}

	public void glUniformBufferEXT(int program, int location, int buffer) {
		gl.glUniformBufferEXT(program, location, buffer);
	}

	public void glUniformMatrix2fvARB(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix2fvARB(location, count, transpose, value);
	}

	public void glUniformMatrix2fvARB(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix2fvARB(location, count, transpose, value, value_offset);
	}

	public void glUniformMatrix3fvARB(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix3fvARB(location, count, transpose, value);
	}

	public void glUniformMatrix3fvARB(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix3fvARB(location, count, transpose, value, value_offset);
	}

	public void glUniformMatrix4fvARB(int location, int count, boolean transpose, FloatBuffer value) {
		gl.glUniformMatrix4fvARB(location, count, transpose, value);
	}

	public void glUniformMatrix4fvARB(int location, int count, boolean transpose, float[] value, int value_offset) {
		gl.glUniformMatrix4fvARB(location, count, transpose, value, value_offset);
	}

	public void glUnlockArraysEXT() {
		gl.glUnlockArraysEXT();
	}

	public boolean glUnmapNamedBufferEXT(int buffer) {
		return gl.glUnmapNamedBufferEXT(buffer);
	}

	public void glUnmapTexture2DINTEL(int texture, int level) {
		gl.glUnmapTexture2DINTEL(texture, level);
	}

	public void glUseProgramObjectARB(long programObj) {
		gl.glUseProgramObjectARB(programObj);
	}

	public void glVDPAUFiniNV() {
		gl.glVDPAUFiniNV();
	}

	public void glVDPAUGetSurfaceivNV(long surface, int pname, int bufSize, IntBuffer length, IntBuffer values) {
		gl.glVDPAUGetSurfaceivNV(surface, pname, bufSize, length, values);
	}

	public void glVDPAUGetSurfaceivNV(long surface, int pname, int bufSize, int[] length, int length_offset, int[] values, int values_offset) {
		gl.glVDPAUGetSurfaceivNV(surface, pname, bufSize, length, length_offset, values, values_offset);
	}

	public void glVDPAUInitNV(Buffer vdpDevice, Buffer getProcAddress) {
		gl.glVDPAUInitNV(vdpDevice, getProcAddress);
	}

	public boolean glVDPAUIsSurfaceNV(long surface) {
		return gl.glVDPAUIsSurfaceNV(surface);
	}

	public void glVDPAUMapSurfacesNV(int numSurfaces, PointerBuffer surfaces) {
		gl.glVDPAUMapSurfacesNV(numSurfaces, surfaces);
	}

	public long glVDPAURegisterOutputSurfaceNV(Buffer vdpSurface, int target, int numTextureNames, IntBuffer textureNames) {
		return gl.glVDPAURegisterOutputSurfaceNV(vdpSurface, target, numTextureNames, textureNames);
	}

	public long glVDPAURegisterOutputSurfaceNV(Buffer vdpSurface, int target, int numTextureNames, int[] textureNames, int textureNames_offset) {
		return gl.glVDPAURegisterOutputSurfaceNV(vdpSurface, target, numTextureNames, textureNames, textureNames_offset);
	}

	public long glVDPAURegisterVideoSurfaceNV(Buffer vdpSurface, int target, int numTextureNames, IntBuffer textureNames) {
		return gl.glVDPAURegisterVideoSurfaceNV(vdpSurface, target, numTextureNames, textureNames);
	}

	public long glVDPAURegisterVideoSurfaceNV(Buffer vdpSurface, int target, int numTextureNames, int[] textureNames, int textureNames_offset) {
		return gl.glVDPAURegisterVideoSurfaceNV(vdpSurface, target, numTextureNames, textureNames, textureNames_offset);
	}

	public void glVDPAUSurfaceAccessNV(long surface, int access) {
		gl.glVDPAUSurfaceAccessNV(surface, access);
	}

	public void glVDPAUUnmapSurfacesNV(int numSurface, PointerBuffer surfaces) {
		gl.glVDPAUUnmapSurfacesNV(numSurface, surfaces);
	}

	public void glVDPAUUnregisterSurfaceNV(long surface) {
		gl.glVDPAUUnregisterSurfaceNV(surface);
	}

	public void glValidateProgramARB(long programObj) {
		gl.glValidateProgramARB(programObj);
	}

	public void glVariantPointerEXT(int id, int type, int stride, Buffer addr) {
		gl.glVariantPointerEXT(id, type, stride, addr);
	}

	public void glVariantPointerEXT(int id, int type, int stride, long addr_buffer_offset) {
		gl.glVariantPointerEXT(id, type, stride, addr_buffer_offset);
	}

	public void glVariantbvEXT(int id, ByteBuffer addr) {
		gl.glVariantbvEXT(id, addr);
	}

	public void glVariantbvEXT(int id, byte[] addr, int addr_offset) {
		gl.glVariantbvEXT(id, addr, addr_offset);
	}

	public void glVariantdvEXT(int id, DoubleBuffer addr) {
		gl.glVariantdvEXT(id, addr);
	}

	public void glVariantdvEXT(int id, double[] addr, int addr_offset) {
		gl.glVariantdvEXT(id, addr, addr_offset);
	}

	public void glVariantfvEXT(int id, FloatBuffer addr) {
		gl.glVariantfvEXT(id, addr);
	}

	public void glVariantfvEXT(int id, float[] addr, int addr_offset) {
		gl.glVariantfvEXT(id, addr, addr_offset);
	}

	public void glVariantivEXT(int id, IntBuffer addr) {
		gl.glVariantivEXT(id, addr);
	}

	public void glVariantivEXT(int id, int[] addr, int addr_offset) {
		gl.glVariantivEXT(id, addr, addr_offset);
	}

	public void glVariantsvEXT(int id, ShortBuffer addr) {
		gl.glVariantsvEXT(id, addr);
	}

	public void glVariantsvEXT(int id, short[] addr, int addr_offset) {
		gl.glVariantsvEXT(id, addr, addr_offset);
	}

	public void glVariantubvEXT(int id, ByteBuffer addr) {
		gl.glVariantubvEXT(id, addr);
	}

	public void glVariantubvEXT(int id, byte[] addr, int addr_offset) {
		gl.glVariantubvEXT(id, addr, addr_offset);
	}

	public void glVariantuivEXT(int id, IntBuffer addr) {
		gl.glVariantuivEXT(id, addr);
	}

	public void glVariantuivEXT(int id, int[] addr, int addr_offset) {
		gl.glVariantuivEXT(id, addr, addr_offset);
	}

	public void glVariantusvEXT(int id, ShortBuffer addr) {
		gl.glVariantusvEXT(id, addr);
	}

	public void glVariantusvEXT(int id, short[] addr, int addr_offset) {
		gl.glVariantusvEXT(id, addr, addr_offset);
	}

	public void glVertex2bOES(byte x, byte y) {
		gl.glVertex2bOES(x, y);
	}

	public void glVertex2bvOES(ByteBuffer coords) {
		gl.glVertex2bvOES(coords);
	}

	public void glVertex2bvOES(byte[] coords, int coords_offset) {
		gl.glVertex2bvOES(coords, coords_offset);
	}

	public void glVertex2d(double x, double y) {
		gl.glVertex2d(x, y);
	}

	public void glVertex2dv(DoubleBuffer v) {
		gl.glVertex2dv(v);
	}

	public void glVertex2dv(double[] v, int v_offset) {
		gl.glVertex2dv(v, v_offset);
	}

	public void glVertex2f(float x, float y) {
		gl.glVertex2f(x, y);
	}

	public void glVertex2fv(FloatBuffer v) {
		gl.glVertex2fv(v);
	}

	public void glVertex2fv(float[] v, int v_offset) {
		gl.glVertex2fv(v, v_offset);
	}

	public void glVertex2h(short x, short y) {
		gl.glVertex2h(x, y);
	}

	public void glVertex2hv(ShortBuffer v) {
		gl.glVertex2hv(v);
	}

	public void glVertex2hv(short[] v, int v_offset) {
		gl.glVertex2hv(v, v_offset);
	}

	public void glVertex2i(int x, int y) {
		gl.glVertex2i(x, y);
	}

	public void glVertex2iv(IntBuffer v) {
		gl.glVertex2iv(v);
	}

	public void glVertex2iv(int[] v, int v_offset) {
		gl.glVertex2iv(v, v_offset);
	}

	public void glVertex2s(short x, short y) {
		gl.glVertex2s(x, y);
	}

	public void glVertex2sv(ShortBuffer v) {
		gl.glVertex2sv(v);
	}

	public void glVertex2sv(short[] v, int v_offset) {
		gl.glVertex2sv(v, v_offset);
	}

	public void glVertex3bOES(byte x, byte y, byte z) {
		gl.glVertex3bOES(x, y, z);
	}

	public void glVertex3bvOES(ByteBuffer coords) {
		gl.glVertex3bvOES(coords);
	}

	public void glVertex3bvOES(byte[] coords, int coords_offset) {
		gl.glVertex3bvOES(coords, coords_offset);
	}

	public void glVertex3d(double x, double y, double z) {
		gl.glVertex3d(x, y, z);
	}

	public void glVertex3dv(DoubleBuffer v) {
		gl.glVertex3dv(v);
	}

	public void glVertex3dv(double[] v, int v_offset) {
		gl.glVertex3dv(v, v_offset);
	}

	public void glVertex3f(float x, float y, float z) {
		gl.glVertex3f(x, y, z);
	}

	public void glVertex3fv(FloatBuffer v) {
		gl.glVertex3fv(v);
	}

	public void glVertex3fv(float[] v, int v_offset) {
		gl.glVertex3fv(v, v_offset);
	}

	public void glVertex3h(short x, short y, short z) {
		gl.glVertex3h(x, y, z);
	}

	public void glVertex3hv(ShortBuffer v) {
		gl.glVertex3hv(v);
	}

	public void glVertex3hv(short[] v, int v_offset) {
		gl.glVertex3hv(v, v_offset);
	}

	public void glVertex3i(int x, int y, int z) {
		gl.glVertex3i(x, y, z);
	}

	public void glVertex3iv(IntBuffer v) {
		gl.glVertex3iv(v);
	}

	public void glVertex3iv(int[] v, int v_offset) {
		gl.glVertex3iv(v, v_offset);
	}

	public void glVertex3s(short x, short y, short z) {
		gl.glVertex3s(x, y, z);
	}

	public void glVertex3sv(ShortBuffer v) {
		gl.glVertex3sv(v);
	}

	public void glVertex3sv(short[] v, int v_offset) {
		gl.glVertex3sv(v, v_offset);
	}

	public void glVertex4bOES(byte x, byte y, byte z, byte w) {
		gl.glVertex4bOES(x, y, z, w);
	}

	public void glVertex4bvOES(ByteBuffer coords) {
		gl.glVertex4bvOES(coords);
	}

	public void glVertex4bvOES(byte[] coords, int coords_offset) {
		gl.glVertex4bvOES(coords, coords_offset);
	}

	public void glVertex4d(double x, double y, double z, double w) {
		gl.glVertex4d(x, y, z, w);
	}

	public void glVertex4dv(DoubleBuffer v) {
		gl.glVertex4dv(v);
	}

	public void glVertex4dv(double[] v, int v_offset) {
		gl.glVertex4dv(v, v_offset);
	}

	public void glVertex4f(float x, float y, float z, float w) {
		gl.glVertex4f(x, y, z, w);
	}

	public void glVertex4fv(FloatBuffer v) {
		gl.glVertex4fv(v);
	}

	public void glVertex4fv(float[] v, int v_offset) {
		gl.glVertex4fv(v, v_offset);
	}

	public void glVertex4h(short x, short y, short z, short w) {
		gl.glVertex4h(x, y, z, w);
	}

	public void glVertex4hv(ShortBuffer v) {
		gl.glVertex4hv(v);
	}

	public void glVertex4hv(short[] v, int v_offset) {
		gl.glVertex4hv(v, v_offset);
	}

	public void glVertex4i(int x, int y, int z, int w) {
		gl.glVertex4i(x, y, z, w);
	}

	public void glVertex4iv(IntBuffer v) {
		gl.glVertex4iv(v);
	}

	public void glVertex4iv(int[] v, int v_offset) {
		gl.glVertex4iv(v, v_offset);
	}

	public void glVertex4s(short x, short y, short z, short w) {
		gl.glVertex4s(x, y, z, w);
	}

	public void glVertex4sv(ShortBuffer v) {
		gl.glVertex4sv(v);
	}

	public void glVertex4sv(short[] v, int v_offset) {
		gl.glVertex4sv(v, v_offset);
	}

	public void glVertexArrayBindVertexBufferEXT(int vaobj, int bindingindex, int buffer, long offset, int stride) {
		gl.glVertexArrayBindVertexBufferEXT(vaobj, bindingindex, buffer, offset, stride);
	}

	public void glVertexArrayColorOffsetEXT(int vaobj, int buffer, int size, int type, int stride, long offset) {
		gl.glVertexArrayColorOffsetEXT(vaobj, buffer, size, type, stride, offset);
	}

	public void glVertexArrayEdgeFlagOffsetEXT(int vaobj, int buffer, int stride, long offset) {
		gl.glVertexArrayEdgeFlagOffsetEXT(vaobj, buffer, stride, offset);
	}

	public void glVertexArrayFogCoordOffsetEXT(int vaobj, int buffer, int type, int stride, long offset) {
		gl.glVertexArrayFogCoordOffsetEXT(vaobj, buffer, type, stride, offset);
	}

	public void glVertexArrayIndexOffsetEXT(int vaobj, int buffer, int type, int stride, long offset) {
		gl.glVertexArrayIndexOffsetEXT(vaobj, buffer, type, stride, offset);
	}

	public void glVertexArrayMultiTexCoordOffsetEXT(int vaobj, int buffer, int texunit, int size, int type, int stride, long offset) {
		gl.glVertexArrayMultiTexCoordOffsetEXT(vaobj, buffer, texunit, size, type, stride, offset);
	}

	public void glVertexArrayNormalOffsetEXT(int vaobj, int buffer, int type, int stride, long offset) {
		gl.glVertexArrayNormalOffsetEXT(vaobj, buffer, type, stride, offset);
	}

	public void glVertexArrayParameteriAPPLE(int pname, int param) {
		gl.glVertexArrayParameteriAPPLE(pname, param);
	}

	public void glVertexArrayRangeAPPLE(int length, Buffer pointer) {
		gl.glVertexArrayRangeAPPLE(length, pointer);
	}

	public void glVertexArraySecondaryColorOffsetEXT(int vaobj, int buffer, int size, int type, int stride, long offset) {
		gl.glVertexArraySecondaryColorOffsetEXT(vaobj, buffer, size, type, stride, offset);
	}

	public void glVertexArrayTexCoordOffsetEXT(int vaobj, int buffer, int size, int type, int stride, long offset) {
		gl.glVertexArrayTexCoordOffsetEXT(vaobj, buffer, size, type, stride, offset);
	}

	public void glVertexArrayVertexAttribBindingEXT(int vaobj, int attribindex, int bindingindex) {
		gl.glVertexArrayVertexAttribBindingEXT(vaobj, attribindex, bindingindex);
	}

	public void glVertexArrayVertexAttribDivisorEXT(int vaobj, int index, int divisor) {
		gl.glVertexArrayVertexAttribDivisorEXT(vaobj, index, divisor);
	}

	public void glVertexArrayVertexAttribFormatEXT(int vaobj, int attribindex, int size, int type, boolean normalized, int relativeoffset) {
		gl.glVertexArrayVertexAttribFormatEXT(vaobj, attribindex, size, type, normalized, relativeoffset);
	}

	public void glVertexArrayVertexAttribIFormatEXT(int vaobj, int attribindex, int size, int type, int relativeoffset) {
		gl.glVertexArrayVertexAttribIFormatEXT(vaobj, attribindex, size, type, relativeoffset);
	}

	public void glVertexArrayVertexAttribIOffsetEXT(int vaobj, int buffer, int index, int size, int type, int stride, long offset) {
		gl.glVertexArrayVertexAttribIOffsetEXT(vaobj, buffer, index, size, type, stride, offset);
	}

	public void glVertexArrayVertexAttribLFormatEXT(int vaobj, int attribindex, int size, int type, int relativeoffset) {
		gl.glVertexArrayVertexAttribLFormatEXT(vaobj, attribindex, size, type, relativeoffset);
	}

	public void glVertexArrayVertexAttribLOffsetEXT(int vaobj, int buffer, int index, int size, int type, int stride, long offset) {
		gl.glVertexArrayVertexAttribLOffsetEXT(vaobj, buffer, index, size, type, stride, offset);
	}

	public void glVertexArrayVertexAttribOffsetEXT(int vaobj, int buffer, int index, int size, int type, boolean normalized, int stride, long offset) {
		gl.glVertexArrayVertexAttribOffsetEXT(vaobj, buffer, index, size, type, normalized, stride, offset);
	}

	public void glVertexArrayVertexBindingDivisorEXT(int vaobj, int bindingindex, int divisor) {
		gl.glVertexArrayVertexBindingDivisorEXT(vaobj, bindingindex, divisor);
	}

	public void glVertexArrayVertexOffsetEXT(int vaobj, int buffer, int size, int type, int stride, long offset) {
		gl.glVertexArrayVertexOffsetEXT(vaobj, buffer, size, type, stride, offset);
	}

	public void glVertexAttrib1dARB(int index, double x) {
		gl.glVertexAttrib1dARB(index, x);
	}

	public void glVertexAttrib1dvARB(int index, DoubleBuffer v) {
		gl.glVertexAttrib1dvARB(index, v);
	}

	public void glVertexAttrib1dvARB(int index, double[] v, int v_offset) {
		gl.glVertexAttrib1dvARB(index, v, v_offset);
	}

	public void glVertexAttrib1fARB(int index, float x) {
		gl.glVertexAttrib1fARB(index, x);
	}

	public void glVertexAttrib1fvARB(int index, FloatBuffer v) {
		gl.glVertexAttrib1fvARB(index, v);
	}

	public void glVertexAttrib1fvARB(int index, float[] v, int v_offset) {
		gl.glVertexAttrib1fvARB(index, v, v_offset);
	}

	public void glVertexAttrib1h(int index, short x) {
		gl.glVertexAttrib1h(index, x);
	}

	public void glVertexAttrib1hv(int index, ShortBuffer v) {
		gl.glVertexAttrib1hv(index, v);
	}

	public void glVertexAttrib1hv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib1hv(index, v, v_offset);
	}

	public void glVertexAttrib1sARB(int index, short x) {
		gl.glVertexAttrib1sARB(index, x);
	}

	public void glVertexAttrib1svARB(int index, ShortBuffer v) {
		gl.glVertexAttrib1svARB(index, v);
	}

	public void glVertexAttrib1svARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib1svARB(index, v, v_offset);
	}

	public void glVertexAttrib2dARB(int index, double x, double y) {
		gl.glVertexAttrib2dARB(index, x, y);
	}

	public void glVertexAttrib2dvARB(int index, DoubleBuffer v) {
		gl.glVertexAttrib2dvARB(index, v);
	}

	public void glVertexAttrib2dvARB(int index, double[] v, int v_offset) {
		gl.glVertexAttrib2dvARB(index, v, v_offset);
	}

	public void glVertexAttrib2fARB(int index, float x, float y) {
		gl.glVertexAttrib2fARB(index, x, y);
	}

	public void glVertexAttrib2fvARB(int index, FloatBuffer v) {
		gl.glVertexAttrib2fvARB(index, v);
	}

	public void glVertexAttrib2fvARB(int index, float[] v, int v_offset) {
		gl.glVertexAttrib2fvARB(index, v, v_offset);
	}

	public void glVertexAttrib2h(int index, short x, short y) {
		gl.glVertexAttrib2h(index, x, y);
	}

	public void glVertexAttrib2hv(int index, ShortBuffer v) {
		gl.glVertexAttrib2hv(index, v);
	}

	public void glVertexAttrib2hv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib2hv(index, v, v_offset);
	}

	public void glVertexAttrib2sARB(int index, short x, short y) {
		gl.glVertexAttrib2sARB(index, x, y);
	}

	public void glVertexAttrib2svARB(int index, ShortBuffer v) {
		gl.glVertexAttrib2svARB(index, v);
	}

	public void glVertexAttrib2svARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib2svARB(index, v, v_offset);
	}

	public void glVertexAttrib3dARB(int index, double x, double y, double z) {
		gl.glVertexAttrib3dARB(index, x, y, z);
	}

	public void glVertexAttrib3dvARB(int index, DoubleBuffer v) {
		gl.glVertexAttrib3dvARB(index, v);
	}

	public void glVertexAttrib3dvARB(int index, double[] v, int v_offset) {
		gl.glVertexAttrib3dvARB(index, v, v_offset);
	}

	public void glVertexAttrib3fARB(int index, float x, float y, float z) {
		gl.glVertexAttrib3fARB(index, x, y, z);
	}

	public void glVertexAttrib3fvARB(int index, FloatBuffer v) {
		gl.glVertexAttrib3fvARB(index, v);
	}

	public void glVertexAttrib3fvARB(int index, float[] v, int v_offset) {
		gl.glVertexAttrib3fvARB(index, v, v_offset);
	}

	public void glVertexAttrib3h(int index, short x, short y, short z) {
		gl.glVertexAttrib3h(index, x, y, z);
	}

	public void glVertexAttrib3hv(int index, ShortBuffer v) {
		gl.glVertexAttrib3hv(index, v);
	}

	public void glVertexAttrib3hv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib3hv(index, v, v_offset);
	}

	public void glVertexAttrib3sARB(int index, short x, short y, short z) {
		gl.glVertexAttrib3sARB(index, x, y, z);
	}

	public void glVertexAttrib3svARB(int index, ShortBuffer v) {
		gl.glVertexAttrib3svARB(index, v);
	}

	public void glVertexAttrib3svARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib3svARB(index, v, v_offset);
	}

	public void glVertexAttrib4NbvARB(int index, ByteBuffer v) {
		gl.glVertexAttrib4NbvARB(index, v);
	}

	public void glVertexAttrib4NbvARB(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4NbvARB(index, v, v_offset);
	}

	public void glVertexAttrib4NivARB(int index, IntBuffer v) {
		gl.glVertexAttrib4NivARB(index, v);
	}

	public void glVertexAttrib4NivARB(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4NivARB(index, v, v_offset);
	}

	public void glVertexAttrib4NsvARB(int index, ShortBuffer v) {
		gl.glVertexAttrib4NsvARB(index, v);
	}

	public void glVertexAttrib4NsvARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4NsvARB(index, v, v_offset);
	}

	public void glVertexAttrib4NubARB(int index, byte x, byte y, byte z, byte w) {
		gl.glVertexAttrib4NubARB(index, x, y, z, w);
	}

	public void glVertexAttrib4NubvARB(int index, ByteBuffer v) {
		gl.glVertexAttrib4NubvARB(index, v);
	}

	public void glVertexAttrib4NubvARB(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4NubvARB(index, v, v_offset);
	}

	public void glVertexAttrib4NuivARB(int index, IntBuffer v) {
		gl.glVertexAttrib4NuivARB(index, v);
	}

	public void glVertexAttrib4NuivARB(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4NuivARB(index, v, v_offset);
	}

	public void glVertexAttrib4NusvARB(int index, ShortBuffer v) {
		gl.glVertexAttrib4NusvARB(index, v);
	}

	public void glVertexAttrib4NusvARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4NusvARB(index, v, v_offset);
	}

	public void glVertexAttrib4bvARB(int index, ByteBuffer v) {
		gl.glVertexAttrib4bvARB(index, v);
	}

	public void glVertexAttrib4bvARB(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4bvARB(index, v, v_offset);
	}

	public void glVertexAttrib4dARB(int index, double x, double y, double z, double w) {
		gl.glVertexAttrib4dARB(index, x, y, z, w);
	}

	public void glVertexAttrib4dvARB(int index, DoubleBuffer v) {
		gl.glVertexAttrib4dvARB(index, v);
	}

	public void glVertexAttrib4dvARB(int index, double[] v, int v_offset) {
		gl.glVertexAttrib4dvARB(index, v, v_offset);
	}

	public void glVertexAttrib4fARB(int index, float x, float y, float z, float w) {
		gl.glVertexAttrib4fARB(index, x, y, z, w);
	}

	public void glVertexAttrib4fvARB(int index, FloatBuffer v) {
		gl.glVertexAttrib4fvARB(index, v);
	}

	public void glVertexAttrib4fvARB(int index, float[] v, int v_offset) {
		gl.glVertexAttrib4fvARB(index, v, v_offset);
	}

	public void glVertexAttrib4h(int index, short x, short y, short z, short w) {
		gl.glVertexAttrib4h(index, x, y, z, w);
	}

	public void glVertexAttrib4hv(int index, ShortBuffer v) {
		gl.glVertexAttrib4hv(index, v);
	}

	public void glVertexAttrib4hv(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4hv(index, v, v_offset);
	}

	public void glVertexAttrib4ivARB(int index, IntBuffer v) {
		gl.glVertexAttrib4ivARB(index, v);
	}

	public void glVertexAttrib4ivARB(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4ivARB(index, v, v_offset);
	}

	public void glVertexAttrib4sARB(int index, short x, short y, short z, short w) {
		gl.glVertexAttrib4sARB(index, x, y, z, w);
	}

	public void glVertexAttrib4svARB(int index, ShortBuffer v) {
		gl.glVertexAttrib4svARB(index, v);
	}

	public void glVertexAttrib4svARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4svARB(index, v, v_offset);
	}

	public void glVertexAttrib4ubvARB(int index, ByteBuffer v) {
		gl.glVertexAttrib4ubvARB(index, v);
	}

	public void glVertexAttrib4ubvARB(int index, byte[] v, int v_offset) {
		gl.glVertexAttrib4ubvARB(index, v, v_offset);
	}

	public void glVertexAttrib4uivARB(int index, IntBuffer v) {
		gl.glVertexAttrib4uivARB(index, v);
	}

	public void glVertexAttrib4uivARB(int index, int[] v, int v_offset) {
		gl.glVertexAttrib4uivARB(index, v, v_offset);
	}

	public void glVertexAttrib4usvARB(int index, ShortBuffer v) {
		gl.glVertexAttrib4usvARB(index, v);
	}

	public void glVertexAttrib4usvARB(int index, short[] v, int v_offset) {
		gl.glVertexAttrib4usvARB(index, v, v_offset);
	}

	public void glVertexAttribI1iEXT(int index, int x) {
		gl.glVertexAttribI1iEXT(index, x);
	}

	public void glVertexAttribI1ivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI1ivEXT(index, v);
	}

	public void glVertexAttribI1ivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI1ivEXT(index, v, v_offset);
	}

	public void glVertexAttribI1uiEXT(int index, int x) {
		gl.glVertexAttribI1uiEXT(index, x);
	}

	public void glVertexAttribI1uivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI1uivEXT(index, v);
	}

	public void glVertexAttribI1uivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI1uivEXT(index, v, v_offset);
	}

	public void glVertexAttribI2iEXT(int index, int x, int y) {
		gl.glVertexAttribI2iEXT(index, x, y);
	}

	public void glVertexAttribI2ivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI2ivEXT(index, v);
	}

	public void glVertexAttribI2ivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI2ivEXT(index, v, v_offset);
	}

	public void glVertexAttribI2uiEXT(int index, int x, int y) {
		gl.glVertexAttribI2uiEXT(index, x, y);
	}

	public void glVertexAttribI2uivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI2uivEXT(index, v);
	}

	public void glVertexAttribI2uivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI2uivEXT(index, v, v_offset);
	}

	public void glVertexAttribI3iEXT(int index, int x, int y, int z) {
		gl.glVertexAttribI3iEXT(index, x, y, z);
	}

	public void glVertexAttribI3ivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI3ivEXT(index, v);
	}

	public void glVertexAttribI3ivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI3ivEXT(index, v, v_offset);
	}

	public void glVertexAttribI3uiEXT(int index, int x, int y, int z) {
		gl.glVertexAttribI3uiEXT(index, x, y, z);
	}

	public void glVertexAttribI3uivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI3uivEXT(index, v);
	}

	public void glVertexAttribI3uivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI3uivEXT(index, v, v_offset);
	}

	public void glVertexAttribI4bvEXT(int index, ByteBuffer v) {
		gl.glVertexAttribI4bvEXT(index, v);
	}

	public void glVertexAttribI4bvEXT(int index, byte[] v, int v_offset) {
		gl.glVertexAttribI4bvEXT(index, v, v_offset);
	}

	public void glVertexAttribI4iEXT(int index, int x, int y, int z, int w) {
		gl.glVertexAttribI4iEXT(index, x, y, z, w);
	}

	public void glVertexAttribI4ivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI4ivEXT(index, v);
	}

	public void glVertexAttribI4ivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI4ivEXT(index, v, v_offset);
	}

	public void glVertexAttribI4svEXT(int index, ShortBuffer v) {
		gl.glVertexAttribI4svEXT(index, v);
	}

	public void glVertexAttribI4svEXT(int index, short[] v, int v_offset) {
		gl.glVertexAttribI4svEXT(index, v, v_offset);
	}

	public void glVertexAttribI4ubvEXT(int index, ByteBuffer v) {
		gl.glVertexAttribI4ubvEXT(index, v);
	}

	public void glVertexAttribI4ubvEXT(int index, byte[] v, int v_offset) {
		gl.glVertexAttribI4ubvEXT(index, v, v_offset);
	}

	public void glVertexAttribI4uiEXT(int index, int x, int y, int z, int w) {
		gl.glVertexAttribI4uiEXT(index, x, y, z, w);
	}

	public void glVertexAttribI4uivEXT(int index, IntBuffer v) {
		gl.glVertexAttribI4uivEXT(index, v);
	}

	public void glVertexAttribI4uivEXT(int index, int[] v, int v_offset) {
		gl.glVertexAttribI4uivEXT(index, v, v_offset);
	}

	public void glVertexAttribI4usvEXT(int index, ShortBuffer v) {
		gl.glVertexAttribI4usvEXT(index, v);
	}

	public void glVertexAttribI4usvEXT(int index, short[] v, int v_offset) {
		gl.glVertexAttribI4usvEXT(index, v, v_offset);
	}

	public void glVertexAttribIPointerEXT(int index, int size, int type, int stride, Buffer pointer) {
		gl.glVertexAttribIPointerEXT(index, size, type, stride, pointer);
	}

	public void glVertexAttribL1i64NV(int index, long x) {
		gl.glVertexAttribL1i64NV(index, x);
	}

	public void glVertexAttribL1i64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL1i64vNV(index, v);
	}

	public void glVertexAttribL1i64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL1i64vNV(index, v, v_offset);
	}

	public void glVertexAttribL1ui64NV(int index, long x) {
		gl.glVertexAttribL1ui64NV(index, x);
	}

	public void glVertexAttribL1ui64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL1ui64vNV(index, v);
	}

	public void glVertexAttribL1ui64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL1ui64vNV(index, v, v_offset);
	}

	public void glVertexAttribL2i64NV(int index, long x, long y) {
		gl.glVertexAttribL2i64NV(index, x, y);
	}

	public void glVertexAttribL2i64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL2i64vNV(index, v);
	}

	public void glVertexAttribL2i64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL2i64vNV(index, v, v_offset);
	}

	public void glVertexAttribL2ui64NV(int index, long x, long y) {
		gl.glVertexAttribL2ui64NV(index, x, y);
	}

	public void glVertexAttribL2ui64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL2ui64vNV(index, v);
	}

	public void glVertexAttribL2ui64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL2ui64vNV(index, v, v_offset);
	}

	public void glVertexAttribL3i64NV(int index, long x, long y, long z) {
		gl.glVertexAttribL3i64NV(index, x, y, z);
	}

	public void glVertexAttribL3i64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL3i64vNV(index, v);
	}

	public void glVertexAttribL3i64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL3i64vNV(index, v, v_offset);
	}

	public void glVertexAttribL3ui64NV(int index, long x, long y, long z) {
		gl.glVertexAttribL3ui64NV(index, x, y, z);
	}

	public void glVertexAttribL3ui64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL3ui64vNV(index, v);
	}

	public void glVertexAttribL3ui64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL3ui64vNV(index, v, v_offset);
	}

	public void glVertexAttribL4i64NV(int index, long x, long y, long z, long w) {
		gl.glVertexAttribL4i64NV(index, x, y, z, w);
	}

	public void glVertexAttribL4i64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL4i64vNV(index, v);
	}

	public void glVertexAttribL4i64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL4i64vNV(index, v, v_offset);
	}

	public void glVertexAttribL4ui64NV(int index, long x, long y, long z, long w) {
		gl.glVertexAttribL4ui64NV(index, x, y, z, w);
	}

	public void glVertexAttribL4ui64vNV(int index, LongBuffer v) {
		gl.glVertexAttribL4ui64vNV(index, v);
	}

	public void glVertexAttribL4ui64vNV(int index, long[] v, int v_offset) {
		gl.glVertexAttribL4ui64vNV(index, v, v_offset);
	}

	public void glVertexAttribLFormatNV(int index, int size, int type, int stride) {
		gl.glVertexAttribLFormatNV(index, size, type, stride);
	}

	public void glVertexAttribParameteriAMD(int index, int pname, int param) {
		gl.glVertexAttribParameteriAMD(index, pname, param);
	}

	public void glVertexAttribPointerARB(int index, int size, int type, boolean normalized, int stride, Buffer pointer) {
		gl.glVertexAttribPointerARB(index, size, type, normalized, stride, pointer);
	}

	public void glVertexAttribPointerARB(int index, int size, int type, boolean normalized, int stride, long pointer_buffer_offset) {
		gl.glVertexAttribPointerARB(index, size, type, normalized, stride, pointer_buffer_offset);
	}

	public void glVertexAttribs1hv(int index, int n, ShortBuffer v) {
		gl.glVertexAttribs1hv(index, n, v);
	}

	public void glVertexAttribs1hv(int index, int n, short[] v, int v_offset) {
		gl.glVertexAttribs1hv(index, n, v, v_offset);
	}

	public void glVertexAttribs2hv(int index, int n, ShortBuffer v) {
		gl.glVertexAttribs2hv(index, n, v);
	}

	public void glVertexAttribs2hv(int index, int n, short[] v, int v_offset) {
		gl.glVertexAttribs2hv(index, n, v, v_offset);
	}

	public void glVertexAttribs3hv(int index, int n, ShortBuffer v) {
		gl.glVertexAttribs3hv(index, n, v);
	}

	public void glVertexAttribs3hv(int index, int n, short[] v, int v_offset) {
		gl.glVertexAttribs3hv(index, n, v, v_offset);
	}

	public void glVertexAttribs4hv(int index, int n, ShortBuffer v) {
		gl.glVertexAttribs4hv(index, n, v);
	}

	public void glVertexAttribs4hv(int index, int n, short[] v, int v_offset) {
		gl.glVertexAttribs4hv(index, n, v, v_offset);
	}

	public void glVertexBlendARB(int count) {
		gl.glVertexBlendARB(count);
	}

	public void glVertexWeightPointerEXT(int size, int type, int stride, Buffer pointer) {
		gl.glVertexWeightPointerEXT(size, type, stride, pointer);
	}

	public void glVertexWeightPointerEXT(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glVertexWeightPointerEXT(size, type, stride, pointer_buffer_offset);
	}

	public void glVertexWeightfEXT(float weight) {
		gl.glVertexWeightfEXT(weight);
	}

	public void glVertexWeightfvEXT(FloatBuffer weight) {
		gl.glVertexWeightfvEXT(weight);
	}

	public void glVertexWeightfvEXT(float[] weight, int weight_offset) {
		gl.glVertexWeightfvEXT(weight, weight_offset);
	}

	public void glVertexWeighth(short weight) {
		gl.glVertexWeighth(weight);
	}

	public void glVertexWeighthv(ShortBuffer weight) {
		gl.glVertexWeighthv(weight);
	}

	public void glVertexWeighthv(short[] weight, int weight_offset) {
		gl.glVertexWeighthv(weight, weight_offset);
	}

	public int glVideoCaptureNV(int video_capture_slot, IntBuffer sequence_num, LongBuffer capture_time) {
		return gl.glVideoCaptureNV(video_capture_slot, sequence_num, capture_time);
	}

	public int glVideoCaptureNV(int video_capture_slot, int[] sequence_num, int sequence_num_offset, long[] capture_time, int capture_time_offset) {
		return gl.glVideoCaptureNV(video_capture_slot, sequence_num, sequence_num_offset, capture_time, capture_time_offset);
	}

	public void glVideoCaptureStreamParameterdvNV(int video_capture_slot, int stream, int pname, DoubleBuffer params) {
		gl.glVideoCaptureStreamParameterdvNV(video_capture_slot, stream, pname, params);
	}

	public void glVideoCaptureStreamParameterdvNV(int video_capture_slot, int stream, int pname, double[] params, int params_offset) {
		gl.glVideoCaptureStreamParameterdvNV(video_capture_slot, stream, pname, params, params_offset);
	}

	public void glVideoCaptureStreamParameterfvNV(int video_capture_slot, int stream, int pname, FloatBuffer params) {
		gl.glVideoCaptureStreamParameterfvNV(video_capture_slot, stream, pname, params);
	}

	public void glVideoCaptureStreamParameterfvNV(int video_capture_slot, int stream, int pname, float[] params, int params_offset) {
		gl.glVideoCaptureStreamParameterfvNV(video_capture_slot, stream, pname, params, params_offset);
	}

	public void glVideoCaptureStreamParameterivNV(int video_capture_slot, int stream, int pname, IntBuffer params) {
		gl.glVideoCaptureStreamParameterivNV(video_capture_slot, stream, pname, params);
	}

	public void glVideoCaptureStreamParameterivNV(int video_capture_slot, int stream, int pname, int[] params, int params_offset) {
		gl.glVideoCaptureStreamParameterivNV(video_capture_slot, stream, pname, params, params_offset);
	}

	public void glWeightPointerARB(int size, int type, int stride, Buffer pointer) {
		gl.glWeightPointerARB(size, type, stride, pointer);
	}

	public void glWeightPointerARB(int size, int type, int stride, long pointer_buffer_offset) {
		gl.glWeightPointerARB(size, type, stride, pointer_buffer_offset);
	}

	public void glWeightbvARB(int size, ByteBuffer weights) {
		gl.glWeightbvARB(size, weights);
	}

	public void glWeightbvARB(int size, byte[] weights, int weights_offset) {
		gl.glWeightbvARB(size, weights, weights_offset);
	}

	public void glWeightdvARB(int size, DoubleBuffer weights) {
		gl.glWeightdvARB(size, weights);
	}

	public void glWeightdvARB(int size, double[] weights, int weights_offset) {
		gl.glWeightdvARB(size, weights, weights_offset);
	}

	public void glWeightfvARB(int size, FloatBuffer weights) {
		gl.glWeightfvARB(size, weights);
	}

	public void glWeightfvARB(int size, float[] weights, int weights_offset) {
		gl.glWeightfvARB(size, weights, weights_offset);
	}

	public void glWeightivARB(int size, IntBuffer weights) {
		gl.glWeightivARB(size, weights);
	}

	public void glWeightivARB(int size, int[] weights, int weights_offset) {
		gl.glWeightivARB(size, weights, weights_offset);
	}

	public void glWeightsvARB(int size, ShortBuffer weights) {
		gl.glWeightsvARB(size, weights);
	}

	public void glWeightsvARB(int size, short[] weights, int weights_offset) {
		gl.glWeightsvARB(size, weights, weights_offset);
	}

	public void glWeightubvARB(int size, ByteBuffer weights) {
		gl.glWeightubvARB(size, weights);
	}

	public void glWeightubvARB(int size, byte[] weights, int weights_offset) {
		gl.glWeightubvARB(size, weights, weights_offset);
	}

	public void glWeightuivARB(int size, IntBuffer weights) {
		gl.glWeightuivARB(size, weights);
	}

	public void glWeightuivARB(int size, int[] weights, int weights_offset) {
		gl.glWeightuivARB(size, weights, weights_offset);
	}

	public void glWeightusvARB(int size, ShortBuffer weights) {
		gl.glWeightusvARB(size, weights);
	}

	public void glWeightusvARB(int size, short[] weights, int weights_offset) {
		gl.glWeightusvARB(size, weights, weights_offset);
	}

	public void glWindowPos2d(double x, double y) {
		gl.glWindowPos2d(x, y);
	}

	public void glWindowPos2dv(DoubleBuffer v) {
		gl.glWindowPos2dv(v);
	}

	public void glWindowPos2dv(double[] v, int v_offset) {
		gl.glWindowPos2dv(v, v_offset);
	}

	public void glWindowPos2f(float x, float y) {
		gl.glWindowPos2f(x, y);
	}

	public void glWindowPos2fv(FloatBuffer v) {
		gl.glWindowPos2fv(v);
	}

	public void glWindowPos2fv(float[] v, int v_offset) {
		gl.glWindowPos2fv(v, v_offset);
	}

	public void glWindowPos2i(int x, int y) {
		gl.glWindowPos2i(x, y);
	}

	public void glWindowPos2iv(IntBuffer v) {
		gl.glWindowPos2iv(v);
	}

	public void glWindowPos2iv(int[] v, int v_offset) {
		gl.glWindowPos2iv(v, v_offset);
	}

	public void glWindowPos2s(short x, short y) {
		gl.glWindowPos2s(x, y);
	}

	public void glWindowPos2sv(ShortBuffer v) {
		gl.glWindowPos2sv(v);
	}

	public void glWindowPos2sv(short[] v, int v_offset) {
		gl.glWindowPos2sv(v, v_offset);
	}

	public void glWindowPos3d(double x, double y, double z) {
		gl.glWindowPos3d(x, y, z);
	}

	public void glWindowPos3dv(DoubleBuffer v) {
		gl.glWindowPos3dv(v);
	}

	public void glWindowPos3dv(double[] v, int v_offset) {
		gl.glWindowPos3dv(v, v_offset);
	}

	public void glWindowPos3f(float x, float y, float z) {
		gl.glWindowPos3f(x, y, z);
	}

	public void glWindowPos3fv(FloatBuffer v) {
		gl.glWindowPos3fv(v);
	}

	public void glWindowPos3fv(float[] v, int v_offset) {
		gl.glWindowPos3fv(v, v_offset);
	}

	public void glWindowPos3i(int x, int y, int z) {
		gl.glWindowPos3i(x, y, z);
	}

	public void glWindowPos3iv(IntBuffer v) {
		gl.glWindowPos3iv(v);
	}

	public void glWindowPos3iv(int[] v, int v_offset) {
		gl.glWindowPos3iv(v, v_offset);
	}

	public void glWindowPos3s(short x, short y, short z) {
		gl.glWindowPos3s(x, y, z);
	}

	public void glWindowPos3sv(ShortBuffer v) {
		gl.glWindowPos3sv(v);
	}

	public void glWindowPos3sv(short[] v, int v_offset) {
		gl.glWindowPos3sv(v, v_offset);
	}

	public void glWriteMaskEXT(int res, int in, int outX, int outY, int outZ, int outW) {
		gl.glWriteMaskEXT(res, in, outX, outY, outZ, outW);
	}

	public GLBufferStorage mapNamedBufferEXT(int bufferName, int access) throws GLException {
		return gl.mapNamedBufferEXT(bufferName, access);
	}

	public GLBufferStorage mapNamedBufferRangeEXT(int bufferName, long offset, long length, int access) throws GLException {
		return gl.mapNamedBufferRangeEXT(bufferName, offset, length, access);
	}

	public void glVertexAttribPointer(int indx, int size, int type, boolean normalized, int stride, Buffer ptr) {
		gl.glVertexAttribPointer(indx, size, type, normalized, stride, ptr);
	}

	public void glDrawElementsInstanced(int mode, int count, int type, Buffer indices, int instancecount) {
		gl.glDrawElementsInstanced(mode, count, type, indices, instancecount);
	}

	public void glDrawRangeElements(int mode, int start, int end, int count, int type, Buffer indices) {
		gl.glDrawRangeElements(mode, start, end, count, type, indices);
	}

	public void glVertexAttribIPointer(int index, int size, int type, int stride, Buffer pointer) {
		gl.glVertexAttribIPointer(index, size, type, stride, pointer);
	}
}
