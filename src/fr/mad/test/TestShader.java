package fr.mad.test;

import java.io.File;
import java.io.IOException;

import com.jogamp.opengl.GL2;

import fr.mad.Workplace;
import fr.mad.engine.log.LOG;
import fr.mad.engine.model.VAO;
import fr.mad.engine.shader.ShaderProgram;

public class TestShader extends ShaderProgram {
	
	public TestShader(GL2 gl,LOG log) {
		super("shader/vertexShader.txt", "shader/fragmentShader.txt",new LOG(log,"TestShader"), gl);
	}

	@Override
	protected void bindAttributes(GL2 gl) {
		super.bindAttribute(gl, VAO.Attribute.position, "position");
		super.bindAttribute(gl, VAO.Attribute.color, "colour_in");
		super.bindFragAttribute(gl,2,"out_Color");
	}

	
	
}
