package fr.mad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import javax.xml.bind.DatatypeConverter;

import fr.mad.engine.log.LOG;

public class Workplace {
	private static LOG log;
	private static boolean compiled = false;
	private static LOG mainlog;
	private static HashMap<File, String> res;
	private static ArrayList<String> classes;
	public static File currentJar;
	private static boolean haschange = false;
	
	static {
		classes = new ArrayList<String>();
		mainlog = new LOG(null, "Workplace");
		log = new LOG(mainlog, "compil");
		res = new HashMap<File, String>();
		
		try {
			currentJar = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Workplace.putRessources("export/JOGL/", new File("jogl/"));
	}
	
	public static void restartApplication() {
		
		try {
			final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
			
			/* is it a jar file? */
			if (!currentJar.getName().endsWith(".jar"))
				return;
				
			/* Build command: java -jar application.jar */
			final ArrayList<String> command = new ArrayList<String>();
			command.add(javaBin);
			command.add("-jar");
			command.add(currentJar.getPath());
			
			final ProcessBuilder builder = new ProcessBuilder(command);
			builder.start();
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void loadJar() {
		JarInputStream jarFile = null;
		classes = new ArrayList<String>();
		try {
			String jarName = Workplace.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
			jarFile = new JarInputStream(new FileInputStream(jarName));
			JarEntry jarEntry;
			
			while (true) {
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null) {
					break;
				}
				classes.add(jarEntry.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				jarFile.close();
			} catch (IOException e) {
			}
		}
	}
	
	/**
	 * 
	 * @return return true if compil was successful,false if already compil or restart this app without argument
	 */
	public static boolean compil() {
		if (compiled)
			return false;
		//Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		for (File f : res.keySet()) {
			try {
				compilRessource(res.get(f), f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		compiled = true;
		if (haschange)
			restartApplication();
		return true;
	}
	
	public static void putRessource(String input, File output) {
		res.put(output, input);
	}
	
	private static void compilRessource(String input, File output) throws IOException {
		if (output.isDirectory())
			return;
		output.getAbsoluteFile().getParentFile().mkdirs();
		boolean t = !output.createNewFile();
		t = t ? check(getResourceAsStream(input), new FileInputStream(output)) : t;
		
		log.log("\\/" + (t ? "pass" : "----copy file----") + "\\/");
		log.log(input);
		log.log(output.getAbsolutePath());
		
		InputStream is = null;
		FileOutputStream dest = null;
		try {
			if (!t) {
				is = getResourceAsStream(input);
				dest = new FileOutputStream(output);
				int tmp = 0;
				while ((tmp = is.read()) != -1) {
					dest.write(tmp);
				}
				haschange = true;
			}
		} finally {
			try {
				is.close();
			} catch (Exception e) {
			}
			try {
				dest.close();
			} catch (Exception e) {
			}
		}
	}
	
	private static InputStream getResourceAsStream(String input) throws FileNotFoundException {
		return Workplace.class.getClassLoader().getResourceAsStream(input);
	}
	
	public static boolean check(InputStream ais, InputStream bis) {
		return hashFile(ais, "MD5").equals(hashFile(bis, "MD5"));
	}
	
	public static String hashFile(InputStream file, String algorithm) {
		try {
			MessageDigest digest = MessageDigest.getInstance(algorithm);
			
			byte[] bytesBuffer = new byte[1024];
			int bytesRead = -1;
			
			while ((bytesRead = file.read(bytesBuffer)) != -1) {
				digest.update(bytesBuffer, 0, bytesRead);
			}
			
			byte[] hashedBytes = digest.digest();
			//System.err.println(DatatypeConverter.printHexBinary(hashedBytes));
			return DatatypeConverter.printHexBinary(hashedBytes);
		} catch (NoSuchAlgorithmException | IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				file.close();
			} catch (IOException e) {
			}
		}
		return "error";
	}
	
	/**
	 * take all sub file in sub package of input and put them in output floder
	 * 
	 * @param input
	 * @param output
	 * @param b
	 */
	public static void putRessources(String input, File output) {
		if (!output.isDirectory()) {
			if (output.exists())
				return;
			output.mkdirs();
		}
		try {
			loadFJar();
		} catch (Exception e) {
			loadJar();
		}
		System.err.println(input);
		for (String s : classes) {
			if (s.startsWith(input)) {
				File tmpo = new File(output.getAbsolutePath() + File.separator + s.split("/")[s.split("/").length - 1]);
				putRessource(s, tmpo);
			}
		}
	}
	
	private static void loadFJar() {
		loadFJar(new File("bin"));
	}
	
	private static void loadFJar(File file) {
		for (File f : file.listFiles()) {
			if (f.isDirectory())
				loadFJar(f);
			if (f.isFile()) {
				String path = f.getAbsolutePath();
				int i = path.lastIndexOf("bin\\");
				i = i == -1 ? 0 : i;
				path = path.substring(i + 4);
				path = path.replace('\\', '/');
				classes.add(path);
			}
		}
	}
	
	public static boolean isExistInJar(String key) throws URISyntaxException {
		boolean tmp = false;
		String jarName = Workplace.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		log.log("Jar " + jarName + " looking for " + key + "");
		JarInputStream jarFile = null;
		try {
			jarFile = new JarInputStream(new FileInputStream(jarName));
			JarEntry jarEntry;
			
			while (true) {
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null) {
					break;
				}
				if ((jarEntry.getName().equals(key))) {
					
					log.log("Found " + jarEntry.getName());
					tmp = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				jarFile.close();
			} catch (IOException e) {
			}
		}
		return tmp;
	}
	
	public static boolean isFile(String key) throws URISyntaxException {
		boolean tmp = false;
		String jarName = Workplace.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
		log.log("Jar " + jarName + " looking for " + key + " as a File");
		JarInputStream jarFile = null;
		try {
			jarFile = new JarInputStream(new FileInputStream(jarName));
			JarEntry jarEntry;
			
			while (true) {
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null) {
					break;
				}
				if (jarEntry.getName().equals(key) && !jarEntry.isDirectory()) {
					
					log.log("Found " + jarEntry.getName());
					tmp = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				jarFile.close();
			} catch (IOException e) {
			}
		}
		return tmp;
	}
}
