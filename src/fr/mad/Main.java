package fr.mad;

import java.awt.SplashScreen;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import fr.mad.engine.GameWindow;
import fr.mad.engine.log.LOG;

public class Main {
	
	private static ArrayList<String> arg;
	public static File currentJar;
	static{
		try {
			currentJar = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		arg = new ArrayList<String>();
		for (String s : args) {
			arg.add(s);
		}
		if(arg.size()==0){
			arg.add("loadWorkplace");
		}
		if (arg.contains("deleteFile")) {
			for (String s : arg) {
				if (new File(s).exists()) {
					new File(s).delete();
				}
			}
		}
		if (arg.contains("loadWorkplace")) {
			Workplace.putRessource("fr/mad/engine/shader/vertexShader.txt", new File("shader/vertexShader.txt"));
			Workplace.putRessource("fr/mad/engine/shader/fragmentShader.txt", new File("shader/fragmentShader.txt"));
			Workplace.putRessource("export/test.obj", new File("test.obj"));
			
			if(Workplace.compil())
				arg.add("run");
		}
		
		if (arg.contains("run"))
			new GameWindow(null).init().startGame();
			
		if (SplashScreen.getSplashScreen() != null && SplashScreen.getSplashScreen().isVisible()){
			SplashScreen.getSplashScreen().close();
		}
	}
	
	
	
	@SuppressWarnings("unused")
	private static void initPlugin(File file) {
		if (!file.isDirectory())
			return;
		InputStream is = null;
		FileOutputStream dest = null;
		try {
			is = Main.class.getResourceAsStream("/absolute/path/of/source/in/jar/file");
			dest = new FileOutputStream(new File("plugins/"));
			
			int tmp = 0;
			while ((tmp = is.read()) != -1) {
				dest.write(tmp);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				dest.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
