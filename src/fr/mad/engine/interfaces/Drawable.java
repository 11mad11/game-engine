package fr.mad.engine.interfaces;

import fr.mad.engine.GameEngine;

public interface Drawable {
	
	void draw(GameEngine ge);
}
