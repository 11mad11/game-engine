package fr.mad.engine.interfaces;

import fr.mad.engine.GameEngine;

public interface Tickable {
	void tick(GameEngine ge);
}
