package fr.mad.engine.interfaces;

import com.jogamp.opengl.GL;

public interface Initiable {
	public void init(GL gl);
}
