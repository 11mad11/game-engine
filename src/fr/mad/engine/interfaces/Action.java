package fr.mad.engine.interfaces;

public interface Action {
	public void fire();
}
