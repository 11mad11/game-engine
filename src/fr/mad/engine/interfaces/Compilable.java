package fr.mad.engine.interfaces;

public interface Compilable {
	public void compile();
}
