package fr.mad.engine.until;

import fr.mad.engine.interfaces.Action;

public class Timer extends Thread{
	private int ups;
	private long lastNanoTime;
	private int ticks;
	private Action action;
	private boolean running;
	private Timer timer;
	private double periode;
	
	public Timer(int ups) {
		this.ups = ups;
		this.lastNanoTime = System.nanoTime();
		this.timer = this;
		this.periode = secToNano(1/(double)ups);
	}
	
	public Timer(Action action,int ups) {
		this(ups);
		this.action = action;
	}

	public void setUPS(int ups) {
		this.ups = ups;
	}
	
	public void run(){
		running = true;
		Thread.currentThread().setPriority(MAX_PRIORITY);
		while(running){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(nano()-lastNanoTime>=periode){
				lastNanoTime+=periode;
				action.fire();
			}
		}
	}
	
	private double secToNano(double i) {
		return i*1000000000;
	}

	public void stopTimer(){
		running = false;
	}
	
	public static double Sec() {
		return System.nanoTime() / 1000000000.0;
	}

	public static double milli() {
		return System.currentTimeMillis();
	}

	public static double nano() {
		return System.nanoTime();
	}
}
