package fr.mad.engine.until;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.PathIterator;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.awt.GLCanvas;

public class StringUntil {
	private static void drawString(GL2 gl, String s, Font f,GLCanvas g) {
        Font font = new Font("Times", Font.PLAIN, 14);// getFont();
        System.out.println(font.toString());
        Graphics2D g2 = (Graphics2D) g.getGraphics();

        //FontMetrics fontInfo = g2.getFontMetrics(font);
        GlyphVector gv = font.createGlyphVector(g2.getFontRenderContext(), s);
        Shape shape = gv.getOutline();
        // System.out.println(gv.toString());
        PathIterator itor = shape.getPathIterator(null, 0.01f);// very fine
        // grain
        int it = 0;
        float seg[] = new float[6];
        gl.glBegin(GL.GL_LINE_LOOP);
        while (!itor.isDone()) {
            System.out.println(++it + " " + seg[0] + " " + seg[1]);
            itor.currentSegment(seg);
            gl.glVertex2f(seg[0], seg[1]);
            itor.next();
            gl.glColor3d(Math.random(), Math.random(), Math.random());
        }
        gl.glEnd();
    }
}
