package fr.mad.engine.until;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.GLBuffers;

import fr.mad.engine.model.MBuffer;

public class BufferUtil {
	public static final int INT = 1;
	public static final int FLOAT = 2;
	public static final int SHORT = 3;
	
	public static Buffer makeBuffer(Object[] a, int GLtype) {
		switch (GLtype) {
			case INT:
				int[] tmp = new int[a.length];
				for (int i = 0; i < tmp.length; i++) {
					tmp[i] = (int) a[i];
				}
				return getIntBuffer(tmp);
			case FLOAT:
				float[] tmpf = new float[a.length];
				for (int i = 0; i < tmpf.length; i++) {
					tmpf[i] = (float) a[i];
				}
				return getFloatBuffer(tmpf);
			case SHORT:
				short[] tmps = new short[a.length];
				for (int i = 0; i < tmps.length; i++) {
					tmps[i] = (short) a[i];
				}
				return getShortBuffer(tmps);
		}
		return null;
	}
	
	public static Buffer getShortBuffer(short[] tmps) {
		return ShortBuffer.wrap(tmps);
	}
	
	public static FloatBuffer getFloatBuffer(float[] tmpf) {
		// TODO Auto-generated method stub
		return FloatBuffer.wrap(tmpf);
	}
	
	public static IntBuffer getIntBuffer(int[] a) {
		return IntBuffer.wrap(a);
	}
	
	public static <T> Buffer wrap(T[] data, int gltype) {
		switch (gltype) {
			case INT:
				int[] tmp = new int[data.length];
				for (int i = 0; i < tmp.length; i++) {
					tmp[i] = (int) data[i];
				}
				return getIntBuffer(tmp);
			case FLOAT:
				float[] tmpf = new float[data.length];
				for (int i = 0; i < tmpf.length; i++) {
					tmpf[i] = (float) data[i];
				}
				return getFloatBuffer(tmpf);
			case SHORT:
				short[] tmps = new short[data.length];
				for (int i = 0; i < tmps.length; i++) {
					tmps[i] = (short) data[i];
				}
				return getShortBuffer(tmps);
		}
		return null;
	}
	
	public static <T extends Buffer> Buffer newDirectBuffer(Class<T> c, int size) {
		if (c == IntBuffer.class) {
			return Buffers.newDirectIntBuffer(size);
		} else if (c == FloatBuffer.class) {
			return Buffers.newDirectFloatBuffer(size);
		} else if (c == ByteBuffer.class) {
			return Buffers.newDirectByteBuffer(size);
		} else if (c == ShortBuffer.class) {
			return Buffers.newDirectShortBuffer(size);
		} else if (c == LongBuffer.class) {
			return Buffers.newDirectLongBuffer(size);
		} else if (c == DoubleBuffer.class) {
			return Buffers.newDirectDoubleBuffer(size);
		}
		return null;
	}
	
	public static ByteBuffer bufferToByteBuffer(Buffer b) {
		ByteBuffer dst = Buffers.newDirectByteBuffer(b.limit() * GLBuffers.sizeOfBufferElem(b));
		MBuffer src = new MBuffer(b);
		while (src.hasData()) {
			Object o = src.get();
			if (o instanceof Integer) {
				dst.putInt((int) o);
			} else if (o instanceof Float) {
				dst.putFloat((float) o);
			} else if (o instanceof Byte) {
				dst.put((byte) o);
			} else if (o instanceof Short) {
				dst.putShort((short) o);
			} else if (o instanceof Long) {
				dst.putLong((long) o);
			} else if (o instanceof Double) {
				dst.putDouble((double) o);
			}
		}
		dst.rewind();
		return dst;
	}
}
