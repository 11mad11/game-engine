package fr.mad.engine.until;

public class Length {
	int km;
	int hm;
	int dam;
	int m;
	int dm;
	int cm;
	int mm;
	public Length(){
		
	}
	public void check(){
		int tmp = mm%10;
		mm -= tmp*10;
		
	}
	public int getKm() {
		return km;
	}
	public void setKm(int km) {
		this.km = km;
	}
	public int getHm() {
		return hm;
	}
	public void setHm(int hm) {
		this.hm = hm;
	}
	public int getDam() {
		return dam;
	}
	public void setDam(int dam) {
		this.dam = dam;
	}
	public int getM() {
		return m;
	}
	public void setM(int m) {
		this.m = m;
	}
	public int getDm() {
		return dm;
	}
	public void setDm(int dm) {
		this.dm = dm;
	}
	public int getCm() {
		return cm;
	}
	public void setCm(int cm) {
		this.cm = cm;
	}
	public int getMm() {
		return mm;
	}
	public void setMm(int mm) {
		this.mm = mm;
	}
}
