package fr.mad.engine.until;

import java.util.ArrayList;

public class Tree<S> {
	private ArrayList<Tree<S>> list;
	private S item;
	private boolean root = false;
	
	public Tree(S item) {
		this();
		this.root = false;
		this.setItem(item);
	}
	
	public Tree() {
		list = new ArrayList<Tree<S>>();
		this.root = true;
	}
	
	public void add(Tree<S> t) {
		this.list.add(t);
	}
	
	public boolean isRoot() {
		return root;
	}
	
	public S getItem() {
		return item;
	}
	
	public void setItem(S item) {
		this.item = item;
	}
	
	public ArrayList<Tree<S>> getChild() {
		return list;
	}

	public boolean is(S a) {
		for(Tree<S> t:list){
			if(t.getItem().equals(a))
				return true;
		}
		return false;
	}

	public Tree<S> getChild(String a) {
		for(Tree<S> t:list){
			if(t.getItem().equals(a))
				return t;
		}
		return null;
	}
	
}
