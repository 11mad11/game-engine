package fr.mad.engine.until;

import java.awt.Dimension;

import com.jogamp.opengl.GL2;

public class ViewPort {
	
	private int x;
	private int y;
	private int w;
	private int h;
	private int sw;
	private int sh;
	private int s;
	
	public ViewPort(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.w = width;
		this.h = height;
	}
	
	public ViewPort() {
		
	}

	public void setupVP(GL2 gl) {
		gl.glViewport(x, y, w, h);
	}
	
	public static void compute(int fw, int fh,Ratio r,ViewPort v) {
		int s = (fh/r.getHRatio())<(fw/r.getWRatio())?(fh/r.getHRatio()):(fw/r.getWRatio());
		int w = s*r.getWRatio();
		int h = s*r.getHRatio();
		v.up((fw - w) / 2, (fh - h) / 2, w, h);
	}

	private ViewPort up(int i, int j, int w2, int h2) {
		this.x = i;
		this.y = j;
		this.w = w2;
		this.h = h2;
		return this;
	}

	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}

	public int getVW() {
		return w;
	}
	
	public int getVH() {
		return h;
	}

	public float getRatio() {
		return w/h;
	}
}
