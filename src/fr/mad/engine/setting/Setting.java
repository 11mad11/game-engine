package fr.mad.engine.setting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;

import fr.mad.engine.GameEngine;
import fr.mad.engine.log.LOG;
import fr.mad.engine.until.Ratio;

public class Setting implements Serializable {
	private static final long serialVersionUID = -951270948825882907L;
	
	//for setting object
	private static int idCount = 0;
	private static HashMap<Integer, Setting> sets = new HashMap<Integer, Setting>();
	
	//for costum setting
	private static int lastId = 0;
	
	
	public static Setting newSetting(boolean autoFill) {
		Setting tmp = new Setting();
		if(autoFill){
			tmp.setLOG(new LOG(null,"game engine"));
			tmp.setTPS(20);
			tmp.setRatio(new Ratio(16, 9));
			tmp.setGameEngine(GameEngine.create(tmp));
		}
		synchronized (sets) {
			sets.put(++idCount, tmp);
		}
		return getSetting(idCount);
	}
	
	public static Setting getSetting(int i){
		return sets.get(i);
	}

	public static int newSetting(File f) throws ClassNotFoundException, FileNotFoundException, IOException {
		Setting tmp = (Setting) new ObjectInputStream(new FileInputStream(f)).readObject();
		synchronized (sets) {
			sets.put(++idCount, tmp);
		}
		return idCount;
	}
	
	////////
	
	public class Object{
		private java.lang.Object o;

		public Object(java.lang.Object o){
			this.o = o;
		}
		
		/**
		 * @param def (default)
		 * @return return this object to boolean if is an instance of Boolean or return 'def'
		 */
		public boolean toBool(boolean def) {
			return ((boolean) (o instanceof Boolean?o:def));
		}
		
	}
	
	private GameEngine ge;
	private Ratio ratio;
	private int tps;
	private LOG log;
	private static HashMap<Integer, Setting.Object> setobj = new HashMap<Integer, Setting.Object>();
	
	private Setting(){
		
	}
	
	public GameEngine getGameEngine(){
		return ge;
	}

	public void setGameEngine(GameEngine gameEngine) {
		ge = gameEngine;
	}

	public Ratio getRatio() {
		return ratio;
	}
	
	public void setRatio(Ratio ratio){
		this.ratio = ratio;
	}
	
	public void setTPS(int i) {
		this.tps = i;
	}
	
	public int getTPS(){
		return this.tps;
	}

	public void setLOG(LOG log) {
		this.log = log;
	}
	public LOG getLOG() {
		return log;
	}

	public Setting.Object get(int id){
		return setobj.get(id)==null?new Object(null):setobj.get(id);
	}
	
	public static int newID() {
		return lastId ++;
	}
}
