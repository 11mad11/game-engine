package fr.mad.engine;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2ES1.*;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.*;

import java.awt.event.KeyListener;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

import fr.mad.JVMArgs;
import fr.mad.engine.handler.plugin.PluginsHandler;
import fr.mad.engine.interfaces.Action;
import fr.mad.engine.interfaces.Cleanable;
import fr.mad.engine.interfaces.Drawable;
import fr.mad.engine.interfaces.Initiable;
import fr.mad.engine.interfaces.Tickable;
import fr.mad.engine.log.LOG;
import fr.mad.engine.setting.Setting;
import fr.mad.engine.until.Ratio;
import fr.mad.engine.until.Timer;
import fr.mad.engine.until.ViewPort;
import fr.mad.test.TestLevel;

public class GameEngine extends GLCanvas implements GLEventListener {
	private static final long serialVersionUID = 7767294114295520998L;
	private Setting setting;
	private GLU glu;
	private ViewPort vP;
	private Ratio ratio;
	//TODO
	@SuppressWarnings("unused")
	private PluginsHandler pluginH;
	private Drawable toDraw;
	private int nullFPS;
	private int ticks;
	private int fps;
	private int debugUpdateTime;
	public static final boolean debugging = JVMArgs.getBool('X', "debug");
	private Tickable toTick;
	private Timer updateLoop;
	private Timer debugLoop;
	private ArrayList<Initiable> toInit;
	private ArrayList<Cleanable> toClean;
	private LOG log;

	private LOG debugLog;
	
	private GameEngine(Setting s, GLCapabilities capabilities) {
		super(capabilities);
		log = s.getLOG();
		debugLog = new LOG(log, "debug");
		this.setting = s;
		ratio = s.getRatio();
		vP = new ViewPort();
		updateLoop = new Timer(GameUpdateLoop(), 20);
		debugLoop = new Timer(GameDebugLoop(), 10);
		toInit = new ArrayList<Initiable>();
		toClean = new ArrayList<Cleanable>();
		
		try {
			this.pluginH = new PluginsHandler(new File("plugins/"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static GameEngine create(Setting s) {
		final GLProfile p = GLProfile.get(GLProfile.GL2);
		GLCapabilities c = new GLCapabilities(p);
		c.setStereo(false);
		
		GameEngine ge = new GameEngine(s, c);
		ge.setAnimator(new FPSAnimator(ge, 60));
		ge.addGLEventListener(ge);
		//		GDI.GetDC(ge.getHandle());
		//		GLContext glc = GLDrawableFactory.getFactory( p ).createExternalGLContext();
		//		ge.setContext(glc, true);
		
		return ge;
	}
	
	public Action GameUpdateLoop() {
		return new Action() {
			public void fire() {
				ticks++;
				if (toTick != null)
					toTick.tick(setting.getGameEngine());
			}
		};
	}
	
	public Action GameDebugLoop() {
		return new Action() {

			public void fire() {
				if (Timer.Sec() >= GameEngine.this.debugUpdateTime + 1 && debugging) {
					if (GameEngine.this.fps != 0) {
						if (GameEngine.this.nullFPS != 0) {
							System.err.println(GameEngine.this.nullFPS);
							GameEngine.this.nullFPS = 0;
						}
						GameEngine.this.debugUpdateTime += 1;
						DebugInfo.fps = GameEngine.this.fps;
						DebugInfo.ticks = GameEngine.this.ticks;
						debugLog.log(DebugInfo.fps + " fps : " + DebugInfo.ticks + " ticks");
						GameEngine.this.fps = 0;
						GameEngine.this.ticks = 0;
					} else {
						GameEngine.this.nullFPS++;
					}
				}
			}
		};
	}
	
	public void add(Object o) {
		if (o instanceof Drawable)
			this.toDraw = (Drawable) o;
		if (o instanceof Tickable)
			this.toTick = (Tickable) o;
		if (o instanceof KeyListener)
			this.addKeyListener((KeyListener) o);
		if (o instanceof Initiable)
			this.toInit.add((Initiable) o);
		if (o instanceof Cleanable)
			this.toClean.add((Cleanable) o);
	}
	
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();
		if (height == 0)
			height = 1;
		ViewPort.compute(width, height, ratio, vP);
		this.vP.setupVP(gl);
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(0, 1, 0, 1);
	}
	
	/**
	 * -1 a 1 sur x,y
	 * 
	 * @param gl
	 * @param i
	 */
	public void GLU2D(GL2 gl, int i) {
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluOrtho2D(-i, i, -i, i);
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();
	}
	
	public void GLU3D(GL2 gl) {
		gl.glMatrixMode(GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(70, this.vP.getRatio(), 0.1, 500);
		//glu.gluLookAt(100, 50, 50, 0, 0, 0, 0, 1, 0);
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity();
	}
	
	public void dispose(GLAutoDrawable drawable) {
		for (final Cleanable c : toClean) {
			new Thread(new Runnable() {
				public void run() {
					c.cleanUP();
				}
			}).start();
		}
	}
	
	public boolean start() {
		preInit();
		if (GameEngine.debugging)
			this.debugLoop.start();
		if (this.getAnimator().isStarted() && this.updateLoop.isAlive())
			return false;
		this.getAnimator().start();
		this.updateLoop.start();
		if (!(this.getAnimator().isStarted() && this.updateLoop.isAlive())) {
			this.getAnimator().stop();
			this.updateLoop.stopTimer();
		}
		return this.getAnimator().isStarted() && this.updateLoop.isAlive();
	}
	
	private void preInit() {
		add(new TestLevel(setting));
	}
	
	public boolean isStarted() {
		return this.getAnimator().isStarted() && this.updateLoop.isAlive();
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		System.out.println("Initiate...");
		
		GL2 gl = this.getGL().getGL2();
		glu = new GLU();
		gl.glClearColor(0f, 0f, 1f, 1f);
		gl.glClearDepth(1.0f);
		gl.glEnable(GL_DEPTH_TEST);
		gl.glEnable(GL_TEXTURE_2D);
		gl.glDepthFunc(GL_LEQUAL);
		gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		//gl.glShadeModel(GL_SMOOTH);
		gl.glEnable(GL2.GL_DOUBLEBUFFER);
		gl.glEnable(GL.GL_BLEND);
		gl.glEnable(GL2.GL_COLOR);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		
		for (Initiable i : toInit) {
			i.init(gl);
		}
		
		this.debugUpdateTime = (int) Timer.Sec();
		
		System.out.println("init ended");
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = this.getGL().getGL2();
		//GLU2D(gl,1);
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		if (this.toDraw != null)
			this.toDraw.draw(this);
		gl.glFlush();
		this.fps++;
	}
	
	public void stop() {
		this.getAnimator().stop();
	}
}
