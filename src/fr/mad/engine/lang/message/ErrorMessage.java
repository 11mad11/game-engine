package fr.mad.engine.lang.message;

public enum ErrorMessage {

	itsNotADir("itsNotADir");
	
	private String s;
	ErrorMessage(String s){
		this.s=s;
	}
	public String get() {
		return s;
	}
}
