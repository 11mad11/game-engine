package fr.mad.engine.lang.message;

public enum Message {
	
	init("init"),
	ended("ended");
	
	private String s;
	Message(String s){
		this.s=s;
	}
	public String get() {
		return s;
	}
}
