package fr.mad.engine.level;

import fr.mad.engine.GameEngine;
import fr.mad.engine.handler.InputHandler;
import fr.mad.engine.setting.Setting;
import fr.mad.engine.until.Ratio;

public class Level{
	
	private Setting setting;
	private GameEngine gameEngine;
	private Ratio ratio;
	protected InputHandler inputHandler;
	
	public Level(Setting s){
		inputHandler = new InputHandler();
		setting = s;
		gameEngine = s.getGameEngine();
		ratio = s.getRatio();
		gameEngine.addKeyListener(inputHandler);
	}
	
	public Setting getSetting() {
		return setting;
	}

	public GameEngine getGameEngine() {
		return gameEngine;
	}

	public Ratio getRatio() {
		return ratio;
	}
	
	public int getTPS(){
		return setting.getTPS();
	}
	
	public void setTPS(int i){
		setting.setTPS(i);
	}

	public InputHandler getInputHandler() {
		return inputHandler;
	}
}
