package fr.mad.engine;

import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import fr.mad.Workplace;
import fr.mad.engine.setting.Setting;

public class GameWindow implements WindowListener {
	private static final int AutoInit = Setting.newID();
	private GraphicsConfiguration gc;
	private Setting setting;
	private GameEngine game;
	private Frame frame;
	private boolean f;
	
	/**
	 * 
	 * ex:<br/>
	 * gamewindow = new GameWindow(null);<br/>
	 * gamewindow.init();<br/>
	 * gamewindow.startGame();
	 * @param s - the setting to pass to game engine or null for defaault setting
	 */
	public GameWindow(Setting s) {
		frame = new Frame();
		if (setting == null)
			setting = Setting.newSetting(true);
		if(setting.get(GameWindow.AutoInit).toBool(false))
			init();
	}
	
	public GameWindow init() {
		GraphicsDevice[] d = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		GraphicsDevice dev = null;
		frame.addWindowListener(this);
		if (d.length > 1) {
			int tmp = JOptionPane.showOptionDialog(null, "Quel ecran utiliser?", null, JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, d, 0);
			if (tmp != JOptionPane.CLOSED_OPTION)
				dev = d[tmp];
		} else
			dev = d[0];
		if (dev != null) {
			gc = dev.getDefaultConfiguration();
		}
		if (gc.getDevice().isFullScreenSupported()) {
			int tmp = JOptionPane.showOptionDialog(null, "Fullscreen?", null, JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, new Boolean[] { true, false }, 0);
			if (tmp == JOptionPane.CLOSED_OPTION || tmp == 1)
				f = false;
			else
				f = true;
		}
		
		this.game = setting.getGameEngine();
		frame.add(game);
		return this;
	}
	
	public void setFullScreen(boolean b) {
		if (frame.isDisplayable())
			frame.dispose();
		if (!gc.getDevice().isFullScreenSupported())
			b = false;
		frame.setUndecorated(b);
		frame.setResizable(!b);
		if (b) {
			gc.getDevice().setFullScreenWindow(frame);
			frame.validate();
			frame.setVisible(true);
		} else {
			frame.setSize(300, 300);
			frame.setVisible(true);
		}
		frame.requestFocus();
	}
	
	public void startGame() {
		setFullScreen(f);
		System.out.println("Starting...");
		System.out.println(this.game.start() ? "Started" : (this.game.isStarted() ? "Already start" : "Unable to start"));
		
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		this.game.stop();
		frame.dispose();
		System.exit(0);
	}
	
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
