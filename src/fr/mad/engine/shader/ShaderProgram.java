package fr.mad.engine.shader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.math.FloatUtil;

import fr.mad.engine.log.LOG;

public abstract class ShaderProgram {
	private int programID;
	private int vertexShaderID;
	private int fragmentShaerID;
	private LOG log;
	
	private float[] scale = new float[16];
	private float[] zRotazion = new float[16];
	private float[] modelToClip = new float[16];
	private GL2 gl2;
	
	public ShaderProgram(String vertexFile, String fragmentFile, LOG log, GL2 gl) {
		this.log = new LOG(log, "shader");
		this.gl2 = gl;
		vertexShaderID = loadShader(vertexFile, GL2ES2.GL_VERTEX_SHADER, gl);
		fragmentShaerID = loadShader(fragmentFile, GL2ES2.GL_FRAGMENT_SHADER, gl);
		programID = loadProgram(gl.getGL2());
		link(programID, new int[] { vertexShaderID, fragmentShaerID }, gl);
	}
	
	public void scale(float x,float y,float z){
		scale = FloatUtil.makeScale(scale, true, 0.5f, 0.5f, 0.5f);
		//zRotazion = FloatUtil.makeRotationEuler(zRotazion, 0, 0, 0, diff);
		modelToClip = FloatUtil.multMatrix(scale, zRotazion);
		gl2.glUniformMatrix4fv(modelToClipMatrixUL, 1, false, modelToClip, 0);
	}
	
	private void link(int p, int[] is, GL2 gl) {
		for (int s : is)
			gl.glAttachShader(p, s);
		gl.glLinkProgram(p);
		
		int[] status = new int[1];
		gl.glGetProgramiv(p, GL2.GL_LINK_STATUS, status, 0);
		if (status[0] == GL2.GL_FALSE) {
			int[] infoLogLength = new int[1];
			gl.glGetProgramiv(p, GL2.GL_INFO_LOG_LENGTH, infoLogLength, 0);
			
			byte[] strInfoLog = new byte[infoLogLength[0] + 1];
			gl.glGetProgramInfoLog(p, 1, infoLogLength, 0, strInfoLog, 0);
			
			System.err.println(new String(strInfoLog));
		}
		
		for (int s : is)
			gl.glDetachShader(p, s);
			
	}
	
	private int loadProgram(GL2 gl) {
		return gl.glCreateProgram();
	}
	
	public void start(GL2 gl) {
		gl.glUseProgram(programID);
	}
	
	public void stop(GL2 gl) {
		gl.glUseProgram(0);
	}
	
	public void clenUp(GL2 gl) {
		stop(gl);
		gl.glDeleteShader(vertexShaderID);
		gl.glDeleteShader(fragmentShaerID);
		gl.glDeleteProgram(programID);
	}
	
	protected abstract void bindAttributes(GL2 gl);
	
	protected void bindAttribute(GL2 gl, int i, String t) {
		gl.glBindAttribLocation(programID, i, t);
	}
	
	protected void bindFragAttribute(GL2 gl, int i, String string) {
		gl.glBindFragDataLocation(programID, i, string);
	}
	
	private int loadShader(String file, int type, GL2 gl) {
		log.log("Loading " + (GL2ES2.GL_VERTEX_SHADER == type ? "Vertex" : "Fragment") + " Shader");
		StringBuilder shaderSource = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null) {
				shaderSource.append(line).append("\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		int shaderID = gl.glCreateShader(type);
		gl.glShaderSource(shaderID, 1, new String[] { shaderSource.toString() }, new int[] { shaderSource.toString().length() }, 0);
		gl.glCompileShader(shaderID);
		
		int[] status = new int[1];
		gl.glGetShaderiv(shaderID, GL2.GL_COMPILE_STATUS, status, 0);
		if (status[0] == GL2.GL_FALSE) {
			int[] infoLogLength = new int[1];
			gl.glGetShaderiv(shaderID, GL2.GL_INFO_LOG_LENGTH, infoLogLength, 0);
			
			byte[] strInfoLog = new byte[infoLogLength.length + 1];
			gl.glGetShaderInfoLog(shaderID, 1, infoLogLength, 0, strInfoLog, 0);
			
			System.err.println(new String(strInfoLog));
		}
		
		return shaderID;
	}
	
	public int id() {
		return programID;
	}
	
}
