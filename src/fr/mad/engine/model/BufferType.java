package fr.mad.engine.model;

import java.nio.*;

public enum BufferType {
	INT, FLOAT, BYTE, SHORT, LONG, DOUBLE;

	public static BufferType valueOf(Buffer b) {
		if(b instanceof IntBuffer){
			return INT;
		}else if(b instanceof FloatBuffer){
			return FLOAT;
		}else if(b instanceof ByteBuffer){
			return BYTE;
		}else if(b instanceof ShortBuffer){
			return SHORT;
		}else if(b instanceof LongBuffer){
			return LONG;
		}else if(b instanceof DoubleBuffer){
			return DOUBLE;
		}
		return null;
	}
}
