package fr.mad.engine.model;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.mad.engine.interfaces.Cleanable;
import fr.mad.engine.interfaces.Compilable;
import fr.mad.engine.until.BufferUtil;

public class VBO implements Cleanable, Compilable {
	
	private static ArrayList<VBO> vbos = new ArrayList<VBO>();
	private ArrayList<Attribute<?>> atts;
	private GL2 gl2;
	int id = 0;
	private int nVertex;
	private static ArrayList<VBO> toClean = new ArrayList<VBO>();
	
	public VBO(int nVertex) {
		this.nVertex = nVertex;
		vbos.add(this);
		atts = new ArrayList<Attribute<?>>();
	}
	
	public <T extends Buffer> VBO addData(int index, T data, int size, int type, boolean normalized) {
		this.atts.add(new Attribute<T>(index, size, type, normalized, data));
		return this;
	}
	
	public static void preCompileAll(GL gl) {
		GL2 gl2 = gl.getGL2();
		IntBuffer ids = IntBuffer.allocate(vbos.size());
		gl2.glGenBuffers(vbos.size(), ids);
		for (VBO vbo : vbos) {
			vbo.gl2 = gl2;
			vbo.id = ids.get();
		}
	}
	
	@Override
	public void compile() {
		if (!vbos.contains(this))
			return;
		gl2.glBindBuffer(GL2.GL_ARRAY_BUFFER, id);
		
		int size = 0;
		int offset = 0;
		int stride = 0;
		for (Attribute<?> att : atts) {
			size += att.dataSize();
			stride += att.SIZEOF();
		}
		
		ByteBuffer[] buf = new ByteBuffer[atts.size()];
		for (int i = 0; i < atts.size(); i++) {
			buf[i] = BufferUtil.bufferToByteBuffer(atts.get(i).getData());
		}
		ByteBuffer vert = Buffers.newDirectByteBuffer(size);
		for (int i = 0; i < nVertex; i++) {
			for (int k = 0; k < atts.size(); k++) {
				for (int j = 0; j < atts.get(k).SIZEOF(); j++) {
					vert.put(buf[k].get());
				}
			}
		}
		vert.flip();
		
		gl2.glBufferData(GL2.GL_ARRAY_BUFFER, size, vert, GL2.GL_STATIC_DRAW);
		
		for (Attribute<?> att : atts) {
			att.gl2 = gl2;
			att.stride = stride;
			att.pointerOffset = offset;
			offset += att.SIZEOF();
			gl2.glEnableVertexAttribArray(att.index);
			att.glVertexAttribPointer();
		}
		gl2.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
		
		toClean.add(this);
		vbos.remove(this);
	}
	
	@Override
	public void cleanUP() {
		// TODO Auto-generated method stub
		
	}
	
	public int id() {
		return id;
	}
	
	public void preDraw() {
		for (Attribute<?> att : atts) {
			gl2.glEnableVertexAttribArray(att.index);
		}
	}
	
	public void check(ByteBuffer tmp) {
		gl2.glGetBufferSubData(id, 0, tmp.limit(), tmp);
	}
	
}