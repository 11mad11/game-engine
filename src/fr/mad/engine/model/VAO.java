package fr.mad.engine.model;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.mad.engine.interfaces.Cleanable;
import fr.mad.engine.interfaces.Compilable;

public class VAO implements Cleanable, Compilable {
	
	public static class Attribute{

		public static final int position = 0;
        public static final int normal = 1;
        public static final int color = 3;
        public static final int texCoord = 4;
        public static final int drawId = 5;
		
	}
	private static ArrayList<VAO> vaos = new ArrayList<VAO>();
	private static ArrayList<VAO> toClean = new ArrayList<VAO>();
	private ArrayList<VBO> vbos;
	int id = 0;
	private GL2 gl2;
	public IBO ibo;
	
	public VAO() {
		vaos.add(this);
		vbos = new ArrayList<VBO>();
	}
	
	public VBO newVBO(int nVertex) {
		VBO vbo = new VBO(nVertex);
		this.vbos.add(vbo);
		return vbo;
	}
	
	public void setIBO(short[] ibo) {
		this.ibo = new IBO(ShortBuffer.wrap(ibo.clone()));
	}
	
	public void setIBO(ShortBuffer ibo) {
		this.ibo = new IBO(Buffers.copyShortBuffer(ibo));
	}
	
	public static void preCompileAll(GL gl) {
		System.out.println("pre compile vao");
		GL2 gl2 = gl.getGL2();
		IntBuffer ids = IntBuffer.allocate(vaos.size());
		gl2.glGenVertexArrays(vaos.size(), ids);
		for (VAO vao : vaos) {
			vao.gl2 = gl2;
			vao.id = ids.get();
		}
		VBO.preCompileAll(gl);
	}
	
	public static void CompileAll() {
		System.out.println("compile all");
		ArrayList<VAO> tmp = new ArrayList<VAO>();
		for (VAO vao : vaos)
			tmp.add(vao);
		for (VAO vao : tmp) {
			vao.compile();
		}
	}
	
	@Override
	public void compile() {
		if (!vaos.contains(this))
			return;
		System.out.println("Vao compile");
		gl2.glBindVertexArray(id);
		ibo.gl2 = gl2;
		ibo.compile();
		for (VBO vbo : vbos) {
			vbo.compile();
		}
		gl2.glBindVertexArray(0);
		
		vaos.remove(this);
		toClean.add(this);
	}
	
	public static void cleanUPALL() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void cleanUP() {
		// TODO Auto-generated method stub
		
	}
	
	public int id() {
		return id;
	}
	
	public void preDraw() {
		gl2.glBindVertexArray(id);
		for (VBO vbo : vbos) {
			vbo.preDraw();
		}
	}
	
}
