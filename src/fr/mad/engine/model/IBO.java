package fr.mad.engine.model;

import java.nio.ShortBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.GLBuffers;

import fr.mad.engine.interfaces.Cleanable;
import fr.mad.engine.interfaces.Compilable;

public class IBO implements Cleanable, Compilable {
	
	//private static ArrayList<IBO> ibos = new ArrayList<IBO>();
	GL2 gl2;
	public int id = 0;
	private ShortBuffer data;
	//private static ArrayList<IBO> toClean = new ArrayList<IBO>();
	
	public IBO(ShortBuffer data) {
		this.data = data;
	}
	
	@Override
	public void compile() {
		int[] tmp = new int[1];
		gl2.glGenBuffers(1, tmp, 0);
		this.id = tmp[0];
		gl2.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, tmp[0]);
		int size = data.limit() * GLBuffers.SIZEOF_SHORT;
		gl2.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, size, data, GL.GL_STATIC_DRAW);
	}
	
	@Override
	public void cleanUP() {
		// TODO Auto-generated method stub
		
	}
	
}
