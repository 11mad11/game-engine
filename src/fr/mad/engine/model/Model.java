package fr.mad.engine.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.FloatBuffer;
import java.util.HashMap;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import fr.mad.engine.GameEngine;
import fr.mad.engine.interfaces.Drawable;

public class Model implements Drawable {
	
	private VAO vao;
	private GL2 gl;
	private float[] vertex;
	private short[] indices;
	private float[] color;
	
	public Model(GL gl, File f) {
		this.gl = gl.getGL2();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			HashMap<Integer, String> tmpV = new HashMap<Integer, String>();
			HashMap<Integer, String> tmpC = new HashMap<Integer, String>();
			HashMap<Integer, String> tmpN = new HashMap<Integer, String>();
			HashMap<Integer, String> tmpT = new HashMap<Integer, String>();
			HashMap<Integer, Integer[][]> tmpF = new HashMap<Integer, Integer[][]>();
			
			String line = null;
			int countV = 0;
			int countN = 0;
			int countT = 0;
			Integer countF = 0;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("v ")) {
					countV++;
					tmpV.put(countV, line.substring(1).trim());
					tmpC.put(countV, "1 0 1");
				} else if (line.startsWith("vn ")) {
					countN++;
					tmpN.put(countN, line.substring(2));
				} else if (line.startsWith("vt ")) {
					countT++;
					tmpT.put(countT, line.substring(2));
				} else if (line.startsWith("f ")) {
					String[] text = line.substring(1).trim().split(" ");
					//System.out.println("-----------------");
					//System.out.println("check -> "+(countF+1));
					if (text.length != 3)
						continue;
					//System.out.println("do -> "+(countF+1));
					Integer[][] tmp = new Integer[3][3];
					for (int i = 0; i < text.length; i++) {
						String[] vert = text[i].split("/");
						//System.out.println(i+" : "+vert.length);
						if (vert.length >= 1) {
							//System.out.println(vert[0] != "" ? Integer.parseInt(vert[0]) : 0);
							tmp[i][0] = vert[0] != "" ? Integer.parseInt(vert[0]) : 0;
							tmp[i][1] = 0;
							tmp[i][2] = 0;
						} /*
							 * else if (vert.length == 2) { tmp[i][0] = vert[0]
							 * != "" ? Integer.parseInt(vert[0]) : 0; tmp[i][1]
							 * = vert[1] != "" ? Integer.parseInt(vert[1]) : 0;
							 * tmp[i][2] = 0; } else if (vert.length == 3) {
							 * tmp[i][0] = vert[0] != "" ?
							 * Integer.parseInt(vert[0]) : 0; tmp[i][1] =
							 * vert[1] != "" ? Integer.parseInt(vert[1]) : 0;
							 * tmp[i][2] = vert[2] != "" ?
							 * Integer.parseInt(vert[2]) : 0; }
							 */
						//System.out.println(tmp[i][0]+"/"+tmp[i][1]+"/"+tmp[i][2]);
					}
					countF++;
					tmpF.put(countF, tmp);
				}
			}
			
			br.close();
			this.vertex = new float[tmpV.size() * 3];
			countV = 0;
			for (int i = 0; i < vertex.length; i += 3) {
				countV++;
				this.vertex[i] = Float.valueOf(tmpV.get(countV).split(" ")[0]);
				this.vertex[i + 1] = Float.valueOf(tmpV.get(countV).split(" ")[1]);
				this.vertex[i + 2] = Float.valueOf(tmpV.get(countV).split(" ")[2]);
			}
			
			this.color = new float[tmpC.size() * 3];
			countV = 0;
			for (int i = 0; i < vertex.length; i += 3) {
				countV++;
				this.color[i] = Float.valueOf(tmpC.get(countV).split(" ")[0]);
				this.color[i + 1] = Float.valueOf(tmpC.get(countV).split(" ")[1]);
				this.color[i + 2] = Float.valueOf(tmpC.get(countV).split(" ")[2]);
			}
			
			//System.out.println("================");
			
			this.indices = new short[tmpF.size() * 3];
			countF = 0;
			for (int i = 0; i < indices.length; i += 3) {
				countF++;
				//System.out.println(i+" : "+countF);
				this.indices[i] = (short) (tmpF.get(countF)[0][0] - 1);
				this.indices[i + 1] = (short) (tmpF.get(countF)[1][0] - 1);
				this.indices[i + 2] = (short) (tmpF.get(countF)[2][0] - 1);
			}
			
			FloatBuffer vertexB = FloatBuffer.wrap(vertex);
			FloatBuffer colorB = FloatBuffer.wrap(color);
			System.err.println(vertexB.limit());
			System.err.println(colorB.limit());
			
			this.vao = new VAO();
			VBO vbo = this.vao.newVBO(vertexB.limit() / 3);
			vbo.addData(VAO.Attribute.position, vertexB, 3, GL2.GL_FLOAT, false);
			//vbo = this.vao.newVBO();
			vbo.addData(VAO.Attribute.color, colorB, 3, GL2.GL_FLOAT, false);
			vao.setIBO(indices);
			VAO.preCompileAll(gl);
			VAO.CompileAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
		
	}
	
	public void cleanUP() {
		vao.cleanUP();
	}
	
	@Override
	public void draw(GameEngine ge) {
		vao.preDraw();
		gl.glDrawElements(GL.GL_TRIANGLES, this.indices.length, GL.GL_UNSIGNED_SHORT, 0);
		
	}
}
