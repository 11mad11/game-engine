package fr.mad.engine.model;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

public class MBuffer {
	
	private Buffer buffer;

	public MBuffer(Buffer b){
		this.buffer = b;
	}
	
	public Object get(){
		switch(BufferType.valueOf(buffer)){
			case BYTE:
				return ((ByteBuffer)buffer).get();
			case DOUBLE:
				return ((DoubleBuffer)buffer).get();
			case FLOAT:
				return ((FloatBuffer)buffer).get();
			case INT:
				return ((IntBuffer)buffer).get();
			case LONG:
				return ((LongBuffer)buffer).get();
			case SHORT:
				return ((ShortBuffer)buffer).get();
			default:
				return null;
			
		}
	}
	
	public void put(Object o){
		switch(BufferType.valueOf(buffer)){
			case BYTE:
				((ByteBuffer)buffer).put((byte) o);
			case DOUBLE:
				((DoubleBuffer)buffer).put((double) o);
			case FLOAT:
				((FloatBuffer)buffer).put((float) o);
			case INT:
				((IntBuffer)buffer).put((int) o);
			case LONG:
				((LongBuffer)buffer).put((long) o);
			case SHORT:
				((ShortBuffer)buffer).put((short) o);
			default:
				break;
			
		}
	}

	public boolean hasData() {
		return buffer.hasRemaining();
	}

}
