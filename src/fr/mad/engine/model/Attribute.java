package fr.mad.engine.model;

import java.nio.Buffer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.GLBuffers;

public class Attribute<T extends Buffer> {
	
	int index;
	GL2 gl2;
	private int size;
	private int type;
	private boolean normalized;
	int stride;
	int pointerOffset;
	private Buffer data;
	
	public Attribute(int index2, int size2, int type2, boolean normalized2, T data2) {
		this.index = index2;
		this.type = type2;
		this.size = size2;
		this.normalized = normalized2;
		this.data = data2;
	}
	
	public void glVertexAttribPointer() {
		gl2.glVertexAttribPointer(index, size, type, normalized, stride, pointerOffset);
	}
	
	public int SIZEOF() {
		return GLBuffers.sizeOfGLType(type) * size;
	}
	
	public int dataSize() {
		return GLBuffers.sizeOfGLType(type) * data.limit();
	}
	
	public void glBufferSubData(int dataSize) {
		gl2.glBufferSubData(GL2.GL_ARRAY_BUFFER, pointerOffset, dataSize, data);
	}
	
	public void enable(GL2 gl2) {
		gl2.glEnableVertexAttribArray(index);
	}
	
	@SuppressWarnings("unchecked")
	public T getData() {
		return (T) data;
	}
	
}
