package fr.mad.engine.log;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultCaret;

public class TestPane extends JComponent implements Runnable {
	
	private File sf;
	private PipedInputStream is;
	private PipedOutputStream os;
	private boolean running;
	private Thread thread;
	private FileOutputStream fos;
	private PrintStream ps;
	private JScrollPane jsp;
	private BufferedReader reader;
	private JTextPane jtp;
	
	public TestPane(String s, String name) {
		try {
			jsp = new JScrollPane();
			jsp.setAutoscrolls(true);
			jsp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			
			jtp = new JTextPane();
			jtp.setEditable(false);
			((DefaultCaret) jtp.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
			
			this.setLayout(new BorderLayout());
			this.add(jsp, BorderLayout.CENTER);
			jsp.setViewportView(jtp);
			
			s = "log" + File.separator + s;
			new File(s).mkdirs();
			s = new File(s).getAbsolutePath();
			s += File.separator + name + ".txt";
			sf = new File(s);
			sf.createNewFile();
			fos = new FileOutputStream(sf);
			os = new PipedOutputStream();
			ps = new PrintStream(os, true);
			is = new PipedInputStream(os);
			reader = new BufferedReader(new InputStreamReader(is));
			
			this.thread = new Thread(this);
			this.thread.start();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static final long serialVersionUID = 1L;
	
	public void println(String s) {
		ps.println(s);
	}
	
	@Override
	public void run() {
		running = true;
		while (running) {
			try {
				if (reader.ready()) {
					String tmp = "";
					while (reader.ready()) {
						tmp += Character.valueOf((char) reader.read());
					}
					fos.write(tmp.getBytes());
					jtp.setText(jtp.getText() + tmp);
				} else {
					Thread.sleep(10);
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public PrintStream getPs() {
		return ps;
	}
	
}
