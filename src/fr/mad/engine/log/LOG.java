package fr.mad.engine.log;

import java.awt.BorderLayout;
import java.io.File;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import fr.mad.JVMArgs;
import fr.mad.engine.until.Tree;

public class LOG {
	
	public static final LOG SYSTEM;

	private static final String SPLIT_CHAR = ",";
	
	private static Tree<String> map;
	private static HashMap<String, TestPane> tabs;
	private static JFrame frame = new JFrame();
	private static JPanel lastPanel;
	private static HashMap<String, LOG> list;

	private static PrintStream p;
	
	static {

		frame.setLocation(100, 100);
		frame.setSize(300, 300);
		tabs = new HashMap<String, TestPane>();
		list = new HashMap<String, LOG>();
		map = new Tree<String>();
		p = System.out;
		SYSTEM = new LOG(null, "System");
		SYSTEM.canBeConsole = true;
		SYSTEM.canHaveSub = false;
		SYSTEM.create("System.c");
		System.setOut(SYSTEM.tp.getPs());
	}
	
	private String name;
	private boolean canBeConsole;
	private boolean canHaveSub;
	private TestPane tp;
	
	public LOG(LOG log, String s) {
		if (log == null ? true : log.canHaveSub) {
			canBeConsole = true;
			canHaveSub = log == null ? true : true;
			
			name = (log != null ? log.name + SPLIT_CHAR + s : s);
			list.put(name, this);
		} else {
			throw new Error("this log cannot have a sub log");
		}
		
	}
	
	public void log(String string2) {
		String tmp = name;
		if (!tmp.endsWith(".c") && canBeConsole) {
			tmp += ".c";
			if (null == tabs.get(tmp))
				create(tmp);
			tabs.get(tmp).println(string2);
			//System.err.println(Calendar.getInstance().getTime().toString()+" : "+string2);
		} else {
			try {
				throw new Exception(canBeConsole ? "it's not suposed to end with \".c\"" : "cannot be a console");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void up() {
		if (lastPanel != null){
			frame.remove(lastPanel);
		}
		frame.setTitle("Logging");
		
		lastPanel = new JPanel();
		frame.getContentPane().add(lastPanel, BorderLayout.CENTER);
		lastPanel.setLayout(new BorderLayout(0, 0));
		boucle(map, lastPanel, null);
		
		frame.validate();
		if(JVMArgs.getBool('X', "debug"))
		frame.setVisible(true);
	}
	
	private static JPanel boucle(Tree<String> key, JPanel p, String s) {
		JTabbedPane n = new JTabbedPane(JTabbedPane.TOP);
		p.add(n, BorderLayout.CENTER);
		if (s != null) {
			s += ",";
		} else {
			s = "";
		}
		for (Tree<String> child : key.getChild()) {
			if (!child.getItem().endsWith(".c")) {
				JPanel pa = new JPanel();
				pa.setLayout(new BorderLayout(0, 0));
				n.addTab(child.getItem(), pa);
				boucle(child, pa, s + child.getItem());
			} else {
				if (tabs.get(s + child.getItem()) != null) {
					n.addTab(child.getItem(), tabs.get(s + child.getItem()));
				}
			}
		}
		return p;
	}
	
	private void create(String s) {
		String[] tmp = s.split(",");
		Tree<String> m = map;
		String path = new String();
		String n = "";
		for (int i = 0; i < tmp.length; i++) {
			if (!m.is(tmp[i]))
				m.add(new Tree<String>(tmp[i]));
			m = m.getChild(tmp[i]);
			if (i != tmp.length-1)
				path += File.separator + tmp[i];
			else
				n = tmp[i];
		}
		tp = new TestPane(path, n);
		
		//n.addTab(s.split("\\.")[s.split("\\.").length-1]+".c", tp);
		tabs.put(s, tp);
		LOG.up();
	}
	
	public static LOG get(String s) {
		if(list.get(s) != null)
		return list.get(s);
		String[] tmp = s.split(SPLIT_CHAR);
		LOG last = null;
		String n = "";
		for(String a:tmp){
			n += a;
			if(list.get(n)!=null){
				last = list.get(n);
			}else{
				last = new LOG(last,a);
			}
			n += SPLIT_CHAR;
		}
		return get(s);
	}
	
}
