package fr.mad.engine.handler.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import fr.mad.engine.lang.Traducter;
import fr.mad.engine.lang.message.ErrorMessage;

public class PluginsHandler {
	
	private ArrayList<PluginI> pluginsList;
	
	public PluginsHandler(File dir) throws Exception {
		if (!dir.isDirectory()&&dir.isFile())
			throw new Exception(dir.getAbsolutePath() + " : " + Traducter.get(ErrorMessage.itsNotADir));
		dir.mkdirs();
		pluginsList = new ArrayList<PluginI>();
		parse(dir.listFiles());
		System.out.println("Name("+pluginsList.size()+") :");
		nameAll();
	}

	private void nameAll() {
		for(PluginI p : pluginsList)
			System.out.println(p.name());
	}

	private void parse(File[] listFiles) {
		for (File f : listFiles) {
			if (f.isFile())
				check(f);
			if (f.isDirectory())
				parse(f.listFiles());
		}
	}
	
	private void check(File f) {
		try {
			URL[] urls = { new URL("jar:file:" + f.getAbsolutePath() + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);
			if (f.getName().endsWith(".jar")) {
				ZipFile zip = new ZipFile(f);
				Enumeration<? extends ZipEntry> en = zip.entries();
				while (en.hasMoreElements()) {
					try {
						ZipEntry tmp = en.nextElement();
						if (!tmp.isDirectory() && tmp.getName().endsWith(".class")) {
							System.out.println(tmp.getName());
							String className = tmp.getName().substring(0, tmp.getName().length() - 6);
							className = className.replace('/', '.');
							Class<?> c = cl.loadClass(className);
							for (Class<?> i : c.getInterfaces()) {
								if (i == PluginI.class && c.getSuperclass() == Plugin.class) {
									System.out.println(true);
									boolean load = false;
									for (Constructor<?> co : c.getConstructors()) {
										if (!load){
											try {
												this.pluginsList.add((PluginI) co.newInstance());
												load = true;
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										}
									}
								}else{
									System.out.println(false);
								}
							}
						}
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
