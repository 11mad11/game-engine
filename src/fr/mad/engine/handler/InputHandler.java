package fr.mad.engine.handler;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import fr.mad.engine.interfaces.Action;
import fr.mad.engine.setting.Setting;

public class InputHandler implements KeyListener {
	private HashMap<Integer, Action> keyMap;
	
	public InputHandler() {
		keyMap = new HashMap<Integer,Action>();
	}

	public void addKey(Integer key, Action a){
		keyMap.put(key, a);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println(e);
		System.out.println(keyMap.get(e.getExtendedKeyCode()));
		if(keyMap.get(e.getKeyCode()) != null)
			keyMap.get(e.getKeyCode()).fire();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
